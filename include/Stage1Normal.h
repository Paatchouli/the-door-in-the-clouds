/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef STAGE1NORMAL_H
#define STAGE1NORMAL_H
#include <SFML/Graphics.hpp>
//#include "SFML/Graphics.hpp"
#include <vector>
#include <iostream>
#define NB_ATTACK 8

class Stage1Normal
{
public:
Stage1Normal();
~Stage1Normal();
int update();
    int watch;
    bool midBossDeafeted;
    bool bossDeafeted;
    int arrayLifeInit[NB_ATTACK];
    int arrayLifeFina[NB_ATTACK];
    int arrayWatch[NB_ATTACK];
    int indexAttack;
};

#endif // STAGE1NORMAL_H
