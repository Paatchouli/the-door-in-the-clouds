/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef APPARITIONBULLET_H
#define APPARITIONBULLET_H
#include <vector>
#include <iostream>
#include <SFML/Graphics.hpp>

#define NB_APPARITION 2000
#define RADIUS_MAX 30
#define DURATION_APPARITION 30

class ApparitionBullet
{
public:
ApparitionBullet();
~ApparitionBullet();
void update();
void draw();
void drawAlt();
void reset(int newPosx, int newPosy, bool newCreation, sf::Color newColor);
static void addApparition(int newPosx, int newPosy, bool newCreation, int idBullet);
static void globalUpdate();
static void globalDraw();
static void globalDrawAlt();
    int watch;
    bool exist;
    bool creation;
    int posx;
    int posy;
    sf::Color color;
    sf::CircleShape spriteApparition;
    static ApparitionBullet arrayApparition[NB_APPARITION];
};

#endif // APPARITIONBULLET_H
