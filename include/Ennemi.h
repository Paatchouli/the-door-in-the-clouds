/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef ENNEMI_H
#define ENNEMI_H
#include <SFML/Graphics.hpp>
//#include "SFML/Graphics.hpp"
#include <vector>
#include <iostream>
#define NB_ENNEMI 40
#define NB_FORM 20
#define NB_ITEM 5
#define NB_DAMAGE 15
#define DURATION_DAMAGE 60
#define NB_HP_SPRITE 300
#define RADIUS_HP 100
#define SIZE_CHRONO 60
#define THICKNESS_CHRONO 3
#define NB_DRAW_CHRONO 16

class Ennemi
{
public:
Ennemi();
~Ennemi();
void draw();
void drawAlt();
static void drawChrono();
void drawChronoAlt();
void updateInstant();
void move();
void move2();
void update();
void addBullet(int newPosx, int newPosy, int newBulletID, std::vector<std::vector<double> > newTrajectory, std::vector<std::vector<double> > newShot, bool useStatic = true);
void reset(int newPosx, int newPosy, int newEnnemiID, std::vector<std::vector<double> > newTrajectory, std::vector<std::vector<double> > newShot, int newHp, int newNbPoint, int newNbPower, bool newDebug);
static void setVectorTexture();
void die();
void dieAlt();
void relaunchPower();
void relaunchPoint();
void displayDebug();
static void getIndexBoss();
static void updateBossAttack();
static void updateBossAttack2();

void shootBasic();
void shootBasicVarSpeed();
void shootGroup();
void shootGroupVarSpeed();
void shootSpiral();
void shootSpiralVarSpeed();
void shootStraight();
void shootStraightVarSpeed();

void shootParallelVarSpeed();

void shootPermutation();

void shootJohan1();
void shootJohan2();
void shootJohan3();
void shootJohan4();
void shootJohan5();
void shootJohan6();

void shootA1();

void shootSquareSpirale();
void shootSnake();
void shootStatic();

void simpleTraj();
void trajVar();
void trajRand();
void trajCircle();
void destinationTraj();
void trajBezier();

    int watch;
    int width;
    int height;
    int posx;
    int posy;
    int posHitboxx;
    int posHitboxy;
    double sizeHitbox;
    int hp;
    int memoryHp;
    std::vector<std::vector<double> > shot;
    std::vector<std::vector<double> > trajectory;
    int indexShot;
    int indexTrajectory;
    std::vector<double> memoryMove;
    std::vector<double> memoryShot;
    bool spellcard;
    sf::Text spellName;
    int speedx;
    int speedy;
    int nbSprite;
    int speedDisplay;
    std::vector<sf::Sprite> vectorSprite;
    static int commonWatch;
    bool exist;
    static std::vector<sf::Texture> vectorTexture;
    int ID;
    std::vector<sf::Sprite> listForm;
    
    sf::Texture textureSparkle;
    static int timeForm;
    int listTimeForm [NB_FORM];
    int currentAlpha;
    double listAngleForm [NB_FORM];
    double listSpeed [NB_FORM];
    int listCentreFormx [NB_FORM];
    int listCentreFormy [NB_FORM];
    
    //=====================================
    //Damage    
    sf::Sprite spriteDamage [NB_DAMAGE];
    sf::CircleShape circleDamage [NB_DAMAGE];
    int posxDamage [NB_DAMAGE];
    int posyDamage [NB_DAMAGE];
    int watchDamage [NB_DAMAGE];
    int sizeDamage [NB_DAMAGE];
    bool existDamage [NB_DAMAGE];
    int watchResetDamage;
    
    //=====================================
    
    static std::vector<double> inVector;
    static std::vector<double> outVector;
    
    static int nbEnnemi;
    static int indexBoss;
    bool isBoss;
    static bool bossBattle;
    static int baseBossHp;
    static int bossHpInit;
    static int bossHpFina;
    static int nbSpriteHpExist;
    static sf::Sprite arraySpriteHp[NB_HP_SPRITE];
    static std::vector<int> vectorLifeInit;
    static std::vector<int> vectorLifeFina;
    static std::vector<int> vectorWatch;
    static int indexAttack;
    static std::string strChrono;
    
    sf::CircleShape circleDeath;
    
    int nbItemPoint;
    int nbItemPower;
    
    bool debug;
};

#endif // ENNEMI_H
