/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef SCORE_H
#define SCORE_H
#include <iostream>

#define NB_SCORE 10
#define SIZE_SCORE_ENTERED 40
#define THICKNESS_SCORE_ENTERED 3
#define NB_DRAW_SCORE_ENTERED 16

#define DURATION_SCORE_ENTERED 30

#define DURATION_OVER_TITLE 15
#define SIZE_OVER_TITLE 60
#define THICKNESS_OVER_TITLE 3
#define NB_DRAW_OVER_TITLE 16

#define DURATION_OVER 15
#define SIZE_OVER 50
#define THICKNESS_OVER 3
#define NB_DRAW_OVER 16

class Score
{
public:
Score();
~Score();
static void initialize();
static void rewrite();
static void write();
static void read();
static void update();
static void enterScore();
    static int watch;
    static int watchColor;
    static int currentScore;
    static int hiScore;
    static int moveTicker;
    
    static int value [5][NB_SCORE + 1];
    static std::__cxx11::string name [5][NB_SCORE + 1];
    static std::__cxx11::string date [5][NB_SCORE + 1];
    static std::__cxx11::string hour [5][NB_SCORE + 1];
    
    static int scorePractice [24 + 1];
    
    static int playMode;
    static int indexNewScore;
    static int index1; //Line index
    static int index2; //Column index
    static int indexColor1;
    static int indexColor2;
    static int sizeName;
    static std::__cxx11::string currentName;
    
    static std::__cxx11::string arrayChar[88];
    
    static std::__cxx11::string over[3];
};

#endif // SCORE_H
