/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef ITEMPOINT_H
#define ITEMPOINT_H
#include <SFML/Graphics.hpp>
#define NB_ITEM_POINT 100

class ItemPoint
{
public:
ItemPoint();
~ItemPoint();
void draw();
void drawAlt();
void update();
void reset(int newPosx, int newPosy);
static void globalDraw();
static void globalDrawAlt();
static void globalUpdate();
static void addItem(int newPosx, int newPosy);
static void initialize();
    int watch;
    sf::Sprite spritePoint;
    bool exist;
    int posx;
    int posy;
    double directionx;
    double directiony;
    bool attracted;
    static ItemPoint arrayItemPoint [NB_ITEM_POINT];
    static sf::Texture textureItem;
    static bool isInitialized;
};

#endif // ITEMPOINT_H
