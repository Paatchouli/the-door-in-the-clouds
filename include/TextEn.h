/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef TEXTEN_H
#define TEXTEN_H
#include <SFML/Graphics.hpp>
//#include "SFML/Graphics.hpp"

class TextEn
{
public:
TextEn();
~TextEn();
int getHeight(int sizeText, std::string str);
int getWidth(int sizeText, std::string str);
void write(int characterSize, int posx, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::string str);
void writeCenterx(int characterSize, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::string str);
void writeCenterxDecal(int characterSize, int posx,int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::string str);
void writeCrossLeftx(int characterSize, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::string str);
void writeCrossLeftxDecal(int characterSize, int posx, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::string str);
void writeCrossRightx(int characterSize, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::string str);
void writeCrossRightxDecal(int characterSize, int posx, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::string str);

void writeMovement(int posInitx, int posInity, int posEndx, int posEndy, int durationMovement, int watchMov,
                   int characterSize, double thickness, int nbDraw, sf::Color in, sf::Color out, std::string str,
                   int movement, bool acc, int alpha = 255);

void writeHud(sf::Text text, double thickness, int nbDraw, sf::Color in, sf::Color out);
void writeHudAlt(sf::Text text, double thickness, int nbDraw, sf::Color in, sf::Color out);
void draw();
void drawAlt();
void update();
    int watch;
    //===============================================================================
    //Here is the text appaering when playing stages
    sf::Text bomb;
    sf::Text live;
    sf::Text scoreName;
    sf::Text hiScore;
    sf::Text point;
    sf::Text power;
    sf::Text bonus;
    sf::Text valScore;
    sf::Text valHiScore;
    sf::Text valPoint;
    sf::Text valPower;
    sf::Text valBonus;
    sf::Text bossName;
    sf::Text spellName;
    sf::Text timer;
    sf::Text fps;
    sf::Text mode;
    //===============================================================================
    sf::Text movingText;
    
    std::string strMode [5];
    
};

#endif // TEXTEN_H
