/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef DIALOGUE_H
#define DIALOGUE_H
#include <SFML/Graphics.hpp>
#include <vector>
#define DURATION_APPEARING 30
#define SIZE_DIALOG 30
#define THICKNESS_DIALOG 3
#define NB_DRAW_DIALOG 16
#define POSITION_X_DIALOG 80
#define POSITION_Y_DIALOG 800

class Dialogue
{
public:
Dialogue();
~Dialogue();
static void set(int newId);
static void update();
static void draw();
static void drawAlt();
static void initialize();
    static int watch;
    static sf::Texture textureDialog;
    static sf::Sprite spriteDialog;
    static bool on;
    static int id;
    static int state;
    static int moveThicker;
    static std::vector<std::vector<std::__cxx11::string> > vectorDialogEn;
    static std::vector<std::vector<std::__cxx11::string> > vectorDialogFr;
    static std::vector<std::vector<sf::Color> > vectorColorIn;
    static std::vector<std::vector<sf::Color> > vectorColorOut;
};

#endif // DIALOGUE_H
