/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef PARTICLEENNEMI_H
#define PARTICLEENNEMI_H
#include <SFML/Graphics.hpp>
#include <iostream>
#define NB_PARTICLE_FORM 200
#define NB_PARTICLE_SPARKLE 60
#define NB_PARTICLE_LINE 20
#define DURATION_FORM 60
#define DURATION_SPARKLE 60
#define DURATION_LIGHTNING 2

class ParticleEnnemi
{
public:
ParticleEnnemi();
~ParticleEnnemi();
void reset(int centerx, int centery, int ennemiWatch, int id);
void update();
void draw();
void draw2();
void drawAlt();
void drawAlt2();
    //=======================================================================
    //Ennemi
    int watch;
    sf::Texture textureSquare;
    sf::Texture textureTriangle;
    sf::Sprite arraySprite [NB_PARTICLE_FORM];
    int arrayTime [NB_PARTICLE_FORM];
    double arrayAngle [NB_PARTICLE_FORM];
    double arraySpeed [NB_PARTICLE_FORM];
    int arrayCenterx [NB_PARTICLE_FORM];
    int arrayCentery [NB_PARTICLE_FORM];
    bool arrayExist [NB_PARTICLE_FORM];
    //========================================================================
    //Johan
    sf::Texture textureSparkle;
    sf::Sprite arraySpriteSparkle [NB_PARTICLE_SPARKLE];
    int arrayTimeSparkle [NB_PARTICLE_SPARKLE];
    double arrayAngleSparkle [NB_PARTICLE_SPARKLE];
    double arraySpeedSparkle [NB_PARTICLE_SPARKLE];
    int arrayCenterSparklex [NB_PARTICLE_SPARKLE];
    int arrayCenterSparkley [NB_PARTICLE_SPARKLE];
    bool arrayExistSparkle [NB_PARTICLE_SPARKLE];
    
    sf::Vertex lightning [NB_PARTICLE_LINE];
    int timeLightning;
    bool existLightning;
    bool frontLightning;
};

#endif // PARTICLEENNEMI_H
