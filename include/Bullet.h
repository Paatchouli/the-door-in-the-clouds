/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef BULLET_H
#define BULLET_H
#include <SFML/Graphics.hpp>
//#include "SFML/Graphics.hpp"
#include <vector>
#include <iostream>
#define NB_BULLET 2000

class Bullet
{
public:
Bullet();
~Bullet();
void draw();
void drawAlt();
void updateInstant();
void move();
void move2();
void move3();
void update();
void addBullet(int newPosx, int newPosy, int newID, std::vector<std::vector<double> > newTrajectory, std::vector<std::vector<double> > newShot, bool useStatic = true);
void reset(int newPosx, int newPosy, int newID, std::vector<std::vector<double> > newTrajectory, std::vector<std::vector<double> > newShot, bool useStatic);

void straightTraj();
void varSpeedTraj();
void destinationTraj();
void attractionTraj();
void explosionTraj();
void destinationTraj2();
void squareSpiraleTraj();
void snakeTraj();
void varSpeedTraj2();

void explosionShot();
void explosionShot2();

    int watch;
    int width;
    int height;
    int posx;
    int posy;
    int posHitboxx;
    int posHitboxy;
    double sizeHitbox;
    std::vector<std::vector<double> > shot;
    std::vector<std::vector<double> > trajectory;
    int indexShot;
    int indexTrajectory;
    std::vector<double> memoryMove;
    std::vector<double> memoryShot;
    double speedx;
    double speedy;
    int nbSprite;
    int speedDisplay;
    std::vector<sf::Sprite> vectorSprite;
    static int commonWatch;
    bool exist;
    int ID;
    
    int posInitx;
    int posInity;
    int posFinax;
    int posFinay;
    double coeffx;
    double coeffy;
    
    double angle;
    
    bool useStaticWatch;
};

#endif // BULLET_H
