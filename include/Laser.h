/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef LASER_H
#define LASER_H
#include <SFML/Graphics.hpp>

class Laser
{
public:
Laser();
~Laser();
void reset(int newTimeWarning, int newTimeDeployment, int newDuration, double newThickness, sf::Color newColor, int newX, int newY, double newAngle);
void update();
void draw();
void drawAlt();
void supprime();
    bool exist;
    int watch;
    int timeWarning;
    int timeDeployment;
    int duration;
    double thickness;
    sf::Color color;
    int x;
    int y;
    double angle;
    sf::Sprite sprite;
    sf::RenderTexture renderTexture;
    sf::Texture texture;
};

#endif // LASER_H
