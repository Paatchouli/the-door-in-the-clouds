/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef FUNC_H
#define FUNC_H
#include <vector>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <cstdarg>
#include <SFML/Graphics.hpp>

double getNorm(std::vector<double> vect);

double getNormCarre(std::vector<double> vect);

std::vector<double> setNorm(std::vector<double> vect, double new_norm);

double getAngle(std::vector<double> vect);

std::vector<double> rotation(std::vector<double> vect, double angle);

std::__cxx11::string toString(double d, int precision);

int alea(int nbArg, int n1, ...);

int aleaVectorInt(std::vector<int> v);

int fact(int n);

template<class ForwardIterator, class T>
void iota(ForwardIterator first, ForwardIterator last, T value);

std::vector<int> permutation(int n, int reductor);

std::vector<int> permutationAllChanged(int n, int reductor);

std::__cxx11::string getDate();

std::__cxx11::string getHour();

int parseLine(char* line);

int getValue();

void init();

double getCurrentValue();

void write(int characterSize, int posx, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::__cxx11::string str);

void writeAlt(int characterSize, int posx, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::__cxx11::string str);

void writeMovement(int characterSize, int posInitX, int posFinaX, int posInitY, int posFinaY, double thickness, int nbDraw, sf::Color in, sf::Color out, std::__cxx11::string str, int durationMovement,
                   int watchMov, bool acc, bool alpha = false);

int getWidthText(int characterSize, std::__cxx11::string str);

int getHeightText(int characterSize, std::__cxx11::string str);

sf::Keyboard::Key str2key(std::__cxx11::string str);

std::__cxx11::string key2str(sf::Keyboard::Key key);

bool isKeyPressed();

sf::Keyboard::Key getKeyPressed();

template<typename Char, typename Traits, typename Allocator>
std::basic_string<Char, Traits, Allocator> operator * (const std::basic_string<Char, Traits, Allocator> s, size_t n);

template<typename Char, typename Traits, typename Allocator>
std::basic_string<Char, Traits, Allocator> operator * (size_t n, const std::basic_string<Char, Traits, Allocator>& s);

std::__cxx11::string operator * (const std::__cxx11::string str, int n);

std::__cxx11::string operator * (int n, const std::__cxx11::string str);

std::vector<std::vector<double> > createSimpleTraj(int x0, int y0, int x1, int y1, int t0, int t1, int t2, int x2, int y2, double s);

#endif // FUNC_H
