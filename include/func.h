/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef FUNC_H
#define FUNC_H
#include <vector>
#include <iostream>
#include <iomanip>
#include <sstream>

double getNorm(std::vector<double> vect);

double getNormCarre(std::vector<double> vect);

std::vector<double> setNorm(std::vector<double> vect, double new_norm);

double getAngle(std::vector<double> vect);

std::vector<double> rotation(std::vector<double> vect, double angle);

std::string toString(double d, int precision);

#endif // FUNC_H
