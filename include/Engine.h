/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CLOUD_H
#define CLOUD_H
#define NB_CLOUD 15
#define NB_SPIRALE 15
#include <SFML/Graphics.hpp>
//#include "SFML/Graphics.hpp"

class Cloud
{
public:
Cloud();
~Cloud();
void reset(double newSpeedCloudx, double newSpeedCloudy, int newSizeMinx, int newSizeMaxx, int newSizeSmally, int newSizeBigy, double newProbaCloud);
void initialiseCloud(int newPosx, int newPosy, int newSizex, int newSizey, bool newSmall);
void createCloud();
void update();
void move();
void draw();
void drawAlt();

    int watch;
    sf::Texture textureCloudRight;
    sf::Texture textureCloudLeft;
    sf::Texture textureCloudMiddle;
    sf::Sprite spriteCloud[NB_CLOUD][3];
    int posx [NB_CLOUD];
    int posy [NB_CLOUD];
    bool exist [NB_CLOUD];
    bool isSmall [NB_CLOUD];
    double speedx;
    double speedy;
    double speedCloudx;
    double speedCloudy;
    double sizeMinx;
    double sizeMaxx;
    double sizeMiny;
    double sizeMaxy;
    int sizeSmally;
    int sizeBigy;
    double angle;
    double probaCloud; //Odd for a cloud to appear at a frame at speed 1
    
    //double geneProba; //geneProba = taillemin/(taillemax*(taillemax - taillemin))
    
    //===================================================
    //Version 2.0
    
void reset2(std::vector<double> newVectorScale, std::vector<double> newVectorProba);
void initialiseCloud2(int newPosx, int newPosy, int newSizex, int newSizey, int newScale);
void createCloud2();
void update2();
void move2();
void draw2();
void drawAlt2();
    sf::Texture textureCloud;
    int nbScale;
    std::vector<double> vectorScale;  //All the possibles scales. Keep in mind that the scale and the odd of a cloud of this scale appearing are EXACTLY the same thing.
    std::vector<double> vectorProba;
    double arrayScale[NB_CLOUD];
    bool arrayExistCloud[NB_CLOUD];
    bool arrayExistSpirale[NB_CLOUD][NB_SPIRALE];
    int arrayPosxCloud[NB_CLOUD];
    int arrayPosyCloud[NB_CLOUD];
    int arrayPosxSpirale[NB_CLOUD][NB_SPIRALE];
    int arrayPosySpirale[NB_CLOUD][NB_SPIRALE];
    sf::Sprite arraySprite[NB_CLOUD][NB_SPIRALE];
    
};

#endif // CLOUD_H
