/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef TRAIL_H
#define TRAIL_H
#include <SFML/Graphics.hpp>

class Trail
{
public:
Trail();
~Trail();
void reset(int newX, int newY);
void update();
void draw();
void drawAlt();
static void add(int newX, int newY);
static void globalUpdate();
static void globalDraw();
static void globalDrawAlt();
static void globalDraw2();
static void globalDrawAlt2();
    int watch;
    bool exist;
    int x;
    int y;
    sf::CircleShape sprite;
    static Trail arrayTrail[180];
};

#endif // TRAIL_H
