/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef PLAYER_H
#define PLAYER_H
#include <vector>
#include <SFML/Graphics.hpp>
//#include "SFML/Graphics.hpp"
#include <iostream>
#define NB_TORPEDO 100
#define TIME_MOTIF 180
#define TORPEDO_DURATION 120
#define RECOVERY_TIME_DURATION 240
#define CIRCLE_LOSS_LIFE_DURATION 120
#define CIRCLE_LOSS_LIFE_MAX_SIZE 600

class Player
{
public:
Player();
~Player();
void draw();
void drawAlt();
void move(int debug);
void update();
void lossLife();
void addTorpedo();
void resetTorpedo(int i);
void insertMotif(int direction);
void shrinkMotif(int direction);
void displayMotif(std::string errMessage);
void resetMotif();
void updateTrail();
void drawTrail();
void drawTrailAlt();

int distanceBigSquare(bool boo);
int distanceBigSquareInclined(bool boo);

int distanceTriangleUp();
int distanceTriangleLeft();
int distanceTriangleDown();
int distanceTriangleRight();

int distanceTriangleUpEasy(bool debug, bool boo);
int distanceTriangleLeftEasy(bool debug, bool boo);
int distanceTriangleDownEasy(bool debug, bool boo);
int distanceTriangleRightEasy(bool debug, bool boo);

int distanceTriangle45(bool debug, bool boo);
int distanceTriangle135(bool debug, bool boo);
int distanceTriangle225(bool debug, bool boo);
int distanceTriangle315(bool debug, bool boo);

int distanceCircle(bool debug, bool boo);

void drawForm();
void drawFormAlt();
void damageForm();
void animationSquare();
void animationTriangleRight();
void animationTriangleDown();
void animationTriangleLeft();
void animationTriangleUp();
void animationCircle();
void animationSquareInclined();
void animationTriangle45();
void animationTriangle135();
void animationTriangle225();
void animationTriangle315();
    int watch;
    int width;
    int height;
    int posx;
    int posy;
    int posHitboxx;
    int posHitboxy;
    double sizeHitbox;
    int nbSprite;
    int speedDisplay;
    std::vector<sf::Sprite> vectorSprite;
    int speedx;
    int speedy;
    int nbLife;
    static const int nbLifeMax = 5;
    int recoveryTime;
    sf::Texture texture1;
    sf::Texture texture2;
    sf::Texture texture3;
    double angle;
    double angleModifier;
    double angleMax;
    
    std::vector<double> inVector;
    std::vector<double> outVector;
    
    //Torpedo
    sf::Texture textureTorpedo;
    sf::Sprite arrayTorpedo [NB_TORPEDO];
    int arrayWatch [NB_TORPEDO];
    int arrayPosx [NB_TORPEDO];
    int arrayPosy [NB_TORPEDO];
    double arraySpeedx [NB_TORPEDO];
    double arraySpeedy [NB_TORPEDO];
    
    //Motif
    std::vector<std::vector<int> > motif;
    int sizex;
    int sizey;
    int posPlayx;
    int posPlayy;
    
    sf::Texture textureTrail;
    sf::Sprite spriteTrail [TIME_MOTIF];
    int watchTrail [TIME_MOTIF];
    
    int point;
    double power;
    int watchForm;
    int idForm;
    int sizeForm;
    int posxForm;
    int posyForm;
    int powerForm;
    int slotLaser[16];
    int cursorLaser;
    int countLaser;
    
    sf::Texture textureInnerSquare;
    sf::Texture textureInnerTriangle;
    sf::Texture textureInnerCircle;
    sf::Sprite spriteInnerSquare;
    sf::Sprite spriteInnerTriangle;
    sf::Sprite spriteInnerCircle;
    
    sf::Texture textureOuterSquare;
    sf::Texture textureOuterTriangle;
    sf::Texture textureOuterCircle;
    sf::Sprite spriteOuterSquare;
    sf::Sprite spriteOuterTriangle;
    sf::Sprite spriteOuterCircle;
    
    sf::Sprite spriteLaser[16];
    
    int timeApparitionForm;
    int timeApparitionLaser;
    int timeHoldLaser;
    
    sf::CircleShape circleLossLife;
};

#endif // PLAYER_H
