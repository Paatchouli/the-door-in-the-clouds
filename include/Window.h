/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef WINDOW_H
#define WINDOW_H
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
//#include "SFML/System.hpp"
//#include "SFML/Window.hpp"
//#include "SFML/Graphics.hpp"
#include <vector>
#include <iostream>

class Window
{
public:
Window();
~Window();
static void initialize();
void setBackground();
void setForeground();
void remapKey();
    sf::RenderWindow mainWindow;
    sf::ContextSettings settings;
    sf::Color backgroundColor;
    sf::Font font;
    int height;
    int width;
    int posScreenx;
    int posScreeny;
    int heightScreen;
    int widthScreen;
    std::vector<sf::Texture> vectorTexturePlayer;
    std::vector<sf::Texture> vectorTextureEnnemi;
    static const int nbBullet = 2000;
    static const int nbEnnemi = 40;
    sf::Texture textureMenu;
    sf::Sprite spriteMenu;
    
    sf::Texture textureTitlePrimary;
    sf::Sprite spriteTitlePrimary;
    
    sf::Texture textureTitleSecondary;
    sf::Sprite spriteTitleSecondary;
    
    sf::Texture textureSquare;
    sf::Sprite spriteSquare;
    
    sf::Texture textureEnnemi;
    sf::Sprite spriteEnnemi;
    
    sf::Texture textureBlueCloud;
    
    sf::Texture textureRondRouge;
    sf::Sprite spriteRondRouge;
    sf::Texture textureRondJaune;
    sf::Sprite spriteRondJaune;
    sf::Texture textureRondVert;
    sf::Sprite spriteRondVert;
    sf::Texture textureRondCyan;
    sf::Sprite spriteRondCyan;
    sf::Texture textureRondBleu;
    sf::Sprite spriteRondBleu;
    sf::Texture textureRondMagenta;
    sf::Sprite spriteRondMagenta;
    
    sf::Texture textureLaserRed;
    sf::Texture textureLaserOrange;
    sf::Texture textureLaserYellow;
    sf::Texture textureLaserGreen;
    sf::Texture textureLaserBlue;
    sf::Texture textureLaserIndigo;
    sf::Texture textureLaserPurple;
    
    sf::Sprite spriteLaserRed;
    sf::Sprite spriteLaserOrange;
    sf::Sprite spriteLaserYellow;
    sf::Sprite spriteLaserGreen;
    sf::Sprite spriteLaserBlue;
    sf::Sprite spriteLaserIndigo;
    sf::Sprite spriteLaserPurple;
    
    //======================================================
    //Texture bullet
    
    sf::Texture textureBulletRed;
    sf::Texture textureBulletOrange;
    sf::Texture textureBulletYellow;
    sf::Texture textureBulletGreen;
    sf::Texture textureBulletBlue;
    sf::Texture textureBulletIndigo;
    sf::Texture textureBulletPurple;
    sf::Texture textureBulletLithium;
    
    sf::Texture textureDoubleRed;
    sf::Texture textureDoubleOrange;
    sf::Texture textureDoubleYellow;
    sf::Texture textureDoubleGreen;
    sf::Texture textureDoubleBlue;
    sf::Texture textureDoubleIndigo;
    sf::Texture textureDoublePurple;
    sf::Texture textureDoubleLithium;
    
    sf::Texture textureMoonRed;
    sf::Texture textureMoonOrange;
    sf::Texture textureMoonYellow;
    sf::Texture textureMoonGreen;
    sf::Texture textureMoonBlue;
    sf::Texture textureMoonIndigo;
    sf::Texture textureMoonPurple;
    
    sf::Texture textureTriangleRed;
    sf::Texture textureTriangleOrange;
    sf::Texture textureTriangleYellow;
    sf::Texture textureTriangleGreen;
    sf::Texture textureTriangleBlue;
    sf::Texture textureTriangleIndigo;
    sf::Texture textureTrianglePurple;
    sf::Texture textureTriangleLithium;
    
    sf::Texture textureSmallRed;
    sf::Texture textureSmallOrange;
    sf::Texture textureSmallYellow;
    sf::Texture textureSmallGreen;
    sf::Texture textureSmallBlue;
    sf::Texture textureSmallIndigo;
    sf::Texture textureSmallPurple;
    //sf::Texture textureTriangleLithium;

    int stageDifficulty;
    
    sf::Texture textureFrame;
    sf::Sprite spriteFrame;

    sf::RenderTexture renderBackground;
    sf::Sprite spriteBackground;
    sf::RenderTexture renderForeground;
    sf::Sprite spriteForeground;
    
    sf::Texture texturePause;
    sf::Sprite spritePause;
    
    sf::Texture firstSky;
    sf::Texture textureSky;
    sf::Sprite spriteSky;
    
    sf::Texture textureHp;
    
    sf::Texture textureBonus;
    
    //=====================================================
    //Control key
    sf::Keyboard::Key left;
    sf::Keyboard::Key right;
    sf::Keyboard::Key up;
    sf::Keyboard::Key down;
    sf::Keyboard::Key bomb;
    sf::Keyboard::Key slow;
    sf::Keyboard::Key fast;
    sf::Keyboard::Key pause;
    
    //====================================================
    static sf::Texture staticTextureGame;
    static sf::Sprite staticSpriteGame;
};

#endif // WINDOW_H
