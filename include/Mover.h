/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef MOVER_H
#define MOVER_H
#include <iostream>
#include <vector>

#define SIZE_MENU 55
#define THICKNESS_MENU 4
#define NB_DRAW_MENU 16
#define SPACE_MENU 70
#define X_INIT_MENU -500
#define Y_INIT_MENU 450
#define INTERVAL_MENU 10
#define DURATION_MENU 60

#define DURATION_OPTION 15
#define DURATION_SCORE 15

#define SIZE_TITLE 100
#define THICKNESS_TITLE 4
#define NB_DRAW_TITLE 16

#define SIZE_MODE 80
#define THICKNESS_MODE 4
#define NB_DRAW_MODE 16
#define POS_Y_MODE_TITLE 20
#define POS_Y_MODE 355
#define EASY_NORMAL 36
#define NORMAL_HARD 35
#define HARD_FEERIC 36
#define DURATION_MODE 15

#define SIZE_OPTION 60
#define THICKNESS_OPTION 3
#define NB_DRAW_OPTION 16
#define POS_X_OPTION_TITLE 20
#define POS_Y_OPTION_TITLE_INIT -100
#define POS_Y_OPTION_TITLE 20
#define POS_X_OPTION_INIT -400
#define POS_X_OPTION 20
#define POS_Y_OPTION_UP 200
#define SPACE_OPTION 100
#define POS_X_OPTION_NUMBER_INIT 2000
#define POS_X_OPTION_NUMBER 600
#define SPACE_OPTION_NUMBER 50
#define NB_OPTION_RIGHT 9
#define NB_OPTION_LEFT 6
#define PERIOD_KEY 120

#define SIZE_SCORE 40
#define SIZE_SCORE_MODE 50
#define THICKNESS_SCORE 3
#define NB_DRAW_SCORE 16
#define POS_Y_SCORE_INIT -100
#define POS_X_SCORE_INIT -1100
#define POS_X_SCORE_TITLE 20
#define POS_Y_SCORE_TITLE 20
#define POS_X_SCORE_MODE 20
#define POS_Y_SCORE_MODE 150
#define POS_Y_SCORE_FIRST 250
#define SPACE_Y_SCORE 70
#define POS_X_SCORE_NAME 20
#define POS_X_SCORE_RESULT 650
#define POS_X_SCORE_DATE 750
#define POS_X_SCORE_HOUR 1000

#define DURATION_STAGE 15
#define SIZE_STAGE 90
#define THICKNESS_STAGE 5
#define NB_DRAW_STAGE 16
#define X_INIT_STAGE -400
#define X_FINA_STAGE 20
#define Y_STAGE 300
#define SPACE_STAGE 100

class Mover
{
public:
Mover();
~Mover();
static void initialize();
static void moveCursor(int nbPosition);
static int update();
static int update0();
static int update00();
static int update1();
static int update10();
static int update4();
static int update40();
static int update41();
static int update410();
static int update5();
static int update50();
static int update7();
static int update70();
    static int watch;
    static int watch2;
    
    static int moveTicker;
    static int index;
    static int index2;
    static int watchColor;
    static int watchColor2;
    static int watchColor3;
    static int indexColor1;
    static int indexColor2;
    static int indexColorVol;
    static int BGMvolume0;
    static int SEvolume0;
    
    static std::vector<int> stateID;
    static std::vector<int> stateID2;
    
    static bool right;
    static bool chosingKey;
    
    static double posx;
    static double posy;
    
    static int posMenu [8];
    static std::__cxx11::string menu[8];
    static std::__cxx11::string option[6];
    static std::__cxx11::string controlKey[9];
    static std::__cxx11::string modeString;
    
};

#endif // MOVER_H
