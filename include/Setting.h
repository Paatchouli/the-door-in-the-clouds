/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef SETTING_H
#define SETTING_H
#include <SFML/Graphics.hpp>

class Setting
{
public:
Setting();
~Setting();
static void initialize();
static void rewrite();
static bool isError();
static void write();
static void read();
    /*
    static sf::Keyboard::Key left;
    static sf::Keyboard::Key right;
    static sf::Keyboard::Key up;
    static sf::Keyboard::Key down;
    static sf::Keyboard::Key bomb;
    static sf::Keyboard::Key slow;
    static sf::Keyboard::Key fast;
    static sf::Keyboard::Key pause;
    static sf::Keyboard::Key screenShot;
    */
    static int nbLifeInit;
    static int nbBombInit;
    static int BGMvolume;
    static int SEvolume;
    static int language;
    /**
    * 0 : Left
    * 1 : Right
    * 2 : Up
    * 3 : Down
    * 4 : Bomb
    * 5 : Slow
    * 6 : Fast
    * 7 : Pause
    * 8 : Screenshot
    */
    static sf::Keyboard::Key arrayKey[9];
};

#endif // SETTING_H
