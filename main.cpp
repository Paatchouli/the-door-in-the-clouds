#include <iostream>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "include/Window.h"
#include "include/Player.h"
#include "include/Ennemi.h"
#include "include/Engine.h"
#include "include/Bullet.h"
#include "include/TextEn.h"
#include "include/Mover.h"
#include "include/Score.h"
#include "include/Stage1Normal.h"
#include <vector>

int main(int argc, char **argv)
{
    extern Window window;
    extern Engine engine;
    extern TextEn textEn;
    extern Mover mover;
    extern Score score;
    score.initialize();
    Ennemi::setVectorTexture();
    
    while (true)
    {
        
        /*
        textEn.write(80, 50, 200, 4, 16, sf::Color::White, sf::Color::Black, "Start");
        textEn.write(80, 50, 300, 4, 16, sf::Color::White, sf::Color::Black, "Extra Start");
        textEn.write(80, 50, 400, 4, 16, sf::Color::White, sf::Color::Black, "Spell Practice");
        textEn.write(80, 50, 500, 4, 16, sf::Color::White, sf::Color::Black, "Practice Start");
        textEn.write(80, 50, 600, 4, 16, sf::Color::White, sf::Color::Black, "Result");
        textEn.write(80, 50, 700, 4, 16, sf::Color::White, sf::Color::Black, "Music Room");
        textEn.write(80, 50, 800, 4, 16, sf::Color::White, sf::Color::Black, "Option");
        textEn.write(80, 50, 900, 4, 16, sf::Color::White, sf::Color::Black, "Quit");
        if (window.index == 0) {textEn.write(80, 50, 200, 2, 8, sf::Color(0, 0, 255), sf::Color::Black, "Start");}
        if (window.index == 1) {textEn.write(80, 50, 300, 2, 4, sf::Color(18, 0, 255), sf::Color::Black, "Extra Start");}
        if (window.index == 2) {textEn.write(80, 50, 400, 2, 4, sf::Color(36, 0, 255), sf::Color::Black, "Spell Practice");}
        if (window.index == 3) {textEn.write(80, 50, 500, 2, 4, sf::Color(54, 0, 255), sf::Color::Black, "Practice Start");}
        if (window.index == 4) {textEn.write(80, 50, 600, 2, 4, sf::Color(72, 0, 255), sf::Color::Black, "Result");}
        if (window.index == 5) {textEn.write(80, 50, 700, 2, 4, sf::Color(90, 0, 255), sf::Color::Black, "Music Room");}
        if (window.index == 6) {textEn.write(80, 50, 800, 2, 4, sf::Color(108, 0, 255), sf::Color::Black, "Option");}
        if (window.index == 7) {textEn.write(80, 50, 900, 2, 4, sf::Color(127, 0, 255), sf::Color::Black, "Quit");}
        */
        if (!mover.update())
        {
            return 0;   
        }
        if (window.moveTicker) {window.moveTicker = window.moveTicker - 1;}
        window.mainWindow.display();
    }
    
    
    return 0;
}