/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Player.h"
#include "../include/Window.h"
#include "../include/Bullet.h"
#include "../include/func.h"
#include "../include/Ennemi.h"
#include "../include/Dialogue.h"
#include "../include/Replay.h"
#include "../include/Setting.h"
#include <vector>
#include <SFML/Graphics.hpp>
//#include "../include/SFML/Graphics.hpp"
#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>
#include <algorithm>
#include <cmath>

extern Window window;
extern std::vector<Bullet> vectorBullet;
extern std::vector<Ennemi> vectorEnnemi;
extern Ennemi arrayEnnemi [NB_ENNEMI];
extern Dialogue dialogue;
extern Replay replay;

Player player;

Player::Player()
{
    watch = 0;
    width = 110;
    height = 110;
    posx = 200;
    posy = 200;
    posHitboxx = 55;
    posHitboxy = 55;
    sizeHitbox = 4.0;
    nbSprite = 4;
    speedDisplay = 10;
    speedx = 0;
    speedy = 0;
    nbLife = nbLifeMax;
    recoveryTime = 0;
    angle = 0.0;
    angleModifier = 1.0;
    angleMax = 30.0;
    vectorSprite.resize(4);
    //vectorSprite[0].setTexture(window.vectorTexturePlayer[0]); 
    //vectorSprite[0].setPosition(200, 200);
    /*
    texture1.loadFromFile("spriteFairy4.png"); //texture1.convertToPremultipliedAlpha();
    texture2.loadFromFile("spriteFairy4.png");
    texture3.loadFromFile("spriteFairy4.png");
    */
    
    texture1.loadFromFile("fee31.png");
    texture2.loadFromFile("fee32.png");
    texture3.loadFromFile("fee33.png");
    
    /*
    texture1.loadFromFile("rond_rouge.png");
    texture2.loadFromFile("rond_bleu.png");
    texture3.loadFromFile("rond_vert.png");
    */
    /*
    texture1.loadFromFile("johan21.png");
    texture2.loadFromFile("johan22.png");
    texture3.loadFromFile("johan23.png");
    */
    vectorSprite[0].setTexture(texture2); vectorSprite[0].setOrigin(width/2, height/2); vectorSprite[0].setPosition(200, 200);
    vectorSprite[1].setTexture(texture1); vectorSprite[1].setOrigin(width/2, height/2); vectorSprite[1].setPosition(200, 200);
    vectorSprite[2].setTexture(texture3); vectorSprite[2].setOrigin(width/2, height/2); vectorSprite[2].setPosition(200, 200);
    vectorSprite[3].setTexture(texture1); vectorSprite[3].setOrigin(width/2, height/2); vectorSprite[3].setPosition(200, 200);
    
    inVector.push_back(1.0);
    inVector.push_back(0.0);
    outVector.resize(2);
    
    textureTorpedo.loadFromFile("torpedo.png");
    for (int i = 0; i < NB_TORPEDO; i++)
    {
        arrayTorpedo[i].setTexture(textureTorpedo);
        arrayTorpedo[i].setOrigin(8, 8);
        arrayWatch[i] = 0;   
    }
    std::vector<int> tmp;
    tmp.push_back(TIME_MOTIF);
    motif.push_back(tmp);
    sizex = 1;
    sizey = 1;
    posPlayx = 0;
    posPlayy = 0;
    
    textureTrail.loadFromFile("trail1.png");
    for (int i = 0; i < TIME_MOTIF; i++)
    {
        spriteTrail[i].setTexture(textureTrail);
        watchTrail[i] = 0;
    }
    point = 0;
    power = 1; //five levels
    watchForm = 0;
    idForm = 0;
    sizeForm = 0;
    posxForm = 0;
    posyForm = 0;
    powerForm = 0;
    countLaser = 0;
    
    textureInnerSquare.loadFromFile("innerSquare.png");
    textureInnerTriangle.loadFromFile("innerTriangle.png");
    textureInnerCircle.loadFromFile("innerCircle.png");
    spriteInnerSquare.setTexture(textureInnerSquare);
    spriteInnerTriangle.setTexture(textureInnerTriangle);
    spriteInnerCircle.setTexture(textureInnerCircle);
    
    textureOuterSquare.loadFromFile("outerSquare.png");
    textureOuterTriangle.loadFromFile("outerTriangle.png");
    textureOuterCircle.loadFromFile("outerCircle.png");
    spriteOuterSquare.setTexture(textureOuterSquare);
    spriteOuterTriangle.setTexture(textureOuterTriangle);
    spriteOuterCircle.setTexture(textureOuterCircle);
    
    spriteInnerSquare.setOrigin(300, 300);
    spriteOuterSquare.setOrigin(300, 300);
    spriteInnerTriangle.setOrigin(200, 400);
    spriteOuterTriangle.setOrigin(200, 400);
    spriteInnerCircle.setOrigin(300, 300);
    spriteOuterCircle.setOrigin(300, 300);
    
    for (int i = 0; i < 16; i++)
    {
        spriteLaser[i].setTexture(window.textureLaserBlue);
        spriteLaser[i].setOrigin(35, 35);
        slotLaser[i] = 0;
    }
    timeApparitionForm = 15;
    timeApparitionLaser = 15;
    timeHoldLaser = 120;
    
    circleLossLife.setFillColor(sf::Color(0, 0, 0, 0));
}

Player::~Player()
{

}

void Player::draw()
{
    //glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    //drawTrail();
    if ((recoveryTime/10) % 2 == 0)
    {
        window.mainWindow.draw(vectorSprite[(watch % (speedDisplay*nbSprite)) / speedDisplay]);
    }
    if (recoveryTime > 120)
    {
        window.mainWindow.draw(circleLossLife);   
    }
    for (int i = 0; i < NB_TORPEDO; i++)
    {
        if (arrayWatch[i])
        {
            window.mainWindow.draw(arrayTorpedo[i]);
        }
    }
}

void Player::drawAlt()
{
    //drawTrailAlt();
    if ((recoveryTime/10) % 2 == 0)
    {
        window.renderBackground.draw(vectorSprite[(watch % (speedDisplay*nbSprite)) / speedDisplay]);
    }
    if (recoveryTime > 120)
    {
        window.renderBackground.draw(circleLossLife);   
    }
    for (int i = 0; i < NB_TORPEDO; i++)
    {
        if (arrayWatch[i])
        {
            window.renderBackground.draw(arrayTorpedo[i]);
        }
    }
}

void Player::move(int debug)
{
    int movex;
    int movey;
    //if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
    if (sf::Keyboard::isKeyPressed(Setting::arrayKey[5]))
    {
        //if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        if (sf::Keyboard::isKeyPressed(Setting::arrayKey[1]))
        {
            speedx = 6;
        }
        //if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        if (sf::Keyboard::isKeyPressed(Setting::arrayKey[0]))
        {
            speedx = speedx - 6;
        }
        //if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        if (sf::Keyboard::isKeyPressed(Setting::arrayKey[3]))
        {
            if (speedx)
            {
                speedx = speedx*4/6;
                speedy = 4;
            }
            else
            {
                speedy = 6;
            }
        }
        //if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        if (sf::Keyboard::isKeyPressed(Setting::arrayKey[2]))
        {
            if (speedx)
            {
                speedx = speedx*4/6;
                speedy = speedy - 4;
            }
            else
            {
                speedy = speedy - 6;
            }
        }
    }
    //else if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
    else if (sf::Keyboard::isKeyPressed(Setting::arrayKey[6]))
    {
        //if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        if (sf::Keyboard::isKeyPressed(Setting::arrayKey[1]))
        {
            speedx = 17;
        }
        //if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        if (sf::Keyboard::isKeyPressed(Setting::arrayKey[0]))
        {
            speedx = speedx - 17;
        }
        //if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        if (sf::Keyboard::isKeyPressed(Setting::arrayKey[3]))
        {
            if (speedx)
            {
                speedx = speedx*12/17;
                speedy = 12;
            }
            else
            {
                speedy = 17;
            }
        }
        //if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        if (sf::Keyboard::isKeyPressed(Setting::arrayKey[2]))
        {
            if (speedx)
            {
                speedx = speedx*12/17;
                speedy = speedy - 12;
            }
            else
            {
                speedy = speedy - 17;
            }
        }
    }
    else
    {
        //if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        if (sf::Keyboard::isKeyPressed(Setting::arrayKey[1]))
        {
            speedx = 10;
        }
        //if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        if (sf::Keyboard::isKeyPressed(Setting::arrayKey[0]))
        {
            speedx = speedx - 10;
        }
        //if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        if (sf::Keyboard::isKeyPressed(Setting::arrayKey[3]))
        {
            if (speedx)
            {
                speedx = speedx*7/10;
                speedy = 7;
            }
            else
            {
                speedy = 10;
            }
        }
        //if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        if (sf::Keyboard::isKeyPressed(Setting::arrayKey[2]))
        {
            if (speedx)
            {
                speedx = speedx*7/10;
                speedy = speedy - 7;
            }
            else
            {
                speedy = speedy - 10;
            }
        }
    }
    if (debug)
    {
        std::cout << "speedx = " << speedx << "\n" << "speedy = " << speedy << std::endl;   
    }
    if (speedx < 0)
    {
        angle = std::max(angle - angleModifier, -angleMax);
    }
    else if (speedx > 0)
    {
        angle = std::min(angle + angleModifier, angleMax);
    }
    else
    {
        if (angle < 0.0)
        {
            angle = angle + angleModifier;   
        }
        else if (angle > 0.0)
        {
            angle = angle - angleModifier;   
        }
    }
    for (int i = 0; i < nbSprite; i++)
    {
        vectorSprite[i].setRotation(angle);
    }
        
    if (posx + speedx + width > window.width - 1)
    {
        movex = window.width - 1 - width - posx;
    }
    else if (posx + speedx < window.posScreenx)
    {
        movex = - posx + window.posScreenx;
    }
    else
    {
        movex = speedx;
    }

    if (posy + speedy + height > window.height - 1)
    {
        movey = window.height - 1 - height - posy;
    }
    else if (posy + speedy < window.posScreeny)
    {
        movey = - posy + window.posScreeny;
    }
    else
    {
        movey = speedy;
    }
    if ((movey || movex) && watch % 10 == 0 && !dialogue.on)
    {
        addTorpedo();
    }

    posx = posx + movex;
    posy = posy + movey;
    posHitboxx = posHitboxx + movex;
    posHitboxy = posHitboxy + movey;
    for(int i = 0;  i < nbSprite; ++i)
    {
        vectorSprite[i].move(movex, movey);
    }
}


void Player::update()
{
    watch = watch + 1;
    replay.vectorInput.push_back(std::vector<int>(7, 0));
    if (recoveryTime)
    {
        recoveryTime = recoveryTime - 1;   
    }
    if (recoveryTime > 120)
    {
        double coeff = CIRCLE_LOSS_LIFE_MAX_SIZE/double(CIRCLE_LOSS_LIFE_DURATION);
        circleLossLife.setRadius(coeff*(RECOVERY_TIME_DURATION - recoveryTime));
        circleLossLife.setOrigin(circleLossLife.getRadius(), circleLossLife.getRadius());
        circleLossLife.setOutlineThickness(CIRCLE_LOSS_LIFE_MAX_SIZE*(recoveryTime - CIRCLE_LOSS_LIFE_DURATION)/double(4*CIRCLE_LOSS_LIFE_DURATION));
        circleLossLife.setOutlineColor(sf::Color(0, 0, 255, 255*(recoveryTime - CIRCLE_LOSS_LIFE_DURATION)/double(CIRCLE_LOSS_LIFE_DURATION)));
    }
    speedx = 0;
    speedy = 0;
    int posInitx = posx;
    int posInity = posy;
    move(false);
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::C))
    {
        /*
        motif.clear();
        std::vector<int> tmp;
        tmp.push_back(1);
        motif.push_back(tmp);
        sizey = 1;
        sizex = 1;
        */
        /*
        for (int i = 0; i < sizey; i++)
        {
            for (int j = 0; j < sizey; j++)
            {
                if (motif[i][j] < 179)
                {
                    motif[i][j] = 0;
                }
            }
        }
        */
    }
    

    
    //====================================================================================================================================================================================================
    //Torpedo
    for (int i = 0; i < NB_TORPEDO; i++)
    {
        if (arrayWatch[i])
        {
            arrayWatch[i] = arrayWatch[i] - 1;
            if (arrayWatch[i] < TORPEDO_DURATION/2)
            {
                arrayTorpedo[i].setColor(sf::Color(255, 255, 255, int(255*arrayWatch[i]/double(TORPEDO_DURATION/2))));
            }
            if (Ennemi::nbEnnemi)
            {
               /*
                list_dist = []
        for i in range(len(glob_var.liste_ennemi)):
            if self.hitbox.colliderect(glob_var.liste_ennemi[i].hitbox):
                glob_var.liste_ennemi[i].hp = glob_var.liste_ennemi[i].hp - self.degat
                #liste_deg.append(Degat([self.hitbox.centerx, self.hitbox.centery]))
                glob_var.liste_torp.remove(self)
                self = None
                return True
            list_dist.append(func.distance(self.rect, glob_var.liste_ennemi[i].rect))
            index = list_dist.index(min(list_dist))
            self.speed = [glob_var.liste_ennemi[index].hitbox.centerx - self.hitbox.centerx, glob_var.liste_ennemi[index].hitbox.centery - self.hitbox.centery]
            self.speed = func.normalise(self.speed, 10)
            */
                double distanceMin = 5000000.0;
                int index = 0;
                for (int j = 0; j < window.nbEnnemi; j++)
                {
                    if (arrayEnnemi[j].exist)
                    {
                        std::vector<double> vectDist;
                        vectDist.push_back(arrayPosx[i] - arrayEnnemi[j].posx);
                        vectDist.push_back(arrayPosy[i] - arrayEnnemi[j].posy);
                        double distance = getNormCarre(vectDist);
                        if (distance < distanceMin)
                        {
                            distanceMin = distance;
                            index = j;
                        }
                    }
                }
                std::vector<double> vectDist;
                vectDist.push_back(arrayPosx[i] - arrayEnnemi[index].posx);
                vectDist.push_back(arrayPosy[i] - arrayEnnemi[index].posy);
                if (getNormCarre(vectDist) < 400.0)
                {
                    arrayWatch[i] = 0;
                    arrayEnnemi[index].watchResetDamage = 0;
                    arrayEnnemi[index].hp = arrayEnnemi[index].hp - 1;
                }
                vectDist = setNorm(vectDist, 12.0);
                arrayPosx[i] = arrayPosx[i] - vectDist[0];
                arrayPosy[i] = arrayPosy[i] - vectDist[1];
                arrayTorpedo[i].move(-vectDist[0], -vectDist[1]);
            }
            else
            {
                arrayPosx[i] = arrayPosx[i] + arraySpeedx[i];
                arrayPosy[i] = arrayPosy[i] + arraySpeedy[i];
                arrayTorpedo[i].move(arraySpeedx[i], arraySpeedy[i]);
            }
        }
    }
    //====================================================================================================================================================================================================
    //Motif
    for (int i = 0; i < sizey; i++)
    {
        for (int j = 0; j < sizex; j++)
        {
            if (motif[i][j])
            {
                motif[i][j] = motif[i][j] - 1;
            }
        }
    }
    posPlayx = posPlayx + (posx / 20) - (posInitx / 20);
    posPlayy = posPlayy + (posy / 20) - (posInity / 20);
    if (posPlayx == -1)
    {
        insertMotif(3);
        posPlayx = 0;
    }
    else if (posPlayx == sizex)
    {
        insertMotif(1);
    }
    if (posPlayy == -1)
    {
        insertMotif(0);
        posPlayy = 0;
    }
    else if (posPlayy == sizey)
    {
        insertMotif(2);
    }
    motif[posPlayy][posPlayx] = TIME_MOTIF;
    bool shrinker = false;
    for (int i = 0; i < sizex; i++)
    {
        shrinker = shrinker + motif[0][i];   
    }
    if (!shrinker)
    {
        shrinkMotif(0);   
    }
    shrinker = false;
    for (int i = 0; i < sizey; i++)
    {
        shrinker = shrinker + motif[i][sizex - 1];   
    }
    if (!shrinker)
    {
        shrinkMotif(1);   
    }
    shrinker = false;
    for (int i = 0; i < sizex; i++)
    {
        shrinker = shrinker + motif[sizey - 1][i];   
    }
    if (!shrinker)
    {
        shrinkMotif(2);   
    }
    shrinker = false;
    for (int i = 0; i < sizey; i++)
    {
        shrinker = shrinker + motif[i][0];   
    }
    if (!shrinker)
    {
        shrinkMotif(3);   
    }
    if (dialogue.on)
    {
        resetMotif();
    }
    //====================================================================================================================================================================================================

    //updateTrail();
    if (watchForm == 0)
    {
        if (distanceBigSquare(false) && !watchForm)
        {
            std::cout << "Vous venez de dessiner un carre" << std::endl;
            watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
            idForm = 0;
            sizeForm = sizey;
            posxForm = posx - posPlayx*20;
            posyForm = posy - posPlayy*20;
            powerForm = power;
        }
        if (distanceTriangleRightEasy(false, false))
        {
            std::cout << "Vous venez de dessiner un triangle droite" << std::endl;
            watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
            idForm = 1;
            sizeForm = sizey;
            posxForm = posx - posPlayx*20;
            posyForm = posy - posPlayy*20;
            powerForm = power;
        }
        if (distanceTriangleLeftEasy(false, false))
        {
            std::cout << "Vous venez de dessiner un triangle gauche" << std::endl;
            watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
            idForm = 3;
            sizeForm = sizey;
            posxForm = posx - posPlayx*20;
            posyForm = posy - posPlayy*20;
            powerForm = power;
        }
        if (distanceTriangleUpEasy(false, false))
        {
            std::cout << "Vous venez de dessiner un triangle haut" << std::endl;
            watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
            idForm = 4;
            sizeForm = sizex;
            posxForm = posx - posPlayx*20;
            posyForm = posy - posPlayy*20;
            powerForm = power;
        }
        if (distanceTriangleDownEasy(false, false))
        {
            std::cout << "Vous venez de dessiner un triangle bas" << std::endl;
            watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
            idForm = 2;
            sizeForm = sizex;
            posxForm = posx - posPlayx*20;
            posyForm = posy - posPlayy*20;
            powerForm = power;
        }
        if (distanceCircle(false, false))
        {
            std::cout << "Vous venez de dessiner un cercle" << std::endl;
            watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
            idForm = 5;
            sizeForm = sizex;
            posxForm = posx - posPlayx*20;
            posyForm = posy - posPlayy*20;
            powerForm = power;
        }
        if (distanceBigSquareInclined(false))
        {
            std::cout << "Vous venez de dessiner un carre incline" << std::endl;
            watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
            idForm = 6;
            sizeForm = sizex;
            posxForm = posx - posPlayx*20;
            posyForm = posy - posPlayy*20;
            powerForm = power;
        }
        if (distanceTriangle45(false, false))
        {
            std::cout << "Vous venez de dessiner un triangle 45" << std::endl;
            watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
            idForm = 7;
            sizeForm = sizex;
            posxForm = posx - posPlayx*20;
            posyForm = posy - posPlayy*20;
            powerForm = power;
        }
        if (distanceTriangle135(false, false))
        {
            std::cout << "Vous venez de dessiner un triangle 135" << std::endl;
            watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
            idForm = 8;
            sizeForm = sizex;
            posxForm = posx - posPlayx*20;
            posyForm = posy - posPlayy*20;
            powerForm = power;
        }
        if (distanceTriangle225(false, false))
        {
            std::cout << "Vous venez de dessiner un triangle 225" << std::endl;
            watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
            idForm = 9;
            sizeForm = sizex;
            posxForm = posx - posPlayx*20;
            posyForm = posy - posPlayy*20;
            powerForm = power;
        }
        if (distanceTriangle315(false, false))
        {
            std::cout << "Vous venez de dessiner un triangle 315" << std::endl;
            watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
            idForm = 10;
            sizeForm = sizex;
            posxForm = posx - posPlayx*20;
            posyForm = posy - posPlayy*20;
            powerForm = power;
        }
    }
    /*
    distanceBigSquare(false);
    distanceTriangleUpEasy(false, false);
    distanceTriangleRightEasy(false, false);
    distanceTriangleDownEasy(false, false);
    distanceTriangleLeftEasy(false, false);
    distanceBigSquareInclined(false);
    distanceTriangle45(false, false);
    distanceTriangle135(false, false);
    distanceTriangle225(false, false);
    distanceTriangle315(false, false);
    */
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad0) && watchForm == 0)
    {
        watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
        idForm = 0;
        sizeForm = 15;
        posxForm = posx - posPlayx*20;
        posyForm = posy - posPlayy*20;
        powerForm = power;
    }
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad1) && watchForm == 0)
    {
        watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
        idForm = 1;
        sizeForm = 15;
        posxForm = posx - posPlayx*20;
        posyForm = posy - posPlayy*20;
        powerForm = power;
    }
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad2) && watchForm == 0)
    {
        watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
        idForm = 2;
        sizeForm = 15;
        posxForm = posx - posPlayx*20;
        posyForm = posy - posPlayy*20;
        powerForm = power;
    }
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad3) && watchForm == 0)
    {
        watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
        idForm = 3;
        sizeForm = 15;
        posxForm = posx - posPlayx*20;
        posyForm = posy - posPlayy*20;
        powerForm = power;
    }
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad4) && watchForm == 0)
    {
        watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
        idForm = 4;
        sizeForm = 15;
        posxForm = posx - posPlayx*20;
        posyForm = posy - posPlayy*20;
        powerForm = power;
    }
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad5) && watchForm == 0)
    {
        watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
        idForm = 5;
        sizeForm = 15;
        posxForm = posx - posPlayx*20;
        posyForm = posy - posPlayy*20;
        powerForm = power;
    }
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad6) && watchForm == 0)
    {
        watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
        idForm = 6;
        sizeForm = 10;
        posxForm = posx - posPlayx*20;
        posyForm = posy - posPlayy*20;
        powerForm = power;
    }
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad7) && watchForm == 0)
    {
        watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
        idForm = 7;
        sizeForm = 15;
        posxForm = posx - posPlayx*20;
        posyForm = posy - posPlayy*20;
        powerForm = power;
    }
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad8) && watchForm == 0)
    {
        watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
        idForm = 8;
        sizeForm = 15;
        posxForm = posx - posPlayx*20;
        posyForm = posy - posPlayy*20;
        powerForm = power;
    }
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad9) && watchForm == 0)
    {
        watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
        idForm = 9;
        sizeForm = 15;
        posxForm = posx - posPlayx*20;
        posyForm = posy - posPlayy*20;
        powerForm = power;
    }
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) && watchForm == 0)
    {
        watchForm = 2*timeApparitionForm + 2*timeApparitionLaser + timeHoldLaser;
        idForm = 10;
        sizeForm = 15;
        posxForm = posx - posPlayx*20;
        posyForm = posy - posPlayy*20;
        powerForm = power;
    }
    
    if (watchForm)
    {
        if (idForm == 0)
        {
            animationSquare();
        }
        else if (idForm == 1)
        {
            animationTriangleRight();
        }
        else if (idForm == 2)
        {
            animationTriangleDown();
        }
        else if (idForm == 3)
        {
            animationTriangleLeft();
        }
        else if (idForm == 4)
        {
            animationTriangleUp();
        }
        else if (idForm == 5)
        {
            animationCircle();
        }
        else if (idForm == 6)
        {
            animationSquareInclined();
        }
        else if (idForm == 7)
        {
            animationTriangle45();
        }
        else if (idForm == 8)
        {
            animationTriangle135();
        }
        else if (idForm == 9)
        {
            animationTriangle225();
        }
        else
        {
            animationTriangle315();
        }
    }
    damageForm();
}

void Player::lossLife()
{
    recoveryTime = RECOVERY_TIME_DURATION;
    nbLife = nbLife - 1;
    circleLossLife.setPosition(posx, posy);
}

void Player::addTorpedo()
{
    for (int i = 0; i < NB_TORPEDO; i++)
    {
        if (arrayWatch[i] == 0)
        {
            resetTorpedo(i);
            return;
        }
    }
}

void Player::resetTorpedo(int i)
{
    arrayWatch[i] = TORPEDO_DURATION;
    arrayPosx[i] = posx;
    arrayPosy[i] = posy;
    arrayTorpedo[i].setPosition(posx, posy);
    arrayTorpedo[i].setColor(sf::Color(255, 255, 255, 255));
    double angle = 2.0*6.28*rand()/(RAND_MAX);
    outVector = rotation(inVector, angle);
    arraySpeedx[i] = outVector[0];
    arraySpeedy[i] = outVector[1];
}

/**
 * 0 : Up
 * 1 : Right
 * 2 : Down
 * 3 : Left
**/
void Player::insertMotif(int direction)
{
    if (direction == 0)
    {
        std::vector<int> tmp(sizex, 0);
        motif.insert(motif.begin(), tmp);
        sizey = sizey + 1;
    }
    else if (direction == 1)
    {
        for (int i = 0; i < sizey; i++)
        {
            motif[i].push_back(0);
        }
        sizex = sizex + 1;
    }
    else if (direction == 2)
    {
        std::vector<int> tmp(sizex, 0);
        motif.push_back(tmp);
        sizey = sizey + 1;
    }
    else
    {
        for (int i = 0; i < sizey; i++)
        {
            motif[i].insert(motif[i].begin(), 0);   
        }
        sizex = sizex + 1;
    }
}

/**
 * 0 : Up
 * 1 : Right
 * 2 : Down
 * 3 : Left
**/
void Player::shrinkMotif(int direction)
{
    if (direction == 0)
    {
        motif.erase(motif.begin());
        sizey = sizey - 1;
        posPlayy = posPlayy - 1;
    }
    else if (direction == 1)
    {
        for (int i = 0; i < sizey; i++)
        {
            motif[i].erase(motif[i].begin() + sizex - 1);
        }
        sizex = sizex - 1;
    }
    else if (direction == 2)
    {
        motif.erase(motif.begin() + sizey - 1);
        sizey = sizey - 1;
    }
    else
    {
        for (int i = 0; i < sizey; i++)
        {
            motif[i].erase(motif[i].begin());  
        }
        sizex = sizex - 1;
        posPlayx = posPlayx - 1;
    }
}

void Player::displayMotif(std::string errMessage)
{
    std::cout << errMessage << std::endl;
    for (int i = 0; i < sizey; i++)
    {
        std::cout << "\n";
        for (int j = 0; j < sizex; j++)
        {
            if (motif[i][j])
            {
                std::cout << "X";    
            }
            else
            {
                std::cout << "O";   
            }
        }
    }
    std::cout << "\n";
    std::cout << "sizex = " << sizex << std::endl;
    std::cout << "sizey = " << sizey << std::endl;
    std::cout << "posPlayx = " << posPlayx << std::endl;
    std::cout << "posPlayy = " << posPlayy << std::endl;
}

void Player::resetMotif()
{
    motif = std::vector< std::vector<int> >(1, std::vector<int>(1, TIME_MOTIF));
    posPlayx = 0;
    posPlayy = 0;
    sizex = 1;
    sizey = 1;
}

void Player::updateTrail()
{
    watchTrail[watch % TIME_MOTIF] = TIME_MOTIF;
    spriteTrail[watch % TIME_MOTIF].setPosition(posx - 17, posy - 25);
}

void Player::drawTrail()
{
    for (int i = 0; i < TIME_MOTIF ; i++)
    {
        spriteTrail[i].setColor(sf::Color(255, 255, 255, watchTrail[i]));
        watchTrail[i] = watchTrail[i] - 1;
        window.mainWindow.draw(spriteTrail[i]);
    }
}

void Player::drawTrailAlt()
{
    for (int i = 0; i < TIME_MOTIF ; i++)
    {
        spriteTrail[i].setColor(sf::Color(255, 255, 255, watchTrail[i]));
        window.renderBackground.draw(spriteTrail[i]);
    }
}

int Player::distanceBigSquare(bool boo)
{
    if (sizex != sizey || sizex < 4)
    {
        return false;
    }
    int res = sizex;
    int True = 0;
    int False = 0;
    
    for (int i = 0; i < sizey; i++)
    {
        for (int j = 0; j < sizex; j++)
        {
            if (i > 0 && i < sizey - 1 && j > 0 && j < sizex - 1)
            {
                if (motif[i][j])
                {
                    False = False + 1;
                }
                else
                {
                    
                }
            }
            else
            {
                if (motif[i][j])
                {
                    True = True + 1;
                }
                else
                {
                    
                }
            }
        }
    }
    if (!False && True > 4*sizey - 5)
    {
        return true;
    }
    else
    {
        return false;
    }  
        
        
    /* 
    for (int i = 1; i < sizey - 1; i++)
    {
        res = res*motif[i][0];
        for (int j = 1; j < sizex - 1; j++)
        {
            res = res*!motif[i][j];
        }
        res = res*motif[i][sizex - 1];
    }
    for (int j = 1; j < sizex - 1; j++)
    {
        res = res*motif[0][j];
        res = res*motif[sizey - 1][j];
    }
    if (res && boo)
    {
        window.mainWindow.draw(window.spriteRondRouge);
    }
    return res;
    */
}

int Player::distanceBigSquareInclined(bool boo)
{
    if (sizey < 6)
    {
        return 0;   
    }
    if (sizex < sizey - 1 || sizex > sizey + 1)
    {
        return 0;
    }
    int True = 0;
    int False = 0;
    for (int i = 0; i < sizey; i++)
    {
        for (int j = 0; j < sizex; j++)
        {
            if (fabs(i - (sizey - 1)/2.0) + fabs(j - (sizey - 1)/2.0) < (sizey - 1)/2.0 - 1 || fabs(i - (sizey - 1)/2.0) + fabs(j - sizey/2.0) > sizey/2.0 + 1)
            {
                if (motif[i][j])
                {
                    False = False + 1;
                    if (boo)
                    {
                        std::cout << "N";   
                    }
                }
                else
                {
                    if (boo)
                    {
                        std::cout << " ";   
                    }
                }
            }
            else
            {
                if (motif[i][j])
                {
                    True = True + 1;
                    False = False + 1;
                    if (boo)
                    {
                        std::cout << "O";   
                    }
                }
                else
                {
                    if (boo)
                    {
                        std::cout << "o";   
                    }
                }
            }
        }
        if (boo)
        {
            std::cout << "\n";
        }
    }
    if (!False && True > 2*sizey - 5)
    {
        return true;   
    }
    else
    {
        return false;
    }
}

int Player::distanceTriangleUp()
{
    
}

int Player::distanceTriangleLeft()
{
    if (sizey < 6)
    {
        return false;   
    }
    if (sizey % 2)
    {
        if (sizex*2 - 1 != sizey)
        {
            return 0;   
        }
        else
        {
            int res = sizey;
            for (int i = 0; i < sizey/2 - 1; i++) //Haut du triangle
            {
                for (int j = 2 + i; j < sizex; j++)
                {
                    res = res*!motif[i][j];
                }
            }
            for (int i = sizey/2 + 1; i < sizey; i++) //Bas du triangle
            {
                for (int j = sizey - i + 1; j < sizex; j++)
                {
                    res = res*!motif[i][j];
                }
            }
            for (int i = 2; i < sizey/2; i++) //Millieu haut du triangle
            {
                for (int j = 1; j < i; j++)
                {
                    res = res*!motif[i][j];
                }
            }
            for (int i = sizey/2; i < sizey - 2; i++) //Millieu bas du triangle
            {
                for (int j = 1; j < sizey - i - 1; j++)
                {
                    res = res*!motif[i][j];
                }
            }
            for (int j = 1; j < sizex - 1; j++) //Millieu du triangle
            {
                res = res*!motif[sizey/2 + 1][j];   
            }
            if (res)
            {
                window.mainWindow.draw(window.spriteRondBleu);
            }
            return res;
        }
    }
    else
    {
        if (sizex*2 - 2 != sizey)
        {
            return 0;   
        }
        else
        {
            int res = sizey;
            for (int i = 0; i < sizey/2 - 1; i++) //Haut du triangle
            {
                for (int j = 2 + i; j < sizex; j++)
                {
                    res = res*!motif[i][j];
                }
            }
            for (int i = sizey/2 + 1; i < sizey; i++) //Bas du triangle
            {
                for (int j = sizey - i + 1; j < sizex; j++)
                {
                    res = res*!motif[i][j];
                }
            }
            for (int i = 2; i < sizey/2; i++) //Millieu haut du triangle
            {
                for (int j = 1; j < i; j++)
                {
                    res = res*!motif[i][j];
                }
            }
            for (int i = sizey/2; i < sizey - 2; i++) //Millieu bas du triangle
            {
                for (int j = 1; j < sizey - i - 1; j++)
                {
                    res = res*!motif[i][j];
                }
            }
            if (res)
            {
                window.mainWindow.draw(window.spriteRondBleu);
            }
            return res;
        }
    }
}

int Player::distanceTriangleDown()
{
    
}

int Player::distanceTriangleRight()
{
    
}


int Player::distanceTriangleUpEasy(bool debug, bool boo)
{
    if (sizex < 7 || sizey < 5)
    {
        return 0;   
    }
    int False = 0;
    int True = 0;
    for (int i = 0; i < sizey; i++)
    {
        if (debug)
        {
            std::cout << "\n";
        }
        for (int j = 0; j < sizex; j++)
        {
            if (i < sizey - 2 && (i + j < sizey - 3 || j - i > sizex - sizey + 2  || (i + j > sizey - 1 && j - i < sizex - sizey && i > sizey - (sizex + 2)/2)))    
            {
                if (motif[i][j])
                {
                    False = False + 1;
                    if (debug)
                    {
                        std::cout << "N";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << " ";
                    }
                }
            }
            else
            {
                if (motif[i][j])
                {
                    True = True + 1;
                    if (debug)
                    {
                        std::cout << "O";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << "o";
                    }
                }
            }
        }
    }
    if (debug)
    {
        std::cout << "\n";
    }
    if (False == 0 && True > 2*sizey - 5 && boo)
    {
        window.mainWindow.draw(window.spriteRondJaune);
    }
    if (debug)
    {
        std::cout << "True = " << True << "\n" << "False = " << False << std::endl;    
    }
    if (!False && True > 2*sizex - 5)
    {
        return true;
    }
    else
    {
        return false;
    }   
}

int Player::distanceTriangleLeftEasy(bool debug, bool boo)
{
    if (sizey < 7 || sizex < 5)
    {
        return 0;   
    }
    int False = 0;
    int True = 0;
    for (int i = 0; i < sizey; i++)
    {
        if (debug)
        {
            std::cout << "\n";
        }
        for (int j = 0; j < sizex; j++)
        {
            if (j < sizex - 2 && (j + i < sizex - 3 || i - j > sizey - sizex + 2 || (i + j > sizex - 1 && i - j < sizey - sizex && j > sizex - (sizey + 2)/2)))
            {
                if (motif[i][j])
                {
                    False = False + 1;
                    if (debug)
                    {
                        std::cout << "N";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << " ";
                    }
                }
            }
            else
            {
                if (motif[i][j])
                {
                    True = True + 1;
                    if (debug)
                    {
                        std::cout << "O";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << "o";
                    }
                }
            }
        }
    }
    if (debug)
    {
        std::cout << "\n";
    }
    if (False == 0 && True > 2*sizey - 5 && boo)
    {
        window.mainWindow.draw(window.spriteRondVert);
    }
    if (debug)
    {
        std::cout << "True = " << True << "\n" << "False = " << False << std::endl;    
    }
    if (!False && True > 2*sizey - 5)
    {
        return true;
    }
    else
    {
        return false;
    }   
}

int Player::distanceTriangleDownEasy(bool debug, bool boo)
{
    if (sizex < 7 || sizey < 5)
    {
        return 0;   
    }
    int False = 0;
    int True = 0;
    for (int i = 0; i < sizey; i++)
    {
        if (debug)
        {
            std::cout << "\n";
        }
        for (int j = 0; j < sizex; j++)
        {
            if (i > 1 && (i - j > 2 || i + j > sizex + 1 || (j - i > 0 && i + j < sizex - 1 and i < (sizex - 1)/2)))
            {
                if (motif[i][j])
                {
                    False = False + 1;
                    if (debug)
                    {
                        std::cout << "N";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << " ";
                    }
                }
            }
            else
            {
                if (motif[i][j])
                {
                    True = True + 1;
                    if (debug)
                    {
                        std::cout << "O";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << "o";
                    }
                }
            }
        }
    }
    if (debug)
    {
        std::cout << "\n";
    }
    if (False == 0 && True > 2*sizex - 5 && boo)
    {
        window.mainWindow.draw(window.spriteRondCyan);
    }
    if (debug)
    {
        std::cout << "True = " << True << "\n" << "False = " << False << std::endl;    
    }
    if (!False && True > 2*sizey - 5)
    {
        return true;
    }
    else
    {
        return false;
    }   
}

int Player::distanceTriangleRightEasy(bool debug, bool boo)
{
    if (sizey < 7 || sizex < 5)
    {
        return 0;   
    }
    int False = 0;
    int True = 0;
    for (int i = 0; i < sizey; i++)
    {
        if (debug)
        {
            std::cout << "\n";
        }
        for (int j = 0; j < sizex; j++)
        {
            if (j > 1 && (j - i > 2 || j + i > 1 + sizey || (i - j > 0 && i + j < sizey - 1 && j < (sizey - 1)/2)))
                //if j < largeur -2 and (j + i < largeur - 3 or i - j > hauteur - largeur + 2  or (i + j > largeur - 1 and i - j < hauteur - largeur and j > largeur - ((hauteur + 2)/2))):
            {
                if (motif[i][j])
                {
                    False = False + 1;
                    if (debug)
                    {
                        std::cout << "N";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << " ";
                    }
                }
            }
            else
            {
                if (motif[i][j])
                {
                    True = True + 1;
                    if (debug)
                    {
                        std::cout << "O";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << "o";
                    }
                }
            }
        }
    }
    if (debug)
    {
        std::cout << "\n";
    }
    if (False == 0 && True > 2*sizey - 5 && boo)
    {
        window.mainWindow.draw(window.spriteRondBleu);
    }
    if (debug)
    {
        std::cout << "True = " << True << "\n" << "False = " << False << std::endl;    
    }
    if (!False && True > 2*sizey - 5)
    {
        return true;
    }
    else
    {
        return false;
    }   
}

int Player::distanceTriangle45(bool debug, bool boo)
{
    if (sizey < 6)
    {
        return false;
    }
    if (sizex < sizey - 1 || sizex > sizey + 1)
    {
        return false;
    }
    int False = 0;
    int True = 0;
    for (int i = 0; i < sizey; i++)
    {
        if (debug)
        {
            std::cout << "\n";
        }
        for (int j = 0; j < sizex; j++)
        {
            if (i + j < sizey - 1 || i + j > sizex + 1 && i < sizey - 1 && j < sizex - 1)
            {
                if (motif[i][j])
                {
                    False = False + 1;
                    if (debug)
                    {
                        std::cout << "N";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << " ";
                    }
                }
            }
            else
            {
                if (motif[i][j])
                {
                    True = True + 1;
                    if (debug)
                    {
                        std::cout << "O";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << "o";
                    }
                }
            }
        }
    }
    if (debug)
    {
        std::cout << "\n";
    }
    if (!False && True > 3*sizey - 5)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int Player::distanceTriangle135(bool debug, bool boo)
{
    if (sizey < 6)
    {
        return false;
    }
    if (sizex < sizey - 1 || sizex > sizey + 1)
    {
        return false;
    }
    int False = 0;
    int True = 0;
    for (int i = 0; i < sizey; i++)
    {
        if (debug)
        {
            std::cout << "\n";
        }
        for (int j = 0; j < sizex; j++)
        {
            if (i - j < -1 || (i - j > 1 && i < sizey - 1 && j > 0))
            {
                if (motif[i][j])
                {
                    False = False + 1;
                    if (debug)
                    {
                        std::cout << "N";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << " ";
                    }
                }
            }
            else
            {
                if (motif[i][j])
                {
                    True = True + 1;
                    if (debug)
                    {
                        std::cout << "O";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << "o";
                    }
                }
            }
        }
    }
    if (debug)
    {
        std::cout << "\n";
    }
    if (!False && True > 3*sizey - 5)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int Player::distanceTriangle225(bool debug, bool boo)
{
    if (sizey < 6)
    {
        return false;
    }
    if (sizex < sizey - 1 || sizex > sizey + 1)
    {
        return false;
    }
    int False = 0;
    int True = 0;
    for (int i = 0; i < sizey; i++)
    {
        if (debug)
        {
            std::cout << "\n";
        }
        for (int j = 0; j < sizex; j++)
        {
            if (i + j > sizey + 1 || i + j < sizex - 1 && i > 0 && j > 0)
            {
                if (motif[i][j])
                {
                    False = False + 1;
                    if (debug)
                    {
                        std::cout << "N";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << " ";
                    }
                }
            }
            else
            {
                if (motif[i][j])
                {
                    True = True + 1;
                    if (debug)
                    {
                        std::cout << "O";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << "o";
                    }
                }
            }
        }
    }
    if (debug)
    {
        std::cout << "\n";
    }
    if (!False && True > 3*sizey - 5)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int Player::distanceTriangle315(bool debug, bool boo)
{
    if (sizey < 6)
    {
        return false;
    }
    if (sizex < sizey - 1 || sizex > sizey + 1)
    {
        return false;
    }
    int False = 0;
    int True = 0;
    for (int i = 0; i < sizey; i++)
    {
        if (debug)
        {
            std::cout << "\n";
        }
        for (int j = 0; j < sizex; j++)
        {
            if (i - j > 1 || (i - j < - 1 && i > 0 && j > sizex - 1))
            {
                if (motif[i][j])
                {
                    False = False + 1;
                    if (debug)
                    {
                        std::cout << "N";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << " ";
                    }
                }
            }
            else
            {
                if (motif[i][j])
                {
                    True = True + 1;
                    if (debug)
                    {
                        std::cout << "O";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << "o";
                    }
                }
            }
        }
    }
    if (debug)
    {
        std::cout << "\n";
    }
    if (!False && True > 3*sizey - 5)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int Player::distanceCircle(bool debug, bool boo)
{
    if (sizey < 6)
    {
        return false;
    }
    if (sizex < sizey - 1 || sizex > sizey + 1)
    {
        return false;
    }
    int False = 0;
    int True = 0;
    for (int i = 0; i < sizey; i++)
    {
        if (debug)
        {
            std::cout << "\n";
        }
        for (int j = 0; j < sizex; j++)
        {
            if ((i - (sizey - 1)/2.0)*(i - (sizey - 1)/2.0) + (j - (sizey - 1)/2.0)*(j - (sizey - 1)/2.0) < ((sizey - 1.0)/2.0 - 1.0)*((sizey - 1.0)/2.0 - 1.0)
             || (i - (sizey - 1)/2.0)*(i - (sizey - 1)/2.0) + (j - (sizey - 1)/2.0)*(j - (sizey - 1)/2.0) > ((sizey - 1.0)/2.0 + 1.0)*((sizey - 1.0)/2.0 + 1.0))
            {
                if (motif[i][j])
                {
                    False = False + 1;
                    if (debug)
                    {
                        std::cout << "N";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << " ";
                    }
                }
            }
            else
            {
                if (motif[i][j])
                {
                    True = True + 1;
                    if (debug)
                    {
                        std::cout << "O";
                    }
                }
                else
                {
                    if (debug)
                    {
                        std::cout << "o";
                    }
                }
            }
        }
    }
    if (debug)
    {
        std::cout << "\n";
    }
    if (!False && True > 4*sizey - 5)
    {
        return true;
    }
    else
    {
        return false;
    }  
}

void Player::drawForm()
{
    if (watchForm)
    {
        if (idForm == 0 || idForm == 6)
        {
            window.mainWindow.draw(spriteOuterSquare);
            for (int i = 0; i < 16; i++)
            {
                if (slotLaser[i])
                {
                    window.mainWindow.draw(spriteLaser[i]);
                }
            }
        window.mainWindow.draw(spriteInnerSquare); 
        }
        else if (idForm == 1 || idForm == 2 || idForm == 3 || idForm == 4 || idForm == 7 || idForm == 8 || idForm == 9 || idForm == 10)
        {
            window.mainWindow.draw(spriteOuterTriangle);
            for (int i = 0; i < 8; i++)
            {
                if (slotLaser[i])
                {
                    window.mainWindow.draw(spriteLaser[i]);
                }
            }
            window.mainWindow.draw(spriteInnerTriangle); 
        }
        else if (idForm == 5)
        {
            window.mainWindow.draw(spriteOuterCircle);
            window.mainWindow.draw(spriteInnerCircle);
        }
    }
}

void Player::drawFormAlt()
{
    if (watchForm)
    {
        if (idForm == 0 || idForm == 6)
        {
            window.renderBackground.draw(spriteOuterSquare);
            for (int i = 0; i < 16; i++)
            {
                if (slotLaser[i])
                {
                    window.renderBackground.draw(spriteLaser[i]);
                }
            }
        window.renderBackground.draw(spriteInnerSquare); 
        }
        else if (idForm == 1 || idForm == 2 || idForm == 3 || idForm == 4 || idForm == 7 || idForm == 8 || idForm == 9 || idForm == 10)
        {
            window.renderBackground.draw(spriteOuterTriangle);
            for (int i = 0; i < 8; i++)
            {
                if (slotLaser[i])
                {
                    window.renderBackground.draw(spriteLaser[i]);
                }
            }
            window.renderBackground.draw(spriteInnerTriangle); 
        }
        else if (idForm == 5)
        {
            window.renderBackground.draw(spriteOuterCircle);
            window.renderBackground.draw(spriteInnerCircle);
        }
    }
}

void Player::damageForm()
{
    if (watchForm)
    {
        if (idForm == 0)
        {
            /*
            std::cout << "arrayEnnemi[0].posx = " << arrayEnnemi[0].posx << std::endl;
            std::cout << "arrayEnnemi[0].posy = " << arrayEnnemi[0].posy << std::endl;
            std::cout << "posxForm = " << posxForm << std::endl;
            std::cout << "posyForm = " << posyForm << std::endl;
            std::cout << "sizeForm*20 = " << sizeForm*20 << std::endl;
            std::cout << "\n";
            */
            
            for (int i = 0; i < window.nbEnnemi; i++)
            {
                if ((arrayEnnemi[i].posx > posxForm
                  && arrayEnnemi[i].posx < posxForm + sizeForm*20)
                 
                 || (arrayEnnemi[i].posy > posyForm
                  && arrayEnnemi[i].posy < posyForm + sizeForm*20))
                {
                    arrayEnnemi[i].hp = arrayEnnemi[i].hp - powerForm;
                }
            }
        }
        else if (idForm == 1)
        {
            for (int i = 0; i < window.nbEnnemi; i++)
            {
                if (arrayEnnemi[i].posx > posxForm && arrayEnnemi[i].posy > posyForm && arrayEnnemi[i].posy < posyForm + sizeForm*20)
                {
                    arrayEnnemi[i].hp = arrayEnnemi[i].hp - powerForm*2;   
                }
            }
        }
        else if (idForm == 2)
        {
            for (int i = 0; i < window.nbEnnemi; i++)
            {
                if (arrayEnnemi[i].posy > posyForm && arrayEnnemi[i].posx > posxForm && arrayEnnemi[i].posx < posxForm + sizeForm*20)
                {
                    arrayEnnemi[i].hp = arrayEnnemi[i].hp - powerForm*2;   
                }
            }
        }
        else if (idForm == 3)
        {
            for (int i = 0; i < window.nbEnnemi; i++)
            {
                if (arrayEnnemi[i].posx < posxForm + sizeForm*10 && arrayEnnemi[i].posy > posyForm && arrayEnnemi[i].posy < posyForm + sizeForm*20)
                {
                    arrayEnnemi[i].hp = arrayEnnemi[i].hp - powerForm*2;   
                }
            }
        }
        else if (idForm == 4)
        {
            for (int i = 0; i < window.nbEnnemi; i++)
            {
                if (arrayEnnemi[i].posy < posyForm + sizeForm*10 && arrayEnnemi[i].posx > posxForm && arrayEnnemi[i].posx < posxForm + sizeForm*20)
                {
                    arrayEnnemi[i].hp = arrayEnnemi[i].hp - powerForm*2;   
                }
            }
        }
        else if (idForm == 5)
        {
            //This is the circle, which have no attack
        }
        else if (idForm == 6)
        {
            for (int i = 0; i < window.nbEnnemi; i++)
            {
                if ((arrayEnnemi[i].posx - arrayEnnemi[i].posy > posxForm - posyForm + sizeForm*20 - sizeForm*10.0/1.4
                  && arrayEnnemi[i].posx - arrayEnnemi[i].posy < posxForm - posyForm + sizeForm*20 + sizeForm*10.0/1.4)
                
                 || (arrayEnnemi[i].posx + arrayEnnemi[i].posy > posxForm + posyForm - sizeForm*10.0/1.4
                  && arrayEnnemi[i].posx + arrayEnnemi[i].posy < posxForm + posyForm + sizeForm*10.0/1.4))
                {
                    arrayEnnemi[i].hp = arrayEnnemi[i].hp - powerForm;   
                }
            }
        }
        else if (idForm == 7)
        {
            for (int i = 0; i < window.nbEnnemi; i++)
            {
                /*
                if (arrayEnnemi[i].posx - arrayEnnemi[i].posy < posxForm - posyForm + sizeForm*20.0/1.4
                 && arrayEnnemi[i].posx + arrayEnnemi[i].posy > posxForm + posyForm - sizeForm*20.0/1.4 - sqrt(pow(arrayEnnemi[i].posx - posxForm, 2) + pow(arrayEnnemi[i].posy - posyForm, 2))
                 && arrayEnnemi[i].posx + arrayEnnemi[i].posy < posxForm + posyForm + sizeForm*20.0/1.4 - sqrt(pow(arrayEnnemi[i].posx - posxForm, 2) + pow(arrayEnnemi[i].posy - posyForm, 2)))
                 */
                if (arrayEnnemi[i].posx + arrayEnnemi[i].posy > posxForm + posyForm + sizeForm*20
                 && arrayEnnemi[i].posx - arrayEnnemi[i].posy > posxForm - posyForm - sizeForm*20
                 && arrayEnnemi[i].posx - arrayEnnemi[i].posy < posxForm - posyForm + sizeForm*20)
                {
                    arrayEnnemi[i].hp = arrayEnnemi[i].hp - powerForm*2;
                    std::cout << "degat" << std::endl;
                }
            }
        }
        else if (idForm == 8)
        {
            for (int i = 0; i < window.nbEnnemi; i++)
            {
                if (arrayEnnemi[i].posx - arrayEnnemi[i].posy < posxForm - posyForm
                 && arrayEnnemi[i].posx + arrayEnnemi[i].posy > posxForm + posyForm
                 && arrayEnnemi[i].posx + arrayEnnemi[i].posy < posxForm + posyForm + 2*sizeForm*20.0/1.4)
                {
                    arrayEnnemi[i].hp = arrayEnnemi[i].hp - powerForm*2;   
                }
            }
        }
        else if (idForm == 9)
        {
            for (int i = 0; i < window.nbEnnemi; i++)
            {
                if (arrayEnnemi[i].posx + arrayEnnemi[i].posy < posxForm + posyForm + sizeForm*20.0/1.4
                 && arrayEnnemi[i].posx - arrayEnnemi[i].posy > posxForm - posyForm - sizeForm*20.0/1.4
                 && arrayEnnemi[i].posx - arrayEnnemi[i].posy < posxForm - posyForm + sizeForm*20.0/1.4)
                {
                    arrayEnnemi[i].hp = arrayEnnemi[i].hp - powerForm*2;   
                }
            }
        }
        else
        {
            for (int i = 0; i < window.nbEnnemi; i++)
            {
                if (arrayEnnemi[i].posx - arrayEnnemi[i].posy > posxForm - posyForm
                 && arrayEnnemi[i].posx + arrayEnnemi[i].posy > posxForm + posyForm
                 && arrayEnnemi[i].posx + arrayEnnemi[i].posy < posxForm + posyForm + 2*sizeForm*20.0/1.4)
                {
                    arrayEnnemi[i].hp = arrayEnnemi[i].hp - powerForm*2;   
                }
            }
        }
    }
}


void Player::animationSquare() //The square have a maximum size of 25 (500px)
{
    double scaleMax = double(sizeForm)/25.0;
    double scale;
    if (watchForm == 180)
    {
        powerForm = 4;
        spriteInnerSquare.setRotation(0);
        spriteOuterSquare.setRotation(0);
        for (int i = 0; i < 16; i++)
        {
            slotLaser[i] = 0;  
        }
        if (powerForm == 1)
        {
            spriteLaser[0].setPosition(posxForm + sizeForm*20 - 70, posyForm + sizeForm*10); spriteLaser[0].setRotation(0);
            spriteLaser[1].setPosition(posxForm + sizeForm*10, posyForm + sizeForm*20 - 70); spriteLaser[1].setRotation(90);
            spriteLaser[2].setPosition(posxForm + 70, posyForm + sizeForm*10); spriteLaser[2].setRotation(180);
            spriteLaser[3].setPosition(posxForm + sizeForm*10, posyForm + 70); spriteLaser[3].setRotation(270);
        }
        else if (powerForm == 2)
        {
            spriteLaser[0].setPosition(posxForm + sizeForm*20 - 70, posyForm + sizeForm*10 - 40.0*sizeForm/10.0); spriteLaser[0].setRotation(0);
            spriteLaser[1].setPosition(posxForm + sizeForm*10 - 40.0*sizeForm/10.0, posyForm + sizeForm*20 - 70); spriteLaser[1].setRotation(90);
            spriteLaser[2].setPosition(posxForm + 70, posyForm + sizeForm*10 - 40.0*sizeForm/10.0); spriteLaser[2].setRotation(180);
            spriteLaser[3].setPosition(posxForm + sizeForm*10 - 40.0*sizeForm/10.0, posyForm + 70); spriteLaser[3].setRotation(270);
            
            spriteLaser[4].setPosition(posxForm + sizeForm*20 - 70, posyForm + sizeForm*10 + 40.0*sizeForm/10.0); spriteLaser[4].setRotation(0);
            spriteLaser[5].setPosition(posxForm + sizeForm*10 + 40.0*sizeForm/10.0, posyForm + sizeForm*20 - 70); spriteLaser[5].setRotation(90);
            spriteLaser[6].setPosition(posxForm + 70, posyForm + sizeForm*10 + 40.0*sizeForm/10.0); spriteLaser[6].setRotation(180);
            spriteLaser[7].setPosition(posxForm + sizeForm*10 + 40.0*sizeForm/10.0, posyForm + 70); spriteLaser[7].setRotation(270);
        }
        else if (powerForm == 3)
        {
            spriteLaser[0].setPosition(posxForm + sizeForm*20 - 70, posyForm + sizeForm*10 - 80.0*sizeForm/10.0); spriteLaser[0].setRotation(0);
            spriteLaser[1].setPosition(posxForm + sizeForm*10 - 80.0*sizeForm/10.0, posyForm + sizeForm*20 - 70); spriteLaser[1].setRotation(90);
            spriteLaser[2].setPosition(posxForm + 70, posyForm + sizeForm*10 - 80.0*sizeForm/10.0); spriteLaser[2].setRotation(180);
            spriteLaser[3].setPosition(posxForm + sizeForm*10 - 80.0*sizeForm/10.0, posyForm + 70); spriteLaser[3].setRotation(270);
            
            spriteLaser[4].setPosition(posxForm + sizeForm*20 - 70, posyForm + sizeForm*10); spriteLaser[4].setRotation(0);
            spriteLaser[5].setPosition(posxForm + sizeForm*10, posyForm + sizeForm*20 - 70); spriteLaser[5].setRotation(90);
            spriteLaser[6].setPosition(posxForm + 70, posyForm + sizeForm*10); spriteLaser[6].setRotation(180);
            spriteLaser[7].setPosition(posxForm + sizeForm*10, posyForm + 70); spriteLaser[7].setRotation(270);
            
            spriteLaser[8].setPosition(posxForm + sizeForm*20 - 70, posyForm + sizeForm*10 + 80.0*sizeForm/10.0); spriteLaser[8].setRotation(0);
            spriteLaser[9].setPosition(posxForm + sizeForm*10 + 80.0*sizeForm/10.0, posyForm + sizeForm*20 - 70); spriteLaser[9].setRotation(90);
            spriteLaser[10].setPosition(posxForm + 70, posyForm + sizeForm*10 + 80.0*sizeForm/10.0); spriteLaser[10].setRotation(180);
            spriteLaser[11].setPosition(posxForm + sizeForm*10 + 80.0*sizeForm/10.0, posyForm + 70); spriteLaser[11].setRotation(270);
        }
        else
        {
            spriteLaser[0].setPosition(posxForm + sizeForm*20 - 70, posyForm + sizeForm*10 - 45.0*sizeForm/10.0); spriteLaser[0].setRotation(0);
            spriteLaser[1].setPosition(posxForm + sizeForm*10 - 45.0*sizeForm/10.0, posyForm + sizeForm*20 - 70); spriteLaser[1].setRotation(90);
            spriteLaser[2].setPosition(posxForm + 70, posyForm + sizeForm*10 - 45.0*sizeForm/10.0); spriteLaser[2].setRotation(180);
            spriteLaser[3].setPosition(posxForm + sizeForm*10 - 45.0*sizeForm/10.0, posyForm + 70); spriteLaser[3].setRotation(270);
            
            spriteLaser[4].setPosition(posxForm + sizeForm*20 - 70, posyForm + sizeForm*10 - 15.0*sizeForm/10.0); spriteLaser[4].setRotation(0);
            spriteLaser[5].setPosition(posxForm + sizeForm*10 - 15.0*sizeForm/10.0, posyForm + sizeForm*20 - 70); spriteLaser[5].setRotation(90);
            spriteLaser[6].setPosition(posxForm + 70, posyForm + sizeForm*10 - 15.0*sizeForm/10.0); spriteLaser[6].setRotation(180);
            spriteLaser[7].setPosition(posxForm + sizeForm*10 - 15.0*sizeForm/10.0, posyForm + 70); spriteLaser[7].setRotation(270);
            
            spriteLaser[8].setPosition(posxForm + sizeForm*20 - 70, posyForm + sizeForm*10 + 15.0*sizeForm/10.0); spriteLaser[8].setRotation(0);
            spriteLaser[9].setPosition(posxForm + sizeForm*10 + 15.0*sizeForm/10.0, posyForm + sizeForm*20 - 70); spriteLaser[9].setRotation(90);
            spriteLaser[10].setPosition(posxForm + 70, posyForm + sizeForm*10 + 15.0*sizeForm/10.0); spriteLaser[10].setRotation(180);
            spriteLaser[11].setPosition(posxForm + sizeForm*10 + 15.0*sizeForm/10.0, posyForm + 70); spriteLaser[11].setRotation(270);
            
            spriteLaser[12].setPosition(posxForm + sizeForm*20 - 70, posyForm + sizeForm*10 + 45.0*sizeForm/10.0); spriteLaser[12].setRotation(0);
            spriteLaser[13].setPosition(posxForm + sizeForm*10 + 45.0*sizeForm/10.0, posyForm + sizeForm*20 - 70); spriteLaser[13].setRotation(90);
            spriteLaser[14].setPosition(posxForm + 70, posyForm + sizeForm*10 + 45.0*sizeForm/10.0); spriteLaser[14].setRotation(180);
            spriteLaser[15].setPosition(posxForm + sizeForm*10 + 45.0*sizeForm/10.0, posyForm + 70); spriteLaser[15].setRotation(270);
        }
    }
    if (watchForm > 165)
    {
        scale = scaleMax*(180.0 - watchForm)/15.0;
        spriteInnerSquare.setScale(scale, scale);
        spriteInnerSquare.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*10);
        spriteOuterSquare.setScale(scale, scale);
        spriteOuterSquare.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*10);
        
    }
    else if (watchForm > 85)
    {
        if ((watchForm) % (20/powerForm) == 0 && countLaser < powerForm * 4)
        {
            countLaser = countLaser + 1;
            std::vector<int> vectorSlot;
            for (int i = 0; i < 4*powerForm; i++)
            {
                if (!slotLaser[i])
                {
                    vectorSlot.push_back(i);
                }
            }
            cursorLaser = aleaVectorInt(vectorSlot);
            slotLaser[cursorLaser] = 1;
            spriteLaser[cursorLaser].setScale(slotLaser[cursorLaser]/5.0, slotLaser[cursorLaser]/5.0);
        }
    }
    if (slotLaser[cursorLaser] < 5 && slotLaser[cursorLaser] && watchForm > 85)
    {
        slotLaser[cursorLaser] = slotLaser[cursorLaser] + 1;
        if (cursorLaser % 2 == 0)
        {
            spriteLaser[cursorLaser].setScale(slotLaser[cursorLaser]/5.0, slotLaser[cursorLaser]/5.0);
        }
        else
        {
            spriteLaser[cursorLaser].setScale(slotLaser[cursorLaser]/5.0, slotLaser[cursorLaser]/5.0);
        }
    }
    
    else if (watchForm > 15 && watchForm < 30)
    {
        for (int i = 0; i < 16; i++)
        {
            if (slotLaser[i])
            {
                slotLaser[i] = slotLaser[i] - 1;
                spriteLaser[i].setScale(slotLaser[i]/5.0, slotLaser[i]/5.0);
            }
        }
    }
    
    else if (watchForm < 15)
    {
        scale = scaleMax*watchForm/double(timeApparitionForm);
        spriteInnerSquare.setScale(scale, scale);
        spriteInnerSquare.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*10);
        spriteOuterSquare.setScale(scale, scale);
        spriteOuterSquare.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*10);
    }
    watchForm = watchForm - 1;
    if (!watchForm)
    {
        for (int i = 0; i < 16; i++)
        {
            slotLaser[i] = 0;
        }
        countLaser = 0;
    }
}

void Player::animationTriangleRight() //The triangle have a maximum height of 33
{
    double scaleMax = double(sizeForm)/33.0;
    double scale;
    double scaleLaser;
    if (watchForm == 180)
    {
        powerForm = 2;
        spriteInnerTriangle.setRotation(0);
        spriteOuterTriangle.setRotation(0);
        /*
        spriteLaser[0].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 - 105); spriteLaser[0].setRotation(0);
        spriteLaser[1].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 - 75); spriteLaser[1].setRotation(0);
        spriteLaser[2].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 - 45); spriteLaser[2].setRotation(0);
        spriteLaser[3].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 - 15); spriteLaser[3].setRotation(0);     
        spriteLaser[4].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 + 15); spriteLaser[4].setRotation(0);
        spriteLaser[5].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 + 45); spriteLaser[5].setRotation(0);
        spriteLaser[6].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 + 75); spriteLaser[6].setRotation(0);
        spriteLaser[7].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 + 105); spriteLaser[7].setRotation(0);
        */
        spriteLaser[0].setPosition(posxForm + sizeForm*4, posyForm + sizeForm*10 - 7.0*sizeForm*20/24.0); spriteLaser[0].setRotation(0);
        spriteLaser[1].setPosition(posxForm + sizeForm*4, posyForm + sizeForm*10 - 5.0*sizeForm*20/24.0); spriteLaser[1].setRotation(0);
        spriteLaser[2].setPosition(posxForm + sizeForm*4, posyForm + sizeForm*10 - 3.0*sizeForm*20/24.0); spriteLaser[2].setRotation(0);
        spriteLaser[3].setPosition(posxForm + sizeForm*4, posyForm + sizeForm*10 - sizeForm*20/24.0); spriteLaser[3].setRotation(0);     
        spriteLaser[4].setPosition(posxForm + sizeForm*4, posyForm + sizeForm*10 + sizeForm*20/24.0); spriteLaser[4].setRotation(0);
        spriteLaser[5].setPosition(posxForm + sizeForm*4, posyForm + sizeForm*10 + 3.0*sizeForm*20/24.0); spriteLaser[5].setRotation(0);
        spriteLaser[6].setPosition(posxForm + sizeForm*4, posyForm + sizeForm*10 + 5.0*sizeForm*20/24.0); spriteLaser[6].setRotation(0);
        spriteLaser[7].setPosition(posxForm + sizeForm*4, posyForm + sizeForm*10 + 7.0*sizeForm*20/24.0); spriteLaser[7].setRotation(0);
        countLaser = 0;
    }
    if (watchForm > 165)
    {
        scale = scaleMax*(180.0 - watchForm)/15.0;
        spriteInnerTriangle.setScale(scale, scale);
        spriteInnerTriangle.setPosition(posxForm + sizeForm*5, posyForm + sizeForm*10);
        spriteOuterTriangle.setScale(scale, scale);
        spriteOuterTriangle.setPosition(posxForm + sizeForm*5, posyForm + sizeForm*10);
        
    }
    else if (watchForm > 85)
    {
        if (watchForm % 20 == 0 && countLaser < 4)
        {
            slotLaser[countLaser] = powerForm * 20;
            slotLaser[7 - countLaser] = powerForm * 20;
            countLaser = countLaser + 1;
        }
    }
    else if (watchForm < 15)
    {
        scale = scaleMax*watchForm/double(timeApparitionForm);
        spriteInnerTriangle.setScale(scale, scale);
        spriteInnerTriangle.setPosition(posxForm + sizeForm*5, posyForm + sizeForm*10);
        spriteOuterTriangle.setScale(scale, scale);
        spriteOuterTriangle.setPosition(posxForm + sizeForm*5, posyForm + sizeForm*10);
    }
    for (int i = 0; i < 8; i++)
    {
        if (slotLaser[i] > 20*powerForm - 5)
        {
            spriteLaser[i].setScale((20*powerForm - slotLaser[i])/5.0, (20*powerForm - slotLaser[i])/5.0);
        }
        else if (slotLaser[i] < 5)
        {
            spriteLaser[i].setScale(slotLaser[i]/5.0, slotLaser[i]/5.0);
        }
        if (slotLaser[i])
        {
            slotLaser[i] = slotLaser[i] - 1;   
        }
    }
    watchForm = watchForm - 1;
    if (!watchForm)
    {
        for (int i = 0; i < 16; i++)
        {
            slotLaser[i] = 0;
        }
        countLaser = 0;
    }
}

void Player::animationTriangleDown() //The triangle have a maximum height of 33
{
    double scaleMax = double(sizeForm)/33.0;
    double scale;
    double scaleLaser;
    if (watchForm == 180)
    {
        powerForm = 2;
        spriteInnerTriangle.setRotation(90);
        spriteOuterTriangle.setRotation(90);
        /*
        spriteLaser[0].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 - 105); spriteLaser[0].setRotation(0);
        spriteLaser[1].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 - 75); spriteLaser[1].setRotation(0);
        spriteLaser[2].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 - 45); spriteLaser[2].setRotation(0);
        spriteLaser[3].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 - 15); spriteLaser[3].setRotation(0);     
        spriteLaser[4].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 + 15); spriteLaser[4].setRotation(0);
        spriteLaser[5].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 + 45); spriteLaser[5].setRotation(0);
        spriteLaser[6].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 + 75); spriteLaser[6].setRotation(0);
        spriteLaser[7].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 + 105); spriteLaser[7].setRotation(0);
        */
        spriteLaser[0].setPosition(posxForm + sizeForm*10 - 7.0*sizeForm*20/24.0, posyForm + sizeForm*4); spriteLaser[0].setRotation(90);
        spriteLaser[1].setPosition(posxForm + sizeForm*10 - 5.0*sizeForm*20/24.0, posyForm + sizeForm*4); spriteLaser[1].setRotation(90);
        spriteLaser[2].setPosition(posxForm + sizeForm*10 - 3.0*sizeForm*20/24.0, posyForm + sizeForm*4); spriteLaser[2].setRotation(90);
        spriteLaser[3].setPosition(posxForm + sizeForm*10 - sizeForm*20/24.0, posyForm + sizeForm*4); spriteLaser[3].setRotation(90);     
        spriteLaser[4].setPosition(posxForm + sizeForm*10 + sizeForm*20/24.0, posyForm + sizeForm*4); spriteLaser[4].setRotation(90);
        spriteLaser[5].setPosition(posxForm + sizeForm*10 + 3.0*sizeForm*20/24.0, posyForm + sizeForm*4); spriteLaser[5].setRotation(90);
        spriteLaser[6].setPosition(posxForm + sizeForm*10 + 5.0*sizeForm*20/24.0, posyForm + sizeForm*4); spriteLaser[6].setRotation(90);
        spriteLaser[7].setPosition(posxForm + sizeForm*10 + 7.0*sizeForm*20/24.0, posyForm + sizeForm*4); spriteLaser[7].setRotation(90);
        countLaser = 0;
    }
    if (watchForm > 165)
    {
        scale = scaleMax*(180.0 - watchForm)/15.0;
        spriteInnerTriangle.setScale(scale, scale);
        spriteInnerTriangle.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*5);
        spriteOuterTriangle.setScale(scale, scale);
        spriteOuterTriangle.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*5);
        
    }
    else if (watchForm > 85)
    {
        if (watchForm % 20 == 0 && countLaser < 4)
        {
            slotLaser[countLaser] = powerForm * 20;
            slotLaser[7 - countLaser] = powerForm * 20;
            countLaser = countLaser + 1;
        }
    }
    else if (watchForm < 15)
    {
        scale = scaleMax*watchForm/double(timeApparitionForm);
        spriteInnerTriangle.setScale(scale, scale);
        spriteInnerTriangle.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*5);
        spriteOuterTriangle.setScale(scale, scale);
        spriteOuterTriangle.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*5);
    }
    for (int i = 0; i < 8; i++)
    {
        if (slotLaser[i] > 20*powerForm - 5)
        {
            spriteLaser[i].setScale((20*powerForm - slotLaser[i])/5.0, (20*powerForm - slotLaser[i])/5.0);
        }
        else if (slotLaser[i] < 5)
        {
            spriteLaser[i].setScale(slotLaser[i]/5.0, slotLaser[i]/5.0);
        }
        if (slotLaser[i])
        {
            slotLaser[i] = slotLaser[i] - 1;   
        }
    }
    watchForm = watchForm - 1;
    if (!watchForm)
    {
        for (int i = 0; i < 16; i++)
        {
            slotLaser[i] = 0;
        }
        countLaser = 0;
    }
}

void Player::animationTriangleLeft() //The triangle have a maximum height of 33
{
    double scaleMax = double(sizeForm)/33.0;
    double scale;
    double scaleLaser;
    if (watchForm == 180)
    {
        powerForm = 2;
        spriteInnerTriangle.setRotation(180);
        spriteOuterTriangle.setRotation(180);
        /*
        spriteLaser[0].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 - 105); spriteLaser[0].setRotation(0);
        spriteLaser[1].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 - 75); spriteLaser[1].setRotation(0);
        spriteLaser[2].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 - 45); spriteLaser[2].setRotation(0);
        spriteLaser[3].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 - 15); spriteLaser[3].setRotation(0);     
        spriteLaser[4].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 + 15); spriteLaser[4].setRotation(0);
        spriteLaser[5].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 + 45); spriteLaser[5].setRotation(0);
        spriteLaser[6].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 + 75); spriteLaser[6].setRotation(0);
        spriteLaser[7].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 + 105); spriteLaser[7].setRotation(0);
        */
        spriteLaser[0].setPosition(posxForm + sizeForm*6, posyForm + sizeForm*10 - 7.0*sizeForm*20/24.0); spriteLaser[0].setRotation(180);
        spriteLaser[1].setPosition(posxForm + sizeForm*6, posyForm + sizeForm*10 - 5.0*sizeForm*20/24.0); spriteLaser[1].setRotation(180);
        spriteLaser[2].setPosition(posxForm + sizeForm*6, posyForm + sizeForm*10 - 3.0*sizeForm*20/24.0); spriteLaser[2].setRotation(180);
        spriteLaser[3].setPosition(posxForm + sizeForm*6, posyForm + sizeForm*10 - sizeForm*20/24.0); spriteLaser[3].setRotation(180);     
        spriteLaser[4].setPosition(posxForm + sizeForm*6, posyForm + sizeForm*10 + sizeForm*20/24.0); spriteLaser[4].setRotation(180);
        spriteLaser[5].setPosition(posxForm + sizeForm*6, posyForm + sizeForm*10 + 3.0*sizeForm*20/24.0); spriteLaser[5].setRotation(180);
        spriteLaser[6].setPosition(posxForm + sizeForm*6, posyForm + sizeForm*10 + 5.0*sizeForm*20/24.0); spriteLaser[6].setRotation(180);
        spriteLaser[7].setPosition(posxForm + sizeForm*6, posyForm + sizeForm*10 + 7.0*sizeForm*20/24.0); spriteLaser[7].setRotation(180);
        countLaser = 0;
    }
    if (watchForm > 165)
    {
        scale = scaleMax*(180.0 - watchForm)/15.0;
        spriteInnerTriangle.setScale(scale, scale);
        spriteInnerTriangle.setPosition(posxForm + sizeForm*5, posyForm + sizeForm*10);
        spriteOuterTriangle.setScale(scale, scale);
        spriteOuterTriangle.setPosition(posxForm + sizeForm*5, posyForm + sizeForm*10);
        
    }
    else if (watchForm > 85)
    {
        if (watchForm % 20 == 0 && countLaser < 4)
        {
            slotLaser[countLaser] = powerForm * 20;
            slotLaser[7 - countLaser] = powerForm * 20;
            countLaser = countLaser + 1;
        }
    }
    else if (watchForm < 15)
    {
        scale = scaleMax*watchForm/double(timeApparitionForm);
        spriteInnerTriangle.setScale(scale, scale);
        spriteInnerTriangle.setPosition(posxForm + sizeForm*5, posyForm + sizeForm*10);
        spriteOuterTriangle.setScale(scale, scale);
        spriteOuterTriangle.setPosition(posxForm + sizeForm*5, posyForm + sizeForm*10);
    }
    for (int i = 0; i < 8; i++)
    {
        if (slotLaser[i] > 20*powerForm - 5)
        {
            spriteLaser[i].setScale((20*powerForm - slotLaser[i])/5.0, (20*powerForm - slotLaser[i])/5.0);
        }
        else if (slotLaser[i] < 5)
        {
            spriteLaser[i].setScale(slotLaser[i]/5.0, slotLaser[i]/5.0);
        }
        if (slotLaser[i])
        {
            slotLaser[i] = slotLaser[i] - 1;   
        }
    }
    watchForm = watchForm - 1;
    if (!watchForm)
    {
        for (int i = 0; i < 16; i++)
        {
            slotLaser[i] = 0;
        }
        countLaser = 0;
    }
}

void Player::animationTriangleUp() //The triangle have a maximum size of 33
{
    double scaleMax = double(sizeForm)/33.0;
    double scale;
    double scaleLaser;
    if (watchForm == 180)
    {
        powerForm = 2;
        spriteInnerTriangle.setRotation(270);
        spriteOuterTriangle.setRotation(270);
        /*
        spriteLaser[0].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 - 105); spriteLaser[0].setRotation(0);
        spriteLaser[1].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 - 75); spriteLaser[1].setRotation(0);
        spriteLaser[2].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 - 45); spriteLaser[2].setRotation(0);
        spriteLaser[3].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 - 15); spriteLaser[3].setRotation(0);     
        spriteLaser[4].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 + 15); spriteLaser[4].setRotation(0);
        spriteLaser[5].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 + 45); spriteLaser[5].setRotation(0);
        spriteLaser[6].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 + 75); spriteLaser[6].setRotation(0);
        spriteLaser[7].setPosition(posxForm + sizeForm*10 - 120, posyForm + sizeForm*10 + 105); spriteLaser[7].setRotation(0);
        */
        spriteLaser[0].setPosition(posxForm + sizeForm*10 - 7.0*sizeForm*20/24.0, posyForm + sizeForm*6); spriteLaser[0].setRotation(270);
        spriteLaser[1].setPosition(posxForm + sizeForm*10 - 5.0*sizeForm*20/24.0, posyForm + sizeForm*6); spriteLaser[1].setRotation(270);
        spriteLaser[2].setPosition(posxForm + sizeForm*10 - 3.0*sizeForm*20/24.0, posyForm + sizeForm*6); spriteLaser[2].setRotation(270);
        spriteLaser[3].setPosition(posxForm + sizeForm*10 - sizeForm*20/24.0, posyForm + sizeForm*6); spriteLaser[3].setRotation(270);     
        spriteLaser[4].setPosition(posxForm + sizeForm*10 + sizeForm*20/24.0, posyForm + sizeForm*6); spriteLaser[4].setRotation(270);
        spriteLaser[5].setPosition(posxForm + sizeForm*10 + 3.0*sizeForm*20/24.0, posyForm + sizeForm*6); spriteLaser[5].setRotation(270);
        spriteLaser[6].setPosition(posxForm + sizeForm*10 + 5.0*sizeForm*20/24.0, posyForm + sizeForm*6); spriteLaser[6].setRotation(270);
        spriteLaser[7].setPosition(posxForm + sizeForm*10 + 7.0*sizeForm*20/24.0, posyForm + sizeForm*6); spriteLaser[7].setRotation(270);
        countLaser = 0;
    }
    if (watchForm > 165)
    {
        scale = scaleMax*(180.0 - watchForm)/15.0;
        spriteInnerTriangle.setScale(scale, scale);
        spriteInnerTriangle.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*5);
        spriteOuterTriangle.setScale(scale, scale);
        spriteOuterTriangle.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*5);
        
    }
    else if (watchForm > 85)
    {
        if (watchForm % 20 == 0 && countLaser < 4)
        {
            slotLaser[countLaser] = powerForm * 20;
            slotLaser[7 - countLaser] = powerForm * 20;
            countLaser = countLaser + 1;
        }
    }
    else if (watchForm < 15)
    {
        scale = scaleMax*watchForm/double(timeApparitionForm);
        spriteInnerTriangle.setScale(scale, scale);
        spriteInnerTriangle.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*5);
        spriteOuterTriangle.setScale(scale, scale);
        spriteOuterTriangle.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*5);
    }
    for (int i = 0; i < 8; i++)
    {
        if (slotLaser[i] > 20*powerForm - 5)
        {
            spriteLaser[i].setScale((20*powerForm - slotLaser[i])/5.0, (20*powerForm - slotLaser[i])/5.0);
        }
        else if (slotLaser[i] < 5)
        {
            spriteLaser[i].setScale(slotLaser[i]/5.0, slotLaser[i]/5.0);
        }
        if (slotLaser[i])
        {
            slotLaser[i] = slotLaser[i] - 1;   
        }
    }
    watchForm = watchForm - 1;
    if (!watchForm)
    {
        for (int i = 0; i < 16; i++)
        {
            slotLaser[i] = 0;
        }
        countLaser = 0;
    }
}

void Player::animationCircle() //The circle have a maximum size of 25
{
    double scaleMax = double(sizeForm)/25.0;
    double scale;
    if (watchForm > 165)
    {
        scale = scaleMax*(180.0 - watchForm)/double(timeApparitionForm);
        spriteInnerCircle.setScale(scale, scale);
        spriteInnerCircle.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*10);
        spriteOuterCircle.setScale(scale, scale);
        spriteOuterCircle.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*10);
    }
    else if (watchForm < 15)
    {
        scale = scaleMax*watchForm/double(timeApparitionForm);
        spriteInnerCircle.setScale(scale, scale);
        spriteInnerCircle.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*10);
        spriteOuterCircle.setScale(scale, scale);
        spriteOuterCircle.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*10);
    }
    watchForm = watchForm - 1;
}

void Player::animationSquareInclined()
{
    double scaleMax = double(sizeForm)/25.0;
    double scale;
    if (watchForm == 180)
    {
        powerForm = 4;
        spriteInnerSquare.setRotation(45);
        spriteOuterSquare.setRotation(45);
        for (int i = 0; i < 16; i++)
        {
            slotLaser[i] = 0;  
        }
        if (powerForm == 1)
        {
            spriteLaser[0].setPosition(posxForm + sizeForm*15 - 70, posyForm + sizeForm*15 - 70); spriteLaser[0].setRotation(45);
            spriteLaser[1].setPosition(posxForm + sizeForm*5 + 70, posyForm + sizeForm*15 - 70); spriteLaser[1].setRotation(135);
            spriteLaser[2].setPosition(posxForm + sizeForm*5 + 70, posyForm + sizeForm*5 + 70); spriteLaser[2].setRotation(225);
            spriteLaser[3].setPosition(posxForm + sizeForm*15 - 70, posyForm + sizeForm*5 + 70); spriteLaser[3].setRotation(315);
        }
        else if (powerForm == 2)
        {
            spriteLaser[0].setPosition(posxForm + sizeForm*15 - 70 + 28, posyForm + sizeForm*15 - 70 - 28); spriteLaser[0].setRotation(45);
            spriteLaser[1].setPosition(posxForm + sizeForm*5 + 70 - 28, posyForm + sizeForm*15 - 70 - 28); spriteLaser[1].setRotation(135);
            spriteLaser[2].setPosition(posxForm + sizeForm*5 + 70 - 28, posyForm + sizeForm*5 + 70 + 28); spriteLaser[2].setRotation(225);
            spriteLaser[3].setPosition(posxForm + sizeForm*15 - 70 - 28, posyForm + sizeForm*5 + 70 - 28); spriteLaser[3].setRotation(315);
            
            spriteLaser[4].setPosition(posxForm + sizeForm*15 - 70 - 28.0*sizeForm/10.0, posyForm + sizeForm*15 - 70 + 28.0*sizeForm/10.0); spriteLaser[4].setRotation(45);
            spriteLaser[5].setPosition(posxForm + sizeForm*5 + 70 + 28.0*sizeForm/10.0, posyForm + sizeForm*15 - 70 + 28.0*sizeForm/10.0); spriteLaser[5].setRotation(135);
            spriteLaser[6].setPosition(posxForm + sizeForm*5 + 70 + 28.0*sizeForm/10.0, posyForm + sizeForm*5 + 70 - 28.0*sizeForm/10.0); spriteLaser[6].setRotation(225);
            spriteLaser[7].setPosition(posxForm + sizeForm*15 - 70 + 28.0*sizeForm/10.0, posyForm + sizeForm*5 + 70 + 28.0*sizeForm/10.0); spriteLaser[7].setRotation(315);
        }
        else if (powerForm == 3)
        {
            spriteLaser[0].setPosition(posxForm + sizeForm*15 - 70 + 28.0*sizeForm/10.0, posyForm + sizeForm*15 - 70 - 28.0*sizeForm/10.0); spriteLaser[0].setRotation(45);
            spriteLaser[1].setPosition(posxForm + sizeForm*5 + 70 - 28.0*sizeForm/10.0, posyForm + sizeForm*15 - 70 - 28.0*sizeForm/10.0); spriteLaser[1].setRotation(135);
            spriteLaser[2].setPosition(posxForm + sizeForm*5 + 70 - 28.0*sizeForm/10.0, posyForm + sizeForm*5 + 70 + 28.0*sizeForm/10.0); spriteLaser[2].setRotation(225);
            spriteLaser[3].setPosition(posxForm + sizeForm*15 - 70 - 28.0*sizeForm/10.0, posyForm + sizeForm*5 + 70 - 28.0*sizeForm/10.0); spriteLaser[3].setRotation(315);
            
            spriteLaser[4].setPosition(posxForm + sizeForm*15 - 70, posyForm + sizeForm*15 - 70); spriteLaser[4].setRotation(45);
            spriteLaser[5].setPosition(posxForm + sizeForm*5 + 70, posyForm + sizeForm*15 - 70); spriteLaser[5].setRotation(135);
            spriteLaser[6].setPosition(posxForm + sizeForm*5 + 70, posyForm + sizeForm*5 + 70); spriteLaser[6].setRotation(225);
            spriteLaser[7].setPosition(posxForm + sizeForm*15 - 70, posyForm + sizeForm*5 + 70); spriteLaser[7].setRotation(315);
            
            spriteLaser[8].setPosition(posxForm + sizeForm*15 - 70 - 28.0*sizeForm/10.0, posyForm + sizeForm*15 - 70 + 28.0*sizeForm/10.0); spriteLaser[8].setRotation(45);
            spriteLaser[9].setPosition(posxForm + sizeForm*5 + 70 + 28.0*sizeForm/10.0, posyForm + sizeForm*15 - 70 + 28.0*sizeForm/10.0); spriteLaser[9].setRotation(135);
            spriteLaser[10].setPosition(posxForm + sizeForm*5 + 70 + 28.0*sizeForm/10.0, posyForm + sizeForm*5 + 70 - 28.0*sizeForm/10.0); spriteLaser[10].setRotation(225);
            spriteLaser[11].setPosition(posxForm + sizeForm*15 - 70 + 28.0*sizeForm/10.0, posyForm + sizeForm*5 + 70 + 28.0*sizeForm/10.0); spriteLaser[11].setRotation(315);
        }
        else
        {
            spriteLaser[0].setPosition(posxForm + sizeForm*15 - 70 + 30.0*sizeForm/10.0, posyForm + sizeForm*15 - 70 - 30.0*sizeForm/10.0); spriteLaser[0].setRotation(45);
            spriteLaser[1].setPosition(posxForm + sizeForm*5 + 70 - 30.0*sizeForm/10.0, posyForm + sizeForm*15 - 70 - 30.0*sizeForm/10.0); spriteLaser[1].setRotation(135);
            spriteLaser[2].setPosition(posxForm + sizeForm*5 + 70 - 30.0*sizeForm/10.0, posyForm + sizeForm*5 + 70 + 30.0*sizeForm/10.0); spriteLaser[2].setRotation(225);
            spriteLaser[3].setPosition(posxForm + sizeForm*15 - 70 - 30.0*sizeForm/10.0, posyForm + sizeForm*5 + 70 - 30.0*sizeForm/10.0); spriteLaser[3].setRotation(315);
            
            spriteLaser[4].setPosition(posxForm + sizeForm*15 - 70 + 10.0*sizeForm/10.0, posyForm + sizeForm*15 - 70 - 10.0*sizeForm/10.0); spriteLaser[4].setRotation(45);
            spriteLaser[5].setPosition(posxForm + sizeForm*5 + 70 - 10.0*sizeForm/10.0, posyForm + sizeForm*15 - 70 - 10.0*sizeForm/10.0); spriteLaser[5].setRotation(135);
            spriteLaser[6].setPosition(posxForm + sizeForm*5 + 70 - 10.0*sizeForm/10.0, posyForm + sizeForm*5 + 70 + 10.0*sizeForm/10.0); spriteLaser[6].setRotation(225);
            spriteLaser[7].setPosition(posxForm + sizeForm*15 - 70 - 10.0*sizeForm/10.0, posyForm + sizeForm*5 + 70 - 10.0*sizeForm/10.0); spriteLaser[7].setRotation(315);
            
            spriteLaser[8].setPosition(posxForm + sizeForm*15 - 70 - 10.0*sizeForm/10.0, posyForm + sizeForm*15 - 70 + 10.0*sizeForm/10.0); spriteLaser[8].setRotation(45);
            spriteLaser[9].setPosition(posxForm + sizeForm*5 + 70 + 10.0*sizeForm/10.0, posyForm + sizeForm*15 - 70 + 10.0*sizeForm/10.0); spriteLaser[9].setRotation(135);
            spriteLaser[10].setPosition(posxForm + sizeForm*5 + 70 + 10.0*sizeForm/10.0, posyForm + sizeForm*5 + 70 - 10.0*sizeForm/10.0); spriteLaser[10].setRotation(225);
            spriteLaser[11].setPosition(posxForm + sizeForm*15 - 70 + 10.0*sizeForm/10.0, posyForm + sizeForm*5 + 70 + 10.0*sizeForm/10.0); spriteLaser[11].setRotation(315);
            
            spriteLaser[12].setPosition(posxForm + sizeForm*15 - 70 - 30.0*sizeForm/10.0, posyForm + sizeForm*15 - 70 + 30.0*sizeForm/10.0); spriteLaser[12].setRotation(45);
            spriteLaser[13].setPosition(posxForm + sizeForm*5 + 70 + 30.0*sizeForm/10.0, posyForm + sizeForm*15 - 70 + 30.0*sizeForm/10.0); spriteLaser[13].setRotation(135);
            spriteLaser[14].setPosition(posxForm + sizeForm*5 + 70 + 30.0*sizeForm/10.0, posyForm + sizeForm*5 + 70 - 30.0*sizeForm/10.0); spriteLaser[14].setRotation(225);
            spriteLaser[15].setPosition(posxForm + sizeForm*15 - 70 + 30.0*sizeForm/10.0, posyForm + sizeForm*5 + 70 + 30.0*sizeForm/10.0); spriteLaser[15].setRotation(315);
        }
    }
    if (watchForm > 165)
    {
        scale = scaleMax*(180.0 - watchForm)/15.0;
        spriteInnerSquare.setScale(scale, scale);
        spriteInnerSquare.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*10);
        spriteOuterSquare.setScale(scale, scale);
        spriteOuterSquare.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*10);
        
    }
    else if (watchForm > 85)
    {
        if ((watchForm) % (20/powerForm) == 0 && countLaser < powerForm * 4)
        {
            countLaser = countLaser + 1;
            std::vector<int> vectorSlot;
            for (int i = 0; i < 4*powerForm; i++)
            {
                if (!slotLaser[i])
                {
                    vectorSlot.push_back(i);
                }
            }
            cursorLaser = aleaVectorInt(vectorSlot);
            slotLaser[cursorLaser] = 1;
            spriteLaser[cursorLaser].setScale(slotLaser[cursorLaser]/5.0, slotLaser[cursorLaser]/5.0);
        }
    }
    if (slotLaser[cursorLaser] < 5 && slotLaser[cursorLaser] && watchForm > 85)
    {
        slotLaser[cursorLaser] = slotLaser[cursorLaser] + 1;
        if (cursorLaser % 2 == 0)
        {
            spriteLaser[cursorLaser].setScale(slotLaser[cursorLaser]/5.0, slotLaser[cursorLaser]/5.0);
        }
        else
        {
            spriteLaser[cursorLaser].setScale(slotLaser[cursorLaser]/5.0, slotLaser[cursorLaser]/5.0);
        }
    }
    
    else if (watchForm > 15 && watchForm < 30)
    {
        for (int i = 0; i < 16; i++)
        {
            if (slotLaser[i])
            {
                slotLaser[i] = slotLaser[i] - 1;
                spriteLaser[i].setScale(slotLaser[i]/5.0, slotLaser[i]/5.0);
            }
        }
    }
    
    else if (watchForm < 15)
    {
        scale = scaleMax*watchForm/double(timeApparitionForm);
        spriteInnerSquare.setScale(scale, scale);
        spriteInnerSquare.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*10);
        spriteOuterSquare.setScale(scale, scale);
        spriteOuterSquare.setPosition(posxForm + sizeForm*10, posyForm + sizeForm*10);
    }
    watchForm = watchForm - 1;
    if (!watchForm)
    {
        for (int i = 0; i < 16; i++)
        {
            slotLaser[i] = 0;
        }
        countLaser = 0;
    }
}

void Player::animationTriangle45()
{
    double scaleMax = double(sizeForm)/33.0;
    double scale;
    if (watchForm == 180)
    {
        powerForm = 2;
        spriteInnerTriangle.setRotation(45);
        spriteOuterTriangle.setRotation(45);
        spriteLaser[0].setPosition(posxForm + sizeForm*10.5 - 7.0*sizeForm*20/36.0, posyForm + sizeForm*10.5 + 7.0*sizeForm*20/36.0); spriteLaser[0].setRotation(45);
        spriteLaser[1].setPosition(posxForm + sizeForm*10.5 - 5.0*sizeForm*20/36.0, posyForm + sizeForm*10.5 + 5.0*sizeForm*20/36.0); spriteLaser[1].setRotation(45);
        spriteLaser[2].setPosition(posxForm + sizeForm*10.5 - 3.0*sizeForm*20/36.0, posyForm + sizeForm*10.5 + 3.0*sizeForm*20/36.0); spriteLaser[2].setRotation(45);
        spriteLaser[3].setPosition(posxForm + sizeForm*10.5 - 1.0*sizeForm*20/36.0, posyForm + sizeForm*10.5 + 1.0*sizeForm*20/36.0); spriteLaser[3].setRotation(45);     
        spriteLaser[4].setPosition(posxForm + sizeForm*10.5 + 1.0*sizeForm*20/36.0, posyForm + sizeForm*10.5 - 1.0*sizeForm*20/36.0); spriteLaser[4].setRotation(45);
        spriteLaser[5].setPosition(posxForm + sizeForm*10.5 + 3.0*sizeForm*20/36.0, posyForm + sizeForm*10.5 - 3.0*sizeForm*20/36.0); spriteLaser[5].setRotation(45);
        spriteLaser[6].setPosition(posxForm + sizeForm*10.5 + 5.0*sizeForm*20/36.0, posyForm + sizeForm*10.5 - 5.0*sizeForm*20/36.0); spriteLaser[6].setRotation(45);
        spriteLaser[7].setPosition(posxForm + sizeForm*10.5 + 7.0*sizeForm*20/36.0, posyForm + sizeForm*10.5 - 7.0*sizeForm*20/36.0); spriteLaser[7].setRotation(45);
        countLaser = 0;
    }
    if (watchForm > 165)
    {
        scale = scaleMax*(180.0 - watchForm)/15.0;
        spriteInnerTriangle.setScale(scale*1.4, scale*1.4);
        spriteInnerTriangle.setPosition(posxForm + sizeForm*20*0.65, posyForm + sizeForm*20*0.65);
        spriteOuterTriangle.setScale(scale*1.4, scale*1.4);
        spriteOuterTriangle.setPosition(posxForm + sizeForm*20*0.65, posyForm + sizeForm*20*0.65);
        
    }
    else if (watchForm > 85)
    {
        if (watchForm % 20 == 0 && countLaser < 4)
        {
            slotLaser[countLaser] = powerForm * 20;
            slotLaser[7 - countLaser] = powerForm * 20;
            countLaser = countLaser + 1;
        }
    }
    else if (watchForm < 15)
    {
        scale = scaleMax*watchForm/double(timeApparitionForm);
        spriteInnerTriangle.setScale(scale*1.4, scale*1.4);
        //spriteInnerTriangle.setPosition(posxForm + sizeForm*20*0.25, posyForm + sizeForm*20*0.5);
        spriteInnerTriangle.setPosition(posxForm + sizeForm*20*0.65, posyForm + sizeForm*20*0.65);
        spriteOuterTriangle.setScale(scale*1.4, scale*1.4);
        //spriteOuterTriangle.setPosition(posxForm + sizeForm*20*0.25, posyForm + sizeForm*20*0.5);
        spriteOuterTriangle.setPosition(posxForm + sizeForm*20*0.65, posyForm + sizeForm*20*0.65);
    }
    for (int i = 0; i < 8; i++)
    {
        if (slotLaser[i] > 20*powerForm - 5)
        {
            spriteLaser[i].setScale((20*powerForm - slotLaser[i])/5.0, (20*powerForm - slotLaser[i])/5.0);
        }
        else if (slotLaser[i] < 5)
        {
            spriteLaser[i].setScale(slotLaser[i]/5.0, slotLaser[i]/5.0);
        }
        if (slotLaser[i])
        {
            slotLaser[i] = slotLaser[i] - 1;   
        }
    }
    watchForm = watchForm - 1;
    if (!watchForm)
    {
        for (int i = 0; i < 16; i++)
        {
            slotLaser[i] = 0;
        }
        countLaser = 0;
    }
}

void Player::animationTriangle135()
{
    double scaleMax = double(sizeForm)/33.0;
    double scale;
    if (watchForm == 180)
    {
        powerForm = 2;
        spriteInnerTriangle.setRotation(135);
        spriteOuterTriangle.setRotation(135);
        spriteLaser[0].setPosition(posxForm + sizeForm*7.5 - 7.0*sizeForm*20/36.0, posyForm + sizeForm*10.5 - 7.0*sizeForm*20/36.0); spriteLaser[0].setRotation(135);
        spriteLaser[1].setPosition(posxForm + sizeForm*7.5 - 5.0*sizeForm*20/36.0, posyForm + sizeForm*10.5 - 5.0*sizeForm*20/36.0); spriteLaser[1].setRotation(135);
        spriteLaser[2].setPosition(posxForm + sizeForm*7.5 - 3.0*sizeForm*20/36.0, posyForm + sizeForm*10.5 - 3.0*sizeForm*20/36.0); spriteLaser[2].setRotation(135);
        spriteLaser[3].setPosition(posxForm + sizeForm*7.5 - 1.0*sizeForm*20/36.0, posyForm + sizeForm*10.5 - 1.0*sizeForm*20/36.0); spriteLaser[3].setRotation(135);     
        spriteLaser[4].setPosition(posxForm + sizeForm*7.5 + 1.0*sizeForm*20/36.0, posyForm + sizeForm*10.5 + 1.0*sizeForm*20/36.0); spriteLaser[4].setRotation(135);
        spriteLaser[5].setPosition(posxForm + sizeForm*7.5 + 3.0*sizeForm*20/36.0, posyForm + sizeForm*10.5 + 3.0*sizeForm*20/36.0); spriteLaser[5].setRotation(135);
        spriteLaser[6].setPosition(posxForm + sizeForm*7.5 + 5.0*sizeForm*20/36.0, posyForm + sizeForm*10.5 + 5.0*sizeForm*20/36.0); spriteLaser[6].setRotation(135);
        spriteLaser[7].setPosition(posxForm + sizeForm*7.5 + 7.0*sizeForm*20/36.0, posyForm + sizeForm*10.5 + 7.0*sizeForm*20/36.0); spriteLaser[7].setRotation(135);
        countLaser = 0;
    }
    if (watchForm > 165)
    {
        scale = scaleMax*(180.0 - watchForm)/15.0;
        spriteInnerTriangle.setScale(scale*1.4, scale*1.4);
        spriteInnerTriangle.setPosition(posxForm + sizeForm*20*0.25, posyForm + sizeForm*20*0.65);
        spriteOuterTriangle.setScale(scale*1.4, scale*1.4);
        spriteOuterTriangle.setPosition(posxForm + sizeForm*20*0.25, posyForm + sizeForm*20*0.65);
        
    }
    else if (watchForm > 85)
    {
        if (watchForm % 20 == 0 && countLaser < 4)
        {
            slotLaser[countLaser] = powerForm * 20;
            slotLaser[7 - countLaser] = powerForm * 20;
            countLaser = countLaser + 1;
        }
    }
    else if (watchForm < 15)
    {
        scale = scaleMax*watchForm/double(timeApparitionForm);
        spriteInnerTriangle.setScale(scale*1.4, scale*1.4);
        spriteInnerTriangle.setPosition(posxForm + sizeForm*20*0.25, posyForm + sizeForm*20*0.65);
        spriteOuterTriangle.setScale(scale*1.4, scale*1.4);
        spriteOuterTriangle.setPosition(posxForm + sizeForm*20*0.25, posyForm + sizeForm*20*0.65);
    }
    for (int i = 0; i < 8; i++)
    {
        if (slotLaser[i] > 20*powerForm - 5)
        {
            spriteLaser[i].setScale((20*powerForm - slotLaser[i])/5.0, (20*powerForm - slotLaser[i])/5.0);
        }
        else if (slotLaser[i] < 5)
        {
            spriteLaser[i].setScale(slotLaser[i]/5.0, slotLaser[i]/5.0);
        }
        if (slotLaser[i])
        {
            slotLaser[i] = slotLaser[i] - 1;   
        }
    }
    watchForm = watchForm - 1;
    if (!watchForm)
    {
        for (int i = 0; i < 16; i++)
        {
            slotLaser[i] = 0;
        }
        countLaser = 0;
    }
}

void Player::animationTriangle225()
{
    double scaleMax = double(sizeForm)/33.0;
    double scale;
    if (watchForm == 180)
    {
        powerForm = 2;
        spriteInnerTriangle.setRotation(225);
        spriteOuterTriangle.setRotation(225);
        spriteLaser[0].setPosition(posxForm + sizeForm*5 - 7.0*sizeForm*20/36.0 + 4.0*sizeForm*20/36.0, posyForm + sizeForm*5 + 7.0*sizeForm*20/36.0 + 4.0*sizeForm*20/36.0); spriteLaser[0].setRotation(225);
        spriteLaser[1].setPosition(posxForm + sizeForm*5 - 5.0*sizeForm*20/36.0 + 4.0*sizeForm*20/36.0, posyForm + sizeForm*5 + 5.0*sizeForm*20/36.0 + 4.0*sizeForm*20/36.0); spriteLaser[1].setRotation(225);
        spriteLaser[2].setPosition(posxForm + sizeForm*5 - 3.0*sizeForm*20/36.0 + 4.0*sizeForm*20/36.0, posyForm + sizeForm*5 + 3.0*sizeForm*20/36.0 + 4.0*sizeForm*20/36.0); spriteLaser[2].setRotation(225);
        spriteLaser[3].setPosition(posxForm + sizeForm*5 - 1.0*sizeForm*20/36.0 + 4.0*sizeForm*20/36.0, posyForm + sizeForm*5 + 1.0*sizeForm*20/36.0 + 4.0*sizeForm*20/36.0); spriteLaser[3].setRotation(225);     
        spriteLaser[4].setPosition(posxForm + sizeForm*5 + 1.0*sizeForm*20/36.0 + 4.0*sizeForm*20/36.0, posyForm + sizeForm*5 - 1.0*sizeForm*20/36.0 + 4.0*sizeForm*20/36.0); spriteLaser[4].setRotation(225);
        spriteLaser[5].setPosition(posxForm + sizeForm*5 + 3.0*sizeForm*20/36.0 + 4.0*sizeForm*20/36.0, posyForm + sizeForm*5 - 3.0*sizeForm*20/36.0 + 4.0*sizeForm*20/36.0); spriteLaser[5].setRotation(225);
        spriteLaser[6].setPosition(posxForm + sizeForm*5 + 5.0*sizeForm*20/36.0 + 4.0*sizeForm*20/36.0, posyForm + sizeForm*5 - 5.0*sizeForm*20/36.0 + 4.0*sizeForm*20/36.0); spriteLaser[6].setRotation(225);
        spriteLaser[7].setPosition(posxForm + sizeForm*5 + 7.0*sizeForm*20/36.0 + 4.0*sizeForm*20/36.0, posyForm + sizeForm*5 - 7.0*sizeForm*20/36.0 + 4.0*sizeForm*20/36.0); spriteLaser[7].setRotation(225);
        countLaser = 0;
    }
    if (watchForm > 165)
    {
        scale = scaleMax*(180.0 - watchForm)/15.0;
        spriteInnerTriangle.setScale(scale*1.4, scale*1.4);
        spriteInnerTriangle.setPosition(posxForm + sizeForm*20*0.25, posyForm + sizeForm*20*0.25);
        spriteOuterTriangle.setScale(scale*1.4, scale*1.4);
        spriteOuterTriangle.setPosition(posxForm + sizeForm*20*0.25, posyForm + sizeForm*20*0.25);
        
    }
    else if (watchForm > 85)
    {
        if (watchForm % 20 == 0 && countLaser < 4)
        {
            slotLaser[countLaser] = powerForm * 20;
            slotLaser[7 - countLaser] = powerForm * 20;
            countLaser = countLaser + 1;
        }
    }
    else if (watchForm < 15)
    {
        scale = scaleMax*watchForm/double(timeApparitionForm);
        spriteInnerTriangle.setScale(scale*1.4, scale*1.4);
        spriteInnerTriangle.setPosition(posxForm + sizeForm*20*0.25, posyForm + sizeForm*20*0.25);
        spriteOuterTriangle.setScale(scale*1.4, scale*1.4);
        spriteOuterTriangle.setPosition(posxForm + sizeForm*20*0.25, posyForm + sizeForm*20*0.25);
    }
    for (int i = 0; i < 8; i++)
    {
        if (slotLaser[i] > 20*powerForm - 5)
        {
            spriteLaser[i].setScale((20*powerForm - slotLaser[i])/5.0, (20*powerForm - slotLaser[i])/5.0);
        }
        else if (slotLaser[i] < 5)
        {
            spriteLaser[i].setScale(slotLaser[i]/5.0, slotLaser[i]/5.0);
        }
        if (slotLaser[i])
        {
            slotLaser[i] = slotLaser[i] - 1;   
        }
    }
    watchForm = watchForm - 1;
    if (!watchForm)
    {
        for (int i = 0; i < 16; i++)
        {
            slotLaser[i] = 0;
        }
        countLaser = 0;
    }
}

void Player::animationTriangle315()
{
    double scaleMax = double(sizeForm)/33.0;
    double scale;
    if (watchForm == 180)
    {
        powerForm = 2;
        spriteInnerTriangle.setRotation(315);
        spriteOuterTriangle.setRotation(315);
        spriteLaser[0].setPosition(posxForm + sizeForm*10.5 - 7.0*sizeForm*20/36.0, posyForm + sizeForm*7.5 - 7.0*sizeForm*20/36.0); spriteLaser[0].setRotation(315);
        spriteLaser[1].setPosition(posxForm + sizeForm*10.5 - 5.0*sizeForm*20/36.0, posyForm + sizeForm*7.5 - 5.0*sizeForm*20/36.0); spriteLaser[1].setRotation(315);
        spriteLaser[2].setPosition(posxForm + sizeForm*10.5 - 3.0*sizeForm*20/36.0, posyForm + sizeForm*7.5 - 3.0*sizeForm*20/36.0); spriteLaser[2].setRotation(315);
        spriteLaser[3].setPosition(posxForm + sizeForm*10.5 - 1.0*sizeForm*20/36.0, posyForm + sizeForm*7.5 - 1.0*sizeForm*20/36.0); spriteLaser[3].setRotation(315);     
        spriteLaser[4].setPosition(posxForm + sizeForm*10.5 + 1.0*sizeForm*20/36.0, posyForm + sizeForm*7.5 + 1.0*sizeForm*20/36.0); spriteLaser[4].setRotation(315);
        spriteLaser[5].setPosition(posxForm + sizeForm*10.5 + 3.0*sizeForm*20/36.0, posyForm + sizeForm*7.5 + 3.0*sizeForm*20/36.0); spriteLaser[5].setRotation(315);
        spriteLaser[6].setPosition(posxForm + sizeForm*10.5 + 5.0*sizeForm*20/36.0, posyForm + sizeForm*7.5 + 5.0*sizeForm*20/36.0); spriteLaser[6].setRotation(315);
        spriteLaser[7].setPosition(posxForm + sizeForm*10.5 + 7.0*sizeForm*20/36.0, posyForm + sizeForm*7.5 + 7.0*sizeForm*20/36.0); spriteLaser[7].setRotation(315);
        countLaser = 0;
    }
    if (watchForm > 165)
    {
        scale = scaleMax*(180.0 - watchForm)/15.0;
        spriteInnerTriangle.setScale(scale*1.4, scale*1.4);
        spriteInnerTriangle.setPosition(posxForm + sizeForm*20*0.65, posyForm + sizeForm*20*0.25);
        spriteOuterTriangle.setScale(scale*1.4, scale*1.4);
        spriteOuterTriangle.setPosition(posxForm + sizeForm*20*0.65, posyForm + sizeForm*20*0.25);
        
    }
    else if (watchForm > 85)
    {
        if (watchForm % 20 == 0 && countLaser < 4)
        {
            slotLaser[countLaser] = powerForm * 20;
            slotLaser[7 - countLaser] = powerForm * 20;
            countLaser = countLaser + 1;
        }
    }
    else if (watchForm < 15)
    {
        scale = scaleMax*watchForm/double(timeApparitionForm);
        spriteInnerTriangle.setScale(scale*1.4, scale*1.4);
        spriteInnerTriangle.setPosition(posxForm + sizeForm*20*0.65, posyForm + sizeForm*20*0.25);
        spriteOuterTriangle.setScale(scale*1.4, scale*1.4);
        spriteOuterTriangle.setPosition(posxForm + sizeForm*20*0.65, posyForm + sizeForm*20*0.25);
    }
    for (int i = 0; i < 8; i++)
    {
        if (slotLaser[i] > 20*powerForm - 5)
        {
            spriteLaser[i].setScale((20*powerForm - slotLaser[i])/5.0, (20*powerForm - slotLaser[i])/5.0);
        }
        else if (slotLaser[i] < 5)
        {
            spriteLaser[i].setScale(slotLaser[i]/5.0, slotLaser[i]/5.0);
        }
        if (slotLaser[i])
        {
            slotLaser[i] = slotLaser[i] - 1;   
        }
    }
    watchForm = watchForm - 1;
    if (!watchForm)
    {
        for (int i = 0; i < 16; i++)
        {
            slotLaser[i] = 0;
        }
        countLaser = 0;
    }
}