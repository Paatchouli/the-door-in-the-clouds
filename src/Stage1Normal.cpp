/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Stage1Normal.h"
#include "../include/Ennemi.h"
#include "../include/Window.h"
#include "../include/Engine.h"
#include "../include/Cloud.h"
#include "../include/Sound.h"
#include "../include/Dialogue.h"
#include "../include/func.h"
#include <vector>
#include <cmath>
#include <SFML/Graphics.hpp>
//#include "../include/SFML/Graphics.hpp"
#include <iostream>

extern Window window;
extern std::vector<Ennemi> vectorEnnemi;
extern Engine engine;
extern Cloud cloud;
extern Ennemi arrayEnnemi [NB_ENNEMI];

Stage1Normal stage1Normal;

Stage1Normal::Stage1Normal()
{
    watch = 0;
    midBossDeafeted = false;
    arrayLifeInit[0] = 0; arrayLifeFina[0] = 100; arrayWatch[0] = -2;
    arrayLifeInit[1] = 0; arrayLifeFina[1] = 100; arrayWatch[1] = 1200;
    arrayLifeInit[2] = 0; arrayLifeFina[2] = 100; arrayWatch[2] = 2400;
    
    arrayLifeInit[3] = 0; arrayLifeFina[3] = 100; arrayWatch[3] = -2;
    arrayLifeInit[4] = 0; arrayLifeFina[4] = 100; arrayWatch[4] = 1200;
    arrayLifeInit[5] = 0; arrayLifeFina[5] = 100; arrayWatch[5] = 2400;
    arrayLifeInit[6] = 0; arrayLifeFina[6] = 100; arrayWatch[6] = 3600;
    arrayLifeInit[7] = 0; arrayLifeFina[7] = 100; arrayWatch[7] = 4800;
    //arrayLifeInit[8] = 0; arrayLifeFina[8] = 100; arrayWatch[8] = 6000;
    //arrayLifeInit[9] = 0; arrayLifeFina[9] = 100; arrayWatch[9] = 7200;
}

Stage1Normal::~Stage1Normal()
{

}

int  Stage1Normal::update()
{
    Sound::reset();
    watch = 0;
    indexAttack = 0;
    Sound::watch = watch;
    cloud.reset(-1.0, 0.0, 200, 500, 50, 100, 0.005);
    window.stageDifficulty = 2;
    Sound::musicMain.stop();
    Sound::musicStage1.play();
    Sound::musicStage1.setPlayingOffset(sf::milliseconds(int(1000.0*double(Sound::watch)/60.0)));
    
    Ennemi::indexAttack = 0;
    Ennemi::vectorLifeInit.resize(8); Ennemi::vectorLifeFina.resize(8); Ennemi::vectorWatch.resize(8);
    Ennemi::vectorLifeInit[0] = 300; Ennemi::vectorLifeFina[0] = 100; Ennemi::vectorWatch[0] = -1;
    Ennemi::vectorLifeInit[1] = 300; Ennemi::vectorLifeFina[1] = 100; Ennemi::vectorWatch[1] = 1200;
    Ennemi::vectorLifeInit[2] = 300; Ennemi::vectorLifeFina[2] = 100; Ennemi::vectorWatch[2] = 2400;
    
    Ennemi::vectorLifeInit[3] = 300; Ennemi::vectorLifeFina[3] = 100; Ennemi::vectorWatch[3] = -1;
    Ennemi::vectorLifeInit[4] = 300; Ennemi::vectorLifeFina[4] = 100; Ennemi::vectorWatch[4] = 1200;
    Ennemi::vectorLifeInit[5] = 300; Ennemi::vectorLifeFina[5] = 100; Ennemi::vectorWatch[5] = 2400;
    Ennemi::vectorLifeInit[6] = 300; Ennemi::vectorLifeFina[6] = 100; Ennemi::vectorWatch[6] = 3600;
    Ennemi::vectorLifeInit[7] = 300; Ennemi::vectorLifeFina[7] = 100; Ennemi::vectorWatch[7] = 4800;
    
    std::vector<std::vector<double> > newTrajectoryRD(3);
    std::vector<std::vector<double> > newTrajectoryRU(3);
    std::vector<std::vector<double> > newTrajectoryUL(3);
    std::vector<std::vector<double> > newTrajectoryDL(3);
    std::vector<std::vector<double> > newTrajectoryUR1(3);
    std::vector<std::vector<double> > newTrajectoryDR1(3);
    std::vector<std::vector<double> > newTrajectoryUR2(3);
    std::vector<std::vector<double> > newTrajectoryDR2(3);
    std::vector<std::vector<double> > newTrajectoryUR3(3);
    std::vector<std::vector<double> > newTrajectoryDR3(3);
    std::vector<std::vector<double> > newTrajectoryUR4(3);
    std::vector<std::vector<double> > newTrajectoryDR4(3);
    std::vector<std::vector<double> > newTrajectoryUD1(1, std::vector<double> (4));
    std::vector<std::vector<double> > newTrajectoryDU1(1, std::vector<double> (4));
    std::vector<std::vector<double> > newTrajectorySpec(2, std::vector<double> (4));
    
    std::vector<std::vector<double> > trajectoryJohan1(2);
    std::vector<std::vector<double> > newTrajectoryJohan0(3);
    std::vector<std::vector<double> > newTrajectoryJohan1(2);
    std::vector<std::vector<double> > trajectoryDialog(1);
    std::vector<std::vector<double> > trajectoryEnd(1);
    
    std::vector<std::vector<double> > trajectoryCircular1(1);
    std::vector<std::vector<double> > trajectoryCircular2(1);
    std::vector<std::vector<double> > trajectoryCircular3(3);
    std::vector<std::vector<double> > trajectoryCircular4(3);
    std::vector<std::vector<double> > trajectoryCircular5(3);
    std::vector<std::vector<double> > trajectoryCircular6(3);
    std::vector<std::vector<double> > trajectoryCircular7(3);
    std::vector<std::vector<double> > trajectoryCircular8(3);
    std::vector<std::vector<double> > trajectoryCircular9(3);
    std::vector<std::vector<double> > trajectoryCircular10(3);
    
    std::vector<std::vector<double> > newShot1(3);
    std::vector<std::vector<double> > newShot2(3);
    std::vector<std::vector<double> > newShot3(3);
    std::vector<std::vector<double> > newShot4(3);
    std::vector<std::vector<double> > newShot5(3);
    std::vector<std::vector<double> > newShot6(3);
    
    std::vector<std::vector<double> > newShot7(1);
    std::vector<std::vector<double> > newShot8(1);
    std::vector<std::vector<double> > newShot9(1);
    std::vector<std::vector<double> > newShot10(1);
    
    std::vector<std::vector<double> > newShot11(3);
    
    std::vector<std::vector<double> > shotJohan0(1);
    std::vector<std::vector<double> > shotJohan1(2);
    std::vector<std::vector<double> > shotJohan2(2);
    std::vector<std::vector<double> > shotJohan3(2);
    std::vector<std::vector<double> > shotJohan4(2);
    std::vector<std::vector<double> > shotJohan5(2);
    std::vector<std::vector<double> > shotJohan6(2);
    std::vector<std::vector<double> > newShotJohan0(5);
    std::vector<std::vector<double> > newShotJohan1(9);
    std::vector<std::vector<double> > shotDialog(1);
    
    newTrajectoryRD[0].resize(7);
    newTrajectoryRD[1].resize(7);
    newTrajectoryRD[2].resize(7);
    
    newTrajectoryRD[0][0] = 0;
    newTrajectoryRD[1][0] = 60;
    newTrajectoryRD[2][0] = 120;
    
    newTrajectoryRD[1][1] = 0;
    newTrajectoryRD[1][2] = 0;
    newTrajectoryRD[1][3] = 0;
    
    newTrajectoryRD[0][1] = 1;
    newTrajectoryRD[0][2] = -1;
    newTrajectoryRD[0][3] = 0;
    newTrajectoryRD[0][4] = 8;
    newTrajectoryRD[0][5] = 0;
    newTrajectoryRD[0][6] = 60;
    
    newTrajectoryRD[2][1] = 1;
    newTrajectoryRD[2][2] = 0;
    newTrajectoryRD[2][3] = 1;
    newTrajectoryRD[2][4] = 0;
    newTrajectoryRD[2][5] = 8;
    newTrajectoryRD[2][6] = 60;
    
    newTrajectoryRU = newTrajectoryRD;
    newTrajectoryRU[0][2] = -1;
    newTrajectoryRU[0][3] = 0;
    newTrajectoryRU[2][2] = 0;
    newTrajectoryRU[2][3] = -1;
    
    newTrajectoryUL = newTrajectoryRD;
    newTrajectoryUL[0][2] = 0;
    newTrajectoryUL[0][3] = 1;
    newTrajectoryUL[2][2] = -1;
    newTrajectoryUL[2][3] = 0;
    
    newTrajectoryDL = newTrajectoryRD;
    newTrajectoryDL[0][2] = 0;
    newTrajectoryDL[0][3] = -1;
    newTrajectoryDL[2][2] = -1;
    newTrajectoryDL[2][3] = 0;
    
    newTrajectoryUR1 = newTrajectoryRD;
    newTrajectoryUR1[2][0] = 240;
    newTrajectoryUR1[0][2] = 0;
    newTrajectoryUR1[0][3] = 1;
    newTrajectoryUR1[0][4] = 8;
    newTrajectoryUR1[2][2] = 1;
    newTrajectoryUR1[2][3] = 0;
    
    newTrajectoryUR2 = newTrajectoryUR1;
    newTrajectoryUR2[0][4] = 9;
    
    newTrajectoryUR3 = newTrajectoryUR1;
    newTrajectoryUR3[0][4] = 10;
    
    newTrajectoryUR4 = newTrajectoryUR1;
    newTrajectoryUR4[0][4] = 11;
    
    newTrajectoryDR1 = newTrajectoryUR1;
    newTrajectoryDR1[0][2] = 0;
    newTrajectoryDR1[0][3] = -1;
    newTrajectoryDR1[2][2] = 1;
    newTrajectoryDR1[2][3] = 0;
    
    newTrajectoryDR2 = newTrajectoryDR1;
    newTrajectoryDR2[0][4] = 9;
    
    newTrajectoryDR3 = newTrajectoryDR1;
    newTrajectoryDR3[0][4] = 10;
    
    newTrajectoryDR4 = newTrajectoryDR1;
    newTrajectoryDR4[0][4] = 11;
    
    newTrajectoryUD1[0][0] = 0;
    newTrajectoryUD1[0][1] = 0;
    newTrajectoryUD1[0][2] = 0;
    newTrajectoryUD1[0][3] = 8;
    
    newTrajectoryDU1 = newTrajectoryUD1;
    newTrajectoryDU1[0][3] = -8;
    
    newTrajectorySpec[0][0] = 0;
    newTrajectorySpec[0][1] = 0;
    newTrajectorySpec[0][2] = -8;
    newTrajectorySpec[0][3] = 0;
    newTrajectorySpec[1][0] = 60;
    newTrajectorySpec[1][1] = 0;
    newTrajectorySpec[1][2] = 0;
    newTrajectorySpec[1][3] = 0;
    
    trajectoryJohan1[0].resize(4);
    trajectoryJohan1[1].resize(12);
    trajectoryJohan1[0][0] = 0;
    trajectoryJohan1[0][1] = 0;
    trajectoryJohan1[0][2] = -8;
    trajectoryJohan1[0][3] = 0;
    trajectoryJohan1[1][0] = 90;
    trajectoryJohan1[1][1] = 2;
    trajectoryJohan1[1][2] = 180;
    trajectoryJohan1[1][3] = 30;
    trajectoryJohan1[1][4] = 30;
    trajectoryJohan1[1][5] = 0.2;
    trajectoryJohan1[1][6] = 1.0;
    trajectoryJohan1[1][7] = 0.0;
    trajectoryJohan1[1][8] = 1300;
    trajectoryJohan1[1][9] = 1690;
    trajectoryJohan1[1][10] = 350;
    trajectoryJohan1[1][11] = 850;
    
    newTrajectoryJohan0[0].resize(4);
    newTrajectoryJohan0[1].resize(12);
    newTrajectoryJohan0[2].resize(7);
    newTrajectoryJohan0[0][0] = 0;
    newTrajectoryJohan0[0][1] = 0;
    newTrajectoryJohan0[0][2] = -8;
    newTrajectoryJohan0[0][3] = 0;
    newTrajectoryJohan0[1][0] = 90;
    newTrajectoryJohan0[1][1] = 2;
    newTrajectoryJohan0[1][2] = 180;
    newTrajectoryJohan0[1][3] = 30;
    newTrajectoryJohan0[1][4] = 30;
    newTrajectoryJohan0[1][5] = 0.2;
    newTrajectoryJohan0[1][6] = 1.0;
    newTrajectoryJohan0[1][7] = 0.0;
    newTrajectoryJohan0[1][8] = 1300;
    newTrajectoryJohan0[1][9] = 1690;
    newTrajectoryJohan0[1][10] = 350;
    newTrajectoryJohan0[1][11] = 850;
    newTrajectoryJohan0[2][0] = Ennemi::vectorWatch[2];
    newTrajectoryJohan0[2][1] = 1;
    newTrajectoryJohan0[2][2] = -1;
    newTrajectoryJohan0[2][3] = 0;
    newTrajectoryJohan0[2][4] = 0;
    newTrajectoryJohan0[2][5] = 16;
    newTrajectoryJohan0[2][6] = 120;
    
    newTrajectoryJohan1[0].resize(12);
    newTrajectoryJohan1[1].resize(4);
    newTrajectoryJohan1[0][0] = 0;
    newTrajectoryJohan1[0][1] = 2;
    newTrajectoryJohan1[0][2] = 180;
    newTrajectoryJohan1[0][3] = 30;
    newTrajectoryJohan1[0][4] = 30;
    newTrajectoryJohan1[0][5] = 0.2;
    newTrajectoryJohan1[0][6] = 1.0;
    newTrajectoryJohan1[0][7] = 0.0;
    newTrajectoryJohan1[0][8] = 1300;
    newTrajectoryJohan1[0][9] = 1690;
    newTrajectoryJohan1[0][10] = 350;
    newTrajectoryJohan1[0][11] = 850;
    newTrajectoryJohan1[1][0] = Ennemi::vectorWatch[7];
    newTrajectoryJohan1[1][1] = 0;
    newTrajectoryJohan1[1][2] = 0;
    newTrajectoryJohan1[1][3] = 0;
    
    trajectoryDialog[0].resize(7);
    trajectoryDialog[0][0] = 0;
    trajectoryDialog[0][1] = 1;
    trajectoryDialog[0][2] = -1;
    trajectoryDialog[0][3] = 0;
    trajectoryDialog[0][4] = 16;
    trajectoryDialog[0][5] = 0;
    trajectoryDialog[0][6] = 120;
    
    trajectoryEnd[0].resize(4);
    trajectoryEnd[0][0] = 0;
    trajectoryEnd[0][1] = 0;
    trajectoryEnd[0][2] = 0;
    trajectoryEnd[0][3] = 0;
    
    trajectoryCircular1[0].resize(8);
    trajectoryCircular1[0][0] = 0.0;
    trajectoryCircular1[0][1] = 3.0;
    trajectoryCircular1[0][2] = 1890.0;
    trajectoryCircular1[0][3] = 600.0;
    trajectoryCircular1[0][4] = 400.0;
    trajectoryCircular1[0][5] = 300.0;
    trajectoryCircular1[0][6] = 75.0;
    trajectoryCircular1[0][7] = 1.0;
    
    trajectoryCircular2[0].resize(8);
    trajectoryCircular2[0][0] = 0.0;
    trajectoryCircular2[0][1] = 3.0;
    trajectoryCircular2[0][2] = 1890.0;
    trajectoryCircular2[0][3] = 600.0;
    trajectoryCircular2[0][4] = 350.0;
    trajectoryCircular2[0][5] = 300.0;
    trajectoryCircular2[0][6] = 225.0;
    trajectoryCircular2[0][7] = 0.0;
    
    trajectoryCircular3[0].resize(8);
    trajectoryCircular3[1].resize(4);
    trajectoryCircular3[2].resize(8);
    trajectoryCircular3[0][0] = 0;
    trajectoryCircular3[0][1] = 3;
    trajectoryCircular3[0][2] = 1190;
    trajectoryCircular3[0][3] = 150;
    trajectoryCircular3[0][4] = 180;
    trajectoryCircular3[0][5] = 240;
    trajectoryCircular3[0][6] = 120;
    trajectoryCircular3[0][7] = 1;
    trajectoryCircular3[1][0] = 60;
    trajectoryCircular3[1][1] = 0;
    trajectoryCircular3[1][2] = 0;
    trajectoryCircular3[1][3] = 0;
    trajectoryCircular3[2][0] = 120;
    trajectoryCircular3[2][1] = 3;
    trajectoryCircular3[2][2] = trajectoryCircular3[0][2];
    trajectoryCircular3[2][3] = trajectoryCircular3[0][3];
    trajectoryCircular3[2][4] = trajectoryCircular3[0][4];
    trajectoryCircular3[2][5] = trajectoryCircular3[0][5];
    trajectoryCircular3[2][6] = 60;
    trajectoryCircular3[2][7] = 1;
    
    trajectoryCircular4 = trajectoryCircular3;
    trajectoryCircular4[0][2] = 1190;
    trajectoryCircular4[0][3] = 1050;
    trajectoryCircular4[0][4] = 180;
    trajectoryCircular4[0][6] = 0;
    trajectoryCircular4[2][2] = trajectoryCircular4[0][2];
    trajectoryCircular4[2][3] = trajectoryCircular4[0][3];
    trajectoryCircular4[2][4] = trajectoryCircular4[0][4];
    trajectoryCircular4[2][6] = -60;
    
    trajectoryCircular5 = trajectoryCircular3;
    trajectoryCircular5[0][2] = 1890;
    trajectoryCircular5[0][3] = 510;
    trajectoryCircular5[0][4] = 180;
    trajectoryCircular5[0][6] = 60;
    trajectoryCircular5[2][2] = trajectoryCircular5[0][2];
    trajectoryCircular5[2][3] = trajectoryCircular5[0][3];
    trajectoryCircular5[2][4] = trajectoryCircular5[0][4];
    trajectoryCircular5[2][6] = 0;
    
    trajectoryCircular6 = trajectoryCircular3;
    trajectoryCircular6[0][2] = 1890;
    trajectoryCircular6[0][3] = 690;
    trajectoryCircular6[0][4] = 180;
    trajectoryCircular6[0][6] = 180;
    trajectoryCircular6[0][7] = 0;
    trajectoryCircular6[2][2] = trajectoryCircular6[0][2];
    trajectoryCircular6[2][3] = trajectoryCircular6[0][3];
    trajectoryCircular6[2][4] = trajectoryCircular6[0][4];
    trajectoryCircular6[2][6] = 0;
    trajectoryCircular6[2][7] = trajectoryCircular6[0][7];
    
    trajectoryCircular7 = trajectoryCircular3;
    trajectoryCircular7[0][4] = 130;
    trajectoryCircular7[2][4] = 130;
    
    trajectoryCircular8 = trajectoryCircular4;
    trajectoryCircular8[0][4] = 130;
    trajectoryCircular8[2][4] = 130;
    
    trajectoryCircular9 = trajectoryCircular5;
    trajectoryCircular9[0][4] = 130;
    trajectoryCircular9[2][4] = 130;
    
    trajectoryCircular10 = trajectoryCircular6;
    trajectoryCircular10[0][4] = 130;
    trajectoryCircular10[2][4] = 130;
    
    
    newShot1[0].resize(2);
    newShot1[1].resize(10);
    newShot1[2].resize(2);
    
    newShot1[0][0] = 0;
    newShot1[0][1] = 0;
    newShot1[2][0] = 120;
    newShot1[2][1] = 0;
    
    newShot1[1][0] = 60;
    newShot1[1][1] = 2;
    newShot1[1][2] = 30; //Fire rate
    newShot1[1][3] = 12; //Nb bullet angle
    newShot1[1][4] = 1; //Nb bullet speed
    newShot1[1][5] = 30; //Speed init
    newShot1[1][6] = 6; //Speed fina
    newShot1[1][7] = 5; //Duration var speed
    newShot1[1][8] = 0; //Aimed
    newShot1[1][9] = 11; //Bullet ID
    
    newShot2 = newShot1;
    newShot2[1][2] = 30;
    newShot2[1][3] = 1;
    newShot2[1][8] = 1;
    newShot2[1][9] = 42;
    newShot2[2] = newShot2[1];
    
    newShot3 = newShot1;
    newShot3[1].resize(16);
    newShot3[2][0] = 240;
    newShot3[1][0] = 60;
    newShot3[1][1] = 2;
    newShot3[1][2] = 90;
    newShot3[1][3] = 8;
    newShot3[1][4] = 3;
    newShot3[1][5] = 30;
    newShot3[1][6] = 6;
    newShot3[1][7] = 15;
    newShot3[1][8] = 30;
    newShot3[1][9] = 6;
    newShot3[1][10] = 10;
    newShot3[1][11] = 30;
    newShot3[1][12] = 6;
    newShot3[1][13] = 5;
    newShot3[1][14] = 0;
    newShot3[1][15] = 12;
    
    newShot4 = newShot2;
    newShot4[1][2] = 60;
    newShot4[1][9] = 23;
    newShot4[2] = newShot4[1];
    
    newShot5 = newShot4;
    newShot5[1][2] = 30;
    newShot5[2][2] = 30;
    
    newShot6 = newShot1;
    newShot6[1].resize(18);
    newShot6[1][0] = 60;
    newShot6[1][1] = 4;
    newShot6[1][2] = 60;
    newShot6[1][3] = 3.14/20.0;
    newShot6[1][4] = 6;
    newShot6[1][5] = 2;
    newShot6[1][6] = 3;
    newShot6[1][7] = 30;
    newShot6[1][8] = 6;
    newShot6[1][9] = 15;
    newShot6[1][10] = 30;
    newShot6[1][11] = 6;
    newShot6[1][12] = 10;
    newShot6[1][13] = 30;
    newShot6[1][14] = 6;
    newShot6[1][15] = 5;
    newShot6[1][16] = 0;
    newShot6[1][17] = 12;
    
    newShot7[0].resize(10);
    newShot7[0][0] = 0.0;
    newShot7[0][1] = 2.0;
    newShot7[0][2] = 30.0;
    newShot7[0][3] = 1.0;
    newShot7[0][4] = 1.0;
    newShot7[0][5] = 30.0;
    newShot7[0][6] = 6.0;
    newShot7[0][7] = 5.0;
    newShot7[0][8] = 1.0;
    newShot7[0][9] = 23.0;
    
    newShot8 = newShot7;
    newShot8[0][2] = 25.0;
    
    newShot9 = newShot7;
    newShot9[0][2] = 20.0;
    
    newShot10 = newShot7;
    newShot10[0][2] = 15.0;
    
    newShot11 = newShot1;
    newShot11[1].resize(18);
    newShot11[1][0] = 60;
    newShot11[1][1] = 9;
    newShot11[1][2] = 60;
    newShot11[1][3] = 50;
    newShot11[1][4] = 6;
    newShot11[1][5] = 2;
    newShot11[1][6] = 3;
    newShot11[1][7] = 30;
    newShot11[1][8] = 6;
    newShot11[1][9] = 15;
    newShot11[1][10] = 30;
    newShot11[1][11] = 6;
    newShot11[1][12] = 10;
    newShot11[1][13] = 30;
    newShot11[1][14] = 6;
    newShot11[1][15] = 5;
    newShot11[1][16] = 0;
    newShot11[1][17] = 12;
    
    shotJohan0[0].resize(2);
    shotJohan0[0][0] = 0.0;
    shotJohan0[0][1] = 0.0;
    
    shotJohan1[0].resize(2);
    shotJohan1[1].resize(11);
    shotJohan1[0][0] = 0;
    shotJohan1[0][1] = 0;
    shotJohan1[1][0] = 120;
    shotJohan1[1][1] = 11;
    shotJohan1[1][2] = 180;
    shotJohan1[1][3] = 5;
    shotJohan1[1][4] = 10;
    shotJohan1[1][5] = 200;
    shotJohan1[1][6] = 16;
    shotJohan1[1][7] = 1;
    shotJohan1[1][8] = 12;
    shotJohan1[1][9] = 8;
    shotJohan1[1][10] = 5;
    
    shotJohan2[0].resize(2);
    shotJohan2[1].resize(27);
    shotJohan2[0][0] = 0;
    shotJohan2[0][1] = 0;
    shotJohan2[1][0] = 120;
    shotJohan2[1][1] = 12; 
    shotJohan2[1][2] = 360;    //fire rate 1
    shotJohan2[1][3] = 3;      //fire rate 2
    shotJohan2[1][4] = 3;      //fire rate 3
    shotJohan2[1][5] = 60;     //duration shoot 1
    shotJohan2[1][6] = 60;     //duration shoot 2
    shotJohan2[1][7] = 30;     //duration waiting 1
    shotJohan2[1][8] = 30;     //duration waiting 2
    shotJohan2[1][9] = 60;     //duration explosion
    shotJohan2[1][10] = 5;     //duration stop
    shotJohan2[1][11] = 4;     //speed init
    shotJohan2[1][12] = 0;     //angle
    shotJohan2[1][13] = 200;   //radius
    shotJohan2[1][14] = 0;     //centerx
    shotJohan2[1][15] = 1;     //centery
    shotJohan2[1][16] = 6;     //nb bullet angle 1
    shotJohan2[1][17] = 6;     //nb bullet angle 2
    shotJohan2[1][18] = 1;     //nb bullet speed 1
    shotJohan2[1][19] = 1;     //nb bullet speed 2
    shotJohan2[1][20] = 15;
    shotJohan2[1][21] = 12;
    shotJohan2[1][22] = 6;
    shotJohan2[1][23] = 15;
    shotJohan2[1][24] = 12;
    shotJohan2[1][25] = 6;
    shotJohan2[1][26] = 15;
    
    shotJohan3[0].resize(2);
    shotJohan3[1].resize(17);
    shotJohan3[0][0] = 0;
    shotJohan3[0][1] = 0;
    shotJohan3[1][0] = 120;
    shotJohan3[1][1] = 13;
    shotJohan3[1][2] = 240;
    shotJohan3[1][3] = 60;
    shotJohan3[1][4] = 10;
    shotJohan3[1][5] = 210;
    shotJohan3[1][6] = 3;
    shotJohan3[1][7] = 3;
    shotJohan3[1][8] = 200;
    shotJohan3[1][9] = 500000;
    shotJohan3[1][10] = 0.5;
    shotJohan3[1][11] = 120;
    shotJohan3[1][12] = 16;
    shotJohan3[1][13] = 1;
    shotJohan3[1][14] = 100;
    shotJohan3[1][15] = 15;
    
    shotJohan4[0].resize(2);
    shotJohan4[1].resize(45);
    shotJohan4[0][0] = 0;
    shotJohan4[0][1] = 0;
    shotJohan4[1][0] = 120;
    shotJohan4[1][1] = 14;
    shotJohan4[1][2] = 240;    //Fire rate 1
    shotJohan4[1][3] = 5;      //Fire rate 2
    shotJohan4[1][4] = 120;    //Fire rate 3
    shotJohan4[1][5] = 120;     //Duration branch
    shotJohan4[1][6] = 15;     //Duration deploy 1
    shotJohan4[1][7] = 90;     //Duration deploy 2
    shotJohan4[1][8] = 6;     //Nb branch
    shotJohan4[1][9] = 400;      //Radius
    shotJohan4[1][10] = 16;   //Nb bullet angle
    shotJohan4[1][11] = 1;     //Nb bullet speed
    shotJohan4[1][12] = 12;
    shotJohan4[1][13] = 6;
    shotJohan4[1][14] = 15;
    shotJohan4[1][15] = 1;
    shotJohan4[1][16] = 1;
    shotJohan4[1][17] = 1;
    shotJohan4[1][18] = 1;
    shotJohan4[1][19] = 1;
    shotJohan4[1][20] = 1;
    shotJohan4[1][21] = 1;
    shotJohan4[1][22] = 1;
    shotJohan4[1][23] = 1;
    shotJohan4[1][24] = 1;
    shotJohan4[1][25] = 1;
    shotJohan4[1][26] = 1;
    shotJohan4[1][27] = 1;
    shotJohan4[1][28] = 1;
    shotJohan4[1][29] = 1;
    shotJohan4[1][30] = 1;
    shotJohan4[1][31] = 1;
    shotJohan4[1][32] = 1;
    shotJohan4[1][33] = 1;
    shotJohan4[1][34] = 1;
    shotJohan4[1][35] = 1;
    shotJohan4[1][36] = 1;
    shotJohan4[1][37] = 1;
    shotJohan4[1][38] = 1;
    shotJohan4[1][39] = 1;
    shotJohan4[1][40] = 1;
    shotJohan4[1][41] = 1;
    shotJohan4[1][42] = 1;
    shotJohan4[1][43] = 1;
    shotJohan4[1][44] = 1;
    
    shotJohan5[0].resize(2);
    shotJohan5[1].resize(18);
    shotJohan5[0][0] = 0;
    shotJohan5[0][1] = 0;
    shotJohan5[1][0] = 120;
    shotJohan5[1][1] = 15;
    shotJohan5[1][2] = 180;
    shotJohan5[1][3] = 30;
    shotJohan5[1][4] = 6;
    shotJohan5[1][5] = 6;
    shotJohan5[1][6] = 18;
    shotJohan5[1][7] = 18;
    shotJohan5[1][8] = 12;
    shotJohan5[1][9] = 12;
    shotJohan5[1][10] = 6;
    shotJohan5[1][11] = 6;
    shotJohan5[1][12] = 4;
    shotJohan5[1][13] = 4;
    shotJohan5[1][14] = 20;
    shotJohan5[1][15] = 20;
    shotJohan5[1][16] = 5;
    shotJohan5[1][17] = 5;
    
    shotJohan6[0].resize(2);
    shotJohan6[1].resize(17);
    shotJohan6[0][0] = 0;
    shotJohan6[0][1] = 0;
    shotJohan6[1][0] = 120;
    shotJohan6[1][1] = 16;
    shotJohan6[1][2] = 30;
    shotJohan6[1][3] = 200;
    shotJohan6[1][4] = 12;
    shotJohan6[1][5] = 18;
    shotJohan6[1][6] = 18;
    shotJohan6[1][7] = 12;
    shotJohan6[1][8] = 12;
    shotJohan6[1][9] = 6;
    shotJohan6[1][10] = 6;
    shotJohan6[1][11] = 4;
    shotJohan6[1][12] = 4;
    shotJohan6[1][13] = 20;
    shotJohan6[1][14] = 20;
    shotJohan6[1][15] = 5;
    shotJohan6[1][16] = 5;
    
    newShotJohan0[0].resize(2);
    newShotJohan0[1].resize(11);
    newShotJohan0[2].resize(2);
    newShotJohan0[3].resize(16);
    newShotJohan0[4].resize(2);
    newShotJohan0[0][0] = 0.0;
    newShotJohan0[0][1] = 0.0;
    newShotJohan0[2][0] = Ennemi::vectorWatch[1];
    newShotJohan0[2][1] = 0.0;
    newShotJohan0[4][0] = Ennemi::vectorWatch[2];
    newShotJohan0[4][1] = 0.0;
    //========================
    newShotJohan0[1][0] = 180;
    newShotJohan0[1][1] = 11;
    newShotJohan0[1][2] = 180;
    newShotJohan0[1][3] = 5;
    newShotJohan0[1][4] = 10;
    newShotJohan0[1][5] = 200;
    newShotJohan0[1][6] = 16;
    newShotJohan0[1][7] = 1;
    newShotJohan0[1][8] = 12;
    newShotJohan0[1][9] = 8;
    newShotJohan0[1][10] = 5;
    //========================
    newShotJohan0[3][0] = Ennemi::vectorWatch[1] + 180;
    newShotJohan0[3][1] = 13;
    newShotJohan0[3][2] = 240;
    newShotJohan0[3][3] = 60;
    newShotJohan0[3][4] = 10;
    newShotJohan0[3][5] = 210;
    newShotJohan0[3][6] = 3;
    newShotJohan0[3][7] = 3;
    newShotJohan0[3][8] = 200;
    newShotJohan0[3][9] = 500000;
    newShotJohan0[3][10] = 0.5;
    newShotJohan0[3][11] = 120;
    newShotJohan0[3][12] = 16;
    newShotJohan0[3][13] = 1;
    newShotJohan0[3][14] = 100;
    newShotJohan0[3][15] = 15;
    
    
    newShotJohan1[0].resize(2);  //0
    newShotJohan1[1].resize(18); //15
    newShotJohan1[2].resize(2);  //0
    newShotJohan1[3].resize(17); //16
    newShotJohan1[4].resize(2);  //0
    newShotJohan1[5].resize(45); //14
    newShotJohan1[6].resize(2);  //0
    newShotJohan1[7].resize(27); //12
    newShotJohan1[8].resize(2);  //0
    newShotJohan1[0][0] = 0.0;
    newShotJohan1[0][1] = 0.0;
    newShotJohan1[1][0] = 180;
    newShotJohan1[1][1] = 15;
    newShotJohan1[2][0] = Ennemi::vectorWatch[4];
    newShotJohan1[2][1] = 0.0;
    newShotJohan1[3][0] = Ennemi::vectorWatch[4] + 180;
    newShotJohan1[3][1] = 16;
    newShotJohan1[4][0] = Ennemi::vectorWatch[5];
    newShotJohan1[4][1] = 0.0;
    newShotJohan1[5][0] = Ennemi::vectorWatch[5] + 180;
    newShotJohan1[5][1] = 14;
    newShotJohan1[6][0] = Ennemi::vectorWatch[6];
    newShotJohan1[6][1] = 0.0;
    newShotJohan1[7][0] = Ennemi::vectorWatch[6] + 180;
    newShotJohan1[7][1] = 12;
    newShotJohan1[8][0] = Ennemi::vectorWatch[7];
    newShotJohan1[8][1] = 0.0;
    //========================
    newShotJohan1[1][2] = 180;
    newShotJohan1[1][3] = 30;
    newShotJohan1[1][4] = 6;
    newShotJohan1[1][5] = 6;
    newShotJohan1[1][6] = 18;
    newShotJohan1[1][7] = 18;
    newShotJohan1[1][8] = 12;
    newShotJohan1[1][9] = 12;
    newShotJohan1[1][10] = 6;
    newShotJohan1[1][11] = 6;
    newShotJohan1[1][12] = 4;
    newShotJohan1[1][13] = 4;
    newShotJohan1[1][14] = 20;
    newShotJohan1[1][15] = 20;
    newShotJohan1[1][16] = 5;
    newShotJohan1[1][17] = 5;
    //=========================
    newShotJohan1[3][2] = 30;
    newShotJohan1[3][3] = 200;
    newShotJohan1[3][4] = 12;
    newShotJohan1[3][5] = 18;
    newShotJohan1[3][6] = 18;
    newShotJohan1[3][7] = 12;
    newShotJohan1[3][8] = 12;
    newShotJohan1[3][9] = 6;
    newShotJohan1[3][10] = 6;
    newShotJohan1[3][11] = 4;
    newShotJohan1[3][12] = 4;
    newShotJohan1[3][13] = 20;
    newShotJohan1[3][14] = 20;
    newShotJohan1[3][15] = 5;
    newShotJohan1[3][16] = 5;
    //=========================
    newShotJohan1[5][2] = 240;    //Fire rate 1
    newShotJohan1[5][3] = 5;      //Fire rate 2
    newShotJohan1[5][4] = 120;    //Fire rate 3
    newShotJohan1[5][5] = 120;     //Duration branch
    newShotJohan1[5][6] = 15;     //Duration deploy 1
    newShotJohan1[5][7] = 90;     //Duration deploy 2
    newShotJohan1[5][8] = 6;     //Nb branch
    newShotJohan1[5][9] = 400;      //Radius
    newShotJohan1[5][10] = 16;   //Nb bullet angle
    newShotJohan1[5][11] = 1;     //Nb bullet speed
    newShotJohan1[5][12] = 12;
    newShotJohan1[5][13] = 6;
    newShotJohan1[5][14] = 15;
    newShotJohan1[5][15] = 1;
    newShotJohan1[5][16] = 1;
    newShotJohan1[5][17] = 1;
    newShotJohan1[5][18] = 1;
    newShotJohan1[5][19] = 1;
    newShotJohan1[5][20] = 1;
    newShotJohan1[5][21] = 1;
    newShotJohan1[5][22] = 1;
    newShotJohan1[5][23] = 1;
    newShotJohan1[5][24] = 1;
    newShotJohan1[5][25] = 1;
    newShotJohan1[5][26] = 1;
    newShotJohan1[5][27] = 1;
    newShotJohan1[5][28] = 1;
    newShotJohan1[5][29] = 1;
    newShotJohan1[5][30] = 1;
    newShotJohan1[5][31] = 1;
    newShotJohan1[5][32] = 1;
    newShotJohan1[5][33] = 1;
    newShotJohan1[5][34] = 1;
    newShotJohan1[5][35] = 1;
    newShotJohan1[5][36] = 1;
    newShotJohan1[5][37] = 1;
    newShotJohan1[5][38] = 1;
    newShotJohan1[5][39] = 1;
    newShotJohan1[5][40] = 1;
    newShotJohan1[5][41] = 1;
    newShotJohan1[5][42] = 1;
    newShotJohan1[5][43] = 1;
    newShotJohan1[5][44] = 1;
    //=========================
    newShotJohan1[7][2] = 360;    //fire rate 1
    newShotJohan1[7][3] = 3;      //fire rate 2
    newShotJohan1[7][4] = 3;      //fire rate 3
    newShotJohan1[7][5] = 60;     //duration shoot 1
    newShotJohan1[7][6] = 60;     //duration shoot 2
    newShotJohan1[7][7] = 30;     //duration waiting 1
    newShotJohan1[7][8] = 30;     //duration waiting 2
    newShotJohan1[7][9] = 60;     //duration explosion
    newShotJohan1[7][10] = 5;     //duration stop
    newShotJohan1[7][11] = 4;     //speed init
    newShotJohan1[7][12] = 0;     //angle
    newShotJohan1[7][13] = 200;   //radius
    newShotJohan1[7][14] = 0;     //centerx
    newShotJohan1[7][15] = 1;     //centery
    newShotJohan1[7][16] = 6;     //nb bullet angle 1
    newShotJohan1[7][17] = 6;     //nb bullet angle 2
    newShotJohan1[7][18] = 1;     //nb bullet speed 1
    newShotJohan1[7][19] = 1;     //nb bullet speed 2
    newShotJohan1[7][20] = 15;
    newShotJohan1[7][21] = 12;
    newShotJohan1[7][22] = 6;
    newShotJohan1[7][23] = 15;
    newShotJohan1[7][24] = 12;
    newShotJohan1[7][25] = 6;
    newShotJohan1[7][26] = 15;
    //=========================
    
    shotDialog[0].resize(2);
    shotDialog[0][0] = 0;
    shotDialog[0][1] = 0;
    
    Ennemi::bossBattle = false;
    midBossDeafeted = false;
    bossDeafeted = false;
    while (true)
    {
        watch = watch + 1;
        //std::cout << "Stage watch = " << watch << std::endl;
        cloud.speedx = 5;
        cloud.speedy = 10*cos(cloud.watch/600.0);
        //window.spriteEnnemi.setPosition(watch, 400);
        //std::cout << "Music offset = " << Sound::musicStage1.getPlayingOffset().asMilliseconds() << std::endl;
        if (Sound::musicStage1.getPlayingOffset().asMilliseconds() > 85670)
        {
            Sound::musicStage1.play();
            Sound::musicStage1.setPlayingOffset(sf::milliseconds(6830));
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) || sf::Keyboard::isKeyPressed(sf::Keyboard::A))
        {
            Sound::destroySound();
            window.mainWindow.close();
            return 0;
        }
        engine.update();
        engine.draw();
        if (Dialogue::on)
        {
            continue;
        }
        if (watch == 60)
        {
            /*
            std::vector<std::vector<double> > newTrajectory;
            std::vector<std::vector<double> > newShot;
            std::vector<double> trajectory;
            std::vector<double> shot;
            trajectory.push_back(0.0);
            trajectory.push_back(0.0);
            trajectory.push_back(1.0);
            trajectory.push_back(0.0);
            */
            
            /*
            shot.push_back(0.0);
            shot.push_back(1.0);
            shot.push_back(60.0);
            shot.push_back(6.0);
            shot.push_back(8.0);
            shot.push_back(0.0);
            shot.push_back(11.0);
            */
            /*
            shot.push_back(0.0);
            shot.push_back(2.0);
            shot.push_back(60.0);
            shot.push_back(8.0);
            shot.push_back(3.0);
            
            shot.push_back(12.0);
            shot.push_back(6.0);
            shot.push_back(30.0);
            shot.push_back(10.0);
            shot.push_back(5.0);
            shot.push_back(30.0);
            shot.push_back(8.0);
            shot.push_back(4.0);
            shot.push_back(30.0);
            
            shot.push_back(0.0);
            shot.push_back(11.0);
            */
            /*
            shot.push_back(0.0);
            shot.push_back(3.0);
            shot.push_back(60.0);
            shot.push_back(4.0);
            shot.push_back(3.0);
            shot.push_back(6.0);
            shot.push_back(3.1415/30.0);
            shot.push_back(1.0);
            shot.push_back(11.0);
            */
            /*
            shot.push_back(0.0);
            shot.push_back(4.0);
            shot.push_back(60.0);
            shot.push_back(3.1415926535/30.0);
            shot.push_back(3.0);
            shot.push_back(7.0);
            shot.push_back(3.0);
            
            shot.push_back(12.0);
            shot.push_back(6.0);
            shot.push_back(30.0);
            shot.push_back(10.0);
            shot.push_back(5.0);
            shot.push_back(30.0);
            shot.push_back(8.0);
            shot.push_back(4.0);
            shot.push_back(30.0);
            
            shot.push_back(1.0);
            shot.push_back(11.0);
            */
            /*
            shot.push_back(0.0);
            shot.push_back(5.0);
            shot.push_back(2.0);
            shot.push_back(0.0);
            shot.push_back(2*3.1415/60.0);
            shot.push_back(6.0);
            shot.push_back(2.0);
            shot.push_back(11.0);
            */
            /*
            shot.push_back(0.0);
            shot.push_back(6.0);
            shot.push_back(4.0);
            shot.push_back(0.0);
            shot.push_back(3.1415926535/30.0);
            shot.push_back(2.0);
            shot.push_back(3.0);
            
            shot.push_back(12.0);
            shot.push_back(6.0);
            shot.push_back(30.0);
            shot.push_back(10.0);
            shot.push_back(5.0);
            shot.push_back(30.0);
            shot.push_back(8.0);
            shot.push_back(4.0);
            shot.push_back(30.0);
            
            shot.push_back(11.0);
            */
            /*
            shot.push_back(0.0);
            shot.push_back(7.0);
            shot.push_back(60.0);
            shot.push_back(3.0);
            shot.push_back(1.0);
            shot.push_back(3.1415/30.0);
            shot.push_back(6.0);
            shot.push_back(0.0);
            shot.push_back(11.0);
            */
            /*
            shot.push_back(0.0);
            shot.push_back(8.0);
            shot.push_back(60.0);
            shot.push_back(3.1415/30.0);
            shot.push_back(1.0);
            shot.push_back(0.0);
            shot.push_back(3.0);
            shot.push_back(4.0);
            shot.push_back(2.0);
            
            shot.push_back(16.0);
            shot.push_back(6.0);
            shot.push_back(15.0);
            shot.push_back(12.0);
            shot.push_back(6.0);
            shot.push_back(15.0);
            
            shot.push_back(11.0);
            */
            /*
            int nb = 20;
            std::vector<int> permutation = permutationAllChanged(nb, 2);
            
            shot.push_back(0.0);  //Time shoot
            shot.push_back(9.0);  //Index shoot
            shot.push_back(180.0);//Fire rate
            shot.push_back(100.0);//Radius
            shot.push_back(20.0); //Nb bullet angle
            shot.push_back(2.0);  //Nb bullet speed
            shot.push_back(15.0); //Duration movement 1
            shot.push_back(15.0); //Duration movement 2
            shot.push_back(0.0);  //Interval time 1
            shot.push_back(5.0);  //Interval time 2
            shot.push_back(0.0);  //Interval time 3
            shot.push_back(1.0);  //Random permutation
            */
            /*
            shot.push_back(5.0);
            shot.push_back(3.0);
            shot.push_back(7.0);
            shot.push_back(0.0);
            shot.push_back(1.0);
            shot.push_back(4.0);
            shot.push_back(2.0);
            shot.push_back(6.0);
            */
            /*
            for (int i = 0; i < 20; i++)
            {
                shot.push_back(permutation[i]);
            }
            shot.push_back(12.0);
            shot.push_back(6.0);
            shot.push_back(10.0);
            shot.push_back(10.0);
            shot.push_back(4.0);
            shot.push_back(10.0);
            
            shot.push_back(1.0);  //Aimed
            shot.push_back(21.0); //ID bullet
            */
            
            /*
            newTrajectory.push_back(trajectory);
            newShot.push_back(shot);
            engine.addEnnemi(300, 500, 1, newTrajectory, newShot, 10, 5, 5);
            */
        }
        
        if (watch == 420)
        {
            engine.addEnnemi(1920, 400, 1, newTrajectoryRU, newShot1, 10, 5, 5);
            engine.addEnnemi(1920, 800, 1, newTrajectoryRD, newShot1, 10, 5, 5);
        }
        if (watch == 450)
        {
            engine.addEnnemi(1920, 450, 1, newTrajectoryRU, newShot1, 10, 5, 5);
            engine.addEnnemi(1920, 750, 1, newTrajectoryRD, newShot1, 10, 5, 5);
        }
        if (watch == 480)
        {
            engine.addEnnemi(1920, 500, 1, newTrajectoryRU, newShot1, 10, 5, 5);
            engine.addEnnemi(1920, 700, 1, newTrajectoryRD, newShot1, 10, 5, 5);
        }
        if (watch == 520)
        {
            engine.addEnnemi(1920, 550, 1, newTrajectoryRU, newShot1, 10, 5, 5);
            engine.addEnnemi(1920, 650, 1, newTrajectoryRD, newShot1, 10, 5, 5);
        }
        
        /*
        if (watch == 615)
        {
            engine.addEnnemi(1820, 100, 1, newTrajectoryUL, newShot2, 10, 5, 5);
            engine.addEnnemi(1820, 1050, 1, newTrajectoryDL, newShot2, 10, 5, 5);
        }
        */
        
        /*
        if (watch == 645)
        {
            engine.addEnnemi(1620, 1050, 1, newTrajectoryDR1, newShot3, 10, 5, 5, true);
            engine.addEnnemi(1620, 100, 1, newTrajectoryUR1, newShot3, 10, 5, 5);
        }
        */
        
        if (watch == 630)
        {
            engine.addEnnemi(1520, 1050, 1, newTrajectoryDR1, newShot3, 10, 5, 5);
            engine.addEnnemi(1520, 100, 1, newTrajectoryUR1, newShot3, 10, 5, 5);
        }
        
        if (watch == 660)
        {
            engine.addEnnemi(1620, 1050, 1, newTrajectoryDR2, newShot3, 10, 5, 5);
            engine.addEnnemi(1620, 100, 1, newTrajectoryUR2, newShot3, 10, 5, 5);
        }
        
        if (watch == 690)
        {
            engine.addEnnemi(1720, 1050, 1, newTrajectoryDR3, newShot3, 10, 5, 5);
            engine.addEnnemi(1720, 100, 1, newTrajectoryUR3, newShot3, 10, 5, 5);
        }
        
        if (watch == 720)
        {
            engine.addEnnemi(1820, 1050, 1, newTrajectoryDR4, newShot3, 10, 5, 5);
            engine.addEnnemi(1820, 100, 1, newTrajectoryUR4, newShot3, 10, 5, 5);
        }
        
        if (watch == 820)
        {
            engine.addEnnemi(1770, 100, 1, newTrajectoryUD1, newShot4, 10, 5, 5);
        }
        if (watch == 835)
        {
            engine.addEnnemi(1770, 100, 1, newTrajectoryUD1, newShot4, 10, 5, 5);
        }
        if (watch == 850)
        {
            engine.addEnnemi(1770, 100, 1, newTrajectoryUD1, newShot4, 10, 5, 5);
        }
        if (watch == 865)
        {
            engine.addEnnemi(1770, 100, 1, newTrajectoryUD1, newShot4, 10, 5, 5);
        }
        if (watch == 880)
        {
            engine.addEnnemi(1770, 100, 1, newTrajectoryUD1, newShot4, 10, 5, 5);
        }
        if (watch == 895)
        {
            engine.addEnnemi(1770, 100, 1, newTrajectoryUD1, newShot4, 10, 5, 5);
        }
        if (watch == 910)
        {
            engine.addEnnemi(1770, 100, 1, newTrajectoryUD1, newShot4, 10, 5, 5);
        }
        if (watch == 925)
        {
            engine.addEnnemi(1770, 100, 1, newTrajectoryUD1, newShot4, 10, 5, 5);
        }
        
        if (watch == 1020)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot4, 10, 5, 5);
        }
        if (watch == 1035)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot4, 10, 5, 5);
        }
        if (watch == 1050)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot4, 10, 5, 5);
        }
        if (watch == 1065)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot4, 10, 5, 5);
        }
        if (watch == 1080)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot4, 10, 5, 5);
        }
        if (watch == 1095)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot4, 10, 5, 5);
        }
        if (watch == 1110)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot4, 10, 5, 5);
        }
        if (watch == 1125)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot4, 10, 5, 5);
        }
        
        if (watch == 1230)
        {
            engine.addEnnemi(1770, 100, 1, newTrajectoryUD1, newShot5, 10, 5, 5);
        }
        if (watch == 1245)
        {
            engine.addEnnemi(1770, 100, 1, newTrajectoryUD1, newShot5, 10, 5, 5);
        }
        if (watch == 1260)
        {
            engine.addEnnemi(1770, 100, 1, newTrajectoryUD1, newShot5, 10, 5, 5);
        }
        if (watch == 1275)
        {
            engine.addEnnemi(1770, 100, 1, newTrajectoryUD1, newShot5, 10, 5, 5);
        }
        if (watch == 1290)
        {
            engine.addEnnemi(1770, 100, 1, newTrajectoryUD1, newShot5, 10, 5, 5);
        }
        if (watch == 1305)
        {
            engine.addEnnemi(1770, 100, 1, newTrajectoryUD1, newShot5, 10, 5, 5);
        }
        if (watch == 1320)
        {
            engine.addEnnemi(1770, 100, 1, newTrajectoryUD1, newShot5, 10, 5, 5);
        }
        if (watch == 1335)
        {
            engine.addEnnemi(1770, 100, 1, newTrajectoryUD1, newShot5, 10, 5, 5);
        }
        
        if (watch == 1440)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot5, 10, 5, 5);
        }
        if (watch == 1455)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot5, 10, 5, 5);
        }
        if (watch == 1470)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot5, 10, 5, 5);
        }
        if (watch == 1485)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot5, 10, 5, 5);
        }
        if (watch == 1500)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot5, 10, 5, 5);
        }
        if (watch == 1515)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot5, 10, 5, 5);
        }
        if (watch == 1530)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot5, 10, 5, 5);
        }
        if (watch == 1545)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot5, 10, 5, 5);
        }
        
        if (watch == 1640)
        {
            engine.addEnnemi(1820, 1050, 1, newTrajectoryDR4, newShot11, 10, 5, 5);
            engine.addEnnemi(1820, 100, 1, newTrajectoryUR4, newShot11, 10, 5, 5);
        }
        if (watch == 1670)
        {
            engine.addEnnemi(1720, 1050, 1, newTrajectoryDR4, newShot11, 10, 5, 5);
            engine.addEnnemi(1720, 100, 1, newTrajectoryUR4, newShot11, 10, 5, 5);
        }
        if (watch == 1700)
        {
            engine.addEnnemi(1620, 1050, 1, newTrajectoryDR4, newShot11, 10, 5, 5);
            engine.addEnnemi(1620, 100, 1, newTrajectoryUR4, newShot11, 10, 5, 5);
        }
        if (watch == 1730)
        {
            engine.addEnnemi(1520, 1050, 1, newTrajectoryDR4, newShot11, 10, 5, 5);
            engine.addEnnemi(1520, 100, 1, newTrajectoryUR4, newShot11, 10, 5, 5);
        }
        
        if (watch == 1850)
        {
            engine.addEnnemi(1920, 450, 1, newTrajectoryRU, newShot1, 10, 5, 5);
            engine.addEnnemi(1920, 750, 1, newTrajectoryRD, newShot1, 10, 5, 5);
        }
        if (watch == 1880)
        {
            engine.addEnnemi(1920, 500, 1, newTrajectoryRU, newShot1, 10, 5, 5);
            engine.addEnnemi(1920, 700, 1, newTrajectoryRD, newShot1, 10, 5, 5);
        }
        if (watch == 1910)
        {
            engine.addEnnemi(1920, 450, 1, newTrajectoryRU, newShot1, 10, 5, 5);
            engine.addEnnemi(1920, 650, 1, newTrajectoryRD, newShot1, 10, 5, 5);
        }
        if (watch == 1940)
        {
            engine.addEnnemi(1920, 400, 1, newTrajectoryRU, newShot1, 10, 5, 5);
            engine.addEnnemi(1920, 600, 1, newTrajectoryRD, newShot1, 10, 5, 5);
        }
        
        
        if (watch == 2050)
        {
            engine.addEnnemi(1920, 400, 2, newTrajectoryJohan0, newShotJohan0, 300, 5, 5);
            Ennemi::getIndexBoss();
            arrayEnnemi[Ennemi::indexBoss].isBoss = true;
            Ennemi::bossBattle = true;
            Ennemi::baseBossHp = 1000;
        }
        
        if (Ennemi::bossBattle && !midBossDeafeted)
        {
            if (!midBossDeafeted)
            {
                Ennemi::updateBossAttack();
                if (Ennemi::indexAttack == 2)
                {
                    Ennemi::bossBattle = false;
                    midBossDeafeted = true;
                    arrayEnnemi[Ennemi::indexBoss].isBoss = false;
                }
                /*
                if (arrayEnnemi[Ennemi::indexBoss].hp < arrayLifeFina[indexAttack])
                {
                    indexAttack = indexAttack + 1;
                    arrayEnnemi[Ennemi::indexBoss].hp = arrayLifeInit[indexAttack];
                    arrayEnnemi[Ennemi::indexBoss].watch = arrayWatch[indexAttack];
                }
                */
            }
            continue;
        }
        
        if (watch == 3080)
        {
            engine.addEnnemi(1720, 100, 1, newTrajectoryUD1, newShot7, 10, 5, 5);
        }
        if (watch == 3095)
        {
            engine.addEnnemi(1720, 100, 1, newTrajectoryUD1, newShot7, 10, 5, 5);
        }
        if (watch == 3110)
        {
            engine.addEnnemi(1720, 100, 1, newTrajectoryUD1, newShot7, 10, 5, 5);
        }
        if (watch == 3125)
        {
            engine.addEnnemi(1720, 100, 1, newTrajectoryUD1, newShot7, 10, 5, 5);
        }
        if (watch == 3140)
        {
            engine.addEnnemi(1720, 100, 1, newTrajectoryUD1, newShot7, 10, 5, 5);
        }
        if (watch == 3155)
        {
            engine.addEnnemi(1720, 100, 1, newTrajectoryUD1, newShot7, 10, 5, 5);
        }
        if (watch == 3170)
        {
            engine.addEnnemi(1720, 100, 1, newTrajectoryUD1, newShot7, 10, 5, 5);
        }
        if (watch == 3185)
        {
            engine.addEnnemi(1720, 100, 1, newTrajectoryUD1, newShot7, 10, 5, 5);
        }
        
        if (watch == 3290)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot8, 10, 5, 5);
        }
        if (watch == 3305)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot8, 10, 5, 5);
        }
        if (watch == 3320)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot8, 10, 5, 5);
        }
        if (watch == 3335)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot8, 10, 5, 5);
        }
        if (watch == 3350)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot8, 10, 5, 5);
        }
        if (watch == 3365)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot8, 10, 5, 5);
        }
        if (watch == 3380)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot8, 10, 5, 5);
        }
        if (watch == 3395)
        {
            engine.addEnnemi(1670, 1050, 1, newTrajectoryDU1, newShot8, 10, 5, 5);
        }
        
        if (watch == 3490)
        {
            engine.addEnnemi(1770, 100, 1, trajectoryCircular1, newShot9, 10, 5, 5);
        }
        if (watch == 3505)
        {
            engine.addEnnemi(1770, 100, 1, trajectoryCircular1, newShot9, 10, 5, 5);
        }
        if (watch == 3520)
        {
            engine.addEnnemi(1770, 100, 1, trajectoryCircular1, newShot9, 10, 5, 5);
        }
        if (watch == 3535)
        {
            engine.addEnnemi(1770, 100, 1, trajectoryCircular1, newShot9, 10, 5, 5);
        }
        if (watch == 3550)
        {
            engine.addEnnemi(1770, 100, 1, trajectoryCircular1, newShot9, 10, 5, 5);
        }
        if (watch == 3565)
        {
            engine.addEnnemi(1770, 100, 1, trajectoryCircular1, newShot9, 10, 5, 5);
        }
        if (watch == 3580)
        {
            engine.addEnnemi(1770, 100, 1, trajectoryCircular1, newShot9, 10, 5, 5);
        }
        if (watch == 3595)
        {
            engine.addEnnemi(1770, 100, 1, trajectoryCircular1, newShot9, 10, 5, 5);
        }
        
        if (watch == 3700)
        {
            engine.addEnnemi(1670, 1050, 1, trajectoryCircular2, newShot10, 10, 5, 5);
        }
        if (watch == 3715)
        {
            engine.addEnnemi(1670, 1050, 1, trajectoryCircular2, newShot10, 10, 5, 5);
        }
        if (watch == 3730)
        {
            engine.addEnnemi(1670, 1050, 1, trajectoryCircular2, newShot10, 10, 5, 5);
        }
        if (watch == 3745)
        {
            engine.addEnnemi(1670, 1050, 1, trajectoryCircular2, newShot10, 10, 5, 5);
        }
        if (watch == 3760)
        {
            engine.addEnnemi(1670, 1050, 1, trajectoryCircular2, newShot10, 10, 5, 5);
        }
        if (watch == 3775)
        {
            engine.addEnnemi(1670, 1050, 1, trajectoryCircular2, newShot10, 10, 5, 5);
        }
        if (watch == 3790)
        {
            engine.addEnnemi(1670, 1050, 1, trajectoryCircular2, newShot10, 10, 5, 5);
        }
        if (watch == 3805)
        {
            engine.addEnnemi(1670, 1050, 1, trajectoryCircular2, newShot10, 10, 5, 5);
        }
        
        if (watch == 4110)
        {
            engine.addEnnemi(1520, 1050, 1, newTrajectoryDR1, newShot3, 10, 5, 5);
            engine.addEnnemi(1520, 100, 1, newTrajectoryUR1, newShot3, 10, 5, 5);
        }
        if (watch == 4140)
        {
            engine.addEnnemi(1620, 1050, 1, newTrajectoryDR2, newShot3, 10, 5, 5);
            engine.addEnnemi(1620, 100, 1, newTrajectoryUR2, newShot3, 10, 5, 5);
        }
        if (watch == 4170)
        {
            engine.addEnnemi(1720, 1050, 1, newTrajectoryDR3, newShot3, 10, 5, 5);
            engine.addEnnemi(1720, 100, 1, newTrajectoryUR3, newShot3, 10, 5, 5);
        }
        if (watch == 4200)
        {
            engine.addEnnemi(1820, 1050, 1, newTrajectoryDR4, newShot3, 10, 5, 5);
            engine.addEnnemi(1820, 100, 1, newTrajectoryUR4, newShot3, 10, 5, 5);
        }
        
        if (watch == 4320)
        {
            engine.addEnnemi(1820, 1050, 1, newTrajectoryDR4, newShot11, 10, 5, 5);
            engine.addEnnemi(1820, 100, 1, newTrajectoryUR4, newShot11, 10, 5, 5);
        }
        if (watch == 4350)
        {
            engine.addEnnemi(1720, 1050, 1, newTrajectoryDR4, newShot11, 10, 5, 5);
            engine.addEnnemi(1720, 100, 1, newTrajectoryUR4, newShot11, 10, 5, 5);
        }
        if (watch == 4380)
        {
            engine.addEnnemi(1620, 1050, 1, newTrajectoryDR4, newShot11, 10, 5, 5);
            engine.addEnnemi(1620, 100, 1, newTrajectoryUR4, newShot11, 10, 5, 5);
        }
        if (watch == 4410)
        {
            engine.addEnnemi(1520, 1050, 1, newTrajectoryDR4, newShot11, 10, 5, 5);
            engine.addEnnemi(1520, 100, 1, newTrajectoryUR4, newShot11, 10, 5, 5);
        }
        
        if (watch == 4520)
        {
            engine.addEnnemi(0, 1050, 1, trajectoryCircular3, newShot6, 10, 5, 5);
            engine.addEnnemi(0, 1050, 1, trajectoryCircular7, newShot6, 10, 5, 5);
        }
        if (watch == 4550)
        {
            engine.addEnnemi(0, 1050, 1, trajectoryCircular4, newShot6, 10, 5, 5);
            engine.addEnnemi(0, 1050, 1, trajectoryCircular8, newShot6, 10, 5, 5);
        }
        if (watch == 4580)
        {
            engine.addEnnemi(0, 1050, 1, trajectoryCircular5, newShot6, 10, 5, 5);
            engine.addEnnemi(0, 1050, 1, trajectoryCircular9, newShot6, 10, 5, 5);
        }
        if (watch == 4610)
        {
            engine.addEnnemi(0, 1050, 1, trajectoryCircular6, newShot6, 10, 5, 5);
            engine.addEnnemi(0, 1050, 1, trajectoryCircular10, newShot6, 10, 5, 5);
        }
        
        if (watch == 4730)
        {
            engine.addEnnemi(1920, 400, 1, newTrajectoryRU, newShot1, 10, 5, 5);
            engine.addEnnemi(1920, 800, 1, newTrajectoryRD, newShot1, 10, 5, 5);
        }
        if (watch == 4760)
        {
            engine.addEnnemi(1920, 450, 1, newTrajectoryRU, newShot1, 10, 5, 5);
            engine.addEnnemi(1920, 750, 1, newTrajectoryRD, newShot1, 10, 5, 5);
        }
        if (watch == 4790)
        {
            engine.addEnnemi(1920, 500, 1, newTrajectoryRU, newShot1, 10, 5, 5);
            engine.addEnnemi(1920, 700, 1, newTrajectoryRD, newShot1, 10, 5, 5);
        }
        if (watch == 4820)
        {
            engine.addEnnemi(1920, 550, 1, newTrajectoryRU, newShot1, 10, 5, 5);
            engine.addEnnemi(1920, 650, 1, newTrajectoryRD, newShot1, 10, 5, 5);
        }
        
        if (watch == 5300)
        {
            engine.addEnnemi(1920, 400, 2, trajectoryDialog, shotDialog, 300, 5, 5);
            Ennemi::getIndexBoss();
            //arrayEnnemi[Ennemi::indexBoss].isBoss = true;
            //Ennemi::bossBattle = true;
            //Ennemi::baseBossHp = 1000;
            Dialogue::set(0);
        }
        /*
        if (Ennemi::nbEnnemi)
        {
            std::cout << "watchEnnemi = " << arrayEnnemi[Ennemi::indexBoss].watch << std::endl;
        }
        */
        if (!Dialogue::on && watch > 5300 && Ennemi::bossBattle == false && bossDeafeted == false)
        {
            Sound::musicStage1.stop();
            Sound::musicBoss1.play();
            watch = 5301;
            Ennemi::getIndexBoss();
            arrayEnnemi[Ennemi::indexBoss].isBoss = true;
            Ennemi::bossBattle = true;
            Ennemi::baseBossHp = 1000;
            arrayEnnemi[Ennemi::indexBoss].shot = newShotJohan1;
            arrayEnnemi[Ennemi::indexBoss].indexShot = 0;
            arrayEnnemi[Ennemi::indexBoss].trajectory = newTrajectoryJohan1;
            arrayEnnemi[Ennemi::indexBoss].indexTrajectory = 0;
            arrayEnnemi[Ennemi::indexBoss].watch = 0;
            Ennemi::indexAttack = 3;
        }
        
        if (Ennemi::bossBattle && midBossDeafeted)
        {
            Ennemi::updateBossAttack();
            if (Ennemi::indexAttack == 7)
            {
                Ennemi::bossBattle = false;
                bossDeafeted = true;
                arrayEnnemi[Ennemi::indexBoss].shot = shotDialog;
                arrayEnnemi[Ennemi::indexBoss].indexShot = 0;
                arrayEnnemi[Ennemi::indexBoss].trajectory = trajectoryEnd;
                arrayEnnemi[Ennemi::indexBoss].indexTrajectory = 0;
                Dialogue::set(0);
                //arrayEnnemi[Ennemi::indexBoss].isBoss = false;
            }
        }
        
        /*
        if (watch == 2170)
        {
            dialogue.set();   
        }
        
        if (watch == 2180)
        {
            arrayEnnemi[Ennemi::indexBoss].watch = 0;
            arrayEnnemi[Ennemi::indexBoss].shot = shotJohan1;
        }
        */
        
    }
}
