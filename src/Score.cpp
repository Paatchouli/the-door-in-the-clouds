/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Score.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>

Score score;

Score::Score()
{

}

Score::~Score()
{

}

void Score::initialize()
{
    std::ifstream scoreFile("score.txt");
    if (scoreFile)
    {
        for (int i = 0; i < 10; i++)
        {
            scoreFile >> scoreEasy[i] >> nameEasy[i] >> dateEasy[i] >> hourEasy[i];
        }
        scoreEasy[10] = 0;
        nameEasy[10] = "";
        dateEasy[10] = "";
        hourEasy[10] = "";
        
        scoreFile.ignore();
        
        for (int i = 0; i < 10; i++)
        {
            scoreFile >> scoreNormal[i] >> nameNormal[i] >> dateNormal[i] >> hourNormal[i];
        }
        scoreEasy[10] = 0;
        nameEasy[10] = "";
        dateEasy[10] = "";
        hourEasy[10] = "";
        
        scoreFile.ignore();
        
        for (int i = 0; i < 10; i++)
        {
            scoreFile >> scoreHard[i] >> nameHard[i] >> dateHard[i] >> hourHard[i];
        }
        scoreHard[10] = 0;
        nameHard[10] = "";
        dateHard[10] = "";
        hourHard[10] = "";
        
        scoreFile.ignore();
        
        for (int i = 0; i < 10; i++)
        {
            scoreFile >> scoreFeeric[i] >> nameFeeric[i] >> dateFeeric[i] >> hourFeeric[i];
        }
        scoreFeeric[10] = 0;
        nameFeeric[10] = "";
        dateFeeric[10] = "";
        hourFeeric[10] = "";
        
        scoreFile.ignore();
        
        for (int i = 0; i < 10; i++)
        {
            scoreFile >> scoreExtra[i] >> nameExtra[i] >> dateExtra[i] >> hourExtra[i];
        }
        scoreExtra[10] = 0;
        nameExtra[10] = "";
        dateExtra[10] = "";
        hourExtra[10] = "";
        
        scoreFile.ignore();
        
        for (unsigned int i = 0; i < 24; i++)
        {
            scoreFile >> scorePractice[i];
        }
        scorePractice[24] = 0;
    }
    else
    {
        std::ofstream scoreFile("score.txt", std::ios::out | std::ios::trunc);
        for (int j = 0; j < 5; j++)
        {
            for (int i = 0; i < 10; i++)
            {
                scoreFile << 0 << " " << "Nanashi" << " " << "01/01/2017" << " " <<"00:00" << "\n";
            }
            scoreFile << "\n";
        }
        for (int i = 0; i < 24; i++)
        {
            scoreFile << 0 << " ";   
        }
    }
    scoreFile.close();
}

void Score::update()
{
    
}
