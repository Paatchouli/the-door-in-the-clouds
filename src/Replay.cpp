/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Replay.h"
#include <cstdlib>
#include <ctime> 

int Replay::watch;
unsigned int Replay::seed;
std::vector<std::vector<int> > Replay::vectorInput;

Replay::Replay()
{

}

Replay::~Replay()
{

}

void Replay::initSeed()
{
    seed = (std::time(NULL));
    std::srand(seed);
    vectorInput.clear();
}
