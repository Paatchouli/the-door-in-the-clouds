/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Setting.h"
#include "../include/func.h"
#include <sstream>
#include <fstream>
#include <iomanip>

/*
sf::Keyboard::Key Setting::left;
sf::Keyboard::Key Setting::right;
sf::Keyboard::Key Setting::up;
sf::Keyboard::Key Setting::down;
sf::Keyboard::Key Setting::bomb;
sf::Keyboard::Key Setting::slow;
sf::Keyboard::Key Setting::fast;
sf::Keyboard::Key Setting::pause;
sf::Keyboard::Key Setting::screenShot;
*/
int Setting::nbLifeInit;
int Setting::nbBombInit;
int Setting::BGMvolume;
int Setting::SEvolume;
int Setting::language;
/**
 * 0 : Left
 * 1 : Right
 * 2 : Up
 * 3 : Down
 * 4 : Bomb
 * 5 : Slow
 * 6 : Fast
 * 7 : Pause
 * 8 : Screenshot
 */
sf::Keyboard::Key Setting::arrayKey[9];

Setting::Setting()
{

}

Setting::~Setting()
{

}

void Setting::initialize()
{
    /*
    arrayKey[0] = left;
    arrayKey[0] = right;
    arrayKey[0] = up;
    arrayKey[0] = down;
    arrayKey[0] = bomb;
    arrayKey[0] = slow;
    arrayKey[0] = fast;
    arrayKey[0] = pause;
    arrayKey[0] = screenShot;
    */
    std::ifstream settingFile("setting.txt");
    if (!settingFile)
    {
        rewrite();
    }
    else if (isError())
    {
        rewrite();
    }
    read();
}

void Setting::rewrite()
{
    std::ofstream settingFile("setting.txt", std::ios::out | std::ios::trunc);
    settingFile << 5 << " " << 3 << "\n";
    settingFile << 10 << " " << 10 << "\n";
    settingFile << 0 << "\n";
    settingFile << "Left" << " " << "Right" << " " << "Up" << " " << "Down" << " " << "X" << " " << "LShift" << " " << "W" << " " << "P" << " " << "C";
    settingFile.close();
}

void Setting::write()
{
    std::ofstream settingFile("setting.txt", std::ios::out | std::ios::trunc);
    settingFile << nbLifeInit << " " << nbBombInit << "\n";
    settingFile << BGMvolume << " " << SEvolume << "\n";
    settingFile << language << "\n";
    //settingFile << key2str(left) << " " << key2str(right) << " " << key2str(up) << " " << key2str(down) << " " << key2str(bomb) << " " << key2str(slow) << " " << key2str(fast) << " " << key2str(pause) << " " << key2str(screenShot);
    settingFile << key2str(arrayKey[0]) << " " << key2str(arrayKey[1]) << " " << key2str(arrayKey[2]) << " " << key2str(arrayKey[3]) << " " << key2str(arrayKey[4]) << " " << key2str(arrayKey[5]) << " " << key2str(arrayKey[6]) << " " << key2str(arrayKey[7]) << " " << key2str(arrayKey[8]);
    settingFile.close();
}

bool Setting::isError()
{
    return false;
}


void Setting::read()
{
    std::ifstream settingFile("setting.txt");
    settingFile >> nbLifeInit >> nbBombInit; settingFile.ignore();
    settingFile >> BGMvolume >> SEvolume; settingFile.ignore();
    settingFile >> language; settingFile.ignore();
    std::__cxx11::string str;
    /*
    settingFile >> str; left = str2key(str);
    settingFile >> str; right = str2key(str);
    settingFile >> str; up = str2key(str);
    settingFile >> str; down = str2key(str);
    settingFile >> str; bomb = str2key(str);
    settingFile >> str; slow = str2key(str);
    settingFile >> str; fast = str2key(str);
    settingFile >> str; pause = str2key(str);
    settingFile >> str; screenShot = str2key(str);
    */
    for (int i = 0; i < 9; i++)
    {
        settingFile >> str; arrayKey[i] = str2key(str);
    }
    settingFile.close();
}

