/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/TextEn.h"
#include "../include/Window.h"
#include "../include/Player.h"
#include "../include/Score.h"
#include "../include/func.h"
#include <vector>
#include <cmath>

extern Window window;
extern Player player;

TextEn textEn;

TextEn::TextEn()
{
    bomb.setFont(window.font); bomb.setCharacterSize(40); bomb.setString("Spell"); bomb.setPosition(300, 87);
    live.setFont(window.font); live.setCharacterSize(40); live.setString("Player"); live.setPosition(300, 12);
    scoreName.setFont(window.font); scoreName.setCharacterSize(40); scoreName.setString("Score"); scoreName.setPosition(900, 87);
    hiScore.setFont(window.font); hiScore.setCharacterSize(40); hiScore.setString("HiScore"); hiScore.setPosition(900, 12);
    point.setFont(window.font); point.setCharacterSize(40); point.setString("Point"); point.setPosition(1400, 12);
    power.setFont(window.font); power.setCharacterSize(40); power.setString("Power"); power.setPosition(1400, 87);
    bonus.setFont(window.font); bonus.setCharacterSize(40); bonus.setString("Bonus"); bonus.setPosition(0, 0);
    valScore.setFont(window.font); valScore.setCharacterSize(40); valScore.setString("0000000000"); valScore.setPosition(1100, 87);
    valHiScore.setFont(window.font); valHiScore.setCharacterSize(40); valHiScore.setString("0000000000"); valHiScore.setPosition(1100, 12);
    valPoint.setFont(window.font); valPoint.setCharacterSize(40); valPoint.setString("0"); valPoint.setPosition(1550, 12);
    valPower.setFont(window.font); valPower.setCharacterSize(40); valPower.setString("0"); valPower.setPosition(1550, 87);
    valBonus.setFont(window.font); valBonus.setCharacterSize(40); valBonus.setString("0"); valBonus.setPosition(0, 0);
    bossName.setFont(window.font); bossName.setCharacterSize(40); bossName.setString(""); bossName.setPosition(0, 0);
    spellName.setFont(window.font); spellName.setCharacterSize(40); spellName.setString(""); spellName.setPosition(0, 0);
    timer.setFont(window.font); timer.setCharacterSize(40); timer.setString("0"); timer.setPosition(0, 0);
    fps.setFont(window.font); fps.setCharacterSize(40); fps.setString("0"); fps.setPosition(1800, 0);
    mode.setFont(window.font); mode.setCharacterSize(40); mode.setString("Normal"); mode.setPosition(1700, 87);

    movingText.setFont(window.font); movingText.setCharacterSize(40); movingText.setString(""); movingText.setPosition(0, 0);
    
    strMode[0] = "Easy";
    strMode[1] = "Normal";
    strMode[2] = "Hard";
    strMode[3] = "Feeric";
    strMode[4] = "Extra";
}

TextEn::~TextEn()
{

}

int TextEn::getHeight(int sizeText, std::string str)
{
    movingText.setCharacterSize(sizeText);
    movingText.setString(str);
    return movingText.getGlobalBounds().height;
}

int TextEn::getWidth(int sizeText, std::string str)
{
    movingText.setCharacterSize(sizeText);
    movingText.setString(str);
    return movingText.getGlobalBounds().width;
}

void TextEn::write(int characterSize, int posx, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::string str)
{
    movingText.setCharacterSize(characterSize);
    movingText.setString(str);
    movingText.setPosition(posx, posy);
    movingText.setColor(out);
    double angle = 0.0;
    double pi = atan(1)*4;
    std::vector<double> vectInit;
    std::vector<double> posText;
    vectInit.push_back(double(thickness));
    vectInit.push_back(0.0);
    for (int i = 0; i < nbDraw; i++)
    {
        angle = i*2.0*pi/double(nbDraw);
        posText = rotation(vectInit, angle);
        movingText.move(posText[0], posText[1]);
        window.mainWindow.draw(movingText);
        movingText.move(-posText[0], -posText[1]);
    }
    movingText.setColor(in);
    window.mainWindow.draw(movingText);
}

void TextEn::writeCenterx(int characterSize, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::string str)
{
    movingText.setCharacterSize(characterSize);
    movingText.setString(str);
    movingText.setPosition((1920 - movingText.getLocalBounds().width)/2, posy);
    movingText.setColor(out);
    double angle = 0.0;
    double pi = atan(1)*4;
    std::vector<double> vectInit;
    std::vector<double> posText;
    vectInit.push_back(double(thickness));
    vectInit.push_back(0.0);
    for (int i = 0; i < nbDraw; i++)
    {
        angle = i*2.0*pi/double(nbDraw);
        posText = rotation(vectInit, angle);
        movingText.move(posText[0], posText[1]);
        window.mainWindow.draw(movingText);
        movingText.move(-posText[0], -posText[1]);
    }
    movingText.setColor(in);
    window.mainWindow.draw(movingText);
}

void TextEn::writeCenterxDecal(int characterSize, int posx, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::string str)
{
    movingText.setCharacterSize(characterSize);
    movingText.setString(str);
    movingText.setPosition((1920 - movingText.getLocalBounds().width)/2, posy);
    movingText.move(posx, 0);
    movingText.setColor(out);
    double angle = 0.0;
    double pi = atan(1)*4;
    std::vector<double> vectInit;
    std::vector<double> posText;
    vectInit.push_back(double(thickness));
    vectInit.push_back(0.0);
    for (int i = 0; i < nbDraw; i++)
    {
        angle = i*2.0*pi/double(nbDraw);
        posText = rotation(vectInit, angle);
        movingText.move(posText[0], posText[1]);
        window.mainWindow.draw(movingText);
        movingText.move(-posText[0], -posText[1]);
    }
    movingText.setColor(in);
    window.mainWindow.draw(movingText);
}

void TextEn::writeCrossLeftx(int characterSize, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::string str)
{
    movingText.setCharacterSize(characterSize);
    movingText.setString(str);
    movingText.setPosition(- movingText.getLocalBounds().width/2, posy);
    movingText.setColor(out);
    double angle = 0.0;
    double pi = atan(1)*4;
    std::vector<double> vectInit;
    std::vector<double> posText;
    vectInit.push_back(double(thickness));
    vectInit.push_back(0.0);
    for (int i = 0; i < nbDraw; i++)
    {
        angle = i*2.0*pi/double(nbDraw);
        posText = rotation(vectInit, angle);
        movingText.move(posText[0], posText[1]);
        window.mainWindow.draw(movingText);
        movingText.move(-posText[0], -posText[1]);
    }
    movingText.setColor(in);
    window.mainWindow.draw(movingText);
}

void TextEn::writeCrossLeftxDecal(int characterSize, int posx, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::string str)
{
    movingText.setCharacterSize(characterSize);
    movingText.setString(str);
    movingText.setPosition(- movingText.getLocalBounds().width/2, posy);
    movingText.move(posx, 0);
    movingText.setColor(out);
    double angle = 0.0;
    double pi = atan(1)*4;
    std::vector<double> vectInit;
    std::vector<double> posText;
    vectInit.push_back(double(thickness));
    vectInit.push_back(0.0);
    for (int i = 0; i < nbDraw; i++)
    {
        angle = i*2.0*pi/double(nbDraw);
        posText = rotation(vectInit, angle);
        movingText.move(posText[0], posText[1]);
        window.mainWindow.draw(movingText);
        movingText.move(-posText[0], -posText[1]);
    }
    movingText.setColor(in);
    window.mainWindow.draw(movingText);
}

void TextEn::writeCrossRightx(int characterSize, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::string str)
{
    movingText.setCharacterSize(characterSize);
    movingText.setString(str);
    movingText.setPosition(1920 - movingText.getLocalBounds().width/2, posy);
    movingText.setColor(out);
    double angle = 0.0;
    double pi = atan(1)*4;
    std::vector<double> vectInit;
    std::vector<double> posText;
    vectInit.push_back(double(thickness));
    vectInit.push_back(0.0);
    for (int i = 0; i < nbDraw; i++)
    {
        angle = i*2.0*pi/double(nbDraw);
        posText = rotation(vectInit, angle);
        movingText.move(posText[0], posText[1]);
        window.mainWindow.draw(movingText);
        movingText.move(-posText[0], -posText[1]);
    }
    movingText.setColor(in);
    window.mainWindow.draw(movingText);
}

void TextEn::writeCrossRightxDecal(int characterSize, int posx, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::string str)
{
    movingText.setCharacterSize(characterSize);
    movingText.setString(str);
    movingText.setPosition(1920 - movingText.getLocalBounds().width/2, posy);
    movingText.move(posx, 0);
    movingText.setColor(out);
    double angle = 0.0;
    double pi = atan(1)*4;
    std::vector<double> vectInit;
    std::vector<double> posText;
    vectInit.push_back(double(thickness));
    vectInit.push_back(0.0);
    for (int i = 0; i < nbDraw; i++)
    {
        angle = i*2.0*pi/double(nbDraw);
        posText = rotation(vectInit, angle);
        movingText.move(posText[0], posText[1]);
        window.mainWindow.draw(movingText);
        movingText.move(-posText[0], -posText[1]);
    }
    movingText.setColor(in);
    window.mainWindow.draw(movingText);
}

void TextEn::writeMovement(int posInitx, int posInity, int posEndx, int posEndy, int durationMovement, int watchMov,
                           int characterSize, double thickness, int nbDraw, sf::Color in, sf::Color out, std::string str,
                           int movement, bool acc, int alpha)
{
    
    movingText.setCharacterSize(characterSize);
    movingText.setString(str);
    int distance;
    if (movement == 0)
    {
        distance = posInitx - posEndx;
    }
    else
    {
        distance = posInity - posEndy;
    }
    
    double currentDistance;
    if (acc)
    {
        double speedInit = distance*2/double(durationMovement);
        currentDistance = speedInit*watchMov*(1.0 - watchMov/double(2*durationMovement));
    }
    else
    {
        double speed = distance/double(durationMovement);
        currentDistance = speed*watchMov;   
    }
    
    if (movement == 0)
    {
        movingText.setPosition(posInitx - currentDistance, posInity);
    }
    else
    {
        movingText.setPosition(posInitx, posInity - currentDistance);
    }
    
    movingText.setColor(out);
    double angle = 0.0;
    double pi = atan(1)*4;
    std::vector<double> vectInit;
    std::vector<double> posText;
    vectInit.push_back(double(thickness));
    vectInit.push_back(0.0);
    for (int i = 0; i < nbDraw; i++)
    {
        angle = i*2.0*pi/double(nbDraw);
        posText = rotation(vectInit, angle);
        movingText.move(posText[0], posText[1]);
        window.mainWindow.draw(movingText);
        movingText.move(-posText[0], -posText[1]);
    }
    movingText.setColor(in);
    window.mainWindow.draw(movingText);
    
    //===========================================================================
    /*
    int height = getHeight(characterSize, str);
    int width = getWidth(characterSize, str);
    sf::RenderTexture renderText;
    renderText.create(width + 2*thickness, height + 2*thickness);
    sf::Sprite spriteText;
    spriteText.setTexture(renderText.getTexture());
    
    movingText.setCharacterSize(characterSize);
    movingText.setString(str);
    int distance;
    if (movement == 0)
    {
        distance = posInitx - posEndx;
    }
    else
    {
        distance = posInity - posEndy;
    }
    
    double currentDistance;
    if (acc)
    {
        double speedInit = distance*2/double(durationMovement);
        currentDistance = speedInit*watchMov*(1.0 - watchMov/double(2*durationMovement));
    }
    else
    {
        double speed = distance/double(durationMovement);
        currentDistance = speed*watchMov;   
    }
    
    if (movement == 0)
    {
        spriteText.setPosition(posInitx - currentDistance, posInity);
    }
    else
    {
        spriteText.setPosition(posInitx, posInity - currentDistance);
    }
    
    movingText.setColor(out);
    double angle = 0.0;
    double pi = atan(1)*4;
    std::vector<double> vectInit;
    std::vector<double> posText;
    vectInit.push_back(double(thickness));
    vectInit.push_back(0.0);
    for (int i = 0; i < nbDraw; i++)
    {
        angle = i*2.0*pi/double(nbDraw);
        posText = rotation(vectInit, angle);
        movingText.move(posText[0], posText[1]);
        renderText.draw(movingText);
        movingText.move(-posText[0], -posText[1]);
    }
    movingText.setColor(in);
    renderText.draw(movingText);
    window.mainWindow.draw(spriteText);
    */
}

void TextEn::writeHud(sf::Text text, double thickness, int nbDraw, sf::Color in, sf::Color out)
{
    text.setColor(out);
    double angle = 0.0;
    double pi = atan(1)*4;
    std::vector<double> vectInit;
    std::vector<double> posText;
    vectInit.push_back(double(thickness));
    vectInit.push_back(0.0);
    for (int i = 0; i < nbDraw; i++)
    {
        angle = i*2.0*pi/double(nbDraw);
        posText = rotation(vectInit, angle);
        text.move(posText[0], posText[1]);
        window.mainWindow.draw(text);
        text.move(-posText[0], -posText[1]);
    }
    text.setColor(in);
    window.mainWindow.draw(text);
}

void TextEn::writeHudAlt(sf::Text text, double thickness, int nbDraw, sf::Color in, sf::Color out)
{
    text.setColor(out);
    double angle = 0.0;
    double pi = atan(1)*4;
    std::vector<double> vectInit;
    std::vector<double> posText;
    vectInit.push_back(double(thickness));
    vectInit.push_back(0.0);
    for (int i = 0; i < nbDraw; i++)
    {
        angle = i*2.0*pi/double(nbDraw);
        posText = rotation(vectInit, angle);
        text.move(posText[0], posText[1]);
        window.renderForeground.draw(text);
        text.move(-posText[0], -posText[1]);
    }
    text.setColor(in);
    window.renderForeground.draw(text);
}

void TextEn::draw()
{
    writeHud(fps, 2, 8, sf::Color::White, sf::Color::Black);
    writeHud(live, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHud(bomb, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHud(scoreName, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHud(hiScore, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHud(valScore, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHud(valHiScore, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHud(point, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHud(power, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHud(valPoint, 2, 8, sf::Color::White, sf::Color::Black);
    writeHud(valPower, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHud(mode, 2, 8, sf::Color::White, sf::Color::Black); 
}

void TextEn::drawAlt()
{
    writeHudAlt(fps, 2, 8, sf::Color::White, sf::Color::Black);
    writeHudAlt(live, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHudAlt(bomb, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHudAlt(scoreName, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHudAlt(hiScore, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHudAlt(valScore, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHudAlt(valHiScore, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHudAlt(point, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHudAlt(power, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHudAlt(valPoint, 2, 8, sf::Color::White, sf::Color::Black);
    writeHudAlt(valPower, 2, 8, sf::Color::White, sf::Color::Black); 
    writeHudAlt(mode, 2, 8, sf::Color::White, sf::Color::Black); 
}

void TextEn::update()
{
    valPoint.setString(toString(player.point, 0));
    valPower.setString(toString(player.power, 2));
    valScore.setString(toString(Score::currentScore, 0));
    valHiScore.setString(toString(Score::hiScore, 0));
}
