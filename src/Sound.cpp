/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Sound.h"
#include "../include/Setting.h"
#include <iostream>
//#include <sstream>
#include <fstream>
//#include <iomanip>

//Sound sound;

int Sound::watch;
//=======================================
//Sounds, musics and buffers
sf::Music Sound::musicMain;
sf::Music Sound::musicStage1;
sf::Music Sound::musicBoss1;
sf::Music Sound::musicStage2;
sf::Music Sound::musicBoss2;
sf::Music Sound::musicStage3;
sf::Music Sound::musicBoss3;
sf::Music Sound::musicStage4;
sf::Music Sound::musicBoss4;
sf::Music Sound::musicStage5;
sf::Music Sound::musicBoss5;
sf::Music Sound::musicStage6;
sf::Music Sound::musicBoss6;
sf::Music Sound::musicStageEx;
sf::Music Sound::musicBossEx;

sf::SoundBuffer Sound::bufferCursor;
sf::SoundBuffer Sound::bufferGo;
sf::SoundBuffer Sound::bufferExit;

sf::Sound Sound::soundGo;

sf::Sound Sound::arraySound [NB_SOUND];

Sound::Sound()
{
    /*
    //=================================================================
    //Sounds, musics and buffers
    musicMain.openFromFile("music/musicMain.wav");
    musicMain.setLoop(true);
    musicStage1.openFromFile("music/musicStage1.wav");
    //=================================================================
    
    
    watch = 0;
    BGMvolume = 0;
    SEvolume = 0;
    
    std::ifstream settingFile("setting.txt");
    if (settingFile)
    {
        settingFile.ignore();
        settingFile.ignore();
        settingFile.ignore();
        settingFile >> BGMvolume >> SEvolume;
    }
    else
    {
        BGMvolume = 10;
        SEvolume = 10;
    }
    settingFile.close();
    
    if (BGMvolume < 0)
    {
        BGMvolume = 0;
    }
    else if (BGMvolume > 10)
    {
        BGMvolume = 10;
    }
    if (SEvolume < 0)
    {
        SEvolume = 0;
    }
    else if (SEvolume > 10)
    {
        SEvolume = 10;
    }
    setVolumeMusic();
    setVolumeSound();
    
    //Sound::setVolumeMusic(intBGM*10);
    //Sound::setVolumeSound(intSE*10);
    */
    
}

Sound::~Sound()
{

}

void Sound::loadSound()
{
    watch = 0;
    musicMain.openFromFile("music/musicMain.wav");
    musicMain.setLoop(true);
    musicStage1.openFromFile("music/musicStage1.wav");
    musicBoss1.openFromFile("music/musicBoss1.wav");
    musicBoss1.setLoop(true);
    
    musicStage2.openFromFile("music/musicStage2.wav");
    musicStage2.setLoop(true);
    
    bufferCursor.loadFromFile("sound/soundCursor.wav");
    bufferGo.loadFromFile("sound/soundGo.wav");
    bufferExit.loadFromFile("sound/soundExit.wav");
}

void Sound::setVolumeMusic()
{
    musicMain.setVolume(Setting::BGMvolume*10);   
    musicStage1.setVolume(Setting::BGMvolume*10);
    musicBoss1.setVolume(Setting::BGMvolume*10);
    musicStage2.setVolume(Setting::BGMvolume*10);
}

void Sound::setVolumeSound()
{
    for (int i = 0; i < NB_SOUND; i++)
    {
        arraySound[i].setVolume(Setting::SEvolume*10);
    }
}

void Sound::destroySound()
{
    musicMain.~Music();
    musicStage1.~Music();
    musicBoss1.~Music();
    musicStage2.~Music();
    
    bufferCursor.~SoundBuffer();
    bufferGo.~SoundBuffer();
    bufferExit.~SoundBuffer();
}

void Sound::update()
{
    watch = watch + 1;
    //std::cout << "Sound watch = " << watch << std::endl;
}

void Sound::reset()
{
    watch = 0;   
}

void Sound::addSound(sf::SoundBuffer & buffer)
{
    for (int i = 0; i < NB_SOUND; i++)
    {
        if (arraySound[i].getStatus() == sf::SoundSource::Stopped)
        {
            arraySound[i].setBuffer(buffer);
            arraySound[i].play();
            return;
        }
    }
}
