/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Engine.h"
#include "../include/Window.h"
#include "../include/Player.h"
#include "../include/Ennemi.h"
#include "../include/Bullet.h"
#include "../include/Hud.h"
#include "../include/Cloud.h"
#include "../include/ItemPoint.h"
#include "../include/ItemPower.h"
#include "../include/Bonus.h"
#include "../include/ApparitionBullet.h"
#include "../include/TextEn.h"
#include "../include/Pause.h"
#include "../include/Score.h"
#include "../include/ParticleEnnemi.h"
#include "../include/Dialogue.h"
#include "../include/Sound.h"
#include "../include/Setting.h"
#include "../include/Trail.h"
#include "../include/Stage1Normal.h"
#include "../include/func.h"
#include <vector>
#include <SFML/Graphics.hpp>
//#include "SFML/Graphics.hpp"
#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>

extern Window window;
extern Player player;
extern std::vector<Ennemi> vectorEnnemi;
//extern std::vector<Bullet> vectorBullet;
extern Bullet arrayBullet [NB_BULLET];
extern Ennemi arrayEnnemi [NB_ENNEMI];
extern Bonus arrayBonus [NB_BONUS];
extern ApparitionBullet arrayApparition [NB_BULLET];
extern Hud hud;
extern Cloud cloud;
extern TextEn textEn;
extern ParticleEnnemi particleEnnemi;
extern Stage1Normal stage1Normal;

Engine engine;

Engine::Engine()
{
    for (int i = 0; i < window.nbEnnemi; i++)
    {
        vectorEnnemi.push_back(Ennemi());
    }
    watch = 0;
    IDstage = 0;
    running = true;
}

Engine::~Engine()
{

}

void Engine::update()
{
    //player.displayMotif("");
    arrayFps[watch % 60] = 1/clocK.restart().asSeconds();
    watch = watch + 1;
    //window.mainWindow.clear();
    /*
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
    {
        window.mainWindow.close();
    }
    */
    Sound::update();
    hud.update();
    player.update();
    Trail::add(player.posx, player.posy);
    cloud.update();
    Score::update();
    textEn.update();
    particleEnnemi.update();
    Trail::globalUpdate();
    ApparitionBullet::globalUpdate();
    Bonus::globalUpdate();
    //std::cout << Ennemi::nbEnnemi << std::endl;
    ItemPoint::globalUpdate();
    ItemPower::globalUpdate();
    for (int i = 0; i < NB_ENNEMI; i++)
    {
        arrayEnnemi[i].update(); 
    }
    for (int i = 0; i < NB_BULLET; i++)
    {
        arrayBullet[i].update(); 
    }
    Dialogue::update();
    Bullet::commonWatch++;
    //glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    if (watch % 60 == 0)
    {
        textEn.fps.setString(toString(getFps(), 2));
    }
    Pause::updateMoveTicker();
    if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) || sf::Keyboard::isKeyPressed(Setting::arrayKey[7])) && !Pause::moveTicker)
    {
        Pause::getFrame();
        Pause::update();
    }
    if (player.nbLife < 1)
    {
        Score::enterScore();   
    }

    
}

void Engine::draw()
{
    //window.mainWindow.clear(sf::Color(120, 255, 255));
    window.mainWindow.draw(window.spriteSky);
    cloud.draw();
    Trail::globalDraw();
    particleEnnemi.draw();
    player.drawForm();
    ApparitionBullet::globalDraw();
    Bonus::globalDraw();
    player.draw();
    for (int i = 0; i < NB_ENNEMI; i++)
    {
        arrayEnnemi[i].draw();
        arrayEnnemi[i].die();
    }
    ItemPoint::globalDraw();
    ItemPower::globalDraw();
    for (int i = 0; i < NB_BULLET; i++)
    {
        arrayBullet[i].draw(); 
    }
    particleEnnemi.draw2();
    Dialogue::draw();
    hud.draw();
    textEn.draw();
    if (Ennemi::bossBattle)
    {
        Ennemi::drawChrono();
    }
    window.mainWindow.display();
}

void Engine::start()
{
    while (running)
    {
        update(); 
    }
}

void Engine::addEnnemi(int newPosx, int newPosy, int newID, std::vector<std::vector<double> > newTrajectory, std::vector<std::vector<double> > newShot, int newHp, int newNbPoint, int newNbPower, bool newDebug)
{
    for (unsigned int i = 0; i < NB_BULLET; i++)
    {
        if (!arrayEnnemi[i].exist && !vectorEnnemi[i].watch)
        {
            arrayEnnemi[i].reset(newPosx, newPosy, newID, newTrajectory, newShot, newHp, newNbPoint, newNbPower, newDebug);
            return;
        }
    }
}

double Engine::getFps()
{
    double res = 0;
    for (int i = 0; i < 60; i++)
    {
        res = res + arrayFps[i]; 
    }
    return res/60.0;
    std::cout << res << std::endl;
}
