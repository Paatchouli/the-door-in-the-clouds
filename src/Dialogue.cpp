/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Dialogue.h"
#include "../include/Window.h"
#include "../include/Setting.h"
#include "../include/TextEn.h"
#include "../include/func.h"

extern Window window;
extern TextEn textEn;

int Dialogue::watch;
sf::Texture Dialogue::textureDialog;
sf::Sprite Dialogue::spriteDialog;
bool Dialogue::on;
int Dialogue::id;
int Dialogue::state;
int Dialogue::moveThicker;
std::vector<std::vector<std::__cxx11::string> > Dialogue::vectorDialogEn;
std::vector<std::vector<std::__cxx11::string> > Dialogue::vectorDialogFr;
std::vector<std::vector<sf::Color> > Dialogue::vectorColorIn;
std::vector<std::vector<sf::Color> > Dialogue::vectorColorOut;

Dialogue::Dialogue()
{
    /*
    id = 0;
    state = 0;
    textureDialog.loadFromFile("dialog.png");
    spriteDialog.setTexture(textureDialog);
    spriteDialog.setPosition(0, 1080);
    
    vectorDialogEn.resize(7);
    vectorDialogEn[0].resize(3);
    
    vectorDialogFr.resize(7);
    vectorDialogFr[0].resize(3);
    
    vectorColorIn.resize(7);
    vectorColorIn[0].resize(3);
    
    vectorColorOut.resize(7);
    vectorColorOut[0].resize(3);
    
    vectorDialogEn[0][0] = "A";
    vectorDialogEn[0][1] = "B";
    vectorDialogEn[0][2] = "C";
    
    vectorColorIn[0][0] = sf::Color::White;
    vectorColorIn[0][1] = sf::Color::Blue;
    vectorColorIn[0][2] = sf::Color::White;
    
    vectorColorOut[0][0] = sf::Color::Black;
    vectorColorOut[0][1] = sf::Color::Black;
    vectorColorOut[0][2] = sf::Color::Black;
    */
}

Dialogue::~Dialogue()
{

}

void Dialogue::set(int newId)
{
    watch = 0;
    on = true;
    state = 0;
    id = newId;
}

void Dialogue::update()
{
    if (!on)
    {
        return;   
    }
    watch = watch + 1;
    if (state == vectorDialogEn[id].size())
    {
        double speedInit = 300.0/DURATION_APPEARING;
        double currentDistance = speedInit*watch;
        spriteDialog.setPosition(60, 780 + currentDistance);
        spriteDialog.setColor(sf::Color(255, 255, 255, (DURATION_APPEARING - watch)*255.0/double(DURATION_APPEARING)));
        if (watch == DURATION_APPEARING)
        {
            on = false;   
        }
        return;   
    }
    if (watch < DURATION_APPEARING)
    {
        double speedInit = 2.0*300.0/DURATION_APPEARING;
        double currentDistance = speedInit*watch*(1.0 - watch/double(2*DURATION_APPEARING));
        spriteDialog.setPosition(60, 1080 - currentDistance);
        spriteDialog.setColor(sf::Color(255, 255, 255, watch*255.0/double(DURATION_APPEARING)));
        return;
    }
    if (moveThicker) {moveThicker = moveThicker - 1;}
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && !moveThicker)
    {
        moveThicker = 15;
        state = state + 1;
        if (state == vectorDialogEn[id].size())
        {
            watch = 0;   
        }
    }
}

void Dialogue::draw()
{
    if (!on)
    {
        return;   
    }
    window.mainWindow.draw(spriteDialog);
    if (watch > DURATION_APPEARING)
    {
        switch (Setting::language)
        {
            case 0:
                write(SIZE_DIALOG, POSITION_X_DIALOG, POSITION_Y_DIALOG, THICKNESS_DIALOG, NB_DRAW_DIALOG, vectorColorIn[id][state], vectorColorOut[id][state], vectorDialogEn[id][state]);
                break;
            case 1:
                write(SIZE_DIALOG, POSITION_X_DIALOG, POSITION_Y_DIALOG, THICKNESS_DIALOG, NB_DRAW_DIALOG, vectorColorIn[id][state], vectorColorOut[id][state], vectorDialogFr[id][state]);
                break;
        }
    }
}

void Dialogue::drawAlt()
{
    window.renderBackground.draw(spriteDialog);
}

void Dialogue::initialize()
{
    id = 0;
    state = 0;
    textureDialog.loadFromFile("dialog.png");
    spriteDialog.setTexture(textureDialog);
    spriteDialog.setPosition(0, 1080);
    
    vectorDialogEn.resize(7);
    vectorDialogEn[0].resize(3);
    
    vectorDialogFr.resize(7);
    vectorDialogFr[0].resize(3);
    
    vectorColorIn.resize(7);
    vectorColorIn[0].resize(3);
    
    vectorColorOut.resize(7);
    vectorColorOut[0].resize(3);
    
    vectorDialogEn[0][0] = "A";
    vectorDialogEn[0][1] = "B";
    vectorDialogEn[0][2] = "C";
    
    vectorColorIn[0][0] = sf::Color::White;
    vectorColorIn[0][1] = sf::Color::Blue;
    vectorColorIn[0][2] = sf::Color::White;
    
    vectorColorOut[0][0] = sf::Color::Black;
    vectorColorOut[0][1] = sf::Color::Black;
    vectorColorOut[0][2] = sf::Color::Black;
}


