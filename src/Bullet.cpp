/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Bullet.h"
#include <cmath>
#include "../include/Window.h"
#include "../include/Player.h"
#include "../include/Ennemi.h"
#include "../include/func.h"
#include <vector>
#include <SFML/Graphics.hpp>
//#include "SFML/Graphics.hpp"
#include <iostream>

extern Window window;
extern Player player;
extern Ennemi arrayEnnemi [NB_ENNEMI];
int Bullet::commonWatch = 0;

//std::vector<Bullet> vectorBullet(std::size_t(window.nbBullet));

Bullet arrayBullet [NB_BULLET];

Bullet::Bullet()
{
    exist = false;
    watch = 0;
}

Bullet::~Bullet()
{

}

void Bullet::draw()
{
    if (exist)
    {
        window.mainWindow.draw(vectorSprite[0]);
    }
}

void Bullet::drawAlt()
{
    if (exist)
    {
        window.renderBackground.draw(vectorSprite[0]);
    }
}

void Bullet::updateInstant()
{
    int nbTrajectory = trajectory.size();
    int nbShot = shot.size();
    for (unsigned int i = 0; i < nbTrajectory; i++)
    {
        if (watch == trajectory[i][0])
        {
            indexTrajectory = indexTrajectory + 1;
        }
    }
    for (unsigned int i = 0; i < nbShot; i++)
    {
        if (watch == shot[i][0])
        {
            indexShot = indexShot + 1;
        }
    }
    int IDtrajectory = trajectory[indexTrajectory][1];
    double IDshot = shot[indexShot][1];
    
    if (IDshot == 0.0)
    {
      
    }
    else if (IDshot == 1.0)
    {
        explosionShot();   
    }
    else if (IDshot == 2.0)
    {
        explosionShot2();   
    }
    
    switch (IDtrajectory)
    {
        case 0:
            straightTraj();
            break;
        case 1:
            varSpeedTraj();
            break;
        case 2:
            destinationTraj();
            break;
        case 3:
            attractionTraj();
            break;
        case 4:
            explosionTraj();
            break;
        case 5:
            destinationTraj2();
            break;
        case 6:
            squareSpiraleTraj();
            break;
        case 7:
            snakeTraj();
            break;
        case 8:
            varSpeedTraj2();
            break;
    }
    /*
    if (IDtrajectory == 0.0)
    {
        straightTraj();
    }
    else if (IDtrajectory == 1.0)
    {
        varSpeedTraj();
    }
    else if (IDtrajectory == 2.0)
    {
        destinationTraj();
    }
    else if (IDtrajectory == 3.0)
    {
        attractionTraj();
    }
    else if (IDtrajectory == 4.0)
    {
        explosionTraj();
    }
    else if (IDtrajectory == 5.0)
    {
        destinationTraj2();
    }
    else if (IDtrajectory == 6.0)
    {
        squareSpiraleTraj();
    }
    else if (IDtrajectory == 7.0)
    {
        snakeTraj();
    }
    */
}

void Bullet::move()
{
    int movex = int(speedx);
    int movey = int(speedy);
    double fx = fabs(speedx);
    double fy = fabs(speedy);
    double ix = fx - int(fx);
    double iy = fy - int(fy);
    int w;
    if (useStaticWatch)
    {
        w = commonWatch;   
    }
    else
    {
        w = watch;   
    }
    /*
    std::cout << "speedx = " << speedx << std::endl;
    std::cout << "speedy = " << speedy << std::endl;
    std::cout << "fx = " << fx << std::endl;
    std::cout << "fy = " << fy << std::endl;
    std::cout << "ix = " << ix << std::endl;
    std::cout << "iy = " << iy << std::endl;
    std::cout << "commonWatch * ix = " << commonWatch * ix << std::endl;
    std::cout << "(commonWatch - 1) * ix = " << (commonWatch - 1) * ix << std::endl;
    std::cout << "commonWatch * iy = " << commonWatch * iy << std::endl;
    std::cout << "(commonWatch - 1) * iy = " << (commonWatch - 1) * iy << std::endl;
    */
    if (int(w * ix) != int((w - 1) * ix))
    {
        if (speedx > 0)
        {
            movex = movex + 1;
        }
        else
        {
            movex = movex - 1;
        }
    }
    if (int(w * iy) != int((w - 1) * iy))
    {
        if (speedy > 0)
        {
            movey = movey + 1;
        }
        else
        {
            movey = movey - 1;
        }
    }
    posx = posx + movex;
    posy = posy + movey;
    posHitboxx = posHitboxx + movex;
    posHitboxy = posHitboxy + movey;
    for(int i = 0; i< nbSprite; ++i)
    {
        vectorSprite[i].move(movex, movey);
    }
}

void Bullet::move2()
{
    double movex = posInitx*(1.0 - coeffx) + posFinax*coeffx;
    double movey = posInity*(1.0 - coeffy) + posFinay*coeffy;
    posx = movex;
    posy = movey;
    /*
    std::cout << "posInitx = " << posInitx << std::endl;
    std::cout << "posInity = " << posInity << std::endl;
    std::cout << "posFinax = " << posFinax << std::endl;
    std::cout << "posFinay = " << posFinay << std::endl;
    std::cout << "coeffx = " << coeffx << std::endl;
    std::cout << "coeffy = " << coeffy << std::endl;
    std::cout << "posx = " << posx << std::endl;
    std::cout << "posy = " << posy << std::endl;
    std::cout << std::endl;
    */
    for(int i = 0; i< nbSprite; ++i)
    {
        vectorSprite[i].setPosition(movex, movey);
    }
    
}

void Bullet::move3()
{
    posx = speedx;
    posy = speedy;
    for(int i = 0; i< nbSprite; ++i)
    {
        vectorSprite[i].setPosition(speedx, speedy);
    }
}

void Bullet::update()
{
    if (exist)
    {
        /*
        for (int i = 0; i < trajectory.size(); i++)
        {
            for (int j = 0; j < trajectory[i].size(); j++)
            {
                std::cout << trajectory[i][j] << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
        */
        if (posx < window.posScreenx - width - 10 || posx > window.width + 10 || posy < window.posScreeny - height - 10|| posy > window.height + 10)
        {
            exist = false;
            return;
        }
        watch = watch + 1;
        updateInstant();
        if (trajectory[indexTrajectory][1] == 0 || trajectory[indexTrajectory][1] == 1 || trajectory[indexTrajectory][1] == 3 || trajectory[indexTrajectory][1] == 4)
        {
            move();
            if (fabs(speedx) + fabs(speedy) > 0.01)
            {
                std::vector<double> vectAngle;
                vectAngle.push_back(speedx);
                vectAngle.push_back(speedy);
                for(int i(0); i < nbSprite; ++i)
                {
                    vectorSprite[i].setRotation(- 180.0*getAngle(vectAngle)/3.1415);
                }
            }
        }
        else if (trajectory[indexTrajectory][1] == 2 || trajectory[indexTrajectory][1] == 5)
        {
            int initx = posx;
            int inity = posy;
            move2();
            if (fabs(posx - initx) + fabs(posy - inity) > 0.01)
            {
                std::vector<double> vectAngle;
                vectAngle.push_back(posx - initx);
                vectAngle.push_back(posy - inity);
                for(int i(0); i < nbSprite; ++i)
                {
                    vectorSprite[i].setRotation(- 180.0*getAngle(vectAngle)/3.1415);
                }
            }
        }
        else
        {
            int initx = posx;
            int inity = posy;
            move3();
            if (fabs(posx - inity) + fabs(posx - inity) > 0.01)
            {
                std::vector<double> vectAngle;
                vectAngle.push_back(posx - initx);
                vectAngle.push_back(posy - inity);
                for(int i(0); i < nbSprite; ++i)
                {
                    vectorSprite[i].setRotation(- 180.0*getAngle(vectAngle)/3.1415);
                }
            }
        }
        if (pow((posx - player.posx), 2) + pow((posy - player.posy), 2) < pow(sizeHitbox + player.sizeHitbox, 2))
        {
            exist = false;
            if (!player.recoveryTime)
            {
                player.lossLife();   
            }
        }
    }
}

void Bullet::addBullet(int newPosx, int newPosy, int newID, std::vector<std::vector<double> > newTrajectory, std::vector<std::vector<double> > newShot, bool useStatic)
{
    for (int i = 0; i < window.nbBullet; i++)
    {
        if (!arrayBullet[i].exist)
        {
            arrayBullet[i].reset(newPosx, newPosy, newID, newTrajectory, newShot, useStatic);
            break;
        }
    }
}

void Bullet::reset(int newPosx, int newPosy, int newID, std::vector<std::vector<double> > newTrajectory, std::vector<std::vector<double> > newShot, bool useStatic)
{
    posx = newPosx;
    posy = newPosy;
    useStaticWatch = useStatic;
   
    if (ID != newID)
    {
        ID = newID;
        vectorSprite.clear();
        switch (ID / 10)
        {
            case 1:  //Base bullet
                width = 31;
                height = 31;
                sizeHitbox = 12.0;
                nbSprite = 1;
                speedDisplay = 1;
                vectorSprite.resize(nbSprite);
                switch (ID % 10)
                {
                    case 1:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureBulletRed);
                        }
                        break;
                        
                    case 2:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureBulletOrange);
                        }
                        break;
                        
                    case 3:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureBulletYellow);
                        }
                        break;
                        
                    case 4:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureBulletGreen);
                        }
                        break;
                    
                    case 5:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureBulletBlue);
                        }
                        break;
                        
                    case 6:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureBulletIndigo);
                        }
                        break;
                        
                    case 7:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureBulletPurple);
                        }
                        break;
                        
                    case 8:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureBulletLithium);
                        }
                        break;
                        
                }
                for (int i = 0; i < nbSprite; i++)
                {
                    vectorSprite[i].setOrigin(15, 15);
                }
                break;
            case 2: //Double bullet
                width = 29;
                height = 29;
                sizeHitbox = 10.0;
                nbSprite = 1;
                speedDisplay = 1;
                vectorSprite.resize(nbSprite);
                switch (ID % 10)
                {
                    case 1:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureDoubleRed);
                        }
                        break;
                        
                    case 2:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureDoubleOrange);
                        }
                        break;
                        
                    case 3:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureDoubleYellow);
                        }
                        break;
                        
                    case 4:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureDoubleGreen);
                        }
                        break;
                    
                    case 5:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureDoubleBlue);
                        }
                        break;
                        
                    case 6:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureDoubleIndigo);
                        }
                        break;
                        
                    case 7:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureDoublePurple);
                        }
                        break;
                        
                    case 8:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureDoubleLithium);
                        }
                        break;
                }
                for (int i = 0; i < nbSprite; i++)
                {
                    vectorSprite[i].setOrigin(14, 14);
                }
                break;
            case 3: //Moon bullet
                width = 31;
                height = 31;
                sizeHitbox = 12.0;
                nbSprite = 1;
                speedDisplay = 1;
                vectorSprite.resize(nbSprite);
                switch (ID % 10)
                {
                    case 1:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureMoonRed);
                        }
                        break;
                        
                    case 2:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureMoonOrange);
                        }
                        break;
                        
                    case 3:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureMoonYellow);
                        }
                        break;
                        
                    case 4:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureMoonGreen);
                        }
                        break;
                    
                    case 5:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureMoonBlue);
                        }
                        break;
                        
                    case 6:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureMoonIndigo);
                        }
                        break;
                        
                    case 7:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureMoonPurple);
                        }
                        break;
                }
                for (int i = 0; i < nbSprite; i++)
                {
                    vectorSprite[i].setOrigin(15, 15);
                }
                break;
            case 4: //Triangle bullet
                width = 29;
                height = 29;
                sizeHitbox = 10.0;
                nbSprite = 1;
                speedDisplay = 1;
                vectorSprite.resize(nbSprite);
                switch (ID % 10)
                {
                    case 1:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureTriangleRed);
                        }
                        break;
                        
                    case 2:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureTriangleOrange);
                        }
                        break;
                        
                    case 3:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureTriangleYellow);
                        }
                        break;
                        
                    case 4:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureTriangleGreen);
                        }
                        break;
                    
                    case 5:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureTriangleBlue);
                        }
                        break;
                        
                    case 6:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureTriangleIndigo);
                        }
                        break;
                        
                    case 7:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureTrianglePurple);
                        }
                        break;
                        
                    case 8:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureTriangleLithium);
                        }
                        break;
                    
                }
                for (int i = 0; i < nbSprite; i++)
                {
                    vectorSprite[i].setOrigin(14, 14);
                }
                break;
            case 5: //Small bullet
                width = 15;
                height = 15;
                sizeHitbox = 5.0;
                nbSprite = 1;
                speedDisplay = 1;
                vectorSprite.resize(nbSprite);
                switch (ID % 10)
                {
                    case 1:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureSmallRed);
                        }
                        break;
                        
                    case 2:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureSmallOrange);
                        }
                        break;
                        
                    case 3:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureSmallYellow);
                        }
                        break;
                        
                    case 4:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureSmallGreen);
                        }
                        break;
                    
                    case 5:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureSmallBlue);
                        }
                        break;
                        
                    case 6:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureSmallIndigo);
                        }
                        break;
                        
                    case 7:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureSmallPurple);
                        }
                        break;
                    /*
                    case 8:
                        for (int i = 0; i < nbSprite; i++)
                        {
                            vectorSprite[i].setTexture(window.textureTriangleLithium);
                        }
                        break;
                    */
                    
                }
                for (int i = 0; i < nbSprite; i++)
                {
                    vectorSprite[i].setOrigin(6, 6);
                }
                break;
        }
    }
    
    for (int i = 0; i < nbSprite; i++)
    {
        vectorSprite[i].setPosition(newPosx, newPosy);   
    }
    
    trajectory = newTrajectory;
    shot = newShot;
    
    watch = 0;
    indexTrajectory = 0;
    indexShot = 0;
    
    exist = true;
}

/**
* trajectory[1] = 0;
* trajectory[2] : speedx
* trajectory[3] : speedy
*/
void Bullet::straightTraj()
{
    speedx = trajectory[indexTrajectory][2];
    speedy = trajectory[indexTrajectory][3];
}

/**
* trajectory[1] = 1;
* trajectory[2] : speedx
* trajectory[3] : speedy
* trajectory[4] : Norm speed init
* trajectory[5] : Norm speed final
* trajectory[6] : Duration variation speed
*/
void Bullet::varSpeedTraj()
{
    std::vector<double> speed;
    speed.push_back(trajectory[indexTrajectory][2]);
    speed.push_back(trajectory[indexTrajectory][3]);
    double coeff = std::min(double(watch - trajectory[indexTrajectory][0]), trajectory[indexTrajectory][6])/trajectory[indexTrajectory][6];
    double normSpeed = trajectory[indexTrajectory][4]*(1.0 - coeff) + trajectory[indexTrajectory][5]*coeff;
    speed = setNorm(speed, normSpeed);
    speedx = speed[0];
    speedy = speed[1];
}

/**
* trajectory[1] = 2;
* trajectory[2] : Duration movement
* trajectory[3] : Position init x
* trajectory[4] : Position init y
* trajectory[5] : Position final x
* trajectory[6] : Position final y
*/
void Bullet::destinationTraj()
{
    /*
    std::vector<double> distance;
    distance.push_back(trajectory[indexTrajectory][3] - trajectory[indexTrajectory][5]);
    distance.push_back(trajectory[indexTrajectory][4] - trajectory[indexTrajectory][6]);
    double normDist = getNorm(distance);
    double speedInit = normDist*2.0/trajectory[indexTrajectory][2];
    double speed = speedInit*(trajectory[indexTrajectory][2] - (watch - trajectory[indexTrajectory][0]))/trajectory[indexTrajectory][2];
    distance = setNorm(distance, speed);
    speedx = distance[0];
    speedy = distance[1];
    */
    double t = (watch - trajectory[indexTrajectory][0])/trajectory[indexTrajectory][2];
    coeffx = (t - t*t/2.0)*2.0;
    coeffy = coeffx;
    posInitx = trajectory[indexTrajectory][3];
    posInity = trajectory[indexTrajectory][4];
    posFinax = trajectory[indexTrajectory][5];
    posFinay = trajectory[indexTrajectory][6];
}

/**
* trajectory[1] = 3;
* trajectory[2] = Speed x;
* trajectory[3] = Speed y;
* trajectory[4] = Acceleration;
*/
void Bullet::attractionTraj()
{
    std::vector<double> distance;
    distance.push_back(player.posx - posx);
    distance.push_back(player.posy - posy);
    distance = setNorm(distance, trajectory[indexTrajectory][4]);
    trajectory[indexTrajectory][2] = trajectory[indexTrajectory][2] + distance[0];
    trajectory[indexTrajectory][3] = trajectory[indexTrajectory][3] + distance[1];
    speedx = trajectory[indexTrajectory][2];
    speedy = trajectory[indexTrajectory][3];
}

/**
* trajectory[1] = 4;
* trajectory[2] = Speed init distance 1;
* trajectory[3] = Part speed final;
* trajectory[4] = Duration desceleration;
* trajectory[5] = norm min;
* trajectory[6] = Speed init x;
* trajectory[7] = Speed init y;
*/
void Bullet::explosionTraj()
{
    if (watch == trajectory[indexTrajectory][0] + 1)
    {
        std::vector<double> distance(2);
        distance[0] = posx - arrayEnnemi[Ennemi::indexBoss].posx;
        distance[1] = posy - arrayEnnemi[Ennemi::indexBoss].posy;
        double norm = std::max(getNorm(distance), trajectory[indexTrajectory][5]);
        trajectory[indexTrajectory][6] = trajectory[indexTrajectory][2]*distance[0]/std::pow(norm, 3);
        trajectory[indexTrajectory][7] = trajectory[indexTrajectory][2]*distance[1]/std::pow(norm, 3);
    }
    double t = std::min(watch - trajectory[indexTrajectory][0], trajectory[indexTrajectory][4])/trajectory[indexTrajectory][4];
    double coeff = 1.0 - t + trajectory[indexTrajectory][3]*t;
    speedx = coeff*trajectory[indexTrajectory][6];
    speedy = coeff*trajectory[indexTrajectory][7];
}

/**
* trajectory[1] = 5;
* trajectory[2] : Duration movement
* trajectory[3] : Position init x
* trajectory[4] : Position init y
* trajectory[5] : Position final x
* trajectory[6] : Position final y
*/
void Bullet::destinationTraj2()
{
    double t = (watch - trajectory[indexTrajectory][0])/trajectory[indexTrajectory][2];
    coeffx = t;
    coeffy = coeffx;
    posInitx = trajectory[indexTrajectory][3];
    posInity = trajectory[indexTrajectory][4];
    posFinax = trajectory[indexTrajectory][5];
    posFinay = trajectory[indexTrajectory][6];
    /*
    std::cout << "Dest" << std::endl;
    std::cout << "posInitx = " << posInitx << std::endl;
    std::cout << "posInity = " << posInity << std::endl;
    std::cout << "posFinax = " << posFinax << std::endl;
    std::cout << "posFinay = " << posFinay << std::endl;
    std::cout << std::endl;
    */
}

/**
* trajectory[1] = 6;
* trajectory[2] : Duration movement one space
* trajectory[3] : Space
* trajectory[4] : Direction init
* trajectory[5] : Clock-wise
* trajectory[6] : Pos init x
* trajectory[7] : Pos init y
*/
void Bullet::squareSpiraleTraj()
{
    int duration = trajectory[indexTrajectory][2];
    int space = trajectory[indexTrajectory][3];
    int t = watch - trajectory[indexTrajectory][0];
    int T = t/duration;
    
    int direction = trajectory[indexTrajectory][4];
    int X = 0;
    int Y = 0;
    int it = 0;
    int lim = 1;
    
    for (int i = 0; i < T; i++)
    {
        it = it + 1;
        /**
         * 0 : R
         * 1 : D
         * 2 : L
         * 3 : U
         */
        if (direction == 0)
        {
            X = X + 1;
        }
        else if (direction == 1)
        {
            Y = Y + 1;
        }
        else if (direction == 2)
        {
            X = X - 1;   
        }
        else
        {
            Y = Y - 1;   
        }
        if (it == lim)
        {
            it = 0;
            lim = lim + 1;
            if (trajectory[indexTrajectory][5])
            {
                direction = (direction + 1) % 4;   
            }
            else
            {
                direction = (direction + 3) % 4; 
            }
        }
    }
    double coeff = (t % duration)/double(duration);   
    speedx = trajectory[indexTrajectory][6] + X*space;
    speedy = trajectory[indexTrajectory][7] + Y*space;
    if (direction == 0)
    {
        speedx = speedx + coeff*space;
    }
    else if (direction == 1)
    {
        speedy = speedy + coeff*space;
    }
    else if (direction == 2)
    {
        speedx = speedx - coeff*space;
    }
    else
    {
        speedy = speedy - coeff*space;
    }
}

/**
* trajectory[1] = 7;
* trajectory[2] : Duration movement one space
* trajectory[3] : Space
* trajectory[4] : Nb space y
* trajectory[5] : Iter x
* trajectory[6] : Iter y
* trajectory[7] : On x
* trajectory[8] : Going up
* trajectory[9] : x init
* trajectory[10] : y init
*/
void Bullet::snakeTraj()
{
    int duration = trajectory[indexTrajectory][2];
    int space = trajectory[indexTrajectory][3];
    int nbSpace = trajectory[indexTrajectory][4];
    int iterX = trajectory[indexTrajectory][5];
    int iterY = trajectory[indexTrajectory][6];
    int onX = trajectory[indexTrajectory][7];
    int goingUp = trajectory[indexTrajectory][8];
    int xInit = trajectory[indexTrajectory][9];
    int yInit = trajectory[indexTrajectory][10];
    int t = watch - trajectory[indexTrajectory][0];
    int T = t/duration;
    
    speedx = xInit;
    speedy = yInit;
    
    if (onX)
    {
        speedx = speedx - space*(t % duration)/double(duration);
        if (t % duration == duration - 1)
        {
            trajectory[indexTrajectory][9] = xInit - space;
            trajectory[indexTrajectory][7] = 0;
            trajectory[indexTrajectory][5] = iterX + 1;
        }
    }
    else
    {
        if (goingUp)
        {
            speedy = speedy - space*(t % duration)/double(duration);
        }
        else
        {
            speedy = speedy + space*(t % duration)/double(duration);
        }
        if (t % duration == duration - 1)
        {
            trajectory[indexTrajectory][6] = iterY + 1;
            if (goingUp)
            {
                trajectory[indexTrajectory][10] = yInit - space*(t % duration)/double(duration);
            }
            else
            {
                trajectory[indexTrajectory][10] = yInit + space*(t % duration)/double(duration);
            }
            if (iterY == nbSpace)
            {
                trajectory[indexTrajectory][6] = 0;
                trajectory[indexTrajectory][7] = 1;
                trajectory[indexTrajectory][8] = 1 - goingUp;
                
            }
        }
    }

}

/**
* trajectory[1] = 8;
* trajectory[2] : speedx
* trajectory[3] : speedy
* trajectory[4] : Norm speed init
* trajectory[5] : Norm speed final
* trajectory[6] : Duration variation speed
* trajectory[7] : Pos init x
* trajectory[8] : Pos init y
*/
void Bullet::varSpeedTraj2()
{
    double t = (watch - trajectory[indexTrajectory][0]);
    double d;
    if (t < trajectory[indexTrajectory][6])
    {
        double coeff = t/trajectory[indexTrajectory][6];
        double currentSpeed = trajectory[indexTrajectory][4]*coeff + trajectory[indexTrajectory][5]*(1.0 - coeff);
        d = t*(trajectory[indexTrajectory][4] + currentSpeed)/2.0;
    }
    else
    {
        d = trajectory[indexTrajectory][6]*(trajectory[indexTrajectory][4] + trajectory[indexTrajectory][5])/2.0 + (t - trajectory[indexTrajectory][6])*trajectory[indexTrajectory][5];
    }
    std::vector<double> speed;
    speed.push_back(trajectory[indexTrajectory][2]);
    speed.push_back(trajectory[indexTrajectory][3]);
    speed = setNorm(speed, d);
    speedx = trajectory[indexTrajectory][7] + speed[0];
    speedy = trajectory[indexTrajectory][8] + speed[1];
    //std::cout << "Speed x = " << speedx << std::endl;
    //std::cout << "Speed y = " << speedy << std::endl;
    //std::cout << std::endl;
}

/**
* shot[1] = 1
* shot[2] : Nb bullet
* shot[3] : Speed init max
* shot[4] : Speed init min
* shot[5] : Speed fina max
* shot[6] : Speed fina min
* shot[7] : Duration var speed max
* shot[8] : Duration var speed min
*/
void Bullet::explosionShot()
{
    if (watch == shot[indexShot][0])
    {
        std::vector<std::vector<double> > newTrajectory(1, std::vector<double>(7));
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 1.0;
        
        std::vector<double> distance(2);
        distance[0] = 1.0;
        distance[1] = 0.0;
        double coeff;
        
        for (int i = 0; i < shot[indexShot][2]; i++)
        {
            distance = rotation(distance, 2.0*3.1415*rand()/double(RAND_MAX));
            newTrajectory[0][2] = distance[0];
            newTrajectory[0][3] = distance[1];
            coeff = rand()/double(RAND_MAX);
            newTrajectory[0][4] = shot[indexShot][3]*coeff + shot[indexShot][4]*(1.0 - coeff);
            coeff = rand()/double(RAND_MAX);
            newTrajectory[0][5] = shot[indexShot][5]*coeff + shot[indexShot][6]*(1.0 - coeff);
            coeff = rand()/double(RAND_MAX);
            newTrajectory[0][6] = shot[indexShot][7]*coeff + shot[indexShot][8]*(1.0 - coeff);
            addBullet(posx, posy, 18, newTrajectory, newShot);
        }
    }
}

/**
* shot[1] = 1
* shot[2] : Nb bullet
* shot[3] : Speed init max
* shot[4] : Speed init min
* shot[5] : Speed fina max
* shot[6] : Speed fina min
* shot[7] : Duration var speed max
* shot[8] : Duration var speed min
* shot[9] : Radius
* shot[10] : Angle
* shot[11] : Watch trigger
*/
void Bullet::explosionShot2()
{
    std::vector<double> norm(2);
    norm[0] = posx - arrayEnnemi[Ennemi::indexBoss].posx;
    norm[1] = posy - arrayEnnemi[Ennemi::indexBoss].posy;
    if (getNorm(norm) > shot[indexShot][9])
    {
        std::vector<std::vector<double> > newTrajectory(1, std::vector<double>(7));
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 1.0;
        
        std::vector<double> distance(2);
        std::vector<double> newDistance(2);
        distance[0] = 1.0;
        distance[1] = 0.0;
        double coeff;
        double angle;
        
        for (int i = 0; i < shot[indexShot][2]; i++)
        {
            angle = shot[indexShot][10] - 3.1415/2.0 + 3.1415*rand()/double(RAND_MAX);          
            newDistance = rotation(distance, angle);
            newTrajectory[0][2] = newDistance[0];
            newTrajectory[0][3] = newDistance[1];
            coeff = rand()/double(RAND_MAX);
            newTrajectory[0][4] = shot[indexShot][3]*coeff + shot[indexShot][4]*(1.0 - coeff);
            coeff = rand()/double(RAND_MAX);
            newTrajectory[0][5] = shot[indexShot][5]*coeff + shot[indexShot][6]*(1.0 - coeff);
            coeff = rand()/double(RAND_MAX);
            newTrajectory[0][6] = shot[indexShot][7]*coeff + shot[indexShot][8]*(1.0 - coeff);
            addBullet(posx, posy, 18, newTrajectory, newShot);
        }
        watch = int(shot[indexShot][11]) - 1;
    }
}