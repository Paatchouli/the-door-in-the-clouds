/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Laser.h"
#include "../include/Window.h"

extern Window window;

Laser::Laser()
{
    exist = false;
}

Laser::~Laser()
{
    sprite.setTexture(texture);
}

void Laser::reset(int newTimeWarning, int newTimeDeployment, int newDuration, double newThickness, sf::Color newColor, int newX, int newY, double newAngle)
{
    exist = true;
    watch = 0;
    timeWarning = newTimeWarning;
    timeDeployment = newTimeDeployment;
    duration = newDuration;
    thickness = newThickness;
    color = newColor;
    x = newX;
    y = newY;
    angle = newAngle;
}

void Laser::update()
{
    if (exist)
    {
        watch = watch + 1;
    }
}

void Laser::draw()
{
    if (exist)
    {
        window.mainWindow.draw(sprite);
    }
}

void Laser::drawAlt()
{
    if (exist)
    {
        window.mainWindow.draw(sprite);
    }
}

void Laser::supprime()
{
    exist = false;
    texture.update(NULL);
    renderTexture.clear();
}


