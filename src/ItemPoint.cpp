/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/ItemPoint.h"
#include "../include/Window.h"
#include "../include/Player.h"
#include "../include/Score.h"
#include "../include/func.h"
#include <cmath>

extern Window window;
extern Player player;

sf::Texture ItemPoint::textureItem;
ItemPoint ItemPoint::arrayItemPoint [NB_ITEM_POINT];
bool ItemPoint::isInitialized(false);

ItemPoint::ItemPoint()
{
    if (!isInitialized)
    {
        initialize();
        isInitialized = true;
    }
    watch = 0;
    spritePoint.setTexture(textureItem);
    spritePoint.setOrigin(10, 10);
    exist = false;
}

ItemPoint::~ItemPoint()
{

}

void ItemPoint::draw()
{
    if (exist)
    {
        window.mainWindow.draw(spritePoint);
    }
}

void ItemPoint::drawAlt()
{
    if (exist)
    {
        window.renderBackground.draw(spritePoint);
    }
}

void ItemPoint::update()
{
    if (exist)
    {
        watch = watch + 1; 
        std::vector<double> distance;
        distance.push_back(player.posx - posx);
        distance.push_back(player.posy - posy);
        if (getNormCarre(distance) < 1000)
        {
            exist = false;
            Score::currentScore = Score::currentScore + (10000 + posx*10)*window.stageDifficulty;
            player.point = player.point + 1;
        }
        else if (getNormCarre(distance) < 10000)
        {
            distance = setNorm(distance, 20.0);
            posx = posx + distance[0];
            posy = posy + distance[1];
        }
        else
        {
            if (watch < 15)
            {
                posx = posx + directionx*(watch - 15);
                posy = posy + directiony*(watch - 15);
            }
            posx = posx - 5;
        }
        spritePoint.setPosition(posx, posy);
        if (posx < -30)
        {
            exist = false;
        } 
    }
}

void ItemPoint::reset(int newPosx, int newPosy)
{
    exist = true;
    watch = 0;
    double angle = (2.0*3.14*rand()/double(RAND_MAX))/4.0 - 3.14/4;
    directionx = cos(angle);
    directiony = sin(angle);
    posx = newPosx;
    posy = newPosy;
}

void ItemPoint::globalDraw()
{
    for (int i = 0; i < NB_ITEM_POINT; i++)
    {
        arrayItemPoint[i].draw();   
    }
}

void ItemPoint::globalDrawAlt()
{
    for (int i = 0; i < NB_ITEM_POINT; i++)
    {
        arrayItemPoint[i].drawAlt();   
    }
}

void ItemPoint::globalUpdate()
{
    for (int i = 0; i < NB_ITEM_POINT; i++)
    {
        arrayItemPoint[i].update();   
    }
}

void ItemPoint::addItem(int newPosx, int newPosy)
{
    for (int i = 0; i < NB_ITEM_POINT; i++)
    {
        if (!arrayItemPoint[i].exist)
        {
            arrayItemPoint[i].reset(newPosx, newPosy);
            return;
        }
    }
}

void ItemPoint::initialize()
{
    textureItem.loadFromFile("point.png");
}