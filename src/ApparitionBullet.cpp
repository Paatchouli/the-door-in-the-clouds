/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/ApparitionBullet.h"
#include "../include/Window.h"
#include <cmath>

extern Window window;

ApparitionBullet ApparitionBullet::arrayApparition[NB_APPARITION];

ApparitionBullet::ApparitionBullet()
{
    exist = false;
    spriteApparition.setFillColor(sf::Color(0, 0, 0, 0));
}

ApparitionBullet::~ApparitionBullet()
{

}

void ApparitionBullet::update()
{
    if (!exist)
    {
        return;   
    }
    watch = watch + 1;
    if (watch > DURATION_APPARITION)
    {
        exist = false;
    }
    double coeff;
    if (creation)
    {
        coeff = (DURATION_APPARITION - watch)/double(DURATION_APPARITION);
    }
    else
    {
        coeff = watch/double(DURATION_APPARITION);
    }
    spriteApparition.setRadius(std::sqrt(coeff)*RADIUS_MAX);
    spriteApparition.setOutlineThickness(1.0*RADIUS_MAX*(coeff - coeff*coeff));
    spriteApparition.setOutlineColor(sf::Color(color.r, color.g, color.b, 255*(1.0 - coeff)));
    spriteApparition.setOrigin(spriteApparition.getRadius(), spriteApparition.getRadius());
    //spriteApparition.setPosition(posx, posy);
}

void ApparitionBullet::draw()
{
    if (exist)
    {
        window.mainWindow.draw(spriteApparition);
    }
}

void ApparitionBullet::drawAlt()
{
    if (exist)
    {
        window.renderBackground.draw(spriteApparition);
    }
}

void ApparitionBullet::reset(int newPosx, int newPosy, bool newCreation, sf::Color newColor)
{
    watch = 0;
    exist = true;
    posx = newPosx;
    posy = newPosy;
    creation = newCreation;
    color = newColor;
    spriteApparition.setPosition(posx, posy);
}

void ApparitionBullet::addApparition(int newPosx, int newPosy, bool newCreation, int idBullet)
{
    sf::Color newColor;
    std::cout << "posx = " << newPosx << std::endl;
    std::cout << "posy " << newPosy << std::endl;
    std::cout << "id = " << idBullet << std::endl;
    switch (idBullet)
    {
        case 1:
            std::cout << "merde" << std::endl;
            newColor = sf::Color::Red;
            break;
        case 2:
            newColor = sf::Color(255, 127, 0);
            break;
        case 3:
            newColor = sf::Color::Yellow;
            break;
        case 4:
            newColor = sf::Color::Green;
            break;
        case 5:
            newColor = sf::Color::Blue;
            break;
        case 6:
            newColor = sf::Color(63, 0, 255);
            break;
        case 7:
            newColor = sf::Color(127, 0, 255);
            break;
        case 8:
            newColor = sf::Color(62, 80, 80);
            break;
        case 9:
            newColor = sf::Color(0, 0, 0);
            break;
    }
    for (int i = 0; i < NB_APPARITION; i++)
    {
        if (!arrayApparition[i].exist)
        {
            arrayApparition[i].reset(newPosx, newPosy, newCreation, newColor);
            return;
        }
    }
}

void ApparitionBullet::globalUpdate()
{
    for (int i = 0; i < NB_APPARITION; i++)
    {
        arrayApparition[i].update();
    }
}

void ApparitionBullet::globalDraw()
{
    for (int i = 0; i < NB_APPARITION; i++)
    {
        arrayApparition[i].draw();
    }
}

void ApparitionBullet::globalDrawAlt()
{
    for (int i = 0; i < NB_APPARITION; i++)
    {
        arrayApparition[i].drawAlt();
    }
}
