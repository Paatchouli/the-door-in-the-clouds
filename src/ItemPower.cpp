/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/ItemPower.h"
#include "../include/Window.h"
#include "../include/Player.h"
#include "../include/Score.h"
#include "../include/func.h"
#include <cmath>

extern Window window;
extern Player player;

sf::Texture ItemPower::textureItem;
ItemPower ItemPower::arrayItemPower [NB_ITEM_POWER];
bool ItemPower::isInitialized(false);

ItemPower::ItemPower()
{
    if (!isInitialized)
    {
        initialize();
        isInitialized = true;
    }
    watch = 0;
    spritePower.setTexture(textureItem);
    spritePower.setOrigin(10, 10);
    exist = false;
}

ItemPower::~ItemPower()
{

}

void ItemPower::draw()
{   
    if (exist)
    {
        window.mainWindow.draw(spritePower);
    }
}

void ItemPower::drawAlt()
{   
    if (exist)
    {
        window.renderBackground.draw(spritePower);
    }
}

void ItemPower::update()
{
    if (exist)
    {
        watch = watch + 1; 
        std::vector<double> distance;
        distance.push_back(player.posx - posx);
        distance.push_back(player.posy - posy);
        if (getNormCarre(distance) < 2000)
        {
            exist = false;
            Score::currentScore = Score::currentScore + 5000;
            player.power = player.power + 0.01;
        }
        else if (getNormCarre(distance) < 10000)
        {
            distance = setNorm(distance, 20.0);
            posx = posx + distance[0];
            posy = posy + distance[1];
        }
        else
        {
            if (watch < 15)
            {
                posx = posx + directionx*(watch - 15);
                posy = posy + directiony*(watch - 15);
            }
            posx = posx - 5;
        }
        spritePower.setPosition(posx, posy);
        if (posx < -30)
        {
            exist = false;   
        } 
    }
}

void ItemPower::reset(int newPosx, int newPosy)
{
    exist = true;
    watch = 0;
    double angle = (2.0*3.14*rand()/double(RAND_MAX))/4.0 - 3.14/4;
    directionx = cos(angle);
    directiony = sin(angle);
    posx = newPosx;
    posy = newPosy;
}

void ItemPower::globalDraw()
{
    for (int i = 0; i < NB_ITEM_POWER; i++)
    {
        arrayItemPower[i].draw();   
    }
}

void ItemPower::globalDrawAlt()
{
    for (int i = 0; i < NB_ITEM_POWER; i++)
    {
        arrayItemPower[i].drawAlt();   
    }
}

void ItemPower::globalUpdate()
{
    for (int i = 0; i < NB_ITEM_POWER; i++)
    {
        arrayItemPower[i].update();   
    }
}

void ItemPower::addItem(int newPosx, int newPosy)
{
    for (int i = 0; i < NB_ITEM_POWER; i++)
    {
        if (!arrayItemPower[i].exist)
        {
            arrayItemPower[i].reset(newPosx, newPosy);
            return;
        }
    }
}

void ItemPower::initialize()
{
    textureItem.loadFromFile("power.png");
}

