/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Stage2Normal.h"
#include "../include/Ennemi.h"
#include "../include/Window.h"
#include "../include/Engine.h"
#include "../include/Cloud.h"
#include "../include/Sound.h"
#include "../include/func.h"
#include <vector>
#include <cmath>
#include <SFML/Graphics.hpp>
#include <iostream>

extern Window window;
extern std::vector<Ennemi> vectorEnnemi;
extern Engine engine;
extern Cloud cloud;
extern Ennemi arrayEnnemi [NB_ENNEMI];

int Stage2Normal::watch;

Stage2Normal::Stage2Normal()
{

}

Stage2Normal::~Stage2Normal()
{

}

int  Stage2Normal::update()
{
    watch = 0;
    cloud.reset(-1.0, 0.0, 200, 500, 50, 100, 0.005);
    window.stageDifficulty = 2;
    Sound::musicMain.stop();
    Sound::musicStage2.play();
    
/**
* shot[1] = 17;
* shot[2] : Fire rate 1
* shot[3] : Fire rate 2
* shot[4] : Duration shoot
* shot[5] : Duration space
* shot[6] : Space
* shot[7] : Clock-wise
* shot[8] : Direction init
* shot[9] : Random direction
* shot[10] : Nb Bullet (space)
* shot[11] : Nb Bullet (speed)
* shot[12] : Speed bullet init 1
* shot[13] : Speed bullet final 1
* shot[14] : Duration var speed 1
* .
* .
* .
* shot[Nb bullet*3 (speed) + 9] : Speed bullet init 1
* shot[Nb bullet*3 (speed) + 10] : Speed bullet final 1
* shot[Nb bullet*3 (speed) + 11] : Duration var speed 1
* shot[Nb bullet*3 (speed) + 12] : Id bullet
*/

/**
* shot[1] = 18;
* shot[2] : Fire rate 1
* shot[3] : Fire rate 2
* shot[4] : Duration shot
* shot[5] : Duration space
* shot[6] : Space
* shot[7] : Nb space y
* shot[8] : Up first
* shot[9] : Id bullet
*/

    std::vector<std::vector<double> > stillTraj(1, std::vector<double>(4, 0));
    std::vector<std::vector<double> > testShot1(1, std::vector<double>(16));
    std::vector<std::vector<double> > testShot2(1, std::vector<double>(10));
    
    std::vector<std::vector<double> > traj1(3);
    std::vector<std::vector<double> > traj2a(3);
    std::vector<std::vector<double> > traj2b(3);
    std::vector<std::vector<double> > traj2c(3);
    std::vector<std::vector<double> > traj2d(3);
    std::vector<std::vector<double> > traj2e(3);
    std::vector<std::vector<double> > traj2f(3);
    std::vector<std::vector<double> > traj2g(3);
    std::vector<std::vector<double> > traj2h(3);
    
    std::vector<std::vector<double> > traj3a;
    std::vector<std::vector<double> > traj3b;
    std::vector<std::vector<double> > traj3c;
    std::vector<std::vector<double> > traj3d;
    std::vector<std::vector<double> > traj3e;
    std::vector<std::vector<double> > traj3f;
    std::vector<std::vector<double> > traj3g;
    std::vector<std::vector<double> > traj3h;
    std::vector<std::vector<double> > traj3i;
    std::vector<std::vector<double> > traj3j;
    std::vector<std::vector<double> > traj3k;
    
    std::vector<std::vector<double> > trajectoryCircular1(1);
    std::vector<std::vector<double> > trajectoryCircular2(1);
    
    std::vector<std::vector<double> > shot1(3); //Big spirale
    std::vector<std::vector<double> > shot2(3); //Static aimed
    std::vector<std::vector<double> > shot3a(3); //Snake
    std::vector<std::vector<double> > shot3b(3);
    std::vector<std::vector<double> > shot4(1); //Spreaded shot
    std::vector<std::vector<double> > shot5(3); //Small Spirale
    
    traj1[1] = std::vector<double>(4, 0);
    traj1[1][0] = 120;
    traj1[0].resize(7);
    traj1[0][0] = 0;
    traj1[0][1] = 1;
    traj1[0][2] = -1;
    traj1[0][3] = 0;
    traj1[0][4] = 15;
    traj1[0][5] = 0;
    traj1[0][6] = 120;
    traj1[2].resize(7);
    traj1[2][0] = 540;
    traj1[2][1] = 1;
    traj1[2][2] = 1;
    traj1[2][3] = 0;
    traj1[2][4] = 0;
    traj1[2][5] = 15;
    traj1[2][6] = 120;
    
    traj2a[1] = std::vector<double>(4, 0);
    traj2a[0].resize(7);
    traj2a[2].resize(7);
    traj2a[1][0] = 60;
    traj2a[0][0] = 0;
    traj2a[2][0] = 120;
    traj2a[0][1] = 1;
    traj2a[2][1] = 1;
    traj2a[0][2] = 0;
    traj2a[2][2] = 0;
    traj2a[0][3] = 0;
    traj2a[2][3] = 0;
    traj2a[0][4] = 10;
    traj2a[0][5] = 0;
    traj2a[0][6] = 60;
    traj2a[2][4] = 0;
    traj2a[2][5] = 10;
    traj2a[2][6] = 60;
    traj2b = traj2a;
    traj2c = traj2a;
    traj2d = traj2a;
    traj2e = traj2a;
    traj2f = traj2a;
    traj2g = traj2a;
    traj2h = traj2a;
    //=====================================
    traj2a[0][2] = 0;
    traj2a[0][3] = 1;
    traj2a[2][2] = -1;
    traj2a[2][3] = 0;
    
    traj2b[0][2] = -1;
    traj2b[0][3] = 1;
    traj2b[2][2] = 1;
    traj2b[2][3] = -1;
    
    traj2c[0][2] = -1;
    traj2c[0][3] = 1;
    traj2c[2][2] = -1;
    traj2c[2][3] = 0;
    
    traj2d[0][2] = -1;
    traj2d[0][3] = 0;
    traj2d[2][2] = 1;
    traj2d[2][3] = -1;
    
    traj2e[0][2] = -1;
    traj2e[0][3] = 0;
    traj2e[2][2] = 1;
    traj2e[2][3] = 1;
    
    traj2f[0][2] = -1;
    traj2f[0][3] = -1;
    traj2f[2][2] = -1;
    traj2f[2][3] = 0;
    
    traj2g[0][2] = -1;
    traj2g[0][3] = -1;
    traj2g[2][2] = 1;
    traj2g[2][3] = 1;
    
    traj2h[0][2] = 0;
    traj2h[0][3] = -1;
    traj2h[2][2] = -1;
    traj2h[2][3] = 0;
    //=====================================
    
    traj3a = createSimpleTraj(1950, 400, 1500, 400, 60, 180, 60, 0, -1, 12);
    traj3b = createSimpleTraj(1950, 800, 1500, 800, 60, 180, 60, 0, 1, 12);
    traj3c = createSimpleTraj(1950, 700, 1500, 700, 60, 180, 60, 0, -1, 12);
    traj3d = createSimpleTraj(1950, 600, 1300, 600, 60, 180, 60, 1, 0, 12);
    traj3e = createSimpleTraj(1950, 500, 1500, 500, 60, 180, 60, 0, -1, 12);
    traj3f = createSimpleTraj(1950, 800, 1500, 800, 60, 180, 60, 0, -1, 12);
    traj3g = createSimpleTraj(1950, 300, 1500, 300, 60, 180, 60, 0, -1, 12);
    traj3h = createSimpleTraj(1950, 600, 1500, 600, 60, 180, 60, 1, 0, 12);
    traj3i = createSimpleTraj(1950, 900, 1500, 900, 60, 180, 60, 0, 1, 12);
    traj3j = createSimpleTraj(1950, 375, 1300, 375, 60, 180, 60, 0, -1, 12);
    traj3k = createSimpleTraj(1950, 825, 1300, 825, 60, 180, 60, 0, 1, 12);
    
    trajectoryCircular1[0].resize(8);
    trajectoryCircular1[0][0] = 0.0;
    trajectoryCircular1[0][1] = 3.0;
    trajectoryCircular1[0][2] = 1890.0;
    trajectoryCircular1[0][3] = 600.0;
    trajectoryCircular1[0][4] = 400.0;
    trajectoryCircular1[0][5] = 300.0;
    trajectoryCircular1[0][6] = 75.0;
    trajectoryCircular1[0][7] = 1.0;
    
    trajectoryCircular2[0].resize(8);
    trajectoryCircular2[0][0] = 0.0;
    trajectoryCircular2[0][1] = 3.0;
    trajectoryCircular2[0][2] = 1890.0;
    trajectoryCircular2[0][3] = 600.0;
    trajectoryCircular2[0][4] = 350.0;
    trajectoryCircular2[0][5] = 300.0;
    trajectoryCircular2[0][6] = 225.0;
    trajectoryCircular2[0][7] = 0.0;
    
    shot1[0] = std::vector<double>(2, 0);
    shot1[2] = shot1[0];
    shot1[2][0] = 540;
    shot1[1].resize(17);
    shot1[1][0] = 120;
    shot1[1][1] = 17;
    shot1[1][2] = 180; //Fire rate 1
    shot1[1][3] = 1;  //Fire rate 2
    shot1[1][4] = 0; //Offset
    shot1[1][5] = 120; //Duration shoot
    shot1[1][6] = 1;   //Duration space
    shot1[1][7] = 60;  //Space
    shot1[1][8] = 1;   //Clock-wise
    shot1[1][9] = 2;   //Direction init 0R1D2L3U
    shot1[1][10] = 0;   //Random direction
    shot1[1][11] = 4;  //Nb bullet space
    shot1[1][12] = 1;  //Nb bullet speed
    shot1[1][13] = 12; //Speed init
    shot1[1][14] = 4;  //Speed end
    shot1[1][15] = 5;  //Duration var speed
    shot1[1][16] = 21; //Bullet id
    
    shot2[0] = std::vector<double>(2, 0);
    shot2[2] = shot2[0];
    shot2[2][0] = 120;
    shot2[1].resize(12);
    shot2[1][0] = 60;
    shot2[1][1] = 19;
    shot2[1][2] = 2;
    shot2[1][3] = 0;
    shot2[1][4] = 0;
    shot2[1][5] = 1;
    shot2[1][6] = 1;
    shot2[1][7] = 16;
    shot2[1][8] = 6;
    shot2[1][9] = 5;
    shot2[1][10] = 1;
    shot2[1][11] = 41;
    
    shot3a[0].resize(2);
    shot3a[0][0] = 0;
    shot3a[0][1] = 0;
    shot3a[2].resize(2);
    shot3a[2][0] = 240;
    shot3a[2][1] = 0;
    shot3a[1].resize(11);
    shot3a[1][0] = 60;
    shot3a[1][1] = 18;
    shot3a[1][2] = 180;
    shot3a[1][3] = 1;
    shot3a[1][4] = 120;
    shot3a[1][5] = 180;
    shot3a[1][6] = 3;
    shot3a[1][7] = 50;
    shot3a[1][8] = 1;
    shot3a[1][9] = 0;
    shot3a[1][10] = 44;
    
    shot3b = shot3a;
    shot3b[1][9] = 1;
    
    shot4[0].resize(10);
    shot4[0][0] = 0;
    shot4[0][1] = 2;
    shot4[0][2] = 30;
    shot4[0][3] = 12;
    shot4[0][4] = 1;
    shot4[0][5] = 16;
    shot4[0][6] = 4;
    shot4[0][7] = 5;
    shot4[0][8] = 0;
    shot4[0][9] = 14;
    
    shot5[0] = std::vector<double>(2, 0);
    shot5[2] = shot5[0];
    shot5[2][0] = 180;
    shot5[1].resize(17);
    shot5[1][0] = 60;
    shot5[1][1] = 17;
    shot5[1][2] = 60; //Fire rate 1
    shot5[1][3] = 1;  //Fire rate 2
    shot5[1][4] = 0; //Offset
    shot5[1][5] = 60; //Duration shoot
    shot5[1][6] = 1;   //Duration space
    shot5[1][7] = 30;  //Space
    shot5[1][8] = 1;   //Clock-wise
    shot5[1][9] = 2;   //Direction init 0R1D2L3U
    shot5[1][10] = 0;   //Random direction
    shot5[1][11] = 4;  //Nb bullet space
    shot5[1][12] = 1;  //Nb bullet speed
    shot5[1][13] = 12; //Speed init
    shot5[1][14] = 4;  //Speed end
    shot5[1][15] = 5;  //Duration var speed
    shot5[1][16] = 21; //Bullet id
    
    testShot1[0][0] = 0;
    testShot1[0][1] = 17;
    testShot1[0][2] = 180;
    testShot1[0][3] = 15;
    testShot1[0][4] = 120;
    testShot1[0][5] = 5;
    testShot1[0][6] = 50;
    testShot1[0][7] = 1;
    testShot1[0][8] = 0;
    testShot1[0][9] = 0;
    testShot1[0][10] = 1;
    testShot1[0][11] = 1;
    testShot1[0][12] = 12;
    testShot1[0][13] = 4;
    testShot1[0][14] = 5;
    testShot1[0][15] = 11;
    
    testShot2[0][0] = 0;
    testShot2[0][1] = 18;
    testShot2[0][2] = 180;
    testShot2[0][3] = 5;
    testShot2[0][4] = 120;
    testShot2[0][5] = 10;
    testShot2[0][6] = 30;
    testShot2[0][7] = 6;
    testShot2[0][8] = 1;
    testShot2[0][9] = 11;
    while (true)
    {
        watch = watch + 1;
        if (cloud.watch < 820)
        {
            cloud.speedx = 5;
            cloud.speedy = 0;
        }
        else if (cloud.watch < 925)
        {
            double t = (cloud.watch - 820)/double(925 - 820);
            cloud.speedx = 5 - 4*t;
            cloud.speedy = 20*t;
        }
        else
        {
            cloud.speedx = 1;
            cloud.speedy = 20;
        }
        engine.update();
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) || sf::Keyboard::isKeyPressed(sf::Keyboard::A))
        {
            Sound::destroySound();
            window.mainWindow.close();
            return 0;
        }
        
        if (watch == 60)
        {
            engine.addEnnemi(1940, 600, 1, traj1, shot1, 50, 5, 5);
        }
        
        if (watch == 315)
        {
            engine.addEnnemi(1100, 100, 1, traj2a, shot2, 10, 5, 5);
        }
        if (watch == 330)
        {
            engine.addEnnemi(1700, 100, 1, traj2b, shot2, 10, 5, 5);
        }
        if (watch == 345)
        {
            engine.addEnnemi(1940, 250, 1, traj2c, shot2, 10, 5, 5);
        }
        if (watch == 360)
        {
            engine.addEnnemi(1940, 450, 1, traj2d, shot2, 10, 5, 5);
        }
        if (watch == 375)
        {
            engine.addEnnemi(1940, 750, 1, traj2e, shot2, 10, 5, 5);
        }
        if (watch == 390)
        {
            engine.addEnnemi(1940, 950, 1, traj2f, shot2, 10, 5, 5);
        }
        if (watch == 405)
        {
            engine.addEnnemi(1700, 1100, 1, traj2g, shot2, 10, 5, 5);
        }
        if (watch == 420)
        {
            engine.addEnnemi(1100, 1100, 1, traj2h, shot2, 10, 5, 5);
        }
        
        if (watch == 920)
        {
            engine.addEnnemi(1100, 1100, 1, traj3a, shot3a, 10, 5, 5);
        }
        if (watch == 1020)
        {
            engine.addEnnemi(1100, 1100, 1, traj3b, shot3b, 10, 5, 5);
        }
        if (watch == 1130)
        {
            engine.addEnnemi(1100, 1100, 1, traj3c, shot3b, 10, 5, 5);
        }
        
        if (watch == 1180)
        {
            engine.addEnnemi(1100, 1100, 1, traj3d, shot5, 10, 5, 5);
        }
        
        if (watch == 1230)
        {
            engine.addEnnemi(1100, 1100, 1, trajectoryCircular1, shot4, 10, 5, 5);
        }
        if (watch == 1245)
        {
            engine.addEnnemi(1100, 1100, 1, trajectoryCircular1, shot4, 10, 5, 5);
        }
        if (watch == 1260)
        {
            engine.addEnnemi(1100, 1100, 1, trajectoryCircular1, shot4, 10, 5, 5);
        }
        if (watch == 1275)
        {
            engine.addEnnemi(1100, 1100, 1, trajectoryCircular1, shot4, 10, 5, 5);
        }
        if (watch == 1290)
        {
            engine.addEnnemi(1100, 1100, 1, trajectoryCircular1, shot4, 10, 5, 5);
        }
        if (watch == 1305)
        {
            engine.addEnnemi(1100, 1100, 1, trajectoryCircular1, shot4, 10, 5, 5);
        }
        
        if (watch == 1330)
        {
            engine.addEnnemi(1100, 1100, 1, traj3e, shot3a, 10, 5, 5);
            engine.addEnnemi(1100, 1100, 1, traj3f, shot3b, 10, 5, 5);
        }
        if (watch == 1440)
        {
            engine.addEnnemi(1100, 1100, 1, traj3g, shot3a, 10, 5, 5);
            engine.addEnnemi(1100, 1100, 1, traj3h, shot3a, 10, 5, 5);
            engine.addEnnemi(1100, 1100, 1, traj3i, shot3b, 10, 5, 5);
        }
        
        if (watch == 1540)
        {
            engine.addEnnemi(1100, 1100, 1, trajectoryCircular2, shot4, 10, 5, 5);
        }
        if (watch == 1555)
        {
            engine.addEnnemi(1100, 1100, 1, trajectoryCircular2, shot4, 10, 5, 5);
        }
        if (watch == 1570)
        {
            engine.addEnnemi(1100, 1100, 1, trajectoryCircular2, shot4, 10, 5, 5);
        }
        if (watch == 1585)
        {
            engine.addEnnemi(1100, 1100, 1, trajectoryCircular2, shot4, 10, 5, 5);
        }
        if (watch == 1600)
        {
            engine.addEnnemi(1100, 1100, 1, trajectoryCircular2, shot4, 10, 5, 5);
        }
        if (watch == 1615)
        {
            engine.addEnnemi(1100, 1100, 1, trajectoryCircular2, shot4, 10, 5, 5);
        }
        
        if (watch == 1590)
        {
            engine.addEnnemi(1100, 1100, 1, traj3j, shot5, 10, 5, 5);
            engine.addEnnemi(1100, 1100, 1, traj3k, shot5, 10, 5, 5);
        }
        
        if (watch == 1750)
        {
            engine.addEnnemi(1100, 1100, 1, traj3a, shot3a, 10, 5, 5);
        }
        if (watch == 1850)
        {
            engine.addEnnemi(1100, 1100, 1, traj3b, shot3b, 10, 5, 5);
        }
        
        engine.draw();
    }
}