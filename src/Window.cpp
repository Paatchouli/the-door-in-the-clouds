/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Window.h"
#include <vector>
#include <SFML/Graphics.hpp>
//#include "../include/SFML/Graphics.hpp"
#include <iostream>
#include <SFML/OpenGL.hpp>
#include <GL/gl.h>
#include <GL/glu.h>

#include <sstream>
#include <fstream>
#include <iomanip>

#include "../include/Cloud.h"
#include "../include/Player.h"
#include "../include/Ennemi.h"
#include "../include/ItemPoint.h"
#include "../include/ItemPower.h"
#include "../include/Bullet.h"
#include "../include/Bonus.h"
#include "../include/ApparitionBullet.h"
#include "../include/Hud.h"
#include "../include/TextEn.h"
#include "../include/ParticleEnnemi.h"
#include "../include/Trail.h"

extern Cloud cloud;
extern Player player;
extern Ennemi arrayEnnemi [NB_ENNEMI];
extern ItemPoint arrayItemPoint [NB_ITEM_POINT];
extern ItemPower arrayItemPower [NB_ITEM_POWER];
extern Bullet arrayBullet [NB_BULLET];
extern Hud hud;
extern TextEn textEn;
extern ParticleEnnemi particleEnnemi;

sf::Texture Window::staticTextureGame;
sf::Sprite Window::staticSpriteGame;

Window window;

Window::Window()
{
    settings.depthBits = 0;
    settings.stencilBits = 0;
    settings.antialiasingLevel = 0;
    settings.majorVersion = 3;
    settings.minorVersion = 0;
    //RenderState.SourceBlend = Blend.One; 
    //RenderState.DestinationBlend = Blend.InverseSourceAlpha; 
    
    //glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    
    width = 1920;
    height = 1080;
    posScreenx = 30;
    posScreeny = 150;
    widthScreen = 1860;
    heightScreen = 900;
    mainWindow.create(sf::VideoMode(width, height, 32), "Forms'n'Fairies : The Door in the Clouds", sf::Style::Fullscreen, settings); //sf::Style::Fullscreen a la place de sf::Style::Close pour plein ecran
    //mainWindow.create(sf::VideoMode(width, height, 32), "SFML work!", sf::Style::Close, settings); //sf::Style::Fullscreen a la place de sf::Style::Close pour plein ecran
    //wtf ? En changent le titre de la fenetre, j'ai change le bug (c'est le &)
    mainWindow.create(sf::VideoMode(width, height, 32), "Forms'n'Fairies : The Door in the Clouds", sf::Style::Fullscreen, settings); //To be sure to have fullscreen
    mainWindow.setActive();
    mainWindow.setFramerateLimit(60);
    mainWindow.setMouseCursorVisible(false);
    backgroundColor = sf::Color(0,0,0);
    font.loadFromFile("OpenSans-Regular.ttf");
    vectorTexturePlayer.resize(1);
    vectorTextureEnnemi.resize(1);
    /*
    vectorTexturePlayer[0].loadFromFile("rond_bleu.png");
    vectorTextureEnnemi[0].loadFromFile("rond_rouge.png");
    */
    
    textureMenu.loadFromFile("ciel2.png");
    spriteMenu.setTexture(textureMenu);
    
    textureTitlePrimary.loadFromFile("titlePrimary.png");
    textureTitlePrimary.setSmooth(true);
    spriteTitlePrimary.setTexture(textureTitlePrimary);
    spriteTitlePrimary.setScale(0.5, 0.5);
    
    textureTitleSecondary.loadFromFile("titleSecondary.png");
    textureTitleSecondary.setSmooth(true);
    spriteTitleSecondary.setTexture(textureTitleSecondary);
    spriteTitleSecondary.setScale(0.5, 1.0);
    //textureTitleSecondary.loadFromFile(NULL);
    
    //textureSquare.loadFromFile("squareParticle.png"); spriteSquare.setTexture(textureSquare);
    textureEnnemi.loadFromFile("ennemi1.png"); spriteEnnemi.setTexture(textureEnnemi);
    
    textureBlueCloud.loadFromFile("nuage_bleu.png");
    /*
    
    textureRondRouge.loadFromFile("rond_rouge.png");
    spriteRondRouge.setTexture(textureRondRouge);
    spriteRondRouge.setPosition(30, 150);
    
    textureRondJaune.loadFromFile("rond_jaune.png");
    spriteRondJaune.setTexture(textureRondJaune);
    spriteRondJaune.setPosition(130, 150);
    
    textureRondVert.loadFromFile("rond_vert.png");
    spriteRondVert.setTexture(textureRondVert);
    spriteRondVert.setPosition(230, 150);
    
    textureRondCyan.loadFromFile("rond_cyan.png");
    spriteRondCyan.setTexture(textureRondCyan);
    spriteRondCyan.setPosition(330, 150);
    
    textureRondBleu.loadFromFile("rond_bleu.png");
    spriteRondBleu.setTexture(textureRondBleu);
    spriteRondBleu.setPosition(430, 150);
    
    textureRondMagenta.loadFromFile("rond_magenta.png");
    spriteRondMagenta.setTexture(textureRondMagenta);
    spriteRondMagenta.setPosition(530, 150);
    */
    
    textureLaserRed.loadFromFile("laser/small_laser_rouge2.png");
    textureLaserOrange.loadFromFile("laser/small_laser_orange2.png");
    textureLaserYellow.loadFromFile("laser/small_laser_jaune2.png");
    textureLaserGreen.loadFromFile("laser/small_laser_vert2.png");
    textureLaserBlue.loadFromFile("laser/small_laser_bleu2.png");
    textureLaserIndigo.loadFromFile("laser/small_laser_indigo2.png");
    textureLaserPurple.loadFromFile("laser/small_laser_violet2.png");
    
    spriteLaserRed.setTexture(textureLaserRed);
    spriteLaserOrange.setTexture(textureLaserOrange);
    spriteLaserYellow.setTexture(textureLaserYellow);
    spriteLaserGreen.setTexture(textureLaserGreen);
    spriteLaserBlue.setTexture(textureLaserBlue);
    spriteLaserIndigo.setTexture(textureLaserIndigo);
    spriteLaserPurple.setTexture(textureLaserPurple);
    
    //=========================================================================
    //Texture Bullet
    
    textureBulletRed.loadFromFile("baseBullet/redBullet.png");
    textureBulletOrange.loadFromFile("baseBullet/orangeBullet.png");
    textureBulletYellow.loadFromFile("baseBullet/yellowBullet.png");
    textureBulletGreen.loadFromFile("baseBullet/greenBullet.png");
    textureBulletBlue.loadFromFile("baseBullet/blueBullet.png");
    textureBulletIndigo.loadFromFile("baseBullet/indigoBullet.png");
    textureBulletPurple.loadFromFile("baseBullet/purpleBullet.png");
    textureBulletLithium.loadFromFile("baseBullet/lithiumBullet.png");
    
    textureDoubleRed.loadFromFile("doubleShoot/redDoubleShoot.png");
    textureDoubleOrange.loadFromFile("doubleShoot/orangeDoubleShoot.png");
    textureDoubleYellow.loadFromFile("doubleShoot/yellowDoubleShoot.png");
    textureDoubleGreen.loadFromFile("doubleShoot/greenDoubleShoot.png");
    textureDoubleBlue.loadFromFile("doubleShoot/blueDoubleShoot.png");
    textureDoubleIndigo.loadFromFile("doubleShoot/indigoDoubleShoot.png");
    textureDoublePurple.loadFromFile("doubleShoot/purpleDoubleShoot.png");
    textureDoubleLithium.loadFromFile("doubleShoot/lithiumDoubleShoot.png");
    
    textureMoonRed.loadFromFile("moon/redMoon.png");
    textureMoonOrange.loadFromFile("moon/orangeMoon.png");
    textureMoonYellow.loadFromFile("moon/yellowMoon.png");
    textureMoonGreen.loadFromFile("moon/greenMoon.png");
    textureMoonBlue.loadFromFile("moon/blueMoon.png");
    textureMoonIndigo.loadFromFile("moon/indigoMoon.png");
    textureMoonPurple.loadFromFile("moon/purpleMoon.png");
    
    textureTriangleRed.loadFromFile("triangle/redTriangle.png");
    textureTriangleOrange.loadFromFile("triangle/orangeTriangle.png");
    textureTriangleYellow.loadFromFile("triangle/yellowTriangle.png");
    textureTriangleGreen.loadFromFile("triangle/greenTriangle.png");
    textureTriangleBlue.loadFromFile("triangle/blueTriangle.png");
    textureTriangleIndigo.loadFromFile("triangle/indigoTriangle.png");
    textureTrianglePurple.loadFromFile("triangle/purpleTriangle.png");
    textureTriangleLithium.loadFromFile("triangle/lithiumTriangle.png");
    
    textureSmallRed.loadFromFile("smallBullet/redSmall.png");
    textureSmallOrange.loadFromFile("smallBullet/orangeSmall.png");
    textureSmallYellow.loadFromFile("smallBullet/yellowSmall.png");
    textureSmallGreen.loadFromFile("smallBullet/greenSmall.png");
    textureSmallBlue.loadFromFile("smallBullet/blueSmall.png");
    textureSmallIndigo.loadFromFile("smallBullet/indigoSmall.png");
    textureSmallPurple.loadFromFile("smallBullet/purpleSmall.png");
    //textureTriangleLithium.loadFromFile("smallBullet/lithiumSmall.png");
    //=========================================================================
    //Setting

    
    /*
    std::ifstream settingFile("setting.txt");
    if (settingFile)
    {
        settingFile >> nbLifeInit >> nbBombInit;
    }
    else
    {
        std::ofstream settingFile("setting.txt", std::ios::out | std::ios::trunc);
        settingFile << 3 << " " << 3 << " " << 10 << " " << 10 << "\n" << "Left" << " " << "Right" << " " << "Up" << " " << "Down" << " " << "X" << " " << "LShift" << " " << "W" << " " << "P";
        nbLifeInit = 3;
        nbBombInit = 3;
    }
    settingFile.close();
    remapKey();
    
    
    if (nbLifeInit < 1)
    {
        nbLifeInit = 1;   
    }
    else if (nbLifeInit > 5)
    {
        nbLifeInit = 5;   
    }
    if (nbBombInit < 0)
    {
        nbBombInit = 0;   
    }
    else if (nbBombInit > 3)
    {
        nbBombInit = 3;   
    }
    */
    //=========================================================================
    
    textureFrame.create(1920, 1080);
    spriteFrame.setTexture(textureFrame);
    
    renderBackground.create(1920, 1080);
    renderForeground.create(1920, 1080);
    spriteBackground.setTexture(renderBackground.getTexture());
    spriteForeground.setTexture(renderForeground.getTexture());
    
    texturePause.loadFromFile("pauseCloud3.png");
    spritePause.setTexture(texturePause);
    
    sf::RenderTexture renderSky;
    renderSky.create(1920, 1080);
    sf::Vertex line [2];
    for (int i = 0; i < 1080; i++)
    {
        line[0].position = sf::Vector2f(0, i);
        line[1].position = sf::Vector2f(1919, i);
        line[0].color = sf::Color(127 + i*127.0/1080.0, 127, 255);
        line[1].color = sf::Color(127 + i*127.0/1080.0, 127, 255);
        renderSky.draw(line, 2, sf::LinesStrip);
    }
    renderSky.display();
    firstSky = renderSky.getTexture();
    
    textureSky.loadFromFile("sky.png");
    spriteSky.setTexture(firstSky);
    
    textureHp.loadFromFile("hp.png");
    
    textureBonus.loadFromFile("bonus2.png");
    
    //=========================================================================
    //Control key
    left = sf::Keyboard::Left;
    right = sf::Keyboard::Right;
    up = sf::Keyboard::Up;
    down = sf::Keyboard::Down;
    bomb = sf::Keyboard::X;
    slow = sf::Keyboard::LShift;
    fast = sf::Keyboard::W;
    pause = sf::Keyboard::P;
}

Window::~Window()
{

}

void Window::initialize()
{
    sf::RenderTexture render;
    render.create(1860, 900);
    render.clear(sf::Color(0,0,0,0));
    render.display();
    staticTextureGame = render.getTexture();
    staticSpriteGame.setTexture(staticTextureGame);
    staticSpriteGame.setPosition(30, 150);
}

void Window::setBackground()
{
    window.renderBackground.clear(sf::Color(120, 255, 255));
    renderBackground.draw(spriteSky);
    cloud.drawAlt();
    Trail::globalDrawAlt();
    particleEnnemi.drawAlt();
    player.drawFormAlt();
    ApparitionBullet::globalDrawAlt();
    Bonus::globalDrawAlt();
    player.drawAlt();
    
    for (int i = 0; i < NB_ENNEMI; i++)
    {
        arrayEnnemi[i].drawAlt();
        arrayEnnemi[i].dieAlt();
    }
    ItemPoint::globalDrawAlt();
    ItemPower::globalDrawAlt();
    for (int i = 0; i < NB_BULLET; i++)
    {
        arrayBullet[i].drawAlt(); 
    }
    particleEnnemi.drawAlt2();
    renderBackground.display();
}

void Window::setForeground()
{
    hud.drawAlt();
    textEn.drawAlt();
    renderForeground.display();
}

void Window::remapKey()
{
    std::ifstream settingFile("setting.txt");
    
    
    
    settingFile.close();
}