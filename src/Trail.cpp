/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Trail.h"
#include "../include/Window.h"

extern Window window;

Trail Trail::arrayTrail[180];

Trail::Trail()
{
    exist = false;
    sprite.setRadius(10);
    sprite.setOrigin(10, 10);
}

Trail::~Trail()
{

}

void Trail::reset(int newX, int newY)
{
    exist = true;
    watch = 180;
    x = newX - 5;
    y = newY - 10;
    sprite.setPosition(x, y);
}


void Trail::update()
{
    if (exist)
    {
        watch = watch - 1;
        sprite.setFillColor(sf::Color(0, 0, 255, 255.0*watch/180.0));
        if (watch == 0)
        {
            exist = false;   
        }
    }
}

void Trail::draw()
{
    if (exist)
    {
        window.mainWindow.draw(sprite);
    }
}

void Trail::drawAlt()
{
    if (exist)
    {
        window.renderBackground.draw(sprite);
    }
}

void Trail::add(int newX, int newY)
{
    for (int i = 0; i < 180; i++)
    {
        if (!arrayTrail[i].exist)
        {
            arrayTrail[i].reset(newX, newY);
            return;
        }
    }
}

void Trail::globalUpdate()
{
    for (int i = 0; i < 180; i++)
    {
        arrayTrail[i].update();
    }
}

void Trail::globalDraw()
{
    for (int i = 0; i < 180; i++)
    {
        arrayTrail[i].draw();
    }
}

void Trail::globalDrawAlt()
{
    for (int i = 0; i < 180; i++)
    {
        arrayTrail[i].drawAlt();
    }
}
void Trail::globalDraw2()
{

}

void Trail::globalDrawAlt2()
{

}
