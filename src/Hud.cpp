/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Hud.h"
#include "../include/Window.h"
#include "../include/Player.h"

extern Window window;
extern Player player;

Hud hud;

Hud::Hud()
{
    watch = 0;
    textureHud.loadFromFile("hud1-0.png");
    spriteHud.setTexture(textureHud);
    textureLife.loadFromFile("heart.png");
    for (int i = 0; i < 7; i++)
    {
        spriteLife[i].setTexture(textureLife);   
        spriteLife[i].setPosition(450 + 50*i, 20);
    }
}

Hud::~Hud()
{

}

void Hud::update()
{

}

void Hud::draw()
{
    window.mainWindow.draw(spriteHud);
    for (int i = 0; i < player.nbLife; i++)
    {
        window.mainWindow.draw(spriteLife[i]);   
    }
}

void Hud::drawAlt()
{
    window.renderForeground.draw(spriteHud);
    for (int i = 0; i < player.nbLife; i++)
    {
        window.renderForeground.draw(spriteLife[i]);   
    }
}
