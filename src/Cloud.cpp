/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Cloud.h"
#include "../include/Window.h"
#include <cmath>

extern Window window;

Cloud cloud;

Cloud::Cloud()
{
    textureCloudLeft.loadFromFile("cloudLeft.png");
    textureCloudMiddle.loadFromFile("cloudMiddle.png");
    textureCloudRight.loadFromFile("cloudRight.png");
    
    for (int i = 0; i < NB_CLOUD; i++)
    {
        spriteCloud[i][0].setTexture(textureCloudLeft);
        spriteCloud[i][1].setTexture(textureCloudMiddle);
        spriteCloud[i][2].setTexture(textureCloudRight);
        exist[i] = false;
    }
    
    textureCloud.loadFromFile("cloud.png");
}

Cloud::~Cloud()
{

}

void Cloud::reset(double newSpeedCloudx, double newSpeedCloudy, int newSizeMinx, int newSizeMaxx, int newSizeSmally, int newSizeBigy, double newProbaCloud) //This function is called at the beginning of a stage
{
    watch = 0;
    speedCloudx = newSpeedCloudx;
    speedCloudy = newSpeedCloudy;
    sizeMinx = newSizeMinx;
    sizeMaxx = newSizeMaxx;
    sizeSmally = newSizeSmally;
    sizeBigy = newSizeBigy;
    probaCloud = newProbaCloud;
    for (int i = 0; i < NB_CLOUD; i++)
    {
        exist[i] = false;   
    }
}

void Cloud::initialiseCloud(int newPosx, int newPosy, int newSizex, int newSizey, bool newSmall)
{
    for (int i = 0; i < NB_CLOUD; i++)
    {
        if (!exist[i])
        {
            spriteCloud[i][0].setScale(double(newSizey)/100.0, double(newSizey)/100.0);
            spriteCloud[i][2].setScale(double(newSizey)/100.0, double(newSizey)/100.0);
            spriteCloud[i][1].setScale(double(newSizex)/2000.0, double(newSizey)/100.0);
            int sizex1 = spriteCloud[i][0].getGlobalBounds().width;
            int sizex2 = spriteCloud[i][1].getGlobalBounds().width;
            spriteCloud[i][0].setPosition(newPosx, newPosy);
            spriteCloud[i][1].setPosition(newPosx + sizex1, newPosy);
            spriteCloud[i][2].setPosition(newPosx + sizex1 + sizex2, newPosy);
            posx[i] = newPosx;
            posy[i] = newPosy;
            exist[i] = true;
            isSmall[i] = newSmall;
            return;
        }
    }
}

void Cloud::createCloud() //This function use initialiseCloud
{
    double probaSmallCloud = rand()/double(RAND_MAX);
    bool small = probaSmallCloud > double(sizeSmally)/double(sizeSmally + sizeBigy);
    int sizex;
    int sizey;
    int lengthx;
    int lengthy;
    double size = rand()/double(RAND_MAX);
    if (small)
    {
        sizex = int((sizeMaxx*size + (1.0 - size)*sizeMinx)*sizeSmally/double(sizeBigy));
        sizey = sizeSmally;
        lengthx = sizex + sizeSmally;
        lengthy = sizeSmally;
    }
    else
    {
        sizex = int(sizeMaxx*size + (1.0 - size)*sizeMinx);
        sizey = sizeBigy;
        lengthx = sizex + sizeBigy;
        lengthy = sizeBigy;
    }
    int directionx = speedx + speedCloudx;
    int directiony = speedy + speedCloudy;
    double pente = fabs(double(directionx)/double(directiony + directionx));
    double proba = pente*double(lengthx)/double(lengthy + lengthx);
    double r = rand()/double(RAND_MAX);
    int newPosx;
    int newPosy;
    if (r > proba) //We draw the cloud on the Y-axis
    {
        if (directionx > 0)
        {
            newPosx = 1860 + 30;
            newPosy = (rand() % (900 + lengthy)) + 150 - lengthy;       
        }
        else
        {
            newPosx = 30 - lengthx;
            newPosy = (rand() % (900 + lengthy)) + 150 - lengthy;
        }
    }
    else //We draw the cloud on the X-axis
    {
        if (directiony > 0)
        {
            newPosx = (rand() % (1860 + lengthx)) + 30 - lengthy;
            newPosy = 900 + 150;
        }
        else
        {
            newPosx = (rand() % (1860 + lengthx)) + 30 - lengthy;
            newPosy = 150 - lengthy;
        }
    }
    initialiseCloud(newPosx, newPosy, sizex, sizey, small);
}

void Cloud::update()
{
    watch = watch + 1;
    double r;
    int speed = int(sqrt((speedx + speedCloudx)*(speedx + speedCloudx) + (speedy + speedCloudy)*(speedy + speedCloudy)));
    for (int i = 0; i < speed; i++)
    {
        r = rand()/double(RAND_MAX);
        if (r < probaCloud)
        {
            createCloud();   
        }
    }
    move();
}

void Cloud::move()
{
    for (int i = 0; i < NB_CLOUD; i++)
    {
        if (exist[i])
        {
            double scale = double(sizeSmally)/double(sizeBigy);
            if (isSmall[i])
            {
                spriteCloud[i][0].move((- speedx - speedCloudx)*scale, (- speedy - speedCloudy)*scale);
                spriteCloud[i][1].move((- speedx - speedCloudx)*scale, (- speedy - speedCloudy)*scale);
                spriteCloud[i][2].move((- speedx - speedCloudx)*scale, (- speedy - speedCloudy)*scale);
                posx[i] = posx[i] + (- speedx - speedCloudx)*scale;
                posy[i] = posy[i] + (- speedy - speedCloudy)*scale;
            }
            else
            {
                spriteCloud[i][0].move(- speedx - speedCloudx, - speedy - speedCloudy);
                spriteCloud[i][1].move(- speedx - speedCloudx, - speedy - speedCloudy);
                spriteCloud[i][2].move(- speedx - speedCloudx, - speedy - speedCloudy);
                posx[i] = posx[i] - speedx - speedCloudx;
                posy[i] = posy[i] - speedy - speedCloudy;
            }
            if (posx[i] < - sizeMaxx - sizeBigy || posx[i] > 1980 || posy[i] < sizeBigy - 200 || posy[i] > 1080)
            {
                exist[i] = false;   
            }
        }
    }
}

void Cloud::draw()
{
    for (int i = 0; i < NB_CLOUD; i++)
    {
        if (exist[i] && isSmall[i])
        {
            window.mainWindow.draw(spriteCloud[i][0]);
            window.mainWindow.draw(spriteCloud[i][1]);
            window.mainWindow.draw(spriteCloud[i][2]);
        }
    }
    for (int i = 0; i < NB_CLOUD; i++)
    {
        if (exist[i] && !isSmall[i])
        {
            window.mainWindow.draw(spriteCloud[i][0]);
            window.mainWindow.draw(spriteCloud[i][1]);
            window.mainWindow.draw(spriteCloud[i][2]);
        }
    }
}

void Cloud::drawAlt()
{
    for (int i = 0; i < NB_CLOUD; i++)
    {
        if (exist[i] && isSmall[i])
        {
            window.renderBackground.draw(spriteCloud[i][0]);
            window.renderBackground.draw(spriteCloud[i][1]);
            window.renderBackground.draw(spriteCloud[i][2]);
        }
    }
    for (int i = 0; i < NB_CLOUD; i++)
    {
        if (exist[i] && !isSmall[i])
        {
            window.renderBackground.draw(spriteCloud[i][0]);
            window.renderBackground.draw(spriteCloud[i][1]);
            window.renderBackground.draw(spriteCloud[i][2]);
        }
    }
}

void Cloud::reset2(std::vector<double> newVectorScale, std::vector<double> newVectorProba)
{
    watch = 0;
    nbScale = newVectorScale.size();
    vectorScale = newVectorScale;
    /*
    vectorProba.clear();
    double sum = 0.0;
    for (int i = 0; i < nbScale; i++)
    {
        sum = sum + vectorScale[0]/vectorScale[i];
    }
    vectorProba.push_back(1.0/sum);
    for (int i = 1; i < nbScale; i++)
    {
        vectorProba.push_back(vectorProba[0]*vectorScale[0]/vectorScale[i]);
    }
    */
    vectorProba = newVectorProba;
}

void Cloud::initialiseCloud2(int newPosx, int newPosy, int newSizex, int newSizey, int newScale)
{
    for (int i = 0; i < NB_CLOUD; i++)
    {
        if (!arrayExistCloud[i])
        {
            arrayExistCloud[i] = true;
            arrayScale[i] = newScale;
            arrayPosxCloud[i] = newPosx;
            arrayPosyCloud [i] = newPosy;
            
            double r = rand()/double(RAND_MAX);
            int nbSpirale = int(r*NB_SPIRALE*newSizex*newSizey/double(sizeMaxx*sizeMaxy));
            
            return;
        }
    }
}

void Cloud::createCloud2()
{
    //===================================================================
    //New scale
    double r = rand()/double(RAND_MAX);
    double proba = vectorProba[0];
    int index = 0;
    while (r > proba && index < nbScale - 1)
    {
        index = index + 1;
        proba = proba + vectorProba[index];
    }
    //===================================================================
    //New position
    r = rand()/double(RAND_MAX);
    int sizex = int(vectorScale[index]*(sizeMinx*r + sizeMaxx*(1.0 - r)));
    r = rand()/double(RAND_MAX);
    int sizey = int(sizex*vectorScale[index]*(sizeMiny*r + sizeMaxy*(1.0 - r)));
    
    int lengthx = 1860 + sizex;
    int lengthy = 900 + sizey;
    
    double pente = fabs(double(speedx)/double(speedy + speedx));
    double probaXaxis = pente*double(lengthx)/double(lengthy + lengthx);
    
    int newPosx;
    int newPosy;
    r = rand()/double(RAND_MAX);
    if (r > probaXaxis) //We draw the cloud on the Y-axis
    {
        if (speedx > 0)
        {
            newPosx = 1860 + 30;
        }
        else
        {
            newPosx = 30 - sizex;
        }
        newPosy = (rand() % (900 + sizey)) + 150 - sizey;
    }
    else
    {
        if (speedy > 0)
        {
            newPosy = 900 + 150;
        }
        else
        {
            newPosy = 150 - sizey;
        }
        newPosx = (rand() % (1860 + sizex)) + 30 - sizey;
    }
    initialiseCloud2(newPosx, newPosy, sizex, sizey, vectorScale[index]);
}

void Cloud::update2()
{
    double r;
    int speed = int(sqrt(speedx*speedx + speedy*speedy));
    for (int i = 0; i < speed; i++)
    {
        r = rand()/double(RAND_MAX);
        if (r < probaCloud)
        {
            createCloud2();   
        }
    }
    move();
}

void Cloud::move2()
{
    
}

void Cloud::draw2()
{
    
}

void Cloud::drawAlt2()
{
    
}
