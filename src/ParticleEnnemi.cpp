/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/ParticleEnnemi.h"
#include "../include/func.h"
#include "../include/Window.h"

extern Window window;

ParticleEnnemi particleEnnemi;

ParticleEnnemi::ParticleEnnemi()
{
    textureSquare.loadFromFile("particleRedSquare.png");
    textureTriangle.loadFromFile("particleRedTriangle.png");
    textureSparkle.loadFromFile("sparkle.png");
    for (int i = 0; i < NB_PARTICLE_FORM; i++)
    {
        arrayExist[i] = false;   
    }
    for (int i = 0; i < NB_PARTICLE_SPARKLE; i++)
    {
        arrayExistSparkle[i] = false;
        arraySpriteSparkle[i].setTexture(textureSparkle);
    }
    for (int i = 0; i < NB_PARTICLE_LINE; i++)
    {
        lightning[i].color = sf::Color(127, 255, 255);
    }
    timeLightning = 0;
    existLightning = false;
    frontLightning = false;
}

ParticleEnnemi::~ParticleEnnemi()
{

}

void ParticleEnnemi::reset(int centerx, int centery, int ennemiWatch, int id)
{
    std::vector<double> inVector;
    inVector.push_back(1.0);
    inVector.push_back(0.0);
    if (id == 1)
    {
        if (ennemiWatch % 5 == 0)
        {
            for (int i = 0; i < NB_PARTICLE_FORM; i++)
            {
                if (!arrayExist[i])
                {
                    arrayExist[i] = true;
                    arrayCenterx[i] = centerx;
                    arrayCentery[i] = centery;
                    arrayTime[i] = 0;
                    arraySpeed[i] = 5.0*rand()/double(RAND_MAX);
                    arrayAngle[i] = 2*3.1415*rand()/double(RAND_MAX);
                    int form = rand() % 2;
                    if (form)
                    {
                        arraySprite[i].setTexture(textureSquare);
                    }
                    else
                    {
                        arraySprite[i].setTexture(textureTriangle);
                    }
                    return;
                }
            }
        }
    }
    else if (id == 2)
    {
        if (ennemiWatch % 1 == 0)
        {
            for (int i = 0; i < NB_PARTICLE_SPARKLE; i++)
            {
                if (!arrayExistSparkle[i])
                {
                    arrayExistSparkle[i] = true;
                    arrayCenterSparklex[i] = centerx;
                    arrayCenterSparkley[i] = centery;
                    arrayTimeSparkle[i] = 0;
                    arraySpeedSparkle[i] = 5.0*rand()/double(RAND_MAX);
                    arrayAngleSparkle[i] = 2*3.1415*rand()/double(RAND_MAX);
                    break;
                }
            }
        }
        
        if (ennemiWatch % 5 == 0)
        {
            frontLightning = !frontLightning;
            double angle;
            double distance;
            std::vector<double> vectorDistance(2);
            
            angle = 2*3.1415*rand()/double(RAND_MAX);
            distance = 10.0*rand()/double(RAND_MAX);
            std::vector<double> outVector = rotation(inVector, angle);
            lightning[0].position = sf::Vector2f(centerx + outVector[0]*distance, centery + outVector[1]*distance);
            
            for (int i = 1; i < NB_PARTICLE_LINE; i++)
            {
                angle = 2*3.1415*rand()/double(RAND_MAX);
                distance = 20.0*rand()/double(RAND_MAX);
                std::vector<double> outVector = rotation(inVector, angle);
                vectorDistance[0] = centerx - lightning[i - 1].position.x - outVector[0]*distance;
                vectorDistance[1] = centery - lightning[i - 1].position.y - outVector[1]*distance;
                while (getNormCarre(vectorDistance) > 40000)
                {
                    angle = 2*3.1415*rand()/double(RAND_MAX);
                    distance = 20.0*rand()/double(RAND_MAX);
                    std::vector<double> outVector = rotation(inVector, angle);
                    vectorDistance[0] = centerx - lightning[i - 1].position.x - outVector[0]*distance;
                    vectorDistance[1] = centery - lightning[i - 1].position.y - outVector[1]*distance;
                }
                lightning[i].position = sf::Vector2f(lightning[i - 1].position.x + outVector[0]*distance, lightning[i - 1].position.y + outVector[1]*distance);
            }
            timeLightning = 0;
            existLightning = true;
        }
        
    }
}

void ParticleEnnemi::update()
{
    std::vector<double> inVector;
    inVector.push_back(1.0);
    inVector.push_back(0.0);
    
    for (int i = 0; i < NB_PARTICLE_FORM; i++)
    {
        if (arrayExist[i])
        {
            arrayTime[i] = arrayTime[i] + 1;
            //currentAlpha = int(255*listTimeForm[i]/double(timeForm));
            //listForm[i].setColor(sf::Color(255, 255, 255, 255));
            //listSparkle[i].setFillColor(sf::Color(int(220.0*(1 - (listTimeCircle[i]/double(timeCircle)))), int(220.0*(1 - (listTimeCircle[i]/double(timeCircle)))), int(220.0*(1 - (listTimeCircle[i]/double(timeCircle)))), int(255.0*listTimeCircle[i]/double(timeCircle))));
            //listSparkle[i].setFillColor(sf::Color(255, 255, 255, int(255.0*listTimeCircle[i]/double(timeCircle))));
            float newScale = (DURATION_FORM - arrayTime[i])/double(DURATION_FORM);
            arraySprite[i].setPosition(arrayCenterx[i], arrayCentery[i]);
            arraySprite[i].setScale(newScale, newScale);
            std::vector<double> outVector = rotation(inVector, arrayAngle[i]);
            //listSparkle[i].move(int((Marisa.GetWidth() - (currentRadius*2))/2), int((Marisa.GetHeight() - (currentRadius*2))/2));
            //int t = timeForm - listTimeForm[i];
            //listForm[i].move(int(outVector[0]*(timeForm*t - t*t/2)*listSpeed[i]/10.0), int(outVector[1]*(timeForm*t - t*t/2)*listSpeed[i]/10.0));
            arraySprite[i].move(int(outVector[0]*arrayTime[i]*arraySpeed[i]), int(outVector[1]*arrayTime[i]*arraySpeed[i]));
            
            if (arrayTime[i] > DURATION_FORM)
            {
                arrayExist[i] = false;   
            }
        }
    }
    
    for (int i = 0; i < NB_PARTICLE_SPARKLE; i++)
    {
        if (arrayExistSparkle[i])
        {
            arrayTimeSparkle[i] = arrayTimeSparkle[i] + 1;
            float newScale = (DURATION_SPARKLE - arrayTimeSparkle[i])/double(DURATION_SPARKLE);
            arraySpriteSparkle[i].setPosition(arrayCenterSparklex[i] - 20, arrayCenterSparkley[i] - 15);
            arraySpriteSparkle[i].setScale(newScale, newScale);
            std::vector<double> outVector = rotation(inVector, arrayAngleSparkle[i]);
            arraySpriteSparkle[i].move(int(outVector[0]*arrayTimeSparkle[i]*arraySpeedSparkle[i]), int(outVector[1]*arrayTimeSparkle[i]*arraySpeedSparkle[i]));
            
            if (arrayTimeSparkle[i] > DURATION_SPARKLE)
            {
                arrayExistSparkle[i] = false;   
            }
        }
    }
    if (existLightning)
    {
        timeLightning = timeLightning + 1;
        if (timeLightning > DURATION_LIGHTNING)
        {
            existLightning = false;   
        }
    }
}

void ParticleEnnemi::draw()
{
    for (int i = 0; i < NB_PARTICLE_FORM; i++)
    {
        if (arrayExist[i])
        {
            window.mainWindow.draw(arraySprite[i]);
        }
    }
    
    for (int i = 0; i < NB_PARTICLE_SPARKLE; i++)
    {
        if (arrayExistSparkle[i])
        {
            window.mainWindow.draw(arraySpriteSparkle[i]);
        }
    }
    
    if (existLightning && !frontLightning)
    {
        window.mainWindow.draw(lightning, NB_PARTICLE_LINE, sf::LinesStrip);
    }
}

void ParticleEnnemi::draw2()
{
    if (existLightning && frontLightning)
    {
        window.mainWindow.draw(lightning, NB_PARTICLE_LINE, sf::LinesStrip);
    }
}

void ParticleEnnemi::drawAlt()
{
    for (int i = 0; i < NB_PARTICLE_FORM; i++)
    {
        if (arrayExist[i])
        {
            window.renderBackground.draw(arraySprite[i]);
        }
    }
    
    for (int i = 0; i < NB_PARTICLE_SPARKLE; i++)
    {
        if (arrayExistSparkle[i])
        {
            window.renderBackground.draw(arraySpriteSparkle[i]);
        }
    }
    
    if (existLightning)
    {
        window.renderBackground.draw(lightning, NB_PARTICLE_LINE, sf::LinesStrip);   
    }
}

void ParticleEnnemi::drawAlt2()
{
    if (existLightning && frontLightning)
    {
        window.renderBackground.draw(lightning, NB_PARTICLE_LINE, sf::LinesStrip);
    }
}
