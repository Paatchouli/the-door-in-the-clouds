/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Score.h"
#include "../include/Window.h"
#include "../include/TextEn.h"
#include "../include/func.h"
//#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <cmath>

extern Window window;
extern TextEn textEn;

int Score::watch;
int Score::watchColor;
int Score::currentScore;
int Score::hiScore;
int Score::moveTicker;
    
int Score::value [5][NB_SCORE + 1];
std::__cxx11::string Score::name [5][NB_SCORE + 1];
std::__cxx11::string Score::date [5][NB_SCORE + 1];
std::__cxx11::string Score::hour [5][NB_SCORE + 1];
    
int Score::scorePractice [24 + 1];
    
int Score::playMode;
int Score::indexNewScore;
int Score::index1; //Line index
int Score::index2; //Column index
int Score::indexColor1;
int Score::indexColor2;
int Score::sizeName;
std::__cxx11::string Score::currentName;
    
std::__cxx11::string Score::arrayChar[88];
    
std::__cxx11::string Score::over[3];

Score::Score()
{
    /*
    watch = 0;
    watchColor = 0;
    currentScore = 0;
    hiScore = 0;
    index1 = 0;
    index2 = 0;
    indexColor1 = 0;
    indexColor2 = 0;
    sizeName = 0;
    moveTicker = 0;
    currentName = "";
    
    arrayChar[0] = "A";
    arrayChar[1] = "B";
    arrayChar[2] = "C";
    arrayChar[3] = "D";
    arrayChar[4] = "E";
    arrayChar[5] = "F";
    arrayChar[6] = "G";
    arrayChar[7] = "H";
    arrayChar[8] = "I";
    arrayChar[9] = "J";
    arrayChar[10] = "K";
    arrayChar[11] = "L";
    arrayChar[12] = "M";
    arrayChar[13] = "N";
    arrayChar[14] = "O";
    arrayChar[15] = "P";
    arrayChar[16] = "Q";
    arrayChar[17] = "R";
    arrayChar[18] = "S";
    arrayChar[19] = "T";
    arrayChar[20] = "U";
    arrayChar[21] = "V";
    arrayChar[22] = "W";
    arrayChar[23] = "X";
    arrayChar[24] = "Y";
    arrayChar[25] = "Z";
    arrayChar[26] = ".";
    arrayChar[27] = ",";
    arrayChar[28] = ":";
    arrayChar[29] = ";";
    arrayChar[30] = "/";
    arrayChar[31] = "@";
    arrayChar[32] = "a";
    arrayChar[33] = "b";
    arrayChar[34] = "c";
    arrayChar[35] = "d";
    arrayChar[36] = "e";
    arrayChar[37] = "f";
    arrayChar[38] = "g";
    arrayChar[39] = "h";
    arrayChar[40] = "i";
    arrayChar[41] = "j";
    arrayChar[42] = "k";
    arrayChar[43] = "l";
    arrayChar[44] = "m";
    arrayChar[45] = "n";
    arrayChar[46] = "o";
    arrayChar[47] = "p";
    arrayChar[48] = "q";
    arrayChar[49] = "r";
    arrayChar[50] = "s";
    arrayChar[51] = "t";
    arrayChar[52] = "u";
    arrayChar[53] = "v";
    arrayChar[54] = "w";
    arrayChar[55] = "x";
    arrayChar[56] = "y";
    arrayChar[57] = "z";
    arrayChar[58] = "+";
    arrayChar[59] = "-";
    arrayChar[60] = "*";
    arrayChar[61] = "=";
    arrayChar[62] = "0";
    arrayChar[63] = "1";
    arrayChar[64] = "2";
    arrayChar[65] = "3";
    arrayChar[66] = "4";
    arrayChar[67] = "5";
    arrayChar[68] = "6";
    arrayChar[69] = "7";
    arrayChar[70] = "8";
    arrayChar[71] = "9";
    arrayChar[72] = "(";
    arrayChar[73] = ")";
    arrayChar[74] = "{";
    arrayChar[75] = "}";
    arrayChar[76] = "[";
    arrayChar[77] = "]";
    arrayChar[78] = "<";
    arrayChar[79] = ">";
    arrayChar[80] = "#";
    arrayChar[81] = "!";
    arrayChar[82] = "?";
    arrayChar[83] = "'";
    arrayChar[84] = "$";
    arrayChar[85] = "Space";
    arrayChar[86] = "Del";
    arrayChar[87] = "End";
    
    over[0] = "Continue";
    over[1] = "Return to main menu";
    over[2] = "Give up and retry";
    */
}

Score::~Score()
{

}

/*
void Score::initialize()
{
    std::ifstream scoreFile("score.txt");
    if (scoreFile)
    {
        
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                scoreFile >> value[i][j] >> name[i][j] >> date[i][j] >> hour[i][j];
            }
            value[i][10] = 0;
            name[i][10] = "";
            date[i][10] = "";
            hour[i][10] = "";
            scoreFile.ignore();
        }
        
        for (int i = 0; i < 24; i++)
        {
            scoreFile >> scorePractice[i];
        }
        scorePractice[24] = 0;
    }
    else
    {
        std::ofstream scoreFile("score.txt", std::ios::out | std::ios::trunc);
        for (int j = 0; j < 5; j++)
        {
            for (int i = 0; i < 10; i++)
            {
                scoreFile << 0 << " " << "Nanashi" << " " << "01/01/2017" << " " <<"00:00" << "\n";
            }
            scoreFile << "\n";
        }
        for (int i = 0; i < 24; i++)
        {
            scoreFile << 0 << " ";   
        }
    }
    scoreFile.close();
}
*/

void Score::initialize()
{
    watch = 0;
    watchColor = 0;
    currentScore = 0;
    hiScore = 0;
    index1 = 0;
    index2 = 0;
    indexColor1 = 0;
    indexColor2 = 0;
    sizeName = 0;
    moveTicker = 0;
    currentName = "";
    
    arrayChar[0] = "A";
    arrayChar[1] = "B";
    arrayChar[2] = "C";
    arrayChar[3] = "D";
    arrayChar[4] = "E";
    arrayChar[5] = "F";
    arrayChar[6] = "G";
    arrayChar[7] = "H";
    arrayChar[8] = "I";
    arrayChar[9] = "J";
    arrayChar[10] = "K";
    arrayChar[11] = "L";
    arrayChar[12] = "M";
    arrayChar[13] = "N";
    arrayChar[14] = "O";
    arrayChar[15] = "P";
    arrayChar[16] = "Q";
    arrayChar[17] = "R";
    arrayChar[18] = "S";
    arrayChar[19] = "T";
    arrayChar[20] = "U";
    arrayChar[21] = "V";
    arrayChar[22] = "W";
    arrayChar[23] = "X";
    arrayChar[24] = "Y";
    arrayChar[25] = "Z";
    arrayChar[26] = ".";
    arrayChar[27] = ",";
    arrayChar[28] = ":";
    arrayChar[29] = ";";
    arrayChar[30] = "/";
    arrayChar[31] = "@";
    arrayChar[32] = "a";
    arrayChar[33] = "b";
    arrayChar[34] = "c";
    arrayChar[35] = "d";
    arrayChar[36] = "e";
    arrayChar[37] = "f";
    arrayChar[38] = "g";
    arrayChar[39] = "h";
    arrayChar[40] = "i";
    arrayChar[41] = "j";
    arrayChar[42] = "k";
    arrayChar[43] = "l";
    arrayChar[44] = "m";
    arrayChar[45] = "n";
    arrayChar[46] = "o";
    arrayChar[47] = "p";
    arrayChar[48] = "q";
    arrayChar[49] = "r";
    arrayChar[50] = "s";
    arrayChar[51] = "t";
    arrayChar[52] = "u";
    arrayChar[53] = "v";
    arrayChar[54] = "w";
    arrayChar[55] = "x";
    arrayChar[56] = "y";
    arrayChar[57] = "z";
    arrayChar[58] = "+";
    arrayChar[59] = "-";
    arrayChar[60] = "*";
    arrayChar[61] = "=";
    arrayChar[62] = "0";
    arrayChar[63] = "1";
    arrayChar[64] = "2";
    arrayChar[65] = "3";
    arrayChar[66] = "4";
    arrayChar[67] = "5";
    arrayChar[68] = "6";
    arrayChar[69] = "7";
    arrayChar[70] = "8";
    arrayChar[71] = "9";
    arrayChar[72] = "(";
    arrayChar[73] = ")";
    arrayChar[74] = "{";
    arrayChar[75] = "}";
    arrayChar[76] = "[";
    arrayChar[77] = "]";
    arrayChar[78] = "<";
    arrayChar[79] = ">";
    arrayChar[80] = "#";
    arrayChar[81] = "!";
    arrayChar[82] = "?";
    arrayChar[83] = "'";
    arrayChar[84] = "$";
    arrayChar[85] = "Space";
    arrayChar[86] = "Del";
    arrayChar[87] = "End";
    
    over[0] = "Continue";
    over[1] = "Return to main menu";
    over[2] = "Give up and retry";
       
    std::ifstream scoreFile("score.txt");
    if (!scoreFile)
    {
        rewrite();
    }
    read();
}

void Score::rewrite()
{
    std::ofstream scoreFile("score.txt", std::ios::out | std::ios::trunc);
    for (int j = 0; j < 5; j++)
    {
        for (int i = 0; i < 10; i++)
        {
            scoreFile << 0 << " " << "Nanashi" << " " << "01/01/2017" << " " <<"00:00" << "\n";
        }
        scoreFile << "\n";
    }
    for (int i = 0; i < 24; i++)
    {
        scoreFile << 0 << " ";   
    }
    scoreFile.close();
}

void Score::write()
{
    std::ofstream scoreFile("score.txt", std::ios::out | std::ios::trunc);
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            scoreFile << value[i][j] << " " << name[i][j] << " " << date[i][j] << " " << hour[i][j] << "\n";
        }
        scoreFile << "\n";
    }
    for (int i = 0; i < 24; i++)
    {
        scoreFile << scorePractice[i] << " ";   
    }
    scoreFile.close();
}


void Score::read()
{
    std::ifstream scoreFile("score.txt");
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            scoreFile >> value[i][j] >> name[i][j] >> date[i][j] >> hour[i][j];
        }
        value[i][10] = 0;
        name[i][10] = "";
        date[i][10] = "";
        hour[i][10] = "";
        scoreFile.ignore();
    }
    scoreFile.close();
}

void Score::update()
{
    if (currentScore > hiScore)
    {
        hiScore = currentScore;   
    }
}

void Score::enterScore()
{
    //window.textureFrame.loadFromImage(window.mainWindow.capture());
    window.setBackground();
    window.setForeground();
    if (currentScore > value[playMode][NB_SCORE - 1])
    {
        indexNewScore = NB_SCORE - 1;
        while (currentScore > value[playMode][indexNewScore])
        {
            value[playMode][indexNewScore + 1] = value[playMode][indexNewScore];
            name[playMode][indexNewScore + 1] = name[playMode][indexNewScore];
            date[playMode][indexNewScore + 1] = date[playMode][indexNewScore];
            hour[playMode][indexNewScore + 1] = hour[playMode][indexNewScore];
            indexNewScore = indexNewScore - 1;
            if (indexNewScore == -1)
            {
                break;
            }
        }
        value[playMode][indexNewScore + 1] = currentScore;
        date[playMode][indexNewScore + 1] = getDate();
        hour[playMode][indexNewScore + 1] = getHour();
        while (true)
        {
            watch = watch + 1;
            if (moveTicker) {moveTicker = moveTicker - 1;}
            //window.mainWindow.draw(window.spriteFrame);
            window.mainWindow.draw(window.spriteBackground);
            if (watch < DURATION_SCORE_ENTERED)
            {
                textEn.writeMovement(130, -60, 130, 200, DURATION_SCORE_ENTERED, watch, 60, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, "Enter you name", 1, true);
                for (int i = 0; i < 10; i++)
                {
                    if (i != indexNewScore + 1)
                    {
                        textEn.writeMovement(2000, 250 + i*70, 720, 250 + i*70, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, name[playMode][i], 0, true);
                    }
                    int size = textEn.getWidth(40, toString(value[playMode][i], 0));
                    textEn.writeMovement(2630 - size, 250 + i*70, 1350 - size, 250 + i*70, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, toString(value[playMode][i], 0), 0, true);
                    textEn.writeMovement(2730, 250 + i*70, 1450, 250 + i*70, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, date[playMode][i], 0, true);
                    textEn.writeMovement(2980, 250 + i*70, 1700, 250 + i*70, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, hour[playMode][i], 0, true);
                }
                for (int i = 0; i < 7; i++)
                {
                    for (int j = 0; j < 12; j++)
                    {
                        textEn.writeMovement(-600 + j*50, 530 + i*50, 90 + j*50, 530 + i*50, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, arrayChar[i*12 + j], 0, true);
                    }
                }
                textEn.writeMovement(-400, 880, 290, 880, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, arrayChar[84], 0, true);
                textEn.writeMovement(-350, 880, 340, 880, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, arrayChar[85], 0, true);
                textEn.writeMovement(-200, 880, 490, 880, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, arrayChar[86], 0, true);
                textEn.writeMovement(-100, 880, 590, 880, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, arrayChar[87], 0, true);
            }
            else
            {
                for (int i = 0; i < 10; i++)
                {
                    if (i != indexNewScore + 1)
                    {
                        textEn.write(SIZE_SCORE_ENTERED, 720, 250 + i*70, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, name[playMode][i]);
                    }
                    else
                    {
                        textEn.write(SIZE_SCORE_ENTERED, 720, 250 + i*70, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, currentName);
                    }
                    int size = textEn.getWidth(40, toString(value[playMode][i], 0));
                    textEn.write(SIZE_SCORE_ENTERED, 1350 - size, 250 + i*70, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, toString(value[playMode][i], 0));
                    textEn.write(SIZE_SCORE_ENTERED, 1450, 250 + i*70, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, date[playMode][i]);
                    textEn.write(SIZE_SCORE_ENTERED, 1700, 250 + i*70, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, hour[playMode][i]);
                }
                for (int i = 0; i < 7; i++)
                {
                    for (int j = 0; j < 12; j++)
                    {
                        textEn.write(40, 90 + j*50, 530 + i*50, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, arrayChar[i*12 + j]);
                    }
                }
                textEn.write(40, 290, 880,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, arrayChar[84]);
                textEn.write(40, 340, 880,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, arrayChar[85]);
                textEn.write(40, 490, 880,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, arrayChar[86]);
                textEn.write(40, 590, 880,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, arrayChar[87]);
                textEn.write(60, 130, 200,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, "Enter you name");
                if (watchColor)
                {
                    double coeff;
                    coeff = watchColor/15.0;
                    double coeffColor;
                    if (index1 < 7)
                    {
                        coeffColor = 127.0*std::sqrt(index1*index1 + index2*index2)/std::sqrt(170.0);
                        textEn.write(40, 90 + index2*50, 530 + index1*50,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color(coeffColor + (255 - coeffColor)*coeff, coeff*255, 255), sf::Color::Black, arrayChar[index1*12 + index2]);
                        
                    }
                    else if (index2 == 4)
                    {
                        coeffColor = 127.0*std::sqrt(16.0 + 49.0)/std::sqrt(170.0);
                        textEn.write(40, 290, 880,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color(coeffColor + (255 - coeffColor)*coeff, coeff*255, 255), sf::Color::Black, arrayChar[84]);
                    }
                    else if (index2 == 5)
                    {
                        coeffColor = 127.0*std::sqrt(25.0 + 49.0)/std::sqrt(170.0);
                        textEn.write(40, 340, 880,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color(coeffColor + (255 - coeffColor)*coeff, coeff*255, 255), sf::Color::Black, arrayChar[85]);
                    }
                    else if (index2 == 8)
                    {
                        coeffColor = 127.0*std::sqrt(64.0 + 49.0)/std::sqrt(170.0);
                        textEn.write(40, 490, 880,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color(coeffColor + (255 - coeffColor)*coeff,coeff*255, 255), sf::Color::Black, arrayChar[86]);
                    }
                    else
                    {
                        coeffColor = 127.0*std::sqrt(121.0 + 49.0)/std::sqrt(170.0);
                        textEn.write(40, 590, 880,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color(coeffColor + (255 - coeffColor)*coeff, coeff*255, 255), sf::Color::Black, arrayChar[87]);
                    }
                    coeff = (15.0 - watchColor)/15.0;
                    if (indexColor1 < 7)
                    {
                        coeffColor = 127.0*std::sqrt(indexColor1*indexColor1 + indexColor2*indexColor2)/std::sqrt(170.0);
                        textEn.write(40, 90 + indexColor2*50, 530 + indexColor1*50,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color(coeffColor + (255 - coeffColor)*coeff, coeff*255, 255), sf::Color::Black, arrayChar[indexColor1*12 + indexColor2]);
                    }
                    else if (indexColor2 == 4)
                    {
                        coeffColor = 127.0*std::sqrt(16.0 + 49.0)/std::sqrt(170.0);
                        textEn.write(40, 290, 880,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color(coeffColor + (255 - coeffColor)*coeff, coeff*255, 255), sf::Color::Black, arrayChar[84]);
                    }
                    else if (indexColor2 == 5)
                    {
                        coeffColor = 127.0*std::sqrt(25.0 + 49.0)/std::sqrt(170.0);
                        textEn.write(40, 340, 880,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color(coeffColor + (255 - coeffColor)*coeff, coeff*255, 255), sf::Color::Black, arrayChar[85]);
                    }
                    else if (indexColor2 == 8)
                    {
                        coeffColor = 127.0*std::sqrt(64.0 + 49.0)/std::sqrt(170.0);
                        textEn.write(40, 490, 880,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color(coeffColor + (255 - coeffColor)*coeff, coeff*255, 255), sf::Color::Black, arrayChar[86]);
                    }
                    else
                    {
                        coeffColor = 127.0*std::sqrt(121.0 + 49.0)/std::sqrt(170.0);
                        textEn.write(40, 590, 880,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color(coeffColor + (255 - coeffColor)*coeff, coeff*255, 255), sf::Color::Black, arrayChar[87]);
                    }
                    watchColor = watchColor - 1;              
                }
                else
                {
                    if (index1 < 7)
                    {
                        textEn.write(40, 90 + index2*50, 530 + index1*50,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color(127.0*std::sqrt(index1*index1 + index2*index2)/std::sqrt(170.0), 0, 255), sf::Color::Black, arrayChar[index1*12 + index2]);
                    }
                    else
                    {
                        if (index2 == 4)
                        {
                            textEn.write(40, 290, 880,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color(127.0*std::sqrt(16.0 + 49.0)/std::sqrt(170.0), 0, 255), sf::Color::Black, arrayChar[84]);
                        }
                        else if (index2 == 5)
                        {
                            textEn.write(40, 340, 880,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color(127.0*std::sqrt(25.0 + 49.0)/std::sqrt(170.0), 0, 255), sf::Color::Black, arrayChar[85]);
                        }
                        else if (index2 == 8)
                        {
                            textEn.write(40, 490, 880,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color(127.0*std::sqrt(64.0 + 49.0)/std::sqrt(170.0), 0, 255), sf::Color::Black, arrayChar[86]);
                        }
                        else
                        {
                            textEn.write(40, 590, 880,  THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color(127.0*std::sqrt(121.0 + 49.0)/std::sqrt(170.0), 0, 255), sf::Color::Black, arrayChar[87]);
                        }
                    }
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && !moveTicker)
                    {
                        indexColor2 = index2; moveTicker = 15; watchColor = 15; indexColor1 = index1;
                        if (index1 < 7)
                        {
                            index2 = (index2 + 1) % 12;
                        }
                        else if (index2 == 4)
                        {
                            index2 = 5;
                        }
                        else if (index2 == 5)
                        {
                            index2 = 8;
                        }
                        else if (index2 == 8)
                        {
                            index2 = 10;
                        }
                        else if (index2 == 10)
                        {
                            index2 = 4;
                        }
                    }
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && !moveTicker)
                    {
                        indexColor2 = index2; moveTicker = 15; watchColor = 15; indexColor1 = index1;
                        if (index1 < 7)
                        {
                            index2 = (index2 + 11) % 12;
                        }
                        else if (index2 == 4)
                        {
                            index2 = 10;
                        }
                        else if (index2 == 5)
                        {
                            index2 = 4;
                        }
                        else if (index2 == 8)
                        {
                            index2 = 5;
                        }
                        else if (index2 == 10)
                        {
                            index2 = 8;
                        }
                    }
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && !moveTicker)
                    {
                        indexColor1 = index1; moveTicker = 15; watchColor = 15; indexColor2 = index2;
                        if (index1 != 6)
                        {
                            index1 = (index1 + 1) % 8;   
                        }
                        else
                        {
                            if (index2 < 3)
                            {
                                index1 = 0;
                            }
                            else
                            {
                                index1 = 7;
                                if (index2 > 4 && index2 < 8)
                                {
                                     index2 = 5;
                                }
                                else if (index2 > 7 & index2 < 10)
                                {
                                     index2 = 8; 
                                }
                                else if (index2 > 9)
                                {
                                    index2 = 10;   
                                }
                            }
                        }
                        
                    }
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && !moveTicker)
                    {
                        indexColor1 = index1; moveTicker = 15; watchColor = 15; indexColor2 = index2;
                        if (index1 > 0)
                        {
                            index1 = index1 - 1;   
                        }
                        else
                        {
                            if (index2 < 3)
                            {
                                index1 = 6;
                            }
                            else
                            {
                                index1 = 7;
                                if (index2 > 4 && index2 < 8)
                                {
                                     index2 = 5;
                                }
                                else if (index2 > 7 & index2 < 10)
                                {
                                     index2 = 8; 
                                }
                                else if (index2 > 9)
                                {
                                    index2 = 10;   
                                }
                            }
                        }
                        
                    }
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && !moveTicker)
                    {
                        moveTicker = 15;
                        if (index1 < 7)
                        {
                            if (currentName.size() > 11)
                            {
                                
                            }
                            else
                            {
                                currentName.append(arrayChar[index1*12 + index2]);
                            }
                        }
                        else if (index2 == 4)
                        {
                            if (currentName.size() > 11)
                            {
                                
                            }
                            else
                            {
                                currentName.append(arrayChar[84]);
                            }
                        }
                        else if (index2 == 5)
                        {
                            if (currentName.size() > 11)
                            {
                                
                            }
                            else
                            {
                                currentName.append(" ");
                            }
                        }
                        else if (index2 == 8)
                        {
                            if (currentName.size() < 1)
                            {
                                
                            }
                            else
                            {
                                currentName.erase(currentName.size() - 1);
                            }
                        }
                        else if (index2 == 10)
                        {
                            if (currentName.size() < 1)
                            {
                                
                            }
                            else
                            {
                                name[playMode][indexNewScore + 1] = currentName;
                                write();
                                break;
                            }
                        }
                    }
                }
            }
            window.mainWindow.draw(window.spriteForeground);
            window.mainWindow.display();
        }
        watch = 0;
        while (watch < DURATION_SCORE_ENTERED)
        {
            window.mainWindow.draw(window.spriteBackground);
            watch = watch + 1;
            textEn.writeMovement(130, 200, 130, -60, DURATION_SCORE_ENTERED, watch, 60, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, "Enter you name", 1, false);
            for (int i = 0; i < 10; i++)
            {
                textEn.writeMovement(720, 250 + i*70, 2000, 250 + i*70, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, name[playMode][i], 0, false);
                int size = textEn.getWidth(40, toString(value[playMode][i], 0));
                textEn.writeMovement(1350 - size, 250 + i*70, 2630 - size, 250 + i*70, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, toString(value[playMode][i], 0), 0, false);
                textEn.writeMovement(1450, 250 + i*70, 2730, 250 + i*70, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, date[playMode][i], 0, false);
                textEn.writeMovement(1700, 250 + i*70, 2980, 250 + i*70, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, hour[playMode][i], 0, false);
            }
            for (int i = 0; i < 7; i++)
            {
                for (int j = 0; j < 12; j++)
                {
                    textEn.writeMovement(90 + j*50, 530 + i*50, -600 + j*50, 530 + i*50, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, arrayChar[i*12 + j], 0, false);
                }
            }
            textEn.writeMovement(290, 880, -400, 880, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, arrayChar[84], 0, false);
            textEn.writeMovement(340, 880, -350, 880, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, arrayChar[85], 0, false);
            textEn.writeMovement(490, 880, -200, 880, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, arrayChar[86], 0, false);
            textEn.writeMovement(590, 880, -100, 880, DURATION_SCORE_ENTERED, watch, SIZE_SCORE_ENTERED, THICKNESS_SCORE_ENTERED, NB_DRAW_SCORE_ENTERED, sf::Color::White, sf::Color::Black, arrayChar[87], 0, false);
            window.mainWindow.draw(window.spriteForeground);
            window.mainWindow.display();
        }
    }
    watch = 0;
    index1 = 0;
    while (true)
    {
        //window.mainWindow.draw(window.spriteFrame);
        window.mainWindow.draw(window.spriteBackground);
        watch = watch + 1;
        if (moveTicker) {moveTicker = moveTicker - 1;}
        if (watch < DURATION_OVER)
        {
            double speedInit = 2.0*(1080.0 - 300.0)/15.0;
            double currentDistance = speedInit*watch*(1.0 - watch/30.0);
            window.spritePause.setPosition(460, 1080 - currentDistance);
            window.spritePause.setColor(sf::Color(255, 255, 255, watch*255.0/14.0));
            window.mainWindow.draw(window.spritePause);
            textEn.writeMovement(960 - textEn.getWidth(SIZE_OVER_TITLE, "Game over")/2, 1100, 960 - textEn.getWidth(SIZE_OVER_TITLE, "Pause menu")/2, 320, DURATION_OVER, watch, SIZE_OVER_TITLE, THICKNESS_OVER_TITLE, NB_DRAW_OVER_TITLE, sf::Color::White, sf::Color::Black, "Game over", 1, true);
            for (int i = 0; i < 3; i++)
            {
                textEn.writeMovement(490, 1210 + i*70, 490, 430 + i*70, DURATION_OVER, watch, SIZE_OVER, THICKNESS_OVER, NB_DRAW_OVER, sf::Color::White, sf::Color::Black, over[i], 1, true);
            }
            
        }
        else
        {
            window.mainWindow.draw(window.spritePause);
            textEn.write(SIZE_OVER_TITLE, 960 - textEn.getWidth(SIZE_OVER_TITLE, "Game over")/2, 320, THICKNESS_OVER_TITLE, NB_DRAW_OVER_TITLE, sf::Color::White, sf::Color::Black, "Game over");
            for (int i = 0; i < 3; i++)
            {
                textEn.write(SIZE_OVER, 490, 430 + i*70, THICKNESS_OVER, NB_DRAW_OVER, sf::Color::White, sf::Color::Black, over[i]);
            }
            if (watchColor)
            {
                double coeff;
                coeff = (15.0 - watchColor)/15.0;
                textEn.write(SIZE_OVER, 490, 430 + indexColor1*70, THICKNESS_OVER, NB_DRAW_OVER, sf::Color(63.5*indexColor1 + (255.0 - 63.5*indexColor1)*coeff, 255.0*coeff, 255), sf::Color::Black, over[indexColor1]);
                coeff = watchColor/15.0;
                textEn.write(SIZE_OVER, 490, 430 + index1*70, THICKNESS_OVER, NB_DRAW_OVER, sf::Color(63.5*1 + (255.0 - 63.5*1)*coeff, 255.0*coeff, 255), sf::Color::Black, over[index1]);
                watchColor = watchColor - 1;
            }
            else
            {
                textEn.write(SIZE_OVER, 490, 430 + index1*70, THICKNESS_OVER, NB_DRAW_OVER, sf::Color(63.5*index1, 0, 255), sf::Color::Black, over[index1]);
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && !moveTicker)
                {
                    moveTicker = 15; indexColor1 = index1; index1 = (index1 + 1) % 3; watchColor = 15;
                }
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && !moveTicker)
                {
                    moveTicker = 15; indexColor1 = index1; index1 = (index1 + 2) % 3; watchColor = 15;
                }
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && !moveTicker)
                {
                    if (index1 == 0)
                    {
                        
                    }
                    else if (index1 == 1)
                    {
                        
                    }
                    else
                    {
                        
                    }
                }
            }
        }
        window.mainWindow.draw(window.spriteForeground);
        window.mainWindow.display();
    }
}
