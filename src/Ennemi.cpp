/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Ennemi.h"
#include <cmath>
#include "../include/Window.h"
#include "../include/Bullet.h"
#include "../include/ItemPoint.h"
#include "../include/ItemPower.h"
#include "../include/Bonus.h"
#include "../include/func.h"
#include "../include/Player.h"
#include "../include/ParticleEnnemi.h"
#include "../include/ApparitionBullet.h"
#include <vector>
#include <SFML/Graphics.hpp>
//#include "../include/SFML/Graphics.hpp"
#include <iostream>

extern Window window;
//extern std::vector<Bullet> vectorBullet;
extern Bullet arrayBullet [NB_BULLET];
extern Player player;
extern ParticleEnnemi particleEnnemi;

int Ennemi::commonWatch = 0;

std::vector<Ennemi> vectorEnnemi(size_t(window.nbEnnemi));

Ennemi arrayEnnemi [NB_ENNEMI];

Ennemi::Ennemi()
{
    exist = false;
    isBoss = false;
    watch = 0;
    ID = 0;
    circleDeath.setOutlineThickness(4);
    circleDeath.setFillColor(sf::Color(0,0,0,0));
    circleDeath.setOutlineColor(sf::Color::White);
    
    for (int i = 0; i < NB_DAMAGE; i++)
    {
        circleDamage[i].setOutlineThickness(4);
        circleDamage[i].setFillColor(sf::Color(0,0,0,0));
        circleDamage[i].setOutlineColor(sf::Color::White);
    }
    
}

Ennemi::~Ennemi()
{

}

void Ennemi::draw()
{
    if (exist)
    {
        for (int i = 0; i < NB_FORM; i++)
        {
            if (listTimeForm[i])
            {
                window.mainWindow.draw(listForm[i]);
            }
        }
        window.mainWindow.draw(vectorSprite[(watch % (speedDisplay*nbSprite)) / speedDisplay]);
        for (int i = 0; i < NB_DAMAGE; i++)
        {
            if (watchDamage[i])
            {
                window.mainWindow.draw(circleDamage[i]);
            }
        }
        if (isBoss && bossBattle)
        {
            for (int i = 0; i < nbSpriteHpExist; i++)
            {
                window.mainWindow.draw(arraySpriteHp[i]);
            }
        }
    }
}

void Ennemi::drawAlt()
{
    if (exist)
    {
        /*
        for (int i = 0; i < NB_FORM; i++)
        {
            if (listTimeForm[i])
            {
                window.renderBackground.draw(listForm[i]);
            }
        }
        */
        window.renderBackground.draw(vectorSprite[(watch % (speedDisplay*nbSprite)) / speedDisplay]);
        for (int i = 0; i < NB_DAMAGE; i++)
        {
            if (watchDamage[i])
            {
                window.renderBackground.draw(circleDamage[i]);
            }
        }
        if (isBoss)
        {
            for (int i = 0; i < nbSpriteHpExist; i++)
            {
                window.renderBackground.draw(arraySpriteHp[i]);
            }
        }
    }
}

void Ennemi::drawChrono()
{
    write(SIZE_CHRONO, 40, 160, THICKNESS_CHRONO, NB_DRAW_CHRONO, sf::Color::White, sf::Color::Black, strChrono);
}

void Ennemi::drawChronoAlt()
{
    
}

void Ennemi::updateInstant()
{
    int nbTrajectory = trajectory.size();
    int nbShot = shot.size();
    for (unsigned int i = 0; i < nbTrajectory; i++)
    {
        if (watch == trajectory[i][0])
        {
            indexTrajectory = indexTrajectory + 1;
        }
    }
    for (unsigned int i = 0; i < nbShot; i++)
    {
        if (watch == shot[i][0])
        {
            indexShot = indexShot + 1;
        }
    }
    int IDtrajectory = trajectory[indexTrajectory][1];
    int IDshot = shot[indexShot][1];
    switch (IDtrajectory)
    {
        case 0:
            simpleTraj();
            break;
        case 1:
            trajVar();
            break;
        case 2:
            trajRand();
            break;
        case 3:
            trajCircle();
            break;
        case 4:
            destinationTraj();
            break;
    }
    /*
    if (IDtrajectory == 0.0)
    {
        simpleTraj();
    }
    else if (IDtrajectory == 1.0)
    {
        trajVar();
    }
    else if (IDtrajectory == 2.0)
    {
        trajRand();   
    }
    else if (IDtrajectory == 3.0)
    {
        trajCircle();   
    }
    */
    switch (IDshot)
    {
        case 0:
            
            break;
        case 1:
            shootBasic();
            break;
        case 2:
            shootBasicVarSpeed();
            break;
        case 3:
            shootGroup();
            break;
        case 4:
            shootGroupVarSpeed();
            break;
        case 5:
            shootSpiral();
            break;
        case 6:
            shootSpiralVarSpeed();
            break;
        case 7:
            shootStraight();
            break;
        case 8:
            shootStraightVarSpeed();
            break;
        case 9:
            shootParallelVarSpeed();
            break;
        case 10:
            shootPermutation();
            break;
        case 11:
            shootJohan1();
            break;
        case 12:
            shootJohan2();
            break;
        case 13:
            shootJohan3();
            break;
        case 14:
            shootJohan4();
            break;
        case 15:
            shootJohan5();
            break;
        case 16:
            shootJohan6();
            break;
        case 17:
            shootSquareSpirale();
            break;
        case 18:
            shootSnake();
            break;
        case 19:
            shootStatic();
            break;
        case 20:
            shootA1();
            break;
    }
    /*
    if (IDshot == 0.0)
    {
      
    }
    else if (IDshot == 1.0)
    {
        shootBasic();
    }
    else if (IDshot == 2.0)
    {
        shootBasicVarSpeed();
    }
    else if (IDshot == 3.0)
    {
        shootGroup();
    }
    else if (IDshot == 4.0)
    {
        shootGroupVarSpeed();
    }
    else if (IDshot == 5.0)
    {
        shootSpiral();
    }
    else if (IDshot == 6.0)
    {
        shootSpiralVarSpeed();
    }
    else if (IDshot == 7.0)
    {
        shootStraight();
    }
    else if (IDshot == 8.0)
    {
        shootStraightVarSpeed();
    }
    else if (IDshot == 9.0)
    {
        shootParallelVarSpeed();
    }
    else if (IDshot == 10.0)
    {
        shootPermutation();
    }
    else if (IDshot == 11.0)
    {
        shootJohan1();
    }
    else if (IDshot == 12.0)
    {
        shootJohan2();
    }
    else if (IDshot == 13.0)
    {
        shootJohan3();
    }
    else if (IDshot == 14.0)
    {
        shootJohan4();
    }
    else if (IDshot == 15.0)
    {
        shootJohan5();
    }
    else if (IDshot == 16.0)
    {
        shootJohan6();
    }
    else if (IDshot == 17.0)
    {
        shootSquareSpirale();
    }
    else if (IDshot == 18.0)
    {
        shootSnake();
    }
    */
}

void Ennemi::move()
{
    int movex = int(speedx);
    int movey = int(speedy);
    double fx = fabs(speedx);
    double fy = fabs(speedy);
    double ix = fx - int(fx);
    double iy = fy - int(fy);
    if (int(commonWatch * ix) != int((commonWatch - 1) * ix))
    {
        if (speedx > 0)
        {
            movex = movex + 1;
        }
        else
        {
            movex = movex - 1;
        }
    }
    if (int(commonWatch * iy) != int((commonWatch - 1) * iy))
    {
        if (speedy > 0)
        {
            movey = movey + 1;
        }
        else
        {
            movey = movey - 1;
        }
    }
    posx = posx + movex;
    posy = posy + movey;
    posHitboxx = posHitboxx + movex;
    posHitboxy = posHitboxy + movey;
    for(int i = 0; i < nbSprite; ++i)
    {
        vectorSprite[i].move(movex, movey);
    }
}

void Ennemi::move2()
{
    posx = speedx;
    posy = speedy;
    posHitboxx = speedx;
    posHitboxy = speedy;
    for(int i = 0; i < nbSprite; ++i)
    {
        vectorSprite[i].setPosition(speedx, speedy);
    }
}

void Ennemi::update()
{
    if (exist)
    {
        if (posx < window.posScreenx - width - 50 || posx > window.width + 50 || posy < window.posScreeny - height - 50|| posy > window.height + 50)
        {
            exist = false;
            nbEnnemi = nbEnnemi - 1;
            watch = 0;
            return;
        }
        watch = watch + 1;
        updateInstant(); //Bug lors de combat boss
        if (trajectory[indexTrajectory][1] == 0 || trajectory[indexTrajectory][1] == 1 || trajectory[indexTrajectory][1] == 2)
        {
	    move();
        }
        else
        {
            move2();   
        }
        if (isBoss)
        {
            std::vector<double> base(2);
            std::vector<double> position(2);
            base[0] = RADIUS_HP;
            base[1] = 0.0;
            for (int i = 0; i < NB_HP_SPRITE; i++)
            {
                position = rotation(base, 2.0*3.1415*i/double(NB_HP_SPRITE));
                arraySpriteHp[i].setPosition(posx + position[0], posy + position[1]);
            }
            nbSpriteHpExist = NB_HP_SPRITE*(double(hp) - vectorLifeFina[indexAttack])/double(abs(vectorLifeInit[indexAttack] - vectorLifeFina[indexAttack]));
        }
        
        //=========================================================
        //Here we update ennemy forms
        /*
        for (unsigned int i = 0; i < NB_FORM; i++)
        {
            if (listTimeForm[i])
            {
                listTimeForm[i] = listTimeForm[i] - 1;
                currentAlpha = int(255*listTimeForm[i]/double(timeForm));
                listForm[i].setColor(sf::Color(255, 255, 255, 255));
                //listSparkle[i].setFillColor(sf::Color(int(220.0*(1 - (listTimeCircle[i]/double(timeCircle)))), int(220.0*(1 - (listTimeCircle[i]/double(timeCircle)))), int(220.0*(1 - (listTimeCircle[i]/double(timeCircle)))), int(255.0*listTimeCircle[i]/double(timeCircle))));
                //listSparkle[i].setFillColor(sf::Color(255, 255, 255, int(255.0*listTimeCircle[i]/double(timeCircle))));
                float newScale = listTimeForm[i]/double(timeForm);
                listForm[i].setPosition(listCentreFormx[i], listCentreFormy[i]);
                listForm[i].setScale(newScale, newScale);
                outVector = rotation(inVector, listAngleForm[i]);
                //listSparkle[i].move(int((Marisa.GetWidth() - (currentRadius*2))/2), int((Marisa.GetHeight() - (currentRadius*2))/2));
                int t = timeForm - listTimeForm[i];
                //listForm[i].move(int(outVector[0]*(timeForm*t - t*t/2)*listSpeed[i]/10.0), int(outVector[1]*(timeForm*t - t*t/2)*listSpeed[i]/10.0));
                listForm[i].move(int(outVector[0]*t*listSpeed[i]/1.0), int(outVector[1]*t*listSpeed[i]/1.0));
            }
            else
            {
                listTimeForm[i] = timeForm;
                listAngleForm[i] = 2*3.14*rand()/double(RAND_MAX);
                listSpeed[i] = 1.0*rand()/double(RAND_MAX);
                listCentreFormx[i] = posx + width/2 - 52;
                listCentreFormy[i] = posy + height/2 - 56;
            }
        }
        */
        particleEnnemi.reset(posx, posy, watch, ID); 
        //==========================================================
        
        //==========================================================
        //Here we update damages
        
        if (memoryHp != hp && !(watchResetDamage % 5))
        {
            for (int i = 0; i < NB_DAMAGE; i++)
            {
                if (!watchDamage[i])
                {
                    posxDamage[i] = rand() % 100 + posx - 50;
                    posyDamage[i] = rand() % 100 + posy - 50;
                    sizeDamage[i] = rand() % 15 + 15;
                    circleDamage[i].setPosition(posxDamage[i], posyDamage[i]);
                    watchDamage[i] = DURATION_DAMAGE;
                    memoryHp = hp;
                    break;
                }
            }
        }
        watchResetDamage = watchResetDamage + 1;
        
        
        /*
        for (int i = 0; i < NB_DAMAGE; i++)
        {
            std::cout << watchDamage[i] << " ";
        }
        std::cout << "\n";
        std::cout << "c" << std::endl;
        */
        
        for (int i = 0; i < NB_DAMAGE; i++)
        {
            if (watchDamage[i])
            {
                watchDamage[i] = watchDamage[i] - 1;
                circleDamage[i].setRadius(sizeDamage[i]*(DURATION_DAMAGE - watchDamage[i])/double(DURATION_DAMAGE));
                circleDamage[i].setOrigin(circleDamage[i].getRadius(), circleDamage[i].getRadius());
                circleDamage[i].setOutlineThickness(watchDamage[i]/6);
                circleDamage[i].setOutlineColor(sf::Color(255 - watchDamage[i]*255/double(DURATION_DAMAGE), 255 - watchDamage[i]*255/double(DURATION_DAMAGE), 255, watchDamage[i]*255/double(DURATION_DAMAGE)));
                //window.mainWindow.draw(circleDeath);
            }
        }
        
        //==========================================================
        
        if (debug)
        {
            displayDebug();   
        }
        
        if (hp < 0 && !isBoss)
        {
            exist = false;
            nbEnnemi = nbEnnemi - 1;
            watch = 30;
            circleDeath.setPosition(posx, posy);
            relaunchPoint();
            relaunchPower();
            return;
        }
    }
}

void Ennemi::addBullet(int newPosx, int newPosy, int newBulletID, std::vector<std::vector<double> > newTrajectory, std::vector<std::vector<double> > newShot, bool useStatic)
{
    for (unsigned int i = 0; i < NB_BULLET; i++)
    {
        if (!arrayBullet[i].exist)
        {
            arrayBullet[i].reset(newPosx, newPosy, newBulletID, newTrajectory, newShot, useStatic);
            break;
        }
    }
}

void Ennemi::reset(int newPosx, int newPosy, int newID, std::vector<std::vector<double> > newTrajectory, std::vector<std::vector<double> > newShot, int newHp, int newNbPoint, int newNbPower, bool newDebug)
{
    nbEnnemi = nbEnnemi + 1;
    debug = newDebug;
    posx = newPosx;
    posy = newPosy;
    
    trajectory = newTrajectory;
    shot = newShot;
    indexTrajectory = 0;
    indexShot = 0;
    
    watch = 0;
    exist = true;
    
    hp = newHp;
    memoryHp = newHp;
    nbItemPoint = newNbPoint;
    nbItemPower = newNbPower;
    for (int i = 0; i < NB_DAMAGE; i++)
    {
        watchDamage[i] = 0;
    }
    watchResetDamage = 0;
    
    if (ID != newID)
    {
        ID = newID;
        vectorSprite.clear();
        switch (ID)
        {
            case 1:
                width = 100;
                height = 100;
                sizeHitbox = 4;
                posHitboxx = 70;
                posHitboxy = 70;
                nbSprite = 1;
                speedDisplay = 1;
                vectorSprite.resize(nbSprite);
                for (int i = 0; i < nbSprite; i++)
                {
                    vectorSprite[i].setTexture(vectorTexture[0], true);
                    vectorSprite[i].setOrigin(width/2, height/2);
                }
                break;
            case 2:
                width = 120;
                height = 120;
                sizeHitbox = 4;
                posHitboxx = 70;
                posHitboxy = 70;
                nbSprite = 1;
                speedDisplay = 1;
                vectorSprite.resize(nbSprite);
                for (int i = 0; i < nbSprite; i++)
                {
                    vectorSprite[i].setTexture(vectorTexture[4], true);
                    vectorSprite[i].setOrigin(width/2, height/2);
                }
                break;
                
        }
    }
    
    for (int i = 0; i < nbSprite; i++)
    {
        vectorSprite[i].setPosition(newPosx, newPosy);
    }

    if (listForm.size() == 0)
    {
        listForm.resize(NB_FORM);
        for (int i = 0; i < NB_FORM; i++)
        {
            listTimeForm[i] = i*(timeForm/NB_FORM);
            listAngleForm[i] = 2*3.14*rand()/double(RAND_MAX);
            listForm[i].setColor(sf::Color(255, 255, 255, 255));
            listSpeed[i] = 10.0*rand()/double(RAND_MAX);
            //listCentreFormx[i] = posx + width/2 - 6;
            //listCentreFormy[i] = posy + height/2 - 15;
            listForm[i].setTexture(vectorTexture[(i % 2) + 1]);
        }
        for (int i = 0; i < NB_DAMAGE; i++)
        {
            spriteDamage[i].setTexture(window.textureBlueCloud);   
        }
    }
}

std::vector<sf::Texture> Ennemi::vectorTexture;
std::vector<double> Ennemi::inVector;
std::vector<double> Ennemi::outVector;
int Ennemi::timeForm;
int Ennemi::nbEnnemi;
int Ennemi::indexBoss;
sf::Sprite Ennemi::arraySpriteHp[NB_HP_SPRITE];
int Ennemi::baseBossHp;
int Ennemi::bossHpInit;
int Ennemi::bossHpFina;
bool Ennemi::bossBattle;
int Ennemi::nbSpriteHpExist;
std::vector<int> Ennemi::vectorLifeInit;
std::vector<int> Ennemi::vectorLifeFina;
std::vector<int> Ennemi::vectorWatch;
int Ennemi::indexAttack;
std::string Ennemi::strChrono;

void Ennemi::setVectorTexture()
{
    vectorTexture.resize(5);
    vectorTexture[0].loadFromFile("ennemi1.png");
    vectorTexture[1].loadFromFile("particleRedSquare.png");
    vectorTexture[2].loadFromFile("particleRedTriangle.png");
    vectorTexture[3].loadFromFile("point.png");
    vectorTexture[4].loadFromFile("spriteJohan.png");
    
    inVector.push_back(1.0);
    inVector.push_back(0.0);
    outVector.resize(2);
    
    timeForm = 120;
    nbEnnemi = 0;
    
    for (int i = 0; i < NB_HP_SPRITE; i++)
    {
        arraySpriteHp[i].setTexture(window.textureHp);   
    }
}

void Ennemi::die()
{
    if (!exist && watch)
    {
        watch = watch - 1;
        circleDeath.setRadius(30 - watch);
        circleDeath.setOrigin(30 - watch, 30 - watch);
        circleDeath.setOutlineThickness(watch/2);
        window.mainWindow.draw(circleDeath);
    }
}

void Ennemi::dieAlt()
{
    if (!exist && watch)
    {
        circleDeath.setRadius(30 - watch);
        circleDeath.setOrigin(30 - watch, 30 - watch);
        circleDeath.setOutlineThickness(watch/2);
        window.renderBackground.draw(circleDeath);
    }
}

void Ennemi::relaunchPoint()
{
    for (int i = 0; i < nbItemPoint; i++)
    {
        ItemPoint::addItem(posx, posy);
    }
}

void Ennemi::relaunchPower()
{
    for (int i = 0; i < nbItemPower; i++)
    ItemPower::addItem(posx, posy);
}

void Ennemi::displayDebug()
{
    std::cout << "Position sprite X = " << vectorSprite[0].getPosition().x << std::endl;
    std::cout << "Position sprite Y = " << vectorSprite[0].getPosition().y << std::endl;
    std::cout << "Posx = " << posx << std::endl;
    std::cout << "Posy = " << posy << std::endl;
    for (int i = 0; i < shot.size(); i++)
    {
        for (int j = 0; j < shot[i].size(); j++)
        {
            std::cout << shot[i][j] << " ";   
        }
        std::cout << std::endl;
    }
}

void Ennemi::getIndexBoss()
{
    for (int i = 0; i < NB_ENNEMI; i++)
    {
        if (arrayEnnemi[i].exist && arrayEnnemi[i].ID != 1)
        {
            indexBoss = i;   
        }
    }
}

void Ennemi::updateBossAttack()
{
    /*
    std::cout << "indexAttack = " << indexAttack << std::endl;
    std::cout << "watchAttack = " << arrayEnnemi[Ennemi::indexBoss].watch << std::endl;
    std::cout << "indexShot = " << arrayEnnemi[Ennemi::indexBoss].indexShot << std::endl;
    std::cout << "indexTrajectory = " << arrayEnnemi[Ennemi::indexBoss].indexTrajectory << std::endl;
    std::cout << "trajectory[indexTrajectory][0] = " << arrayEnnemi[Ennemi::indexBoss].trajectory[arrayEnnemi[Ennemi::indexBoss].indexTrajectory][0] << std::endl;
    std::cout << "vectorWatch[indexAttack + 1] = " << vectorWatch[indexAttack + 1] << std::endl << std::endl;
    */
    strChrono = toString((vectorWatch[indexAttack + 1] - arrayEnnemi[Ennemi::indexBoss].watch)/double(60), 2);
    if (arrayEnnemi[Ennemi::indexBoss].hp < vectorLifeFina[indexAttack] || arrayEnnemi[Ennemi::indexBoss].watch == vectorWatch[indexAttack + 1])
    {
        /*
        std::cout << "======before======" << std::endl;
        std::cout << "watch = " << arrayEnnemi[Ennemi::indexBoss].watch << std::endl;
        std::cout << "hp = " << arrayEnnemi[Ennemi::indexBoss].hp << std::endl;
        std::cout << "indexAttack = " << indexAttack << std::endl;
        std::cout << "vectorLifeInit[indexAttack] = " << vectorLifeInit[indexAttack] << std::endl;
        std::cout << "vectorLifeFina[indexAttack] = " << vectorLifeFina[indexAttack] << std::endl;
        std::cout << "vectorWatch[indexAttack] + 1 = " << vectorWatch[indexAttack] + 1 << std::endl;
        std::cout << std::endl;
        */
        indexAttack = indexAttack + 1;
        arrayEnnemi[Ennemi::indexBoss].hp = vectorLifeInit[indexAttack];
        arrayEnnemi[Ennemi::indexBoss].watch = vectorWatch[indexAttack] + 1;
        /*
        std::cout << "======after======" << std::endl;
        std::cout << "watch = " << arrayEnnemi[Ennemi::indexBoss].watch << std::endl;
        std::cout << "hp = " << arrayEnnemi[Ennemi::indexBoss].hp << std::endl;
        std::cout << "indexAttack = " << indexAttack << std::endl;
        std::cout << "vectorLifeInit[indexAttack] = " << vectorLifeInit[indexAttack] << std::endl;
        std::cout << "vectorLifeFina[indexAttack] = " << vectorLifeFina[indexAttack] << std::endl;
        std::cout << "vectorWatch[indexAttack] + 1 = " << vectorWatch[indexAttack] + 1 << std::endl;
        std::cout << std::endl;
        */
        for (int i = 0; i < NB_BULLET; i++)
        {
            if (arrayBullet[i].exist)
            {
                arrayBullet[i].exist = false;
                Bonus::addBonus(arrayBullet[i].posx, arrayBullet[i].posy);
            }
        }
    }
}

void Ennemi::updateBossAttack2()
{
    
}

/**
* shot[1] = 1;
* shot[2] : Fire rate
* shot[3] : Speed bullet
* shot[4] : Nb bullet
* shot[5] : Aimed at player
* shot[6] : ID bullet
*/
void Ennemi::shootBasic()
{
    if (watch % int(shot[indexShot][2]) == 0)
    {
        std::vector<double> distance;
        std::vector<std::vector<double> > newTrajectory(1, std::vector<double>(4));
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 0.0;
        
        if (shot[indexShot][5])
        {
            distance.push_back(player.posx - posx);
            distance.push_back(player.posy - posy);
            distance = setNorm(distance, shot[indexShot][3]);
        }
        else
        {
            distance.push_back(shot[indexShot][3]);
            distance.push_back(0.0);
            distance = rotation(distance, 2*3.1415*rand()/double(RAND_MAX));
        }
        
        std::vector<double> newDistance;
        for (int i = 0; i < shot[indexShot][4]; i++)
        {
            newDistance = rotation(distance, 3.1415*i*2.0/shot[indexShot][4]);
            newTrajectory[0][2] = newDistance[0];
            newTrajectory[0][3] = newDistance[1];
            addBullet(posx, posy, shot[indexShot][6], newTrajectory, newShot);
        }
    }
}

/**
* shot[1] = 2;
* shot[2] : Fire rate
* shot[3] : Nb bullet (angle)
* shot[4] : Nb bullet (speed)
* shot[5] : Speed bullet init 1
* shot[6] : Speed bullet final 1
* shot[7] : Duration var speed 1
* shot[8] : Speed bullet init 2
* shot[9] : Speed bullet final 2
* shot[10] : Duration var speed 2
* .
* .
* . 
* shot[Nb bullet*3 (speed) + 2] : Speed bullet init Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 3] : Speed bullet final Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 4] : Duration var speed Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 5] : Aimed at player
* shot[Nb bullet*3 (speed) + 6] : ID bullet
*/
void Ennemi::shootBasicVarSpeed()
{
    if (watch % int(shot[indexShot][2]) == 0)
    {
        int nbBulletAngle = int(shot[indexShot][3]);
        int nbBulletSpeed = int(shot[indexShot][4]);
        
        std::vector<double> distance(2);
        std::vector<std::vector<double> > newTrajectory(1, std::vector<double>(7));
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 1.0;
        
        if (shot[indexShot][nbBulletSpeed*3 + 5])
        {
            distance[0] = player.posx - posx;
            distance[1] = player.posy - posy;
        }
        else
        {
            distance[0] = 1.0;
            distance[1] = 0.0;
            distance = rotation(distance, 2*3.1415*rand()/double(RAND_MAX));
        }
        
        for (int j = 0; j < nbBulletSpeed; j++)
        {
            std::vector<double> newDistance;
            for (int i = 0; i < nbBulletAngle; i++)
            {
                newDistance = rotation(distance, 3.1415*i*2.0/nbBulletAngle);
                newTrajectory[0][2] = newDistance[0];
                newTrajectory[0][3] = newDistance[1];
                newTrajectory[0][4] = shot[indexShot][j*3 + 5];
                newTrajectory[0][5] = shot[indexShot][j*3 + 6];
                newTrajectory[0][6] = shot[indexShot][j*3 + 7];
                addBullet(posx, posy, shot[indexShot][nbBulletSpeed*3 + 6], newTrajectory, newShot);
            }
        }
        ApparitionBullet::addApparition(posx, posy, true, int(shot[indexShot][nbBulletSpeed*3 + 6]) % 10);
        /*
        switch (int(shot[indexShot][nbBulletSpeed*3 + 6]) % 10)
        {
            case 1:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color::Red);
                break;
            case 2:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color(255, 127, 0, 0));
                break;
            case 3:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color::Yellow);
                break;
            case 4:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color::Green);
                break;
            case 5:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color::Blue);
                break;
            case 6:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color(63, 0, 255, 0));
                break;
            case 7:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color(127, 0, 255, 0));
                break;
        }
        */
    }
}

/**
* shot[1] = 3;
* shot[2] : Fire rate
* shot[3] : Nb bullet (angle)
* shot[4] : Nb bullet (group)
* shot[5] : Speed bullet
* shot[6] : angle
* shot[7] : Aimed at player
* shot[8] : ID bullet
*/
void Ennemi::shootGroup()
{
    if (watch % int(shot[indexShot][2]) == 0)
    {
        std::vector<std::vector<double> > distance(shot[indexShot][4], std::vector<double>(2));
        std::vector<std::vector<double> > newTrajectory(1, std::vector<double>(4));
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 0.0;
        
        if (int(shot[indexShot][4]) % 2)
        {
            distance[0][0] = shot[indexShot][5];
            distance[0][1] = 0.0;
            for (int i = 0; i < int(shot[indexShot][4])/2; i++)
            {
                distance[2*i + 1][0] = shot[indexShot][5];
                distance[2*i + 1][1] = tan((i + 1)*shot[indexShot][6])*shot[indexShot][5];
                distance[2*i + 2][0] = shot[indexShot][5];
                distance[2*i + 2][1] = - tan((i + 1)*shot[indexShot][6])*shot[indexShot][5];
            }
        }
        else
        {
            for (int i = 0; i < shot[indexShot][4]/2; i++)
            {
                 distance[2*i][0] = shot[indexShot][5];
                 distance[2*i][1] = tan((2*i + 1)*shot[indexShot][6]/2.0)*shot[indexShot][5];
                 distance[2*i + 1][0] = shot[indexShot][5];
                 distance[2*i + 1][1] = - tan((2*i + 1)*shot[indexShot][6]/2.0)*shot[indexShot][5];
            }
        }
        
        double angle;
        if (shot[indexShot][7])
        {
            std::vector<double> vectorAngle;
            vectorAngle.push_back(player.posx - posx);
            vectorAngle.push_back(player.posy - posy);
            angle = getAngle(vectorAngle);
        }
        else
        {
            angle = rand()/double(RAND_MAX);
        }

        std::vector<double> newDistance;
        
        for (int i = 0; i < shot[indexShot][3]; i++)
        {
            for (int j = 0; j < shot[indexShot][4]; j++)
            {
                newDistance = rotation(distance[j], angle + i*2.0*3.1415/shot[indexShot][3]);
                newTrajectory[0][2] = newDistance[0];
                newTrajectory[0][3] = newDistance[1];
                addBullet(posx, posy, shot[indexShot][8], newTrajectory, newShot);
            }
        }
    }
}

/**
* shot[1] = 4;
* shot[2] : Fire rate
* shot[3] : Angle
* shot[4] : Nb bullet (angle)
* shot[5] : Nb bullet (group)
* shot[6] : Nb bullet (speed)
* shot[7] : Speed bullet init 1
* shot[8] : Speed bullet final 1
* shot[9] : Duration var speed 1
* shot[10] : Speed bullet init 2
* shot[11] : Speed bullet final 2
* shot[12] : Duration var speed 2
* .
* .
* . 
* shot[Nb bullet*3 (speed) + 4] : Speed bullet init Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 5] : Speed bullet final Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 6] : Duration var speed Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 7] : Aimed at player
* shot[Nb bullet*3 (speed) + 8] : ID bullet
*/
void Ennemi::shootGroupVarSpeed()
{
    if (watch % int(shot[indexShot][2]) == 0)
    {
        int nbBulletAngle = int(shot[indexShot][4]);
        int nbBulletGroup = int(shot[indexShot][5]);
        int nbBulletSpeed = int(shot[indexShot][6]);
        
        std::vector<std::vector<double> > distance(nbBulletGroup, std::vector<double>(2));
        std::vector<std::vector<double> > newTrajectory(1, std::vector<double>(7));
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 1.0;
        
        if (int(nbBulletGroup) % 2)
        {
            distance[0][0] = 1.0;
            distance[0][1] = 0.0;
            for (int i = 0; i < int(nbBulletGroup)/2; i++)
            {
                distance[2*i + 1][0] = 1.0;
                distance[2*i + 1][1] = tan((i + 1)*shot[indexShot][3]);
                distance[2*i + 2][0] = 1.0;
                distance[2*i + 2][1] = - tan((i + 1)*shot[indexShot][3]);
            }
        }
        else
        {
            for (int i = 0; i < nbBulletGroup/2; i++)
            {
                 distance[2*i][0] = 1.0;
                 distance[2*i][1] = tan((2*i + 1)*shot[indexShot][3]/2.0);
                 distance[2*i + 1][0] = 1.0;
                 distance[2*i + 1][1] = - tan((2*i + 1)*shot[indexShot][3]/2.0);
            }
        }
        double angle;
        if (shot[indexShot][nbBulletSpeed*3 + 7])
        {
            std::vector<double> vectorAngle;
            vectorAngle.push_back(player.posx - posx);
            vectorAngle.push_back(player.posy - posy);
            angle = getAngle(vectorAngle);
        }
        else
        {
            angle = rand()/double(RAND_MAX);
        }
        std::vector<double> newDistance;
        
        for (int i = 0; i < nbBulletSpeed; i++)
        {
            for (int j = 0; j < nbBulletAngle; j++)
            {
                for (int k = 0; k < nbBulletGroup; k++)
                {
                    newDistance = rotation(distance[k], angle + 3.1415*j*2.0/nbBulletAngle);
                    newTrajectory[0][2] = newDistance[0];
                    newTrajectory[0][3] = newDistance[1];
                    newTrajectory[0][4] = shot[indexShot][i*3 + 7];
                    newTrajectory[0][5] = shot[indexShot][i*3 + 8];
                    newTrajectory[0][6] = shot[indexShot][i*3 + 9];
                    addBullet(posx, posy, shot[indexShot][nbBulletSpeed*3 + 8], newTrajectory, newShot);
                }
            }
        }
        ApparitionBullet::addApparition(posx, posy, true, int(shot[indexShot][nbBulletSpeed*3 + 8]) % 10);
        /*
        switch (int(shot[indexShot][nbBulletSpeed*3 + 8]/10) % 10)
        {
            case 1:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color::Red);
                break;
            case 2:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color(255, 127, 0, 0));
                break;
            case 3:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color::Yellow);
                break;
            case 4:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color::Green);
                break;
            case 5:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color::Blue);
                break;
            case 6:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color(63, 0, 255, 0));
                break;
            case 7:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color(127, 0, 255, 0));
                break;
        }
        */
    }
}

/**
* shot[1] = 5;
* shot[2] : Fire rate
* shot[3] : Angle
* shot[4] : Var angle
* shot[5] : Speed bullet
* shot[6] : Nb branch
* shot[7] : ID bullet
*/
void Ennemi::shootSpiral()
{
    if (watch % int(shot[indexShot][2]) == 0)
    {
        std::vector<double> distance;
        std::vector<std::vector<double> > newTrajectory(1, std::vector<double>(4));
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 0.0;
        
        shot[indexShot][3] = shot[indexShot][3] + shot[indexShot][4];
        if (shot[indexShot][3] > 2*3.15415)
        {
            shot[indexShot][3] = shot[indexShot][3] - 2*3.1415;
        }
        distance.push_back(shot[indexShot][5]);
        distance.push_back(0.0);
        
        std::vector<double> newDistance;
        for (int i = 0; i < shot[indexShot][6]; i++)
        {
            newDistance = rotation(distance, shot[indexShot][3] + 2*3.1415*i/shot[indexShot][6]);
            newTrajectory[0][2] = newDistance[0];
            newTrajectory[0][3] = newDistance[1];
            addBullet(posx, posy, shot[indexShot][7], newTrajectory, newShot);
        }
    }
}

/**
* shot[1] = 6;
* shot[2] : Fire rate
* shot[3] : Angle
* shot[4] : Var angle
* shot[5] : Nb branch
* shot[6] : Nb bullet (speed)
* shot[7] : Speed bullet init 1
* shot[8] : Speed bullet final 1
* shot[9] : Duration var speed 1
* shot[10] : Speed bullet init 2
* shot[11] : Speed bullet final 2
* shot[12] : Duration var speed 2
* .
* .
* . 
* shot[Nb bullet*3 (speed) + 4] : Speed bullet init Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 5] : Speed bullet final Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 6] : Duration var speed Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 7] : ID bullet
*/
void Ennemi::shootSpiralVarSpeed()
{
    if (watch % int(shot[indexShot][2]) == 0)
    {
        int nbBulletBranch = int(shot[indexShot][5]);
        int nbBulletSpeed = int(shot[indexShot][6]);
        
        std::vector<double> distance(2);
        std::vector<std::vector<double> > newTrajectory(1, std::vector<double>(7));
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 1.0;
        distance[0] = 1.0;
        distance[1] = 0.0;
        
        shot[indexShot][3] = shot[indexShot][3] + shot[indexShot][4];
        if (shot[indexShot][3] > 2*3.15415)
        {
            shot[indexShot][3] = shot[indexShot][3] - 2*3.1415;
        }
        std::vector<double> newDistance;
        
        for (int i = 0; i < nbBulletSpeed; i++)
        {
            for (int j = 0; j < nbBulletBranch; j++)
            {
                newDistance = rotation(distance, shot[indexShot][3] + 2*3.1415*j/nbBulletBranch);
                newTrajectory[0][2] = newDistance[0];
                newTrajectory[0][3] = newDistance[1];
                newTrajectory[0][4] = shot[indexShot][i*3 + 7];
                newTrajectory[0][5] = shot[indexShot][i*3 + 8];
                newTrajectory[0][6] = shot[indexShot][i*3 + 9];
                addBullet(posx, posy, shot[indexShot][nbBulletSpeed*3 + 7], newTrajectory, newShot);
            }
        }
        ApparitionBullet::addApparition(posx, posy, true, int(shot[indexShot][nbBulletSpeed*3 + 7]) % 10);
    }
}

/**
* shot[1] = 7;
* shot[2] : Fire rate
* shot[3] : Nb bullet (angle)
* shot[4] : Nb bullet (group)
* shot[5] : Angle
* shot[6] : Direction x
* shot[7] : Direction y
* shot[8] : ID bullet
*/
void Ennemi::shootStraight()
{
    if (watch % int(shot[indexShot][2]) == 0)
    {
        std::vector<std::vector<double> > distance(shot[indexShot][4], std::vector<double>(2));
        std::vector<std::vector<double> > newTrajectory(1, std::vector<double>(4));
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 0.0;
        std::vector<double> newDistance;
        newDistance.push_back(shot[indexShot][6]);
        newDistance.push_back(shot[indexShot][7]);
        double norm = getNorm(newDistance);
        double angle = getAngle(newDistance);
        
        if (int(shot[indexShot][4]) % 2)
        {
            distance[0][0] = norm;
            distance[0][1] = 0.0;
            for (int i = 0; i < int(shot[indexShot][4]/2); i++)
            {
                distance[2*i + 1][0] = norm;
                distance[2*i + 1][1] = tan((i + 1)*shot[indexShot][5])*norm;
                distance[2*i + 2][0] = norm;
                distance[2*i + 2][1] = - tan((i + 1)*shot[indexShot][5])*norm;
            }
        }
        else
        {
            for (int i = 0; i < shot[indexShot][4]/2; i++)
            {
                 distance[2*i][0] = norm;
                 distance[2*i][1] = tan((2*i + 1)*shot[indexShot][5]/2.0)*norm;
                 distance[2*i + 1][0] = norm;
                 distance[2*i + 1][1] = - tan((2*i + 1)*shot[indexShot][5]/2.0)*norm;
            }
        }
        
        for (int i = 0; i < shot[indexShot][3]; i++)
        {
            for (int j = 0; j < shot[indexShot][4]; j++)
            {
                newDistance = rotation(distance[j], angle + i*2.0*3.1415/shot[indexShot][3]);
                newTrajectory[0][2] = newDistance[0];
                newTrajectory[0][3] = newDistance[1];
                addBullet(posx, posy, shot[indexShot][8], newTrajectory, newShot);
            }
        }
    }
}

/**
* shot[1] = 8;
* shot[2] : Fire rate
* shot[3] : Angle
* shot[4] : Direction x
* shot[5] : Direction y
* shot[6] : Nb bullet (angle)
* shot[7] : Nb bullet (group)
* shot[8] : Nb bullet (speed)
* shot[9] : Speed bullet init 1
* shot[10] : Speed bullet final 1
* shot[11] : Duration var speed 1
* shot[12] : Speed bullet init 2
* shot[13] : Speed bullet final 2
* shot[14] : Duration var speed 2
* .
* .
* . 
* shot[Nb bullet*3 (speed) + 6] : Speed bullet init Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 7] : Speed bullet final Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 8] : Duration var speed Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 9] : ID bullet
*/
void Ennemi::shootStraightVarSpeed()
{
    if (watch % int(shot[indexShot][2]) == 0)
    {
        int nbBulletAngle = int(shot[indexShot][6]);
        int nbBulletGroup = int(shot[indexShot][7]);
        int nbBulletSpeed = int(shot[indexShot][8]);
        
        std::vector<std::vector<double> > distance(nbBulletGroup, std::vector<double>(2));
        std::vector<std::vector<double> > newTrajectory(1, std::vector<double>(7));
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 1.0;
        std::vector<double> newDistance;
        newDistance.push_back(shot[indexShot][4]);
        newDistance.push_back(shot[indexShot][5]);
        double angle = getAngle(newDistance);
        
        if (int(nbBulletGroup) % 2)
        {
            distance[0][0] = 1.0;
            distance[0][1] = 0.0;
            for (int i = 0; i < int(nbBulletGroup)/2; i++)
            {
                distance[2*i + 1][0] = 1.0;
                distance[2*i + 1][1] = tan((i + 1)*shot[indexShot][3]);
                distance[2*i + 2][0] = 1.0;
                distance[2*i + 2][1] = - tan((i + 1)*shot[indexShot][3]);
            }
        }
        else
        {
            for (int i = 0; i < nbBulletGroup/2; i++)
            {
                 distance[2*i][0] = 1.0;
                 distance[2*i][1] = tan((2*i + 1)*shot[indexShot][3]/2.0);
                 distance[2*i + 1][0] = 1.0;
                 distance[2*i + 1][1] = - tan((2*i + 1)*shot[indexShot][3]/2.0);
            }
        }
        
        for (int i = 0; i < nbBulletSpeed; i++)
        {
            for (int j = 0; j < nbBulletAngle; j++)
            {
                for (int k = 0; k < nbBulletGroup; k++)
                {
                    newDistance = rotation(distance[k], angle + 3.1415*j*2.0/nbBulletAngle);
                    newTrajectory[0][2] = newDistance[0];
                    newTrajectory[0][3] = newDistance[1];
                    newTrajectory[0][4] = shot[indexShot][i*3 + 9];
                    newTrajectory[0][5] = shot[indexShot][i*3 + 10];
                    newTrajectory[0][6] = shot[indexShot][i*3 + 11];
                    addBullet(posx, posy, shot[indexShot][nbBulletSpeed*3 + 9], newTrajectory, newShot);
                    
                }
            }
        }
        ApparitionBullet::addApparition(posx, posy, true, int(shot[indexShot][nbBulletSpeed*3 + 9]) % 10);
    }
}

/**
* shot[1] = 9;
* shot[2] : Fire rate
* shot[3] : Space
* shot[4] : Nb bullet (angle)
* shot[5] : Nb bullet (group)
* shot[6] : Nb bullet (speed)
* shot[7] : Speed bullet init 1
* shot[8] : Speed bullet final 1
* shot[9] : Duration var speed 1
* shot[10] : Speed bullet init 2
* shot[11] : Speed bullet final 2
* shot[12] : Duration var speed 2
* .
* .
* . 
* shot[Nb bullet*3 (speed) + 4] : Speed bullet init Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 5] : Speed bullet final Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 6] : Duration var speed Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 7] : Aimed at player
* shot[Nb bullet*3 (speed) + 8] : ID bullet
*/
void Ennemi::shootParallelVarSpeed()
{
    if (watch % int(shot[indexShot][2]) == 0)
    {
        double space = shot[indexShot][3];
        int nbBulletAngle = int(shot[indexShot][4]);
        int nbBulletGroup = int(shot[indexShot][5]);
        int nbBulletSpeed = int(shot[indexShot][6]);
        
        std::vector<std::vector<double> > position(nbBulletGroup, std::vector<double>(2));
        std::vector<double> newPosition;
        std::vector<std::vector<double> > newTrajectory(1, std::vector<double>(7));
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 1.0;
        
        if (int(nbBulletGroup) % 2)
        {
            position[0][0] = 0.0;
            position[0][1] = 0.0;
            for (int i = 0; i < int(nbBulletGroup)/2; i++)
            {
                position[2*i + 1][0] = 0.0;
                position[2*i + 1][1] = (i + 1)*space;
                position[2*i + 2][0] = 0.0;
                position[2*i + 2][1] = (i + 1)*space;
            }
        }
        else
        {
            for (int i = 0; i < nbBulletGroup/2; i++)
            {
                 position[2*i][0] = 0.0;
                 position[2*i][1] = (i + 0.5)*space;
                 position[2*i + 1][0] = 0.0;
                 position[2*i + 1][1] = - (i + 0.5)*space;
            }
        }
        
        double angle;
        if (shot[indexShot][nbBulletSpeed*3 + 7])
        {
            std::vector<double> vectorAngle;
            vectorAngle.push_back(player.posx - posx);
            vectorAngle.push_back(player.posy - posy);
            angle = getAngle(vectorAngle);
        }
        else
        {
            angle = rand()/double(RAND_MAX);
        }
        std::vector<double> distance(2);
        std::vector<double> newDistance;
        distance[0] = 1.0;
        distance[1] = 0.0;
        
        for (int i = 0; i < nbBulletSpeed; i++)
        {
            for (int j = 0; j < nbBulletAngle; j++)
            {
                for (int k = 0; k < nbBulletGroup; k++)
                {
                    newPosition = rotation(position[k],  angle + 3.1415*j*2.0/nbBulletAngle);
                    newDistance = rotation(distance, angle + 3.1415*j*2.0/nbBulletAngle);
                    newTrajectory[0][2] = newDistance[0];
                    newTrajectory[0][3] = newDistance[1];
                    newTrajectory[0][4] = shot[indexShot][i*3 + 7];
                    newTrajectory[0][5] = shot[indexShot][i*3 + 8];
                    newTrajectory[0][6] = shot[indexShot][i*3 + 9];
                    addBullet(posx + newPosition[0], posy + newPosition[1], shot[indexShot][nbBulletSpeed*3 + 8], newTrajectory, newShot);
                }
            }
        }
        ApparitionBullet::addApparition(posx + newPosition[0], posy + newPosition[1], true, int(shot[indexShot][nbBulletSpeed*3 + 8]) % 10);
        /*
        switch (int(shot[indexShot][nbBulletSpeed*3 + 8]) % 10)
        {
            case 1:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color::Red);
                break;
            case 2:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color(255, 127, 0, 0));
                break;
            case 3:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color::Yellow);
                break;
            case 4:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color::Green);
                break;
            case 5:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color::Blue);
                break;
            case 6:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color(63, 0, 255, 0));
                break;
            case 7:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color(127, 0, 255, 0));
                break;
        }
        */
    }
}

/**
* shot[1] = 10;
* shot[2] : Fire rate
* shot[3] : Radius
* shot[4] : Nb bullet (Angle)
* shot[5] : Nb bullet (Speed)
* shot[6] : Duration movement 1
* shot[7] : Duration movement 2
* shot[8] : Interval time 1
* shot[9] : Interval time 2
* shot[10] : Interval time 3
* shot[11] : Random permutation
* shot[12] : New index 1 
* shot[13] : New index 2
* .
* .
* .
* shot[Nb bullet (Angle) + 12] : New index Nb bullet
* shot[Nb bullet (Angle) + 13] : Speed bullet init 2
* shot[Nb bullet (Angle) + 14] : Speed bullet final 2
* shot[Nb bullet (Angle) + 15] : Duration var speed 2
* shot[Nb bullet (Angle) + 16] : Speed bullet init 2
* shot[Nb bullet (Angle) + 17] : Speed bullet final 2
* shot[Nb bullet (Angle) + 18] : Duration var speed 2
* .
* .
* .
* shot[Nb bullet (Angle) + Nb bullet (Speed)*3 + 9] : Speed bullet init 2
* shot[Nb bullet (Angle) + Nb bullet (Speed)*3 + 10] : Speed bullet final 2
* shot[Nb bullet (Angle) + Nb bullet (Speed)*3 + 11] : Duration var speed 2
* shot[Nb bullet (Angle) + Nb bullet (Speed)*3 + 12] : Aimed at player
* shot[Nb bullet (Angle) + Nb bullet (Speed)*3 + 13] : ID bullet
*/
void Ennemi::shootPermutation()
{
    if (watch % int(shot[indexShot][2]) == 0)
    {
        double radius = shot[indexShot][3];
        double nbBulletAngle = shot[indexShot][4];
        double nbBulletSpeed = shot[indexShot][5];
        double durationMovement1 = shot[indexShot][6];
        double durationMovement2 = shot[indexShot][7];
        double intervalTime1 = shot[indexShot][8];
        double intervalTime2 = shot[indexShot][9];
        double intervalTime3 = shot[indexShot][10];
        double randomPermutation = shot[indexShot][11];
        double aimed = shot[indexShot][nbBulletAngle + nbBulletSpeed*3 + 12];
        double IDbullet = shot[indexShot][nbBulletAngle + nbBulletSpeed*3 + 13];
        
        double newIndex [int(nbBulletAngle)];
        double speedInit [int(nbBulletSpeed)];
        double speedFina [int(nbBulletSpeed)];
        double durationVarSpeed [int(nbBulletSpeed)];
        
        for (int i = 0; i < nbBulletSpeed; i++)
        {
            speedInit[i] = shot[indexShot][nbBulletAngle + i*3 + 12];
            speedFina[i] = shot[indexShot][nbBulletAngle + i*3 + 13];
            durationVarSpeed[i] = shot[indexShot][nbBulletAngle + i*3 + 14];
        }  
        
        std::vector<double> distance;
        std::vector<std::vector<double> > newTrajectory(5);
        newTrajectory[0] = std::vector<double>(7);
        newTrajectory[1] = std::vector<double>(4);
        newTrajectory[2] = std::vector<double>(7);
        newTrajectory[3] = std::vector<double>(4);
        newTrajectory[4] = std::vector<double>(7);
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        
        newTrajectory[0][1] = 2.0;
        newTrajectory[1][1] = 0.0;
        newTrajectory[2][1] = 2.0;
        newTrajectory[3][1] = 0.0;
        newTrajectory[4][1] = 1.0;
        
        newTrajectory[0][0] = 0.0;
        newTrajectory[1][0] = durationMovement1;
        newTrajectory[2][0] = 0.0;
        newTrajectory[3][0] = 0.0;
        newTrajectory[4][0] = durationMovement1 + intervalTime1 + (nbBulletAngle - 1.0)*intervalTime2 + durationMovement2 + intervalTime3;
        
        if (randomPermutation)
        {
            std::vector<int> permutatedVector = permutationAllChanged(nbBulletAngle, 2);
            for (int i = 0; i < nbBulletAngle; i++)
            {
                shot[indexShot][i + 10] = permutatedVector[i];
            }
        }
        for (int i = 0; i < nbBulletAngle; i++)
        {
            newIndex[i] = shot[indexShot][i + 10];
        }
        std::vector<double> aim;
        aim.push_back(player.posx - posx);
        aim.push_back(player.posy - posy);
        
        std::vector<std::vector<double> > newDistance(nbBulletAngle, std::vector<double>(2));

        distance.push_back(radius);
        distance.push_back(0.0);
        distance = rotation(distance, 2*3.1415*rand()/double(RAND_MAX));
        for (int i = 0; i < nbBulletAngle; i++)
        {
            newDistance[i] = rotation(distance, 3.1415*i*2.0/nbBulletAngle);
        }
        
        for (int j = 0; j < nbBulletSpeed; j++)
        {
            for (int i = 0; i < nbBulletAngle; i++)
            {
                newTrajectory[0][2] = durationMovement1;
                newTrajectory[0][3] = posx;
                newTrajectory[0][4] = posy;
                newTrajectory[0][5] = posx + newDistance[i][0];
                newTrajectory[0][6] = posy + newDistance[i][1];
                
                newTrajectory[1][2] = 0.00001;
                newTrajectory[1][3] = 0.00001;
                
                newTrajectory[2][0] = durationMovement1 + i*intervalTime2 + intervalTime1;
                newTrajectory[2][2] = durationMovement2;
                newTrajectory[2][3] = posx + newDistance[i][0];
                newTrajectory[2][4] = posy + newDistance[i][1];
                
                newTrajectory[2][5] = posx + newDistance[newIndex[i]][0];
                newTrajectory[2][6] = posy + newDistance[newIndex[i]][1];
                
                newTrajectory[3][0] = durationMovement1 + i*intervalTime2 + intervalTime1 + durationMovement2;
                newTrajectory[3][2] = 0.00001;
                newTrajectory[3][3] = 0.00001;
                
                if (aimed)
                {
                    newTrajectory[4][2] = aim[0];
                    newTrajectory[4][3] = aim[1];
                }
                else
                {
                    newTrajectory[4][2] = newDistance[newIndex[i]][0];
                    newTrajectory[4][3] = newDistance[newIndex[i]][1];
                }
                newTrajectory[4][4] = speedInit[j];
                newTrajectory[4][5] = speedFina[j];
                newTrajectory[4][6] = durationVarSpeed[j];

                addBullet(posx, posy, IDbullet, newTrajectory, newShot);
            }
        }
        ApparitionBullet::addApparition(posx, posy, true, int(IDbullet) % 10);
    }
}

/**
* shot[1] = 11;
* shot[2] : Fire rate
* shot[3] : Nb shoot
* shot[4] : Interval
* shot[5] : Radius
* shot[6] : Nb bullet (angle)
* shot[7] : Nb bullet (speed)
* shot[8] : Speed bullet init 1
* shot[9] : Speed bullet final 1
* shot[10] : Duration var speed 1
* .
* .
* .
* shot[Nb bullet*3 (speed) + 5] : Speed bullet init 1
* shot[Nb bullet*3 (speed) + 6] : Speed bullet final 1
* shot[Nb bullet*3 (speed) + 7] : Duration var speed 1
*/
void Ennemi::shootJohan1()
{
    if (watch % int(shot[indexShot][2]) < shot[indexShot][3]*shot[indexShot][4] && watch % int(shot[indexShot][4]) == 0)
    {
        double radius = shot[indexShot][5];
        int nbBulletAngle = int(shot[indexShot][6]);
        int nbBulletSpeed = int(shot[indexShot][7]);
        
        std::vector<double> position(2);
        position[0] = radius;
        position[1] = 0.0;
        position = rotation(position, 2*3.1415*rand()/double(RAND_MAX));
        
        std::vector<double> distance(2);
        std::vector<std::vector<double> > newTrajectory(1, std::vector<double>(7));
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 1.0;
        
        distance[0] = radius*rand()/double(RAND_MAX);
        distance[1] = 0.0;
        distance = rotation(distance, 2*3.1415*rand()/double(RAND_MAX));
        
        for (int j = 0; j < nbBulletSpeed; j++)
        {
            std::vector<double> newDistance;
            for (int i = 0; i < nbBulletAngle; i++)
            {
                newDistance = rotation(distance, 3.1415*i*2.0/nbBulletAngle);
                newTrajectory[0][2] = newDistance[0];
                newTrajectory[0][3] = newDistance[1];
                newTrajectory[0][4] = shot[indexShot][j*3 + 8];
                newTrajectory[0][5] = shot[indexShot][j*3 + 9];
                newTrajectory[0][6] = shot[indexShot][j*3 + 10];
                addBullet(posx + distance[0], posy + distance[1], 18, newTrajectory, newShot);
            }
        }
        ApparitionBullet::addApparition(posx + distance[0], posy + distance[1], true, 8);
    }
}

/**
* shot[1] = 12;
* shot[2] : Fire rate 1
* shot[3] : Fire rate 2
* shot[4] : Fire rate 3 todo
* shot[5] : Duration shoot 1
* shot[6] : Duration shoot 2 todo
* shot[7] : Duration waiting 1
* shot[8] : Duration waiting 2 todo
* shot[9] : Duration explosion 1
* shot[10] : Duration stop
* shot[11] : Speed init
* shot[12] : angle
* shot[13] : Radius
* shot[14] : Center x
* shot[15] : Center y
* shot[16] : Nb bullet (angle) 1
* shot[17] : Nb bullet (angle) 2
* shot[18] : Nb bullet (speed) 1
* shot[19] : Nb bullet (speed) 2
* shot[20] : Duration movement 1
* shot[21] : Speed bullet init 1 1
* shot[22] : Speed bullet final 1 1
* shot[23] : Duration var speed 1 1
* .
* .
* .
* shot[Nb bullet*4 (speed1) + 16] : Duration movement 1 Nb bullet (speed) 1
* shot[Nb bullet*4 (speed1) + 17] : Speed bullet init 1 Nb bullet (speed) 1
* shot[Nb bullet*4 (speed1) + 18] : Speed bullet final 1 Nb bullet (speed) 1
* shot[Nb bullet*4 (speed1) + 19] : Duration var speed 1 Nb bullet (speed) 1
* 
* shot[Nb bullet*4 (speed1) + 20] : Speed bullet init 2 1
* shot[Nb bullet*4 (speed1) + 21] : Speed bullet final 2 1
* shot[Nb bullet*4 (speed1) + 22] : Duration var speed 2 1
* .
* .
* .
* shot[Nb bullet*4 (speed1) + Nb bullet*4 (speed2) + 17] : Speed bullet init 2 Nb bullet (speed) 2
* shot[Nb bullet*4 (speed1) + Nb bullet*4 (speed2) + 18] : Speed bullet final 2 Nb bullet (speed) 2
* shot[Nb bullet*4 (speed1) + Nb bullet*4 (speed2) + 19] : Duration var speed 2 Nb bullet (speed) 2
*/
void Ennemi::shootJohan2()
{
    if (watch == int(shot[indexShot][0]) + 1)
    {
        shot[indexShot][12] = 2.0*3.1415*rand()/double(RAND_MAX);
        shot[indexShot][14] = posx;
        shot[indexShot][15] = posy;
    }
    if (watch % int(shot[indexShot][2]) == 0)
    {
        shot[indexShot][14] = posx;
        shot[indexShot][15] = posy;
    }
    
    if (shot[indexShot][12] > 6.283)
    {
        shot[indexShot][12] = shot[indexShot][12] - 6.283;
    }
    
    if (watch % int(shot[indexShot][2]) < shot[indexShot][5] && watch % int(shot[indexShot][3]) == 0)
    {
        int fireRate1 = int(shot[indexShot][2]);
        int fireRate2 = int(shot[indexShot][3]);
        int fireRate3 = int(shot[indexShot][4]);
        int durationShoot1 = int(shot[indexShot][5]);
        int durationShoot2 = int(shot[indexShot][6]);
        int durationWaiting1 = int(shot[indexShot][7]);
        int durationWaiting2 = int(shot[indexShot][8]);
        int durationExplosion = int(shot[indexShot][9]);
        int durationStop = int(shot[indexShot][10]);
        double speedInit = shot[indexShot][11];
        double angle = shot[indexShot][12];
        double radius = shot[indexShot][13];
        int centerx = int(shot[indexShot][14]);
        int centery = int(shot[indexShot][15]);
        int nbBulletAngle1 = int(shot[indexShot][16]);
        int nbBulletAngle2 = int(shot[indexShot][17]);
        int nbBulletSpeed1 = int(shot[indexShot][18]);
        int nbBulletSpeed2 = int(shot[indexShot][19]);

        std::vector<double> position(2);
        position[0] = radius;
        position[1] = 0.0;
        position = rotation(position, angle);
        
        std::vector<double> position2(2);
        position2[0] = 0.01;
        position2[1] = 0.0;
        position2 = rotation(position2, angle + 3.1415/2.0);
        
        std::vector<double> distance(2);
        std::vector<std::vector<double> > newTrajectory(3);
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0].resize(7);
        newTrajectory[1].resize(4);
        newTrajectory[2].resize(7);
        newTrajectory[0][0] = 0.0;
        newTrajectory[2][0] = durationShoot1 - (watch % int(shot[indexShot][2])) + durationWaiting1 + durationShoot2 + durationWaiting2 + durationExplosion*(watch % int(shot[indexShot][2]))/double(durationShoot1);
        newTrajectory[0][1] = 2.0;
        newTrajectory[1][1] = 0.0;
        newTrajectory[2][1] = 1.0;
        newTrajectory[0][3] = posx;
        newTrajectory[0][4] = posy;
        newTrajectory[0][5] = centerx + position[0];
        newTrajectory[0][6] = centery + position[1];
        newTrajectory[1][2] = position2[0];
        newTrajectory[1][3] = position2[1];
        
        distance[0] = 1.0;
        distance[1] = 0.0;
        distance = rotation(distance, angle);
        
        for (int j = 0; j < nbBulletSpeed1; j++)
        {
            std::vector<double> newDistance;
            for (int i = 0; i < nbBulletAngle1; i++)
            {
                newDistance = rotation(distance, 3.1415*i*2.0/nbBulletAngle1);
                newTrajectory[1][0] = shot[indexShot][j*4 + 20];
                newTrajectory[0][2] = shot[indexShot][j*4 + 20];
                newTrajectory[2][2] = newDistance[0];
                newTrajectory[2][3] = newDistance[1];
                newTrajectory[2][4] = shot[indexShot][j*4 + 21];
                newTrajectory[2][5] = shot[indexShot][j*4 + 22];
                newTrajectory[2][6] = shot[indexShot][j*4 + 23];
                addBullet(posx, posy, 28, newTrajectory, newShot);
            }
        }
        ApparitionBullet::addApparition(posx, posy, true, 8);
        
        newTrajectory[0][5] = centerx - position[0];
        newTrajectory[0][6] = centery - position[1];
        for (int j = 0; j < nbBulletSpeed1; j++)
        {
            std::vector<double> newDistance;
            for (int i = 0; i < nbBulletAngle1; i++)
            {
                newDistance = rotation(distance, 3.1415*i*2.0/nbBulletAngle1);
                newTrajectory[1][0] = shot[indexShot][j*4 + 20];
                newTrajectory[0][2] = shot[indexShot][j*4 + 20];
                newTrajectory[2][2] = - newDistance[0];
                newTrajectory[2][3] = - newDistance[1];
                newTrajectory[2][4] = shot[indexShot][j*4 + 21];
                newTrajectory[2][5] = shot[indexShot][j*4 + 22];
                newTrajectory[2][6] = shot[indexShot][j*4 + 23];
                addBullet(posx, posy, 28, newTrajectory, newShot);
            }
        }
        shot[indexShot][12] = angle + 3.1415*shot[indexShot][3]/shot[indexShot][5];
        
    }
    
    if (watch % int(shot[indexShot][2]) < shot[indexShot][4] + shot[indexShot][5] + shot[indexShot][6] + shot[indexShot][7]
     && watch % int(shot[indexShot][2]) > shot[indexShot][5] + shot[indexShot][7]
     && watch % int(shot[indexShot][4]) == 0)
    {
        int fireRate1 = int(shot[indexShot][2]);
        int fireRate2 = int(shot[indexShot][3]);
        int fireRate3 = int(shot[indexShot][4]);
        int durationShoot1 = int(shot[indexShot][5]);
        int durationShoot2 = int(shot[indexShot][6]);
        int durationWaiting1 = int(shot[indexShot][7]);
        int durationWaiting2 = int(shot[indexShot][8]);
        int durationExplosion = int(shot[indexShot][9]);
        int durationStop = int(shot[indexShot][10]);
        double speedInit = shot[indexShot][11];
        double angle = shot[indexShot][12];
        double radius = shot[indexShot][13];
        int centerx = int(shot[indexShot][14]);
        int centery = int(shot[indexShot][15]);
        int nbBulletAngle1 = int(shot[indexShot][16]);
        int nbBulletAngle2 = int(shot[indexShot][17]);
        int nbBulletSpeed1 = int(shot[indexShot][18]);
        int nbBulletSpeed2 = int(shot[indexShot][19]);

        std::vector<double> position(2);
        position[0] = radius;
        position[1] = 0.0;
        position = rotation(position, angle);
        
        std::vector<double> distance(2);
        std::vector<std::vector<double> > newTrajectory(3);
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0].resize(7);
        newTrajectory[1].resize(4);
        newTrajectory[2].resize(7);
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 1.0;
        newTrajectory[1][0] = durationStop;
        newTrajectory[1][1] = 0.0;
        newTrajectory[1][2] = 0.0;
        newTrajectory[1][3] = 0.0;
        int t = (watch % fireRate1) - durationShoot1 - durationWaiting1;
        newTrajectory[2][0] = durationShoot2 - t + durationWaiting2 + durationExplosion*t/double(durationShoot2);
        newTrajectory[2][1] = 1.0;
        
        distance[0] = 1.0;
        distance[1] = 0.0;
        distance = rotation(distance, 2*3.1415*rand()/double(RAND_MAX));
        
        for (int j = 0; j < nbBulletSpeed2; j++)
        {
            std::vector<double> newDistance;
            for (int i = 0; i < nbBulletAngle2; i++)
            {
                newDistance = rotation(distance, 3.1415*i*2.0/nbBulletAngle2);
                newTrajectory[0][2] = newDistance[0];
                newTrajectory[0][3] = newDistance[1];
                newTrajectory[0][4] = speedInit;
                newTrajectory[0][5] = 0.0;
                newTrajectory[0][6] = durationStop;
                
                newTrajectory[2][2] = newDistance[0];
                newTrajectory[2][3] = newDistance[1];
                newTrajectory[2][4] = shot[indexShot][nbBulletSpeed1*4 + j*3 + 20];
                newTrajectory[2][5] = shot[indexShot][nbBulletSpeed1*4 + j*3 + 21];
                newTrajectory[2][6] = shot[indexShot][nbBulletSpeed1*4 + j*3 + 22];
                addBullet(centerx + position[0], centery + position[1], 18, newTrajectory, newShot);
            }
        }
        ApparitionBullet::addApparition(centerx + position[0], centery + position[1], true, 8);
        
        distance = rotation(distance, 2*3.1415*rand()/double(RAND_MAX));
        
        for (int j = 0; j < nbBulletSpeed2; j++)
        {
            std::vector<double> newDistance;
            for (int i = 0; i < nbBulletAngle2; i++)
            {
                newDistance = rotation(distance, 3.1415*i*2.0/nbBulletAngle2);
                newTrajectory[0][2] = newDistance[0];
                newTrajectory[0][3] = newDistance[1];
                newTrajectory[0][4] = speedInit;
                newTrajectory[0][5] = 0.0;
                newTrajectory[0][6] = durationStop;
                
                newTrajectory[2][2] = newDistance[0];
                newTrajectory[2][3] = newDistance[1];
                newTrajectory[2][4] = shot[indexShot][nbBulletSpeed1*4 + j*3 + 20];
                newTrajectory[2][5] = shot[indexShot][nbBulletSpeed1*4 + j*3 + 21];
                newTrajectory[2][6] = shot[indexShot][nbBulletSpeed1*4 + j*3 + 22];
                addBullet(centerx - position[0], centery - position[1], 18, newTrajectory, newShot);
            }
        }
        ApparitionBullet::addApparition(centerx - position[0], centery - position[1], true, 8);
        
        shot[indexShot][12] = angle + 3.1415*shot[indexShot][4]/shot[indexShot][6];
    }
}

/**
* shot[1] = 13;
* shot[2] : Fire rate 1
* shot[3] : Fire rate 2
* shot[4] : Fire rate 3
* shot[5] : Time explosion
* shot[6] : Nb shoot 1
* shot[7] : Nb shoot 2
* shot[8] : Radius
* shot[9] : Speed init distance 1
* shot[10] : Part speed final
* shot[11] : Duration desceleration
* shot[12] : Nb bullet angle
* shot[13] : Nb bullet speed
* shot[14] : Radius 1
* shot[15] : Duration stop 1
* .
* .
* .
* shot[Nb bullet*2 (speed) + 12] : Radius 1
* shot[Nb bullet*2 (speed) + 13] : Duration stop 1
*/
void Ennemi::shootJohan3()
{
    if (watch % int(shot[indexShot][2]) < shot[indexShot][3]*shot[indexShot][6]
     && watch % int(shot[indexShot][3]) < shot[indexShot][4]*shot[indexShot][7] 
     && watch % int(shot[indexShot][4]) == 0)
    {
        int fireRate1 = int(shot[indexShot][2]);
        int fireRate2 = int(shot[indexShot][3]);
        int fireRate3 = int(shot[indexShot][4]);
        int timeExplosion = int(shot[indexShot][5]);
        int nbShot1 = int(shot[indexShot][6]);
        int nbShot2 = int(shot[indexShot][7]);
        double radius = shot[indexShot][8];
        double speedInit = shot[indexShot][9];
        double partSpeed = shot[indexShot][10];
        int durationDesceleration = int(shot[indexShot][11]);
        int nbBulletAngle = int(shot[indexShot][12]);
        int nbBulletSpeed = int(shot[indexShot][13]);
        
        std::vector<double> position(2);
        position[0] = radius*rand()/double(RAND_MAX);
        position[1] = 0.0;
        position = rotation(position, 2*3.1415*rand()/double(RAND_MAX));
        
        std::vector<double> distance(2);
        std::vector<std::vector<double> > newTrajectory(3);
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0].resize(7);
        newTrajectory[1].resize(4);
        newTrajectory[2].resize(8);
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 2.0;
        newTrajectory[1][1] = 0.0;
        newTrajectory[1][2] = 0.0;
        newTrajectory[1][3] = 0.0;
        newTrajectory[2][0] = timeExplosion - (watch % int(shot[indexShot][2]));
        newTrajectory[2][1] = 4.0;
        newTrajectory[2][2] = speedInit;
        newTrajectory[2][3] = partSpeed;
        newTrajectory[2][4] = durationDesceleration;
        newTrajectory[2][5] = 20.0;
        newTrajectory[2][6] = 0.0;
        newTrajectory[2][7] = 0.0;
        
        distance[0] = 1.0;
        distance[1] = 0.0;
        distance = rotation(distance, 2*3.1415*rand()/double(RAND_MAX));
        
        for (int j = 0; j < nbBulletSpeed; j++)
        {
            std::vector<double> newDistance;
            for (int i = 0; i < nbBulletAngle; i++)
            {
                newDistance = rotation(distance, 3.1415*i*2.0/nbBulletAngle);
                
                newTrajectory[0][2] = shot[indexShot][j*2 + 15];
                newTrajectory[0][3] = posx + position[0];
                newTrajectory[0][4] = posy + position[1];
                newTrajectory[0][5] = posx + position[0] + newDistance[0]*shot[indexShot][j*2 + 14];
                newTrajectory[0][6] = posy + position[1] + newDistance[1]*shot[indexShot][j*2 + 14];
                
                newTrajectory[1][0] = shot[indexShot][j*2 + 15];
                addBullet(posx + position[0], posy + position[1], 18, newTrajectory, newShot);
            }
        }
        ApparitionBullet::addApparition(posx + position[0], posy + position[1], true, 8);
    }       
}

/**
* shot[1] = 14;
* shot[2] : Fire rate 1
* shot[3] : Fire rate 2
* shot[4] : Fire rate 3
* shot[5] : Duration branch
* shot[6] : Duration deploy 1
* shot[7] : Duration deploy 2 (distance 1000)
* shot[8] : Nb branch
* shot[9] : Radius
* shot[10] : Nb bullet (angle)
* shot[11] : Nb bullet (speed)
* shot[12] : Speed bullet init 1
* shot[13] : Speed bullet final 1
* shot[14] : Duration var speed 1
* .
* .
* . 
* shot[Nb bullet*3 (speed) + 9] : Speed bullet init Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 10] : Speed bullet final Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 11] : Duration var speed Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 12] : Position x init 1
* shot[Nb bullet*3 (speed) + 13] : Position y init 1
* shot[Nb bullet*3 (speed) + 14] : Position x fina 1
* shot[Nb bullet*3 (speed) + 15] : Position y fina 1
* shot[Nb bullet*3 (speed) + 16] : Duration traj 1
* .
* .
* .
* shot[Nb bullet*3 (speed) + Nb branch*4 + 7] : Position x init Nb branch
* shot[Nb bullet*3 (speed) + Nb branch*4 + 8] : Position y init Nb branch
* shot[Nb bullet*3 (speed) + Nb branch*4 + 9] : Position x fina Nb branch
* shot[Nb bullet*3 (speed) + Nb branch*4 + 10] : Position y fina Nb branch
* shot[Nb bullet*3 (speed) + Nb branch*4 + 11] : Duration traj Nb branch
*/
void Ennemi::shootJohan4()
{
    if (watch % int(shot[indexShot][2]) == 0)
    {
        std::vector<double> position(2);
        std::vector<double> distance(2);
        std::vector<double> vectorNorm(2);
        double norm;
        double qt;
        for (int j = 0; j < shot[indexShot][8]; j++)
        {
            position[0] = shot[indexShot][9]*rand()/double(RAND_MAX);
            position[1] = 0.0;
            position = rotation(position, 2*3.1415*rand()/double(RAND_MAX));
            shot[indexShot][int(shot[indexShot][11])*3 + j*5 + 12] = posx + position[0]; //Here we create the position
            shot[indexShot][int(shot[indexShot][11])*3 + j*5 + 13] = posy + position[1]; //were the bullets change of trajectory
            
            vectorNorm[0] = posx - player.posx;
            vectorNorm[1] = posy - player.posy;
            distance[0] = player.posx - posx - position[0];
            distance[1] = player.posy - posy - position[1];
            norm = getNorm(vectorNorm); //If norm == 1000, then the bullet will take durationDeploy2 frames to reach the player
            qt = 2500.0/norm; //The number of vector needed to go offscreen
            distance = setNorm(distance, 2500.0);
            
            shot[indexShot][int(shot[indexShot][11])*3 + j*5 + 14] = posx + position[0] + distance[0];
            shot[indexShot][int(shot[indexShot][11])*3 + j*5 + 15] = posy + position[1] + distance[1];
            
            shot[indexShot][int(shot[indexShot][11])*3 + j*5 + 16] = shot[indexShot][7]*qt*norm/1000.0;
        }
    }
    
    if (watch % int(shot[indexShot][2]) < shot[indexShot][5]
     && watch % int(shot[indexShot][3]) == 0)
    {
        int fireRate1 = int(shot[indexShot][2]);
        int fireRate2 = int(shot[indexShot][3]);
        int fireRate3 = int(shot[indexShot][4]);
        int durationBranch = int(shot[indexShot][5]);
        int durationDeploy1 = int(shot[indexShot][6]);
        int durationDeploy2 = int(shot[indexShot][7]);
        int nbBranch = int(shot[indexShot][8]);
        double radius = shot[indexShot][9];
        int nbBulletSpeed = int(shot[indexShot][11]);
        
        std::vector<double> distance(2);
        double norm;
        double qt;
        
        std::vector<std::vector<double> > newTrajectory(2, std::vector<double>(7));
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0][0] = 0.0;
        newTrajectory[1][0] = durationDeploy1;
        newTrajectory[0][1] = 5.0;
        newTrajectory[1][1] = 5.0;
        newTrajectory[0][2] = durationDeploy1;
        newTrajectory[0][3] = posx;
        newTrajectory[0][4] = posy;
        
        for (int j = 0; j < nbBranch; j++)
        {
            newTrajectory[0][5] = shot[indexShot][nbBulletSpeed*3 + 12 + j*5];
            newTrajectory[0][6] = shot[indexShot][nbBulletSpeed*3 + 13 + j*5];
            
            newTrajectory[1][2] = shot[indexShot][nbBulletSpeed*3 + 16 + j*5];
            
            newTrajectory[1][3] = shot[indexShot][nbBulletSpeed*3 + 12 + j*5];
            newTrajectory[1][4] = shot[indexShot][nbBulletSpeed*3 + 13 + j*5];
            
            newTrajectory[1][5] = shot[indexShot][nbBulletSpeed*3 + 14 + j*5];
            newTrajectory[1][6] = shot[indexShot][nbBulletSpeed*3 + 15 + j*5];
            
            addBullet(posx, posy, 48, newTrajectory, newShot);
        }
        ApparitionBullet::addApparition(posx, posy, true, 8);
    }
    
    if (watch % int(shot[indexShot][4]) == 0)
    {
        int nbBulletAngle = int(shot[indexShot][10]);
        int nbBulletSpeed = int(shot[indexShot][11]);
        
        std::vector<double> distance(2);
        std::vector<std::vector<double> > newTrajectory(1, std::vector<double>(7));
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 1.0;
        
        distance[0] = 1.0;
        distance[1] = 0.0;
        distance = rotation(distance, 2*3.1415*rand()/double(RAND_MAX));
        
        for (int j = 0; j < nbBulletSpeed; j++)
        {
            std::vector<double> newDistance;
            for (int i = 0; i < nbBulletAngle; i++)
            {
                newDistance = rotation(distance, 3.1415*i*2.0/nbBulletAngle);
                newTrajectory[0][2] = newDistance[0];
                newTrajectory[0][3] = newDistance[1];
                newTrajectory[0][4] = shot[indexShot][j*3 + 12];
                newTrajectory[0][5] = shot[indexShot][j*3 + 13];
                newTrajectory[0][6] = shot[indexShot][j*3 + 14];
                addBullet(posx, posy, 18, newTrajectory, newShot);
            }
        }
        ApparitionBullet::addApparition(posx, posy, true, 8);
    }
}

/**
* shot[1] = 15;
* shot[2] : Fire rate
* shot[3] : Explosion delay
* shot[4] : Nb bullet 1
* shot[5] : Nb bullet 2
* shot[6] : Speed init max 1
* shot[7] : Speed init max 2
* shot[8] : Speed init min 1
* shot[9] : Speed init min 2
* shot[10] : Speed fina max 1
* shot[11] : Speed fina max 2
* shot[12] : Speed fina min 1
* shot[13] : Speed fina min 2
* shot[14] : Duration var speed max 1
* shot[15] : Duration var speed max 2
* shot[16] : Duration var speed min 1
* shot[17] : Duration var speed min 2
*/
void Ennemi::shootJohan5()
{
    if (watch % int(shot[indexShot][2]) == 0)
    {
        int explosionDelay = int(shot[indexShot][3]);
        int nbBullet1 = int(shot[indexShot][4]);
        int nbBullet2 = int(shot[indexShot][5]);
        double speedInitMax1 = shot[indexShot][6];
        double speedInitMax2 = shot[indexShot][7];
        double speedInitMin1 = shot[indexShot][8];
        double speedInitMin2 = shot[indexShot][9];
        double speedFinaMax1 = shot[indexShot][10];
        double speedFinaMax2 = shot[indexShot][11];
        double speedFinaMin1 = shot[indexShot][12];
        double speedFinaMin2 = shot[indexShot][13];
        double durationVarSpeedMax1 = shot[indexShot][14];
        double durationVarSpeedMax2 = shot[indexShot][15];
        double durationVarSpeedMin1 = shot[indexShot][16];
        double durationVarSpeedMin2 = shot[indexShot][17];
        
        std::vector<std::vector<double> > newTrajectory(2);
        std::vector<std::vector<double> > newShot(2);
        
        newTrajectory[0].resize(7);
        newTrajectory[1].resize(4);
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 1.0;
        newTrajectory[1][0] = explosionDelay;
        newTrajectory[1][1] = 0.0;
        newTrajectory[1][2] = 2500.0;
        newTrajectory[1][3] = 0.0;
        
        newShot[0].resize(2);
        newShot[1].resize(9);
        newShot[0][0] = 0.0;
        newShot[0][1] = 0.0;
        newShot[1][0] = explosionDelay;
        newShot[1][1] = 1.0;
        newShot[1][2] = nbBullet2;
        newShot[1][3] = speedInitMax2;
        newShot[1][4] = speedInitMin2;
        newShot[1][5] = speedFinaMax2;
        newShot[1][6] = speedFinaMin2;
        newShot[1][7] = durationVarSpeedMax2;
        newShot[1][8] = durationVarSpeedMin2;
        
        std::vector<double> distance(2);
        distance[0] = 1.0;
        distance[1] = 0.0;
        double coeff;
        
        for (int j = 0; j < nbBullet1; j++)
        {
            distance = rotation(distance, 2.0*3.1415*rand()/double(RAND_MAX));
            newTrajectory[0][2] = distance[0];
            newTrajectory[0][3] = distance[1];
            coeff = rand()/double(RAND_MAX);
            newTrajectory[0][4] = speedInitMax1*coeff + speedInitMin1*(1.0 - coeff);
            coeff = rand()/double(RAND_MAX);
            newTrajectory[0][5] = speedFinaMax1*coeff + speedFinaMin1*(1.0 - coeff);
            coeff = rand()/double(RAND_MAX);
            newTrajectory[0][6] = durationVarSpeedMax1*coeff + durationVarSpeedMin1*(1.0 - coeff);
            addBullet(posx, posy, 28, newTrajectory, newShot);
        }
        ApparitionBullet::addApparition(posx, posy, true, 8);
    }
}

/**
* shot[1] = 16;
* shot[2] : Fire rate
* shot[3] : Radius
* shot[4] : Nb bullet
* shot[5] : Speed init max 1
* shot[6] : Speed init max 2
* shot[7] : Speed init min 1
* shot[8] : Speed init min 2
* shot[9] : Speed fina max 1
* shot[10] : Speed fina max 2
* shot[11] : Speed fina min 1
* shot[12] : Speed fina min 2
* shot[13] : Duration var speed max 1
* shot[14] : Duration var speed max 2
* shot[15] : Duration var speed min 1
* shot[16] : Duration var speed min 2
*/
void Ennemi::shootJohan6()
{
    if (watch % int(shot[indexShot][2]) == 0)
    {
        double radius = shot[indexShot][3];
        int nbBullet = int(shot[indexShot][4]);
        double speedInitMax1 = shot[indexShot][5];
        double speedInitMax2 = shot[indexShot][6];
        double speedInitMin1 = shot[indexShot][7];
        double speedInitMin2 = shot[indexShot][8];
        double speedFinaMax1 = shot[indexShot][9];
        double speedFinaMax2 = shot[indexShot][10];
        double speedFinaMin1 = shot[indexShot][11];
        double speedFinaMin2 = shot[indexShot][12];
        double durationVarSpeedMax1 = shot[indexShot][13];
        double durationVarSpeedMax2 = shot[indexShot][14];
        double durationVarSpeedMin1 = shot[indexShot][15];
        double durationVarSpeedMin2 = shot[indexShot][16];
        
        std::vector<std::vector<double> > newTrajectory(2);
        std::vector<std::vector<double> > newShot(1, std::vector<double>(12));
        
        newTrajectory[0].resize(7);
        newTrajectory[1].resize(4);
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 1.0;
        newTrajectory[1][0] = int(radius/speedFinaMin1);
        newTrajectory[1][1] = 0.0;
        newTrajectory[1][2] = 2500.0;
        newTrajectory[1][3] = 0.0;
        
        newShot[0][0] = 0.0;
        newShot[0][1] = 2.0;
        newShot[0][2] = nbBullet;
        newShot[0][3] = speedInitMax2;
        newShot[0][4] = speedInitMin2;
        newShot[0][5] = speedFinaMax2;
        newShot[0][6] = speedFinaMin2;
        newShot[0][7] = durationVarSpeedMax2;
        newShot[0][8] = durationVarSpeedMin2;
        newShot[0][9] = radius;
        newShot[0][11] = int(radius/speedFinaMin1);;
        
        std::vector<double> distance(2);
        distance[0] = 1.0;
        distance[1] = 0.0;
        double coeff;
        double angle = 2.0*3.1415*rand()/double(RAND_MAX);
        newShot[0][10] = angle;
        
        distance = rotation(distance, angle);
        newTrajectory[0][2] = distance[0];
        newTrajectory[0][3] = distance[1];
        coeff = rand()/double(RAND_MAX);
        newTrajectory[0][4] = speedInitMax1*coeff + speedInitMin1*(1.0 - coeff);
        coeff = rand()/double(RAND_MAX);
        newTrajectory[0][5] = speedFinaMax1*coeff + speedFinaMin1*(1.0 - coeff);
        coeff = rand()/double(RAND_MAX);
        newTrajectory[0][6] = durationVarSpeedMax1*coeff + durationVarSpeedMin1*(1.0 - coeff);
        addBullet(posx, posy, 28, newTrajectory, newShot);
        ApparitionBullet::addApparition(posx, posy, true, 8);
    }
}

/**
* shot[1] = 17;
* shot[2] : Fire rate 1
* shot[3] : Fire rate 2
* shot[4] : Offset
* shot[5] : Duration shoot
* shot[6] : Duration space
* shot[7] : Space
* shot[8] : Clock-wise
* shot[9] : Direction init
* shot[10] : Random direction
* shot[11] : Nb Bullet (space)
* shot[12] : Nb Bullet (speed)
* shot[13] : Speed bullet init 1
* shot[14] : Speed bullet final 1
* shot[15] : Duration var speed 1
* .
* .
* .
* shot[Nb bullet*3 (speed) + 10] : Speed bullet init 1
* shot[Nb bullet*3 (speed) + 11] : Speed bullet final 1
* shot[Nb bullet*3 (speed) + 12] : Duration var speed 1
* shot[Nb bullet*3 (speed) + 13] : Id bullet
*/
void Ennemi::shootSquareSpirale()
{
    if ((watch + int(shot[indexShot][4])) % int(shot[indexShot][2]) < shot[indexShot][5] && watch % int(shot[indexShot][3]) == 0)
    {
        if (watch % int(shot[indexShot][2]) == 0)
        {
            shot[indexShot][9] = rand() % 4;
        }
        std::vector<std::vector<double> > newTrajectory(2);
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0].resize(8);
        newTrajectory[1].resize(7);
        newTrajectory[0][0] = 0;
        newTrajectory[0][1] = 6;
        newTrajectory[0][2] = shot[indexShot][6];
        newTrajectory[0][3] = shot[indexShot][7];
        if (shot[indexShot][10])
        {
            newTrajectory[0][4] = rand() % 4;
        }
        else
        {
            newTrajectory[0][4] = shot[indexShot][9];
        }
        newTrajectory[0][5] = shot[indexShot][8];
        newTrajectory[0][6] = posx;
        newTrajectory[0][7] = posy;
        newTrajectory[1][0] = shot[indexShot][5] - ((watch + int(shot[indexShot][4]))  % int(shot[indexShot][2]));
        newTrajectory[1][1] = 1;
        int r;
        for (int i = 0; i < shot[indexShot][11]; i++)
        {
            r = rand() % 4;
            if (r == 0)
            {
                newTrajectory[1][2] = 1;
                newTrajectory[1][3] = 0;
            }
            else if (r == 1)
            {
                newTrajectory[1][2] = 0;
                newTrajectory[1][3] = 1;
            }
            else if (r == 2)
            {
                newTrajectory[1][2] = -1;
                newTrajectory[1][3] = 0;
            }
            else
            {
                newTrajectory[1][2] = 0;
                newTrajectory[1][3] = -1;
            }
            for (int j = 0; j < shot[indexShot][12]; j++)
            {
                //std::cout << "x = " << newTrajectory[1][2] << std::endl;
                //std::cout << "y = " << newTrajectory[1][3] << std::endl;
                newTrajectory[1][4] = shot[indexShot][13 + j*3];
                newTrajectory[1][5] = shot[indexShot][14 + j*3];
                newTrajectory[1][6] = shot[indexShot][15 + j*3];
                addBullet(posx, posy, shot[indexShot][int(shot[indexShot][12])*3 + 13], newTrajectory, newShot);
            }
        }
        ApparitionBullet::addApparition(posx, posy, true, int(shot[indexShot][int(shot[indexShot][12])*3 + 13]) % 10);
        //std::cout << std::endl;
    }
}

/**
* shot[1] = 18;
* shot[2] : Fire rate 1
* shot[3] : Fire rate 2
* shot[4] : Offset
* shot[5] : Duration shot
* shot[6] : Duration space
* shot[7] : Space
* shot[8] : Nb space y
* shot[9] : Up first
* shot[10] : Id bullet
*/
void Ennemi::shootSnake()
{
    if ((watch + int(shot[indexShot][4])) % int(shot[indexShot][2]) < shot[indexShot][5] && watch % int(shot[indexShot][3]) == 0)
    {
        if (watch % int(shot[indexShot][2]) == 0)
        {
            //shot[indexShot][9] = rand() % 2;
        }
        std::vector<std::vector<double> > newTrajectory(1, std::vector<double>(11));
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0][0] = 0;
        newTrajectory[0][1] = 7;
        newTrajectory[0][2] = shot[indexShot][6];
        newTrajectory[0][3] = shot[indexShot][7];
        newTrajectory[0][4] = shot[indexShot][8];
        newTrajectory[0][5] = 0;
        newTrajectory[0][6] = shot[indexShot][8]/2;
        newTrajectory[0][7] = 1;
        newTrajectory[0][8] = shot[indexShot][9];
        newTrajectory[0][9] = posx;
        newTrajectory[0][10] = posy;
        addBullet(posx, posy, shot[indexShot][10], newTrajectory, newShot);
        ApparitionBullet::addApparition(posx, posy, true, int(shot[indexShot][10]) % 10);
    }
}

/**
* shot[1] = 19;
* shot[2] : Fire rate
* shot[3] : Coord x
* shot[4] : Coord y
* shot[5] : Nb bullet (angle)
* shot[6] : Nb bullet (speed)
* shot[7] : Speed bullet init 1
* shot[8] : Speed bullet final 1
* shot[9] : Duration var speed 1
* shot[10] : Speed bullet init 2
* shot[11] : Speed bullet final 2
* shot[12] : Duration var speed 2
* .
* .
* . 
* shot[Nb bullet*3 (speed) + 4] : Speed bullet init Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 5] : Speed bullet final Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 6] : Duration var speed Nb bullet (speed)
* shot[Nb bullet*3 (speed) + 7] : Aimed at player
* shot[Nb bullet*3 (speed) + 8] : ID bullet
*/
void Ennemi::shootStatic()
{
    if (watch - shot[indexShot][0] < 1)
    {
        if (shot[indexShot][int(shot[indexShot][6])*3 + 7])
        {
            shot[indexShot][3] = player.posx - posx;
            shot[indexShot][4] = player.posy - posy;
        }
        else
        {
            std::vector<double> tmp(2);
            tmp[0] = 1.0;
            tmp[1] = 0.0;
            tmp = rotation(tmp, 2*3.1415*rand()/double(RAND_MAX));
            shot[indexShot][3] = tmp[0];
            shot[indexShot][4] = tmp[1];
        }
    }
    if (watch % int(shot[indexShot][2]) == 0)
    {
        int nbBulletAngle = int(shot[indexShot][5]);
        int nbBulletSpeed = int(shot[indexShot][6]);
        
        std::vector<double> distance(2);
        std::vector<std::vector<double> > newTrajectory(1, std::vector<double>(9));
        std::vector<std::vector<double> > newShot(1, std::vector<double>(2, 0));
        newTrajectory[0][0] = 0.0;
        newTrajectory[0][1] = 8.0;
        newTrajectory[0][7] = posx;
        newTrajectory[0][8] = posy;
        
        distance[0] = shot[indexShot][3];
        distance[1] = shot[indexShot][4];
        
        for (int j = 0; j < nbBulletSpeed; j++)
        {
            std::vector<double> newDistance;
            for (int i = 0; i < nbBulletAngle; i++)
            {
                newDistance = rotation(distance, 3.1415*i*2.0/nbBulletAngle);
                newTrajectory[0][2] = newDistance[0];
                newTrajectory[0][3] = newDistance[1];
                newTrajectory[0][4] = shot[indexShot][j*3 + 7];
                newTrajectory[0][5] = shot[indexShot][j*3 + 8];
                newTrajectory[0][6] = shot[indexShot][j*3 + 9];
                addBullet(posx, posy, shot[indexShot][nbBulletSpeed*3 + 8], newTrajectory, newShot, false);
            }
        }
        ApparitionBullet::addApparition(posx, posy, true, int(shot[indexShot][nbBulletSpeed*3 + 8]) % 10);
        /*
        switch (int(shot[indexShot][nbBulletSpeed*3 + 8]) % 10)
        {
            case 1:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color::Red);
                break;
            case 2:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color(255, 127, 0, 0));
                break;
            case 3:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color::Yellow);
                break;
            case 4:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color::Green);
                break;
            case 5:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color::Blue);
                break;
            case 6:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color(63, 0, 255, 0));
                break;
            case 7:
                ApparitionBullet::addApparition(posx, posy, true, sf::Color(127, 0, 255, 0));
                break;
        }
        */
    }
}

void Ennemi::shootA1()
{
    
}

/**
* trajectory[1] = 0;
* trajectory[2] : speedx
* trajectory[3] : speedy
*/
void Ennemi::simpleTraj()
{
    speedx = trajectory[indexTrajectory][2];
    speedy = trajectory[indexTrajectory][3];
}

/**
* trajectory[1] = 1;
* trajectory[2] : speedx
* trajectory[3] : speedy
* trajectory[4] : Norm speed init
* trajectory[5] : Norm speed final
* trajectory[6] : Duration variation speed
*/
void Ennemi::trajVar()
{
    std::vector<double> speed;
    speed.push_back(trajectory[indexTrajectory][2]);
    speed.push_back(trajectory[indexTrajectory][3]);
    double coeff = std::min(double(watch) - trajectory[indexTrajectory][0], trajectory[indexTrajectory][6])/trajectory[indexTrajectory][6];
    double normSpeed = trajectory[indexTrajectory][4]*(1.0 - coeff) + trajectory[indexTrajectory][5]*coeff;
    speed = setNorm(speed, normSpeed);
    speedx = speed[0];
    speedy = speed[1];
}

/**
* trajectory[1] = 2;
* trajectory[2] : Rate movement
* trajectory[3] : Duration movement
* trajectory[4] : Duration acceleration
* trajectory[5] : Acceleration
* trajectory[6] : Direction x
* trajectory[7] : Direction y
* trajectory[8] : Min x
* trajectory[9] : Max x
* trajectory[10] : Min y
* trajectory[11] : Max y
*/
void Ennemi::trajRand()
{
    if (watch % int(trajectory[indexTrajectory][2]) == 0)
    {
        double distance = trajectory[indexTrajectory][5]*trajectory[indexTrajectory][4]*(trajectory[indexTrajectory][4] + trajectory[indexTrajectory][3]);
        double angle;
        if (posx > trajectory[indexTrajectory][9] - distance)
        {
            if (posy > trajectory[indexTrajectory][11] - distance)
            {
                angle = (3.14/2.0) + 3.14*rand()/(double(RAND_MAX)*2.0);
            }
            else if (posy < trajectory[indexTrajectory][10] + distance)
            {
                angle = 3.14 + 3.14*rand()/(double(RAND_MAX)*2.0);
            }
            else
            {
                angle = (3.14/2.0) + 3.14*rand()/double(RAND_MAX);
            }
        }
        else if (posx < trajectory[indexTrajectory][8] + distance)
        {
            if (posy > trajectory[indexTrajectory][11] - distance)
            {
                angle = 3.14*rand()/(double(RAND_MAX)*2.0);
            }
            else if (posy < trajectory[indexTrajectory][10] + distance)
            {
                angle = (3.0*3.14/2.0) + 3.14*rand()/(double(RAND_MAX)*2.0);
            }
            else
            {
                angle = (3.0*3.14/2.0) + 3.14*rand()/double(RAND_MAX);
            }
        }
        else
        {
            if (posy > trajectory[indexTrajectory][11] - distance)
            {
                angle = 3.14*rand()/double(RAND_MAX);
            }
            else if (posy < trajectory[indexTrajectory][10] + distance)
            {
                angle = 3.14 + 3.14*rand()/double(RAND_MAX);
            }
            else
            {
                angle = 2*3.14*rand()/double(RAND_MAX);
            }
        }
        
        //double angle = 2*3.1415*rand()/double(RAND_MAX);
        std::vector<double> speed;
        speed.push_back(1.0);
        speed.push_back(0.0);
        speed = rotation(speed, angle);
        trajectory[indexTrajectory][6] = speed[0];
        trajectory[indexTrajectory][7] = speed[1];
    }
    if (watch % int(trajectory[indexTrajectory][2]) < int(trajectory[indexTrajectory][4]))
    {
        speedx = (watch % int(trajectory[indexTrajectory][2]))*trajectory[indexTrajectory][5]*trajectory[indexTrajectory][6];
        speedy = (watch % int(trajectory[indexTrajectory][2]))*trajectory[indexTrajectory][5]*trajectory[indexTrajectory][7];
    }
    else if (watch % int(trajectory[indexTrajectory][2]) < int(trajectory[indexTrajectory][4]) + int(trajectory[indexTrajectory][3]))
    {
        speedx = trajectory[indexTrajectory][4]*trajectory[indexTrajectory][5]*trajectory[indexTrajectory][6];
        speedy = trajectory[indexTrajectory][4]*trajectory[indexTrajectory][5]*trajectory[indexTrajectory][7];
    }
    else if (watch % int(trajectory[indexTrajectory][2]) < 2*int(trajectory[indexTrajectory][4]) + int(trajectory[indexTrajectory][3]))
    {
        speedx = (2*int(trajectory[indexTrajectory][4]) + int(trajectory[indexTrajectory][3]) - (watch % int(trajectory[indexTrajectory][2])))*trajectory[indexTrajectory][5]*trajectory[indexTrajectory][6];
        speedy = (2*int(trajectory[indexTrajectory][4]) + int(trajectory[indexTrajectory][3]) - (watch % int(trajectory[indexTrajectory][2])))*trajectory[indexTrajectory][5]*trajectory[indexTrajectory][7];
    }
    else
    {
        speedx = 0.0;
        speedy = 0.0;
    }
}

/**
* trajectory[1] = 3;
* trajectory[2] : Center x
* trajectory[3] : Center y
* trajectory[4] : Radius
* trajectory[5] : Period
* trajectory[6] : Phase offset
* trajectory[7] : Counter-clockwise
*/
void Ennemi::trajCircle()
{
    std::vector<double> distance(2);
    distance[0] = trajectory[indexTrajectory][4];
    distance[1] = 0;
    if (trajectory[indexTrajectory][7])
    {
        distance = rotation(distance, 2.0*3.1415*(int(watch + trajectory[indexTrajectory][6]) % int(trajectory[indexTrajectory][5]))/trajectory[indexTrajectory][5]);
    }
    else
    {
        distance = rotation(distance, 2.0*3.1415*(int((trajectory[indexTrajectory][5] - watch) + trajectory[indexTrajectory][6]) % int(trajectory[indexTrajectory][5]))/trajectory[indexTrajectory][5]);
    }
    speedx = trajectory[indexTrajectory][2] + distance[0];
    speedy = trajectory[indexTrajectory][3] + distance[1];
}

/**
* trajectory[1] = 4;
* trajectory[2] : Duration movement
* trajectory[3] : Position init x
* trajectory[4] : Position init y
* trajectory[5] : Position final x
* trajectory[6] : Position final y
*/
void Ennemi::destinationTraj()
{
    double t = (watch - trajectory[indexTrajectory][0])/trajectory[indexTrajectory][2];
    double coeffx = (t - t*t/2.0)*2.0;
    double coeffy = coeffx;
    speedx = trajectory[indexTrajectory][3]*(1.0 - coeffx) + trajectory[indexTrajectory][5]*coeffx;
    speedy = trajectory[indexTrajectory][4]*(1.0 - coeffy) + trajectory[indexTrajectory][6]*coeffy;
}

void Ennemi::trajBezier()
{
    
}