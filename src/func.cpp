/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/func.h"
#include <cmath>
#include <algorithm>
#include <cstring>
#include <ctime>
#include <cstdio>
#include "sys/times.h"
#include "../include/Window.h"

extern Window window;

double getNorm(std::vector<double> vect)
{
    double res = sqrt(pow(vect[0], 2) + pow(vect[1], 2));
    return res;
}

double getNormCarre(std::vector<double> vect)
{
    double res = pow(vect[0], 2) + pow(vect[1], 2);
    return res;
}

std::vector<double> setNorm(std::vector<double> vect, double new_norm)
{
    std::vector<double> res;
    double old_norm = getNorm(vect);
    if (not old_norm)
    {
        return vect;
    }
    res.push_back(new_norm * vect[0] / old_norm);
    res.push_back(new_norm * vect[1] / old_norm);
    return res;
}

// Caution : All angles are in radian. Plus, the y-axis is inverted

double getAngle(std::vector<double> vect)
{
    if (not vect[1])
    {
        return -acos(vect[0] / getNorm(vect));
    }
    return -acos(vect[0] / getNorm(vect)) * ((vect[1] > 0) - (vect[1] < 0));
}

std::vector<double> rotation(std::vector<double> vect, double angle)
{
    std::vector<double> res;
    res.push_back(vect[0] * cos(angle) + vect[1] * sin(angle));
    res.push_back(- vect[0] * sin(angle) + vect[1] * cos(angle));
    return res;
}

std::__cxx11::string toString(double d, int precision)
{
    std::stringstream strStream (std::stringstream::in | std::stringstream::out);
    strStream << std::fixed << std::setprecision(precision) << d;
    return strStream.str();
}


int alea(int nbArg, int n1, ...)
{
    va_list  pa;
    int res, n;
    va_start(pa, n1);
    double r = rand() % nbArg;
    for (int i = 0; i < r + 1; i++)
    {
        res = va_arg(pa, int);   
    }
    va_end(pa);
    return res;
}

int aleaVectorInt(std::vector<int> v)
{
    return v[rand() % v.size()];
}

int fact(int n)
{
    int res = n--;
    while (n>1) res *= n--;
    return res;
}

template<class ForwardIterator, class T>
void iota(ForwardIterator first, ForwardIterator last, T value)
{
    while(first != last)
    {
        *first++ = value;
        ++value;
    }
}

std::vector<int> permutation(int n, int reductor)
{
    /*
    std::cout << "permutation" << std::endl;
    std::vector<int> res(n);
    int permutedArray [n];
    iota(permutedArray, permutedArray + n, 0);
    int range = rand() % fact(n);
    for (int i = 0; i < range; i++)
    {
        std::cout << i << std::endl;
        std::next_permutation(permutedArray, permutedArray + n);
    }
    res.assign(permutedArray, permutedArray + n);
    return res;
    */
    std::vector<int> res(n);
    int permutedArray [n];
    iota(permutedArray, permutedArray + n, 0);
    std::random_shuffle(permutedArray, permutedArray + n);
    res.assign(permutedArray, permutedArray + n);
    return res;
}

std::vector<int> permutationAllChanged(int n, int reductor)
{
    std::vector<int> res(n);
    bool diff = false;
    while(!diff)
    {
        diff = true;
        res = permutation(n, reductor);
        for (int i = 0; i < n; i++)
        {
            if (res[i] == i)
            {
                diff = false;
                //break;
            }
        }
    }
    return res;
}

std::__cxx11::string getDate()
{
    time_t temps;
    struct tm datetime;
    char  format[32];
    std::time(&temps);
    datetime = *std::localtime(&temps);
    std::strftime(format, 32, "%d/%m/%Y", &datetime);
    return format;
}

std::__cxx11::string getHour()
{
    time_t temps;
    struct tm datetime;
    char  format[32];
    std::time(&temps);
    datetime = *std::localtime(&temps);
    std::strftime(format, 32, "%H:%M", &datetime);
    return format;
}

static clock_t lastCPU, lastSysCPU, lastUserCPU;
static int numProcessors;
static clock_t timeSpend;

//This function is meant to parse the line of a file for the followinf function
int parseLine(char* line)
{
    // This assumes that a digit will be found and the line ends in " Kb".
    int i = strlen(line);
    const char* p = line;
    while (*p <'0' || *p > '9') p++;
    line[i-3] = '\0';
    i = atoi(p);
    return i;
}

//This function is meant to calculate the RAM
int getValue() //Note: this value is in KB!
{
    FILE* file = std::fopen("/proc/self/status", "r");
    int result = -1;
    char line[128];

    while (std::fgets(line, 128, file) != NULL){
        if (strncmp(line, "VmSize:", 7) == 0){
            result = parseLine(line);
            break;
        }
    }
    std::fclose(file);
    return result;
}

//The two following functions are meant to calculate the CPU
void init()
{
    FILE* file;
    struct tms timeSample;
    //struct timeval timeSample;
    char line[128];

    lastCPU = times(&timeSample);
    lastSysCPU = timeSample.tms_stime;
    lastUserCPU = timeSample.tms_utime;

    file = std::fopen("/proc/cpuinfo", "r");
    numProcessors = 0;
    while(std::fgets(line, 128, file) != NULL)
    {
        if (strncmp(line, "processor", 9) == 0) numProcessors++;
    }
    std::fclose(file);
}

double getCurrentValue()
{
    struct tms timeSample;
    clock_t now;
    double percent;

    now = times(&timeSample);
    if (now <= lastCPU || timeSample.tms_stime < lastSysCPU ||
        timeSample.tms_utime < lastUserCPU)
    {
        //Overflow detection. Just skip this value.
        percent = -1.0;
    }
    else{
        percent = (timeSample.tms_stime - lastSysCPU) +
            (timeSample.tms_utime - lastUserCPU);
        percent /= (now - lastCPU);
        percent /= numProcessors;
        percent *= 100;
    }
    lastCPU = now;
    lastSysCPU = timeSample.tms_stime;
    lastUserCPU = timeSample.tms_utime;

    return percent;
}

void write(int characterSize, int posx, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::__cxx11::string str)
{
    sf::Text text(str, window.font, characterSize);
    text.setPosition(posx, posy);
    text.setColor(out);
    double angle = 0.0;
    double pi = atan(1)*4;
    std::vector<double> vectInit;
    std::vector<double> posText;
    vectInit.push_back(double(thickness));
    vectInit.push_back(0.0);
    for (int i = 0; i < nbDraw; i++)
    {
        angle = i*2.0*pi/double(nbDraw);
        posText = rotation(vectInit, angle);
        text.move(posText[0], posText[1]);
        window.mainWindow.draw(text);
        text.move(-posText[0], -posText[1]);
    }
    text.setColor(in);
    window.mainWindow.draw(text);
}

void writeAlt(int characterSize, int posx, int posy, double thickness, int nbDraw, sf::Color in, sf::Color out, std::__cxx11::string str)
{
    sf::Text text(str, window.font, characterSize);
    text.setPosition(posx, posy);
    text.setColor(out);
    double angle = 0.0;
    double pi = atan(1)*4;
    std::vector<double> vectInit;
    std::vector<double> posText;
    vectInit.push_back(double(thickness));
    vectInit.push_back(0.0);
    for (int i = 0; i < nbDraw; i++)
    {
        angle = i*2.0*pi/double(nbDraw);
        posText = rotation(vectInit, angle);
        text.move(posText[0], posText[1]);
        window.renderBackground.draw(text);
        text.move(-posText[0], -posText[1]);
    }
    text.setColor(in);
    window.renderBackground.draw(text);
}

void writeMovement(int characterSize, int posInitX, int posFinaX, int posInitY, int posFinaY, double thickness, int nbDraw, sf::Color in, sf::Color out, std::__cxx11::string str, int durationMovement,
                   int watchMov, bool acc, bool alpha)
{
    sf::Text text(str, window.font, characterSize);
    std::vector<double> vectorDist(2);
    vectorDist[0] = posFinaX - posInitX;
    vectorDist[1] = posFinaY - posInitY;
    double distance = getNorm(vectorDist);
    double currentDistance;
    if (acc)
    {
        double speedInit = distance*2/double(durationMovement);
        currentDistance = speedInit*watchMov*(1.0 - watchMov/double(2*durationMovement));
    }
    else
    {
        double speed = distance/double(durationMovement);
        currentDistance = speed*watchMov;   
    }
    vectorDist = setNorm(vectorDist, currentDistance);
    text.setPosition(posInitX + vectorDist[0], posInitY + vectorDist[1]);
    
    text.setColor(out);
    double angle = 0.0;
    double pi = atan(1)*4;
    std::vector<double> vectInit;
    std::vector<double> posText;
    vectInit.push_back(double(thickness));
    vectInit.push_back(0.0);
    for (int i = 0; i < nbDraw; i++)
    {
        angle = i*2.0*pi/double(nbDraw);
        posText = rotation(vectInit, angle);
        text.move(posText[0], posText[1]);
        window.mainWindow.draw(text);
        text.move(-posText[0], -posText[1]);
    }
    text.setColor(in);
    window.mainWindow.draw(text);
}

int getWidthText(int characterSize, std::__cxx11::string str)
{
    return sf::Text(str, window.font, characterSize).getGlobalBounds().width;
}

int getHeightText(int characterSize, std::__cxx11::string str)
{
    return sf::Text(str, window.font, characterSize).getGlobalBounds().height;
}

sf::Keyboard::Key str2key(std::__cxx11::string str)
{
    sf::Keyboard::Key res;
    if (str == "A") {res = sf::Keyboard::A;}
    else if (str == "B") {res = sf::Keyboard::B;}
    else if (str == "C") {res = sf::Keyboard::C;}
    else if (str == "D") {res = sf::Keyboard::D;}
    else if (str == "E") {res = sf::Keyboard::E;}
    else if (str == "F") {res = sf::Keyboard::F;}
    else if (str == "G") {res = sf::Keyboard::G;}
    else if (str == "H") {res = sf::Keyboard::H;}
    else if (str == "I") {res = sf::Keyboard::I;}
    else if (str == "J") {res = sf::Keyboard::J;}
    else if (str == "K") {res = sf::Keyboard::K;}
    else if (str == "L") {res = sf::Keyboard::L;}
    else if (str == "M") {res = sf::Keyboard::M;}
    else if (str == "N") {res = sf::Keyboard::N;}
    else if (str == "O") {res = sf::Keyboard::O;}
    else if (str == "P") {res = sf::Keyboard::P;}
    else if (str == "Q") {res = sf::Keyboard::Q;}
    else if (str == "R") {res = sf::Keyboard::R;}
    else if (str == "S") {res = sf::Keyboard::S;}
    else if (str == "T") {res = sf::Keyboard::T;}
    else if (str == "U") {res = sf::Keyboard::U;}
    else if (str == "V") {res = sf::Keyboard::V;}
    else if (str == "W") {res = sf::Keyboard::W;}
    else if (str == "X") {res = sf::Keyboard::X;}
    else if (str == "Y") {res = sf::Keyboard::Y;}
    else if (str == "Z") {res = sf::Keyboard::Z;}
    else if (str == "Num0") {res = sf::Keyboard::Num0;}
    else if (str == "Num1") {res = sf::Keyboard::Num1;}
    else if (str == "Num2") {res = sf::Keyboard::Num2;}
    else if (str == "Num3") {res = sf::Keyboard::Num3;}
    else if (str == "Num4") {res = sf::Keyboard::Num4;}
    else if (str == "Num5") {res = sf::Keyboard::Num5;}
    else if (str == "Num6") {res = sf::Keyboard::Num6;}
    else if (str == "Num7") {res = sf::Keyboard::Num7;}
    else if (str == "Num8") {res = sf::Keyboard::Num8;}
    else if (str == "Num9") {res = sf::Keyboard::Num9;}
    else if (str == "Escape") {res = sf::Keyboard::Escape;}
    else if (str == "LControl") {res = sf::Keyboard::LControl;}
    else if (str == "LShift") {res = sf::Keyboard::LShift;}
    else if (str == "LAlt") {res = sf::Keyboard::LAlt;}
    else if (str == "LSystem") {res = sf::Keyboard::LSystem;}
    else if (str == "RControl") {res = sf::Keyboard::RControl;}
    else if (str == "RShift") {res = sf::Keyboard::RShift;}
    else if (str == "RAlt") {res = sf::Keyboard::RAlt;}
    else if (str == "RSystem") {res = sf::Keyboard::RSystem;}
    else if (str == "Menu") {res = sf::Keyboard::Menu;}
    else if (str == "LBracket") {res = sf::Keyboard::LBracket;}
    else if (str == "RBracket") {res = sf::Keyboard::RBracket;}
    else if (str == "SemiColon") {res = sf::Keyboard::SemiColon;}
    else if (str == "Comma") {res = sf::Keyboard::Comma;}
    else if (str == "Period") {res = sf::Keyboard::Period;}
    else if (str == "Quote") {res = sf::Keyboard::Quote;}
    else if (str == "Slash") {res = sf::Keyboard::Slash;}
    else if (str == "BackSlash") {res = sf::Keyboard::BackSlash;}
    else if (str == "Tilde") {res = sf::Keyboard::Equal;}
    else if (str == "Dash") {res = sf::Keyboard::Dash;}
    else if (str == "Space") {res = sf::Keyboard::Space;}
    else if (str == "Return") {res = sf::Keyboard::Return;}
    else if (str == "BackSpace") {res = sf::Keyboard::BackSpace;}
    else if (str == "Tab") {res = sf::Keyboard::Tab;}
    else if (str == "PageUp") {res = sf::Keyboard::PageUp;}
    else if (str == "PageDown") {res = sf::Keyboard::PageDown;}
    else if (str == "End") {res = sf::Keyboard::End;}
    else if (str == "Home") {res = sf::Keyboard::Home;}
    else if (str == "Insert") {res = sf::Keyboard::Insert;}
    else if (str == "Delete") {res = sf::Keyboard::Delete;}
    else if (str == "Add") {res = sf::Keyboard::Add;}
    else if (str == "Substract") {res = sf::Keyboard::Subtract;}
    else if (str == "Multiply") {res = sf::Keyboard::Multiply;}
    else if (str == "Divide") {res = sf::Keyboard::Divide;}
    else if (str == "Left") {res = sf::Keyboard::Left;}
    else if (str == "Right") {res = sf::Keyboard::Right;}
    else if (str == "Up") {res = sf::Keyboard::Up;}
    else if (str == "Down") {res = sf::Keyboard::Down;}
    else if (str == "Numpad0") {res = sf::Keyboard::Numpad0;}
    else if (str == "Numpad1") {res = sf::Keyboard::Numpad1;}
    else if (str == "Numpad2") {res = sf::Keyboard::Numpad2;}
    else if (str == "Numpad3") {res = sf::Keyboard::Numpad3;}
    else if (str == "Numpad4") {res = sf::Keyboard::Numpad4;}
    else if (str == "Numpad5") {res = sf::Keyboard::Numpad5;}
    else if (str == "Numpad6") {res = sf::Keyboard::Numpad6;}
    else if (str == "Numpad7") {res = sf::Keyboard::Numpad7;}
    else if (str == "Numpad8") {res = sf::Keyboard::Numpad8;}
    else if (str == "Numpad9") {res = sf::Keyboard::Numpad9;}
    else if (str == "F1") {res = sf::Keyboard::F1;}
    else if (str == "F2") {res = sf::Keyboard::F2;}
    else if (str == "F3") {res = sf::Keyboard::F3;}
    else if (str == "F4") {res = sf::Keyboard::F4;}
    else if (str == "F5") {res = sf::Keyboard::F5;}
    else if (str == "F6") {res = sf::Keyboard::F6;}
    else if (str == "F7") {res = sf::Keyboard::F7;}
    else if (str == "F8") {res = sf::Keyboard::F8;}
    else if (str == "F9") {res = sf::Keyboard::F9;}
    else if (str == "F10") {res = sf::Keyboard::F10;}
    else if (str == "F11") {res = sf::Keyboard::F11;}
    else if (str == "F12") {res = sf::Keyboard::F12;}
    else if (str == "F13") {res = sf::Keyboard::F13;}
    else if (str == "F14") {res = sf::Keyboard::F14;}
    else if (str == "F15") {res = sf::Keyboard::F15;}
    else {res = sf::Keyboard::Pause;} 
    return res;
}

std::__cxx11::string key2str(sf::Keyboard::Key key)
{
    std::__cxx11::string res;
    if (key == sf::Keyboard::A) {res = "A";}
    else if (key == sf::Keyboard::B) {res = "B";}
    else if (key == sf::Keyboard::C) {res = "C";}
    else if (key == sf::Keyboard::D) {res = "D";}
    else if (key == sf::Keyboard::E) {res = "E";}
    else if (key == sf::Keyboard::F) {res = "F";}
    else if (key == sf::Keyboard::G) {res = "G";}
    else if (key == sf::Keyboard::H) {res = "H";}
    else if (key == sf::Keyboard::I) {res = "I";}
    else if (key == sf::Keyboard::J) {res = "J";}
    else if (key == sf::Keyboard::K) {res = "K";}
    else if (key == sf::Keyboard::L) {res = "L";}
    else if (key == sf::Keyboard::M) {res = "M";}
    else if (key == sf::Keyboard::N) {res = "N";}
    else if (key == sf::Keyboard::O) {res = "O";}
    else if (key == sf::Keyboard::P) {res = "P";}
    else if (key == sf::Keyboard::Q) {res = "Q";}
    else if (key == sf::Keyboard::R) {res = "R";}
    else if (key == sf::Keyboard::S) {res = "S";}
    else if (key == sf::Keyboard::T) {res = "T";}
    else if (key == sf::Keyboard::U) {res = "U";}
    else if (key == sf::Keyboard::V) {res = "V";}
    else if (key == sf::Keyboard::W) {res = "W";}
    else if (key == sf::Keyboard::X) {res = "X";}
    else if (key == sf::Keyboard::Y) {res = "Y";}
    else if (key == sf::Keyboard::Z) {res = "Z";}
    else if (key == sf::Keyboard::Num0) {res = "Num0";}
    else if (key == sf::Keyboard::Num1) {res = "Num1";}
    else if (key == sf::Keyboard::Num2) {res = "Num2";}
    else if (key == sf::Keyboard::Num3) {res = "Num3";}
    else if (key == sf::Keyboard::Num4) {res = "Num4";}
    else if (key == sf::Keyboard::Num5) {res = "Num5";}
    else if (key == sf::Keyboard::Num6) {res = "Num6";}
    else if (key == sf::Keyboard::Num7) {res = "Num7";}
    else if (key == sf::Keyboard::Num8) {res = "Num8";}
    else if (key == sf::Keyboard::Num9) {res = "Num9";}
    else if (key == sf::Keyboard::Escape) {res = "Escape";}
    else if (key == sf::Keyboard::LControl) {res = "LControl";}
    else if (key == sf::Keyboard::LShift) {res = "LShift";}
    else if (key == sf::Keyboard::LAlt) {res = "LAlt";}
    else if (key == sf::Keyboard::LSystem) {res = "LSystem";}
    else if (key == sf::Keyboard::RControl) {res = "RControl";}
    else if (key == sf::Keyboard::RShift) {res = "RShift";}
    else if (key == sf::Keyboard::RAlt) {res = "RAlt";}
    else if (key == sf::Keyboard::RSystem) {res = "RSystem";}
    else if (key == sf::Keyboard::Menu) {res = "Menu";}
    else if (key == sf::Keyboard::LBracket) {res = "LBracket";}
    else if (key == sf::Keyboard::RBracket) {res = "RBracket";}
    else if (key == sf::Keyboard::SemiColon) {res = "SemiColon";}
    else if (key == sf::Keyboard::Comma) {res = "Comma";}
    else if (key == sf::Keyboard::Period) {res = "Period";}
    else if (key == sf::Keyboard::Quote) {res = "Quote";}
    else if (key == sf::Keyboard::Slash) {res = "Slash";}
    else if (key == sf::Keyboard::BackSlash) {res = "BackSlash";}
    else if (key == sf::Keyboard::Equal) {res = "Tilde";}
    else if (key == sf::Keyboard::Dash) {res = "Dash";}
    else if (key == sf::Keyboard::Space) {res = "Space";}
    else if (key == sf::Keyboard::Return) {res = "Return";}
    else if (key == sf::Keyboard::BackSpace) {res = "BackSpace";}
    else if (key == sf::Keyboard::Tab) {res = "Tab";}
    else if (key == sf::Keyboard::PageUp) {res = "PageUp";}
    else if (key == sf::Keyboard::PageDown) {res = "PageDown";}
    else if (key == sf::Keyboard::End) {res = "End";}
    else if (key == sf::Keyboard::Home) {res = "Home";}
    else if (key == sf::Keyboard::Insert) {res = "Insert";}
    else if (key == sf::Keyboard::Delete) {res = "Delete";}
    else if (key == sf::Keyboard::Add) {res = "Add";}
    else if (key == sf::Keyboard::Subtract) {res = "Substract";}
    else if (key == sf::Keyboard::Multiply) {res = "Multiply";}
    else if (key == sf::Keyboard::Divide) {res = "Divide";}
    else if (key == sf::Keyboard::Left) {res = "Left";}
    else if (key == sf::Keyboard::Right) {res = "Right";}
    else if (key == sf::Keyboard::Up) {res = "Up";}
    else if (key == sf::Keyboard::Down) {res = "Down";}
    else if (key == sf::Keyboard::Numpad0) {res = "Numpad0";}
    else if (key == sf::Keyboard::Numpad1) {res = "Numpad1";}
    else if (key == sf::Keyboard::Numpad2) {res = "Numpad2";}
    else if (key == sf::Keyboard::Numpad3) {res = "Numpad3";}
    else if (key == sf::Keyboard::Numpad4) {res = "Numpad4";}
    else if (key == sf::Keyboard::Numpad5) {res = "Numpad5";}
    else if (key == sf::Keyboard::Numpad6) {res = "Numpad6";}
    else if (key == sf::Keyboard::Numpad7) {res = "Numpad7";}
    else if (key == sf::Keyboard::Numpad8) {res = "Numpad8";}
    else if (key == sf::Keyboard::Numpad9) {res = "Numpad9";}
    else if (key == sf::Keyboard::F1) {res = "F1";}
    else if (key == sf::Keyboard::F2) {res = "F2";}
    else if (key == sf::Keyboard::F3) {res = "F3";}
    else if (key == sf::Keyboard::F4) {res = "F4";}
    else if (key == sf::Keyboard::F5) {res = "F5";}
    else if (key == sf::Keyboard::F6) {res = "F6";}
    else if (key == sf::Keyboard::F7) {res = "F7";}
    else if (key == sf::Keyboard::F8) {res = "F8";}
    else if (key == sf::Keyboard::F9) {res = "F9";}
    else if (key == sf::Keyboard::F10) {res = "F10";}
    else if (key == sf::Keyboard::F11) {res = "F11";}
    else if (key == sf::Keyboard::F12) {res = "F12";}
    else if (key == sf::Keyboard::F13) {res = "F13";}
    else if (key == sf::Keyboard::F14) {res = "F14";}
    else if (key == sf::Keyboard::F15) {res = "F15";}
    else {res = "Pause";} 
    return res;
}

bool isKeyPressed()
{
    bool res = false;
    for (int i = 0; i < sf::Keyboard::KeyCount; i++)
    {
        res = res || sf::Keyboard::isKeyPressed((sf::Keyboard::Key) i);
    }
    return res;
}

sf::Keyboard::Key getKeyPressed()
{
    sf::Keyboard::Key res = sf::Keyboard::A;
    for (int i = 0; i < sf::Keyboard::KeyCount; i++)
    {
        if (sf::Keyboard::isKeyPressed((sf::Keyboard::Key) i))
        {
            res = (sf::Keyboard::Key) i;
        }
    }
    return res;
}

template<typename Char, typename Traits, typename Allocator>
std::basic_string<Char, Traits, Allocator> operator * (const std::basic_string<Char, Traits, Allocator> s, size_t n)
{
   std::basic_string<Char, Traits, Allocator> tmp = s;
   for (size_t i = 0; i < n; ++i)
   {
      tmp += s;
   }
   return tmp;
}

template<typename Char, typename Traits, typename Allocator>
std::basic_string<Char, Traits, Allocator> operator * (size_t n, const std::basic_string<Char, Traits, Allocator>& s)
{
   return s * n;
}

std::__cxx11::string operator * (const std::__cxx11::string str, int n)
{
    std::__cxx11::string res;
    for (int i = 0; i < n; i++)
    {
        res = res + str;   
    }
    return res;
}

std::__cxx11::string operator * (int n, const std::__cxx11::string str)
{
    return str * n;
}

std::vector<std::vector<double> > createSimpleTraj(int x0, int y0, int x1, int y1, int t0, int t1, int t2, int x2, int y2, double s)
{
    std::vector<std::vector<double> > res(3);
    res[0].resize(7);
    res[1].resize(4);
    res[2].resize(7);
    res[0][0] = 0;
    res[0][1] = 4;
    res[0][2] = t0;
    res[0][3] = x0;
    res[0][4] = y0;
    res[0][5] = x1;
    res[0][6] = y1;
    res[1][0] = t0;
    res[1][1] = 0;
    res[1][2] = 0;
    res[1][3] = 0;
    res[2][0] = t1 + t0;
    res[2][1] = 1;
    res[2][2] = x2;
    res[2][3] = y2;
    res[2][4] = 0;
    res[2][5] = s;
    res[2][6] = t2;
    return res;
}
