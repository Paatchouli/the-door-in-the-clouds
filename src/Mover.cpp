/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "../include/Mover.h"
#include "../include/Window.h"
#include "../include/TextEn.h"
#include "../include/TextEn.h"
#include <algorithm>
#include "../include/Stage1Normal.h"
#include "../include/Stage2Normal.h"
#include "../include/func.h"
#include "../include/Sound.h"
#include "../include/Score.h"
#include "../include/Setting.h"
#include <cmath>

#include <fstream>

extern Window window;
extern TextEn textEn;
extern Stage1Normal stage1Normal;

int Mover::watch;
int Mover::watch2;
    
int Mover::moveTicker;
int Mover::index;
int Mover::index2;
int Mover::watchColor;
int Mover::watchColor2;
int Mover::watchColor3;
int Mover::indexColor1;
int Mover::indexColor2;
int Mover::indexColorVol;
int Mover::BGMvolume0;
int Mover::SEvolume0;
    
std::vector<int> Mover::stateID;
std::vector<int> Mover::stateID2;
    
bool Mover::right;
bool Mover::chosingKey;
    
double Mover::posx;
double Mover::posy;
    
int Mover::posMenu [8];
std::__cxx11::string Mover::menu[8];
std::__cxx11::string Mover::option[6];
std::__cxx11::string Mover::controlKey[9];
std::__cxx11::string Mover::modeString;

Mover::Mover()
{
    
}

Mover::~Mover()
{

}

void Mover::initialize()
{
    watch = 0;
    watch2 = 0;
    stateID.push_back(0);
    stateID.push_back(0);
    stateID2.push_back(0);
    stateID2.push_back(0);

    right = true;
    chosingKey = false;
    
    posx = 960.0 - 80.0;
    posy = 0.0;
    
    
    index = 0;
    moveTicker = 0;
    watchColor = 0;
    indexColor1 = 0;
    indexColor2 = 0;
    
    menu[0] = "Start"; posMenu[0] = 50;
    menu[1] = "Extra Start"; posMenu[1] = 50;
    menu[2] = "Spell Practice"; posMenu[2] = 50;
    menu[3] = "Practice Start"; posMenu[3] = 50;
    menu[4] = "Result"; posMenu[4] = 50;
    menu[5] = "Music Room"; posMenu[5] = 50;
    menu[6] = "Option"; posMenu[6] = 50;
    menu[7] = "Quit"; posMenu[7] = 50;
    
    option[0] = "Player";
    option[1] = "Bomb";
    option[2] = "BGM Volume";
    option[3] = "SE Volume";
    option[4] = "Default";
    option[5] = "Quit";
    
    controlKey[0] = "Left";
    controlKey[1] = "Right";
    controlKey[2] = "Up";
    controlKey[3] = "Down";
    controlKey[4] = "Bomb";
    controlKey[5] = "Slow down";
    controlKey[6] = "Speed up";
    controlKey[7] = "Pause";
    controlKey[8] = "Screenshot";
    
    modeString = "Easy" + std::__cxx11::string(" ")*EASY_NORMAL + "Normal" + std::__cxx11::string(" ")*NORMAL_HARD + "Hard" + std::__cxx11::string(" ")*HARD_FEERIC + "Feeric";
}

void Mover::moveCursor(int nbPosition)
{
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && !moveTicker)
    {
        indexColor1 = index;
        index = (nbPosition + index + 1) % nbPosition;
        moveTicker = 15;
        watchColor = 15;
        indexColor2 = index;
        Sound::addSound(Sound::bufferCursor);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && !moveTicker)
    {
        indexColor1 = index;
        index = (nbPosition + index - 1) % nbPosition;
        moveTicker = 15;
        watchColor = 15;
        indexColor2 = index;
        Sound::addSound(Sound::bufferCursor);
    }
}

int Mover::update()
{
    if (stateID[0] == 0 && stateID[1] == 0)
    {
        return update0();
    }
    else if (stateID[0] == 0 && stateID[1] != 0)
    {
        return update00();   
    }
    else if (stateID[0] == 1 && stateID[1] == 1)
    {
        return update1();   
    }
    else if (stateID[0] == 1 && stateID[1] != 1)
    {
        return update10();   
    }
    else if (stateID[0] == 4 && stateID[1] == 4)
    {
        return update4();   
    }
    else if (stateID[0] == 4 && stateID[1] != 4)
    {
        return update40();   
    }
    else if (stateID[0] == 41 && stateID[1] == 41)
    {
        return update41();   
    }
    else if (stateID[0] == 41 && stateID[1] != 41)
    {
        return update410();   
    }
    else if (stateID[0] == 5 && stateID[1] == 5)
    {
        return update5();
    }
    else if (stateID[0] == 5 && stateID[1] != 5)
    {
        return update50();
    }
    else if (stateID[0] == 7 && stateID[1] == 7)
    {
        return update7();   
    }
    else if (stateID[0] == 7 && stateID[1] != 7)
    {
        return update70();   
    }
}

int Mover::update0()
{
    watch = watch + 1;
    window.mainWindow.draw(window.spriteMenu);
    
    if (watch < 120)
    {
        double speedInit = 2.0*(window.spriteTitlePrimary.getGlobalBounds().height + 20)/120.0;
        double currentDistance = speedInit*watch*(1.0 - watch/240.0);
        window.spriteTitlePrimary.setPosition(20, currentDistance - window.spriteTitlePrimary.getGlobalBounds().height);
        window.spriteTitlePrimary.setColor(sf::Color(255, 255, 255, 255.0*watch/120.0));
    }
    if (watch < 120)
    {
        double speedInit = 2.0*(1080.0 - (window.spriteTitlePrimary.getGlobalBounds().height + 20 + 0))/120.0;
        double currentDistance = speedInit*watch*(1.0 - watch/240.0);
        window.spriteTitleSecondary.setPosition(20, 1080 - currentDistance);
        window.spriteTitleSecondary.setColor(sf::Color(255, 255, 255, 255.0*watch/120.0));
    }
    window.mainWindow.draw(window.spriteTitlePrimary);
    window.mainWindow.draw(window.spriteTitleSecondary);
    
    for (int i = 0; i < 8; i++)
    {
        if (i*INTERVAL_MENU < watch && watch < DURATION_MENU + i*INTERVAL_MENU)
        {
            textEn.writeMovement(X_INIT_MENU, Y_INIT_MENU + i*SPACE_MENU, posMenu[i], Y_INIT_MENU + i*SPACE_MENU, DURATION_MENU, watch - i*INTERVAL_MENU, SIZE_MENU, THICKNESS_MENU, NB_DRAW_MENU, sf::Color::White, sf::Color::Black, menu[i], 0, true);
        }
        else if (watch >= DURATION_MENU + i*INTERVAL_MENU)
        {
            textEn.write(SIZE_MENU, posMenu[0], Y_INIT_MENU + i*SPACE_MENU, THICKNESS_MENU, NB_DRAW_MENU, sf::Color::White, sf::Color::Black, menu[i]);
        }
    }
    
    if ((watch >= 130))
    {
        
        if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) || sf::Keyboard::isKeyPressed(sf::Keyboard::X)) && !moveTicker)
        {
            if (index != 7) {Sound::addSound(Sound::bufferExit); moveTicker = 15; indexColor1 = index; index = 7; watchColor = 15; indexColor2 = 7;}
            else {Sound::addSound(Sound::bufferExit); Sound::destroySound(); window.mainWindow.close(); return 0;}
        }
        
        if (watchColor)
        {
            double coeff;
            coeff = (15.0 - watchColor)/15.0;   
            textEn.write(SIZE_MENU, posMenu[indexColor1], Y_INIT_MENU + indexColor1*SPACE_MENU, THICKNESS_MENU, NB_DRAW_MENU, sf::Color(indexColor1*255.0/14.0 + (255 - indexColor1*255.0/14.0)*coeff, 255*coeff, 255), sf::Color::Black, menu[indexColor1]);      
            coeff = watchColor/15.0;
            textEn.write(SIZE_MENU, posMenu[indexColor2], Y_INIT_MENU + indexColor2*SPACE_MENU, THICKNESS_MENU, NB_DRAW_MENU, sf::Color(indexColor2*255.0/14.0 + (255 - indexColor2*255.0/14.0)*coeff, 255*coeff, 255), sf::Color::Black, menu[indexColor2]);
            //if (watchColor == 1) {indexColor1 = indexColor2;}    
        }
        else
        {
            textEn.write(SIZE_MENU, posMenu[index], Y_INIT_MENU + index*SPACE_MENU, THICKNESS_MENU, NB_DRAW_MENU, sf::Color(index*255.0/14.0, 0, 255), sf::Color::Black, menu[index]);
        }
        
        moveCursor(8);
        if (moveTicker) {moveTicker = moveTicker - 1;}
        if (watchColor) {watchColor = watchColor - 1;}
        
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && !moveTicker)
        {
            if (index < 7) {stateID[1] = index + 1; watch = 0; moveTicker = 15; index = 0;
                            indexColor1 = 0; indexColor2 = 0; indexColorVol = 0; BGMvolume0 = Setting::BGMvolume; SEvolume0 = Setting::SEvolume; watchColor2 = 0; watchColor3 = 0; watch2 = 0; index2 = 0;
                            Sound::addSound(Sound::bufferGo);
                           }
            else
            {
                Sound::addSound(Sound::bufferExit);
                Sound::destroySound();
                window.mainWindow.close();
                return 0;
            }
        }
        
    }
    else
    {
        
        if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Return) 
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::W)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::X)) && !moveTicker)
        {
            moveTicker = 15;
            watch = 130;
            
            double speedInit = 2.0*(window.spriteTitlePrimary.getGlobalBounds().height + 20)/120.0;
            double currentDistance = speedInit*120*(1.0 - 120/240.0);
            window.spriteTitlePrimary.setPosition(20, currentDistance - window.spriteTitlePrimary.getGlobalBounds().height);
            window.spriteTitlePrimary.setColor(sf::Color(255, 255, 255, 255));
            
            speedInit = 2.0*(1080.0 - (window.spriteTitlePrimary.getGlobalBounds().height + 20 + 0))/120.0;
            currentDistance = speedInit*120*(1.0 - 120/240.0);
            window.spriteTitleSecondary.setPosition(20, 1080 - currentDistance);
            window.spriteTitleSecondary.setColor(sf::Color(255, 255, 255, 255));
        }
        
    }
return 1;
}

int Mover::update00()
{  
    watch = watch + 1;
    window.mainWindow.draw(window.spriteMenu);
    
    double speedInit = (window.spriteTitlePrimary.getGlobalBounds().height + 20)/15.0;
    double currentDistance = speedInit*watch;
    window.spriteTitlePrimary.setPosition(20, 20 - currentDistance);
    
    speedInit = (1080.0 - (window.spriteTitlePrimary.getGlobalBounds().height + 20 + 0))/15.0;
    currentDistance = speedInit*watch;
    window.spriteTitleSecondary.setPosition(20, window.spriteTitlePrimary.getGlobalBounds().height + 20 + currentDistance);
    
    window.mainWindow.draw(window.spriteTitlePrimary);
    window.mainWindow.draw(window.spriteTitleSecondary);
    
    for (int i = 0; i < 8; i++)
    {
        textEn.writeMovement(posMenu[i], Y_INIT_MENU + i*SPACE_MENU, X_INIT_MENU, Y_INIT_MENU + i*SPACE_MENU, 15, watch, SIZE_MENU, THICKNESS_MENU, NB_DRAW_MENU, sf::Color::White, sf::Color::Black, menu[i], 0, true);
    }
    if (watch >= 15)
    {
        stateID[0] = stateID[1];
        watch = 0;
    }
    
    if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Return) 
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::W)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::X)) && !moveTicker)
        {
            stateID[0] = stateID[1];
            watch = 0;
            moveTicker = 15;
        }
        
    return 1;
    
}

int Mover::update1()
{
    watch = watch + 1;
    window.mainWindow.draw(window.spriteMenu);
    //Select : 446/80
    //Easy : 161/80
    //Normal : 268/80
    //Hard : 170/80
    //Feeric : 211/80
    //" " : 38/80
    if (watch < DURATION_MODE)
    {
        writeMovement(SIZE_TITLE, 960 - getWidthText(SIZE_TITLE, "Select Mode")/2, 960 - getWidthText(SIZE_TITLE, "Select Mode")/2, -100, POS_Y_MODE_TITLE, THICKNESS_TITLE, NB_DRAW_TITLE, sf::Color::White,
                      sf::Color::Black, "Select Mode", DURATION_MODE, watch, true);
        writeMovement(SIZE_MODE, posx, posx, 1080, POS_Y_MODE, THICKNESS_MODE, NB_DRAW_MODE, sf::Color::White, sf::Color::Black, modeString, DURATION_MODE, watch, true);
        if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Return) 
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::W)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::X)) && !moveTicker)
        {
            watch = DURATION_MODE;
            moveTicker = 15;
        }
        
    }
    else
    {
        write(SIZE_TITLE, 960 - getWidthText(SIZE_TITLE, "Select Mode")/2, POS_Y_MODE_TITLE, THICKNESS_TITLE, NB_DRAW_TITLE, sf::Color::White, sf::Color::Black, "Select Mode");
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && stateID2[0] !=3 && !moveTicker)
        {
            watch2 = 1;
            stateID2[0] = stateID2[0] + 1;
            moveTicker = DURATION_MODE;
            right = true;
            Sound::addSound(Sound::bufferCursor);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && stateID2[0] !=0 && !moveTicker)
        {
            watch2 = 1;
            stateID2[0] = stateID2[0] - 1;
            moveTicker = DURATION_MODE;
            right = false;
            Sound::addSound(Sound::bufferCursor);
        }
        if (watch2)
        {
            if (right)
            {
                writeMovement(SIZE_MODE, posx, posx - 960, POS_Y_MODE, POS_Y_MODE, THICKNESS_MODE, NB_DRAW_MODE, sf::Color::White, sf::Color::Black, modeString, DURATION_MODE, watch2, true);
            }
            else
            {
                writeMovement(SIZE_MODE, posx, posx + 960, POS_Y_MODE, POS_Y_MODE, THICKNESS_MODE, NB_DRAW_MODE, sf::Color::White, sf::Color::Black, modeString, DURATION_MODE, watch2, true);
            }
            watch2 = watch2 + 1;
            if (watch2 == DURATION_MODE)
            {
                watch2 = 0;
                if (right)
                {
                    posx = posx - 960;
                }
                else
                {
                    posx = posx + 960;
                }
            }
        }
        else
        {
            textEn.write(SIZE_MODE, posx, POS_Y_MODE, THICKNESS_MODE, NB_DRAW_MODE, sf::Color::White, sf::Color::Black, modeString);
        }
        if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) || sf::Keyboard::isKeyPressed(sf::Keyboard::X)) && !moveTicker)
        {
            stateID[1] = 0;
            watch = 0;
            moveTicker = 15;
            index = 0;
            indexColor1 = 0;
            indexColor2 = 0;
            Sound::addSound(Sound::bufferExit);
        }
        if ((sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) && !moveTicker)
        {
            Sound::addSound(Sound::bufferGo);
            if (stateID2[0] == 1)
            {
                stateID[0] = 0;
                stateID[1] = 0;
                watch = 0;
                //return stage1Normal.update();
                return Stage2Normal::update();
            }
        }
    }
    return 1;
}
int Mover::update10()
{
    window.mainWindow.draw(window.spriteMenu);
    watch = watch + 1;
    writeMovement(SIZE_TITLE, 960 - getWidthText(SIZE_TITLE, "Select Mode")/2, 960 - getWidthText(SIZE_TITLE, "Select Mode")/2, POS_Y_MODE_TITLE, -100, THICKNESS_TITLE, NB_DRAW_TITLE, sf::Color::White,
                  sf::Color::Black, "Select Mode", DURATION_MODE, watch, false);
    writeMovement(SIZE_MODE, posx, posx, POS_Y_MODE, 1080, THICKNESS_MODE, NB_DRAW_MODE, sf::Color::White, sf::Color::Black, modeString, DURATION_MODE, watch, false);
    if (watch == DURATION_MODE)
    {
        stateID[0] = stateID[1];
        watch = 0;
    }
    if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Return) 
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::W)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::X)) && !moveTicker)
        {
            stateID[0] = stateID[1];
            watch = 0;
            moveTicker = 15;
        }
    return 1;
}

int Mover::update4()
{
    watch = watch + 1;
    window.mainWindow.draw(window.spriteMenu);
    //Select : 446/80
    //Easy : 161/80
    //Normal : 268/80
    //Hard : 170/80
    //Feeric : 211/80
    //" " : 38/80
    if (watch < DURATION_MODE)
    {
        writeMovement(SIZE_TITLE, 960 - getWidthText(SIZE_TITLE, "Select Mode")/2, 960 - getWidthText(SIZE_TITLE, "Select Mode")/2, -100, POS_Y_MODE_TITLE, THICKNESS_TITLE, NB_DRAW_TITLE,sf::Color::White,
                      sf::Color::Black,"Select Mode", DURATION_MODE, watch, true);
        writeMovement(SIZE_MODE, posx, posx, 1080, POS_Y_MODE, THICKNESS_MODE, NB_DRAW_MODE, sf::Color::White, sf::Color::Black, modeString, DURATION_MODE, watch, true);
        if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Return) 
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::W)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::X)) && !moveTicker)
        {
            watch = DURATION_MODE;
            moveTicker = 15;
        }
        
    }
    else
    {
        write(SIZE_TITLE, 960 - getWidthText(SIZE_TITLE, "Select Mode")/2, POS_Y_MODE_TITLE, THICKNESS_TITLE, NB_DRAW_TITLE, sf::Color::White, sf::Color::Black, "Select Mode");
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && stateID2[0] !=3 && !moveTicker)
        {
            watch2 = 1;
            stateID2[0] = stateID2[0] + 1;
            moveTicker = DURATION_MODE;
            right = true;
            Sound::addSound(Sound::bufferCursor);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && stateID2[0] !=0 && !moveTicker)
        {
            watch2 = 1;
            stateID2[0] = stateID2[0] - 1;
            moveTicker = DURATION_MODE;
            right = false;
            Sound::addSound(Sound::bufferCursor);
        }
        if (watch2)
        {
            if (right)
            {
                writeMovement(SIZE_MODE, posx, posx - 960, POS_Y_MODE, POS_Y_MODE, THICKNESS_MODE, NB_DRAW_MODE, sf::Color::White, sf::Color::Black, modeString, DURATION_MODE, watch, true);
            }
            else
            {
                writeMovement(SIZE_MODE, posx, posx + 960, POS_Y_MODE, POS_Y_MODE, THICKNESS_MODE, NB_DRAW_MODE, sf::Color::White, sf::Color::Black, modeString, DURATION_MODE, watch, true);
            }
            watch2 = watch2 + 1;
        }
        if (watch2 == DURATION_MODE)
        {
            watch2 = 0;
            if (right)
            {
                posx = posx - 960;
            }
            else
            {
                posx = posx + 960;
            }
        }
        textEn.write(SIZE_MODE, posx, POS_Y_MODE, THICKNESS_MODE, NB_DRAW_MODE, sf::Color::White, sf::Color::Black, modeString);
        if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) || sf::Keyboard::isKeyPressed(sf::Keyboard::X)) && !moveTicker)
        {
            stateID[1] = 0;
            watch = 0;
            moveTicker = 15;
            index = 0;
            indexColor1 = 0;
            indexColor2 = 0;
            Sound::addSound(Sound::bufferExit);
        }
        if ((sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) && !moveTicker)
        {
            stateID[0] = 4;
            stateID[1] = 41;
            watch = 0;
            moveTicker = 15;
            window.stageDifficulty = stateID2[0];
            Sound::addSound(Sound::bufferGo);
        }
    }
    return 1;
}

int Mover::update40()
{
    window.mainWindow.draw(window.spriteMenu);
    watch = watch + 1;
    writeMovement(SIZE_TITLE, 960 - getWidthText(SIZE_TITLE, "Select Mode")/2, 960 - getWidthText(SIZE_TITLE, "Select Mode")/2, POS_Y_MODE_TITLE, -100, THICKNESS_TITLE, NB_DRAW_TITLE, sf::Color::White,
                  sf::Color::Black, "Select Mode", DURATION_MODE, watch, false);
    writeMovement(SIZE_MODE, posx, posx, POS_Y_MODE, 1080, THICKNESS_MODE, NB_DRAW_MODE, sf::Color::White, sf::Color::Black, modeString, DURATION_MODE, watch, false);
    if (watch == DURATION_MODE)
    {
        stateID[0] = stateID[1];
        watch = 0;
    }
    if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Return) 
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::W)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::X)) && !moveTicker)
        {
            stateID[0] = stateID[1];
            watch = 0;
            moveTicker = 15;
        }
    return 1;
}

int Mover::update41()
{
    //std::cout << textEn.getWidth(, "Stage 4") << std::endl;
    window.mainWindow.draw(window.spriteMenu);
    watch = watch + 1;
    if (moveTicker) {moveTicker = moveTicker - 1;}
    if (watch < DURATION_STAGE)
    {
        textEn.writeMovement(960 - textEn.getWidth(SIZE_TITLE, "Stage Selection")/2, -100, 960 - textEn.getWidth(SIZE_TITLE, "Stage Selection")/2, POS_Y_MODE_TITLE, DURATION_STAGE, watch, SIZE_TITLE, THICKNESS_TITLE, NB_DRAW_TITLE, sf::Color::White, sf::Color::Black, "Stage Selection", 1, true);
        for (int i = 0; i < 6; i++)
        {
            textEn.writeMovement(X_INIT_STAGE, Y_STAGE + i*SPACE_STAGE, X_FINA_STAGE, Y_STAGE + i*SPACE_STAGE, DURATION_STAGE, watch, SIZE_STAGE, THICKNESS_STAGE, NB_DRAW_STAGE, sf::Color::White, sf::Color::Black, "Stage " + toString(i + 1, 0), 0, true);
        }
        return 1;
    }
    write(SIZE_TITLE, 960 - textEn.getWidth(SIZE_TITLE, "Stage Selection")/2, POS_Y_MODE_TITLE, THICKNESS_TITLE, NB_DRAW_TITLE, sf::Color::White, sf::Color::Black, "Stage Selection");
    for (int i = 0; i < 6; i++)
    {
        write(SIZE_STAGE, X_FINA_STAGE, Y_STAGE + i*SPACE_STAGE, THICKNESS_STAGE, NB_DRAW_STAGE, sf::Color::White, sf::Color::Black, "Stage " + toString(i + 1, 0));
    }
    if (watchColor)
    {
        double coeff;
        coeff = watchColor/15.0;
        //coeff = (15.0 - watchColor)/15.0;
        write(SIZE_STAGE, X_FINA_STAGE, Y_STAGE + index*SPACE_STAGE, THICKNESS_STAGE, NB_DRAW_STAGE, sf::Color(25.5*index + (255 - 25.5*index)*coeff, 255*coeff, 255), sf::Color::Black, "Stage " + toString(index + 1, 0));
        //coeff = watchColor/15.0;
        coeff = (15.0 - watchColor)/15.0;
        write(SIZE_STAGE, X_FINA_STAGE, Y_STAGE + index2*SPACE_STAGE, THICKNESS_STAGE, NB_DRAW_STAGE, sf::Color(25.5*index2 + (255 - 25.5*index2)*coeff, 255*coeff, 255), sf::Color::Black, "Stage " + toString(index2 + 1, 0));
        watchColor = watchColor - 1;
        return 1;       
    }
    write(SIZE_STAGE, X_FINA_STAGE, Y_STAGE + index*SPACE_STAGE, THICKNESS_STAGE, NB_DRAW_STAGE, sf::Color(25.4*index, 0, 255), sf::Color::Black, "Stage " + toString(index + 1, 0));
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && !moveTicker)
    {
        index2 = index;
        index = (index + 1) % 6;
        moveTicker = 15;
        watchColor = 15;
        Sound::addSound(Sound::bufferCursor);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && !moveTicker)
    {
        index2 = index;
        index = (index + 5) % 6;
        moveTicker = 15;
        watchColor = 15;
        Sound::addSound(Sound::bufferCursor);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && !moveTicker)
    {
        index = 0;
        index2 = 0;
        stateID[0] = 0;
        stateID[1] = 0;
        watch = 0;
        moveTicker = 15;
        Sound::addSound(Sound::bufferGo);
        switch (index)
        {
            case 0:
                switch (window.stageDifficulty)
                {
                    case 0:
                        
                        break;
                    case 1:
                        return stage1Normal.update();
                        break;
                    case 2:
                        
                        break;
                    case 3:
                        
                        break;
                }
                break;
            case 1:
                switch (window.stageDifficulty)
                {
                    case 0:
                        
                        break;
                    case 1:
                        
                        break;
                    case 2:
                        
                        break;
                    case 3:
                        
                        break;
                }
                break;
            case 2:
                switch (window.stageDifficulty)
                {
                    case 0:
                        
                        break;
                    case 1:
                        
                        break;
                    case 2:
                        
                        break;
                    case 3:
                        
                        break;
                }
                break;
            case 3:
                switch (window.stageDifficulty)
                {
                    case 0:
                        
                        break;
                    case 1:
                        
                        break;
                    case 2:
                        
                        break;
                    case 3:
                        
                        break;
                }
                break;
            case 4:
                switch (window.stageDifficulty)
                {
                    case 0:
                        
                        break;
                    case 1:
                        
                        break;
                    case 2:
                        
                        break;
                    case 3:
                        
                        break;
                }
                break;
            case 5:
                switch (window.stageDifficulty)
                {
                    case 0:
                        
                        break;
                    case 1:
                        
                        break;
                    case 2:
                        
                        break;
                    case 3:
                        
                        break;
                }
                break;
        }
    }
    return 1;
}

int Mover::update410()
{
   
}

int Mover::update5()
{
   window.mainWindow.draw(window.spriteMenu);
    watch = watch + 1;
    if (watch < DURATION_SCORE)
    {
        writeMovement(SIZE_TITLE, POS_X_SCORE_TITLE, POS_X_SCORE_TITLE, POS_Y_SCORE_INIT, POS_Y_SCORE_TITLE,
                      THICKNESS_TITLE, NB_DRAW_TITLE, sf::Color::White, sf::Color::Black, "Result", DURATION_SCORE, watch, true);
        writeMovement(SIZE_SCORE_MODE, POS_X_SCORE_INIT + POS_X_SCORE_MODE, POS_X_SCORE_MODE, POS_Y_SCORE_MODE, POS_Y_SCORE_MODE,
                      THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, "Easy", DURATION_SCORE, watch, true);
        for (int i = 0; i < NB_SCORE; i++)
        {
            writeMovement(SIZE_SCORE, POS_X_SCORE_INIT + POS_X_SCORE_NAME, POS_X_SCORE_NAME, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                          THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::name[0][i], DURATION_SCORE, watch, true);
            int size = textEn.getWidth(SIZE_SCORE, toString(Score::value[0][i], 0));
            writeMovement(SIZE_SCORE, POS_X_SCORE_INIT + POS_X_SCORE_RESULT - size, POS_X_SCORE_RESULT - size, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                          THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, toString(Score::value[0][i], 0), DURATION_SCORE, watch, true);
            writeMovement(SIZE_SCORE, POS_X_SCORE_INIT + POS_X_SCORE_DATE, POS_X_SCORE_DATE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                          THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::date[0][i], DURATION_SCORE, watch, true);
            writeMovement(SIZE_SCORE, POS_X_SCORE_INIT + POS_X_SCORE_HOUR, POS_X_SCORE_HOUR, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                          THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::hour[0][i], DURATION_SCORE, watch, true);
        }
    }
    else if (!watch2)
    {
        write(SIZE_TITLE, POS_X_SCORE_TITLE, POS_Y_SCORE_TITLE, THICKNESS_TITLE, NB_DRAW_TITLE, sf::Color::White, sf::Color::Black, "Result");
        write(SIZE_SCORE_MODE, POS_X_SCORE_MODE, POS_Y_SCORE_MODE, THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, textEn.strMode[index2]);
        for (int i = 0; i < NB_SCORE; i++)
        {
            write(SIZE_SCORE, POS_X_SCORE_NAME, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::name[index2][i]);
            int size = textEn.getWidth(SIZE_SCORE, toString(Score::value[index2][i], 0));
            write(SIZE_SCORE, POS_X_SCORE_RESULT - size, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, toString(Score::value[index2][i], 0));
            write(SIZE_SCORE, POS_X_SCORE_DATE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::date[index2][i]);
            write(SIZE_SCORE, POS_X_SCORE_HOUR, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::hour[index2][i]);
        }
        
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && !moveTicker) {index2 = (index2 + 1) % 5; moveTicker = 15; watch2 = DURATION_SCORE; Sound::addSound(Sound::bufferCursor);}
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && !moveTicker) {index2 = (index2 + 4) % 5; moveTicker = 15; watch2 = DURATION_SCORE; Sound::addSound(Sound::bufferCursor);}
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && !moveTicker) {stateID[1] = 0; moveTicker = 15; watch = 0; index = 5; Sound::addSound(Sound::bufferExit);}
    }
    else
    {
        write(SIZE_TITLE, POS_X_SCORE_TITLE, POS_Y_SCORE_TITLE, THICKNESS_TITLE, NB_DRAW_TITLE, sf::Color::White, sf::Color::Black, "Result");
        
        if ((index < index2 && (index != 0 || index2 != 4)) || (index == 4 && index2 == 0))
        {
            writeMovement(SIZE_SCORE_MODE, POS_X_SCORE_MODE, POS_X_SCORE_MODE - 1920, POS_Y_SCORE_MODE, POS_Y_SCORE_MODE,
                          THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, textEn.strMode[index], DURATION_SCORE, DURATION_SCORE - watch2, true);
            for (int i = 0; i < NB_SCORE; i++)
            {
                writeMovement(SIZE_SCORE, POS_X_SCORE_NAME, POS_X_SCORE_NAME - 1920, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                              THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::name[index][i], DURATION_SCORE, DURATION_SCORE - watch2, true);
                int size = textEn.getWidth(SIZE_SCORE, toString(Score::value[index][i], 0));
                writeMovement(SIZE_SCORE, POS_X_SCORE_RESULT - size, POS_X_SCORE_RESULT - size - 1920, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                              THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, toString(Score::value[index][i], 0), DURATION_SCORE, DURATION_SCORE - watch2, true);
                writeMovement(SIZE_SCORE, POS_X_SCORE_DATE, POS_X_SCORE_DATE - 1920, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                              THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::date[index][i], DURATION_SCORE, DURATION_SCORE - watch2, true);
                writeMovement(SIZE_SCORE, POS_X_SCORE_HOUR, POS_X_SCORE_HOUR - 1920, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                              THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::hour[index][i], DURATION_SCORE, DURATION_SCORE - watch2, true);
            }
            writeMovement(SIZE_SCORE_MODE, POS_X_SCORE_MODE + 1920, POS_X_SCORE_MODE, POS_Y_SCORE_MODE, POS_Y_SCORE_MODE,
                          THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, textEn.strMode[index2], DURATION_SCORE, DURATION_SCORE - watch2, true);
            for (int i = 0; i < NB_SCORE; i++)
            {
                writeMovement(SIZE_SCORE, POS_X_SCORE_NAME + 1920, POS_X_SCORE_NAME, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                              THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::name[index2][i], DURATION_SCORE, DURATION_SCORE - watch2, true);
                int size = textEn.getWidth(SIZE_SCORE, toString(Score::value[index2][i], 0));
                writeMovement(SIZE_SCORE, POS_X_SCORE_RESULT - size + 1920, POS_X_SCORE_RESULT - size, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                              THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, toString(Score::value[index2][i], 0), DURATION_SCORE, DURATION_SCORE - watch2, true);
                writeMovement(SIZE_SCORE, POS_X_SCORE_DATE + 1920, POS_X_SCORE_DATE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                              THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::date[index2][i], DURATION_SCORE, DURATION_SCORE - watch2, true);
                writeMovement(SIZE_SCORE, POS_X_SCORE_HOUR + 1920, POS_X_SCORE_HOUR, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                              THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::hour[index2][i], DURATION_SCORE, DURATION_SCORE - watch2, true);
            }
        }
        else
        {           
            writeMovement(SIZE_SCORE_MODE, POS_X_SCORE_MODE, POS_X_SCORE_MODE + 1920, POS_Y_SCORE_MODE, POS_Y_SCORE_MODE,
                          THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, textEn.strMode[index], DURATION_SCORE, DURATION_SCORE - watch2, true);
            for (int i = 0; i < NB_SCORE; i++)
            {
                writeMovement(SIZE_SCORE, POS_X_SCORE_NAME, POS_X_SCORE_NAME + 1920, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                              THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::name[index][i], DURATION_SCORE, DURATION_SCORE - watch2, true);
                int size = textEn.getWidth(SIZE_SCORE, toString(Score::value[index][i], 0));
                writeMovement(SIZE_SCORE, POS_X_SCORE_RESULT - size, POS_X_SCORE_RESULT - size + 1920, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                              THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, toString(Score::value[index][i], 0), DURATION_SCORE, DURATION_SCORE - watch2, true);
                writeMovement(SIZE_SCORE, POS_X_SCORE_DATE, POS_X_SCORE_DATE + 1920, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                              THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::date[index][i], DURATION_SCORE, DURATION_SCORE - watch2, true);
                writeMovement(SIZE_SCORE, POS_X_SCORE_HOUR, POS_X_SCORE_HOUR + 1920, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                              THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::hour[index][i], DURATION_SCORE, DURATION_SCORE - watch2, true);
            }
            writeMovement(SIZE_SCORE_MODE, POS_X_SCORE_MODE - 1920, POS_X_SCORE_MODE, POS_Y_SCORE_MODE, POS_Y_SCORE_MODE,
                          THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, textEn.strMode[index2], DURATION_SCORE, DURATION_SCORE - watch2, true);
            for (int i = 0; i < NB_SCORE; i++)
            {
                writeMovement(SIZE_SCORE, POS_X_SCORE_NAME - 1920, POS_X_SCORE_NAME, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                              THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::name[index2][i], DURATION_SCORE, DURATION_SCORE - watch2, true);
                int size = textEn.getWidth(SIZE_SCORE, toString(Score::value[index2][i], 0));
                writeMovement(SIZE_SCORE, POS_X_SCORE_RESULT - size - 1920, POS_X_SCORE_RESULT - size, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                              THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, toString(Score::value[index2][i], 0), DURATION_SCORE, DURATION_SCORE - watch2, true);
                writeMovement(SIZE_SCORE, POS_X_SCORE_DATE - 1920, POS_X_SCORE_DATE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                              THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::date[index2][i], DURATION_SCORE, DURATION_SCORE - watch2, true);
                writeMovement(SIZE_SCORE, POS_X_SCORE_HOUR - 1920, POS_X_SCORE_HOUR, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                              THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::hour[index2][i], DURATION_SCORE, DURATION_SCORE - watch2, true);
            }
        }

        if (watch2 == 1) {index = index2;}
        watch2 = watch2 - 1;
    }
    return 1;
}

int Mover::update50()
{
    window.mainWindow.draw(window.spriteMenu);
    watch = watch + 1;
    if (watch == DURATION_SCORE)
    {
        stateID[0] = stateID[1];
        watch = 0;
        return 1;
    }
    writeMovement(SIZE_TITLE, POS_X_SCORE_TITLE, POS_X_SCORE_TITLE, POS_Y_SCORE_TITLE, POS_Y_SCORE_INIT,
                  THICKNESS_TITLE, NB_DRAW_TITLE, sf::Color::White, sf::Color::Black, "Result", DURATION_SCORE, watch, false);
    writeMovement(SIZE_SCORE_MODE, POS_X_SCORE_MODE, POS_X_SCORE_INIT + POS_X_SCORE_MODE, POS_Y_SCORE_MODE, POS_Y_SCORE_MODE,
                  THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, textEn.strMode[index2], DURATION_SCORE, watch, false);
    for (int i = 0; i < NB_SCORE; i++)
    {
        writeMovement(SIZE_SCORE, POS_X_SCORE_NAME, POS_X_SCORE_INIT + POS_X_SCORE_NAME, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                      THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::name[index2][i], DURATION_SCORE, watch, false);
        int size = textEn.getWidth(SIZE_SCORE, toString(Score::value[0][i], 0));
        writeMovement(SIZE_SCORE, POS_X_SCORE_RESULT - size, POS_X_SCORE_INIT + POS_X_SCORE_RESULT - size, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                      THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, toString(Score::value[index2][i], 0), DURATION_SCORE, watch, false);
        writeMovement(SIZE_SCORE, POS_X_SCORE_DATE, POS_X_SCORE_INIT + POS_X_SCORE_DATE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                      THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::date[index2][i], DURATION_SCORE, watch, false);
        writeMovement(SIZE_SCORE, POS_X_SCORE_HOUR, POS_X_SCORE_INIT + POS_X_SCORE_HOUR, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE, POS_Y_SCORE_FIRST + i*SPACE_Y_SCORE,
                      THICKNESS_SCORE, NB_DRAW_SCORE, sf::Color::White, sf::Color::Black, Score::hour[index2][i], DURATION_SCORE, watch, false);
    }
    
    return 1;
}

int Mover::update7()
{
    window.mainWindow.draw(window.spriteMenu);
    watch = watch + 1;
    //=========================================================================================================================================================================================================
    //Text apparition
    if (watch < DURATION_OPTION)
    {
        writeMovement(SIZE_TITLE, POS_X_OPTION_TITLE, POS_X_OPTION_TITLE, POS_Y_OPTION_TITLE_INIT, POS_Y_OPTION_TITLE,
                      THICKNESS_TITLE, NB_DRAW_TITLE, sf::Color::White, sf::Color::Black, "Option", DURATION_OPTION, watch, true);
        for (int i = 0; i < 6; i++)
        {
            writeMovement(SIZE_OPTION, POS_X_OPTION_INIT, POS_X_OPTION, POS_Y_OPTION_UP + i*SPACE_OPTION, POS_Y_OPTION_UP + i*SPACE_OPTION,
                          THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color::White, sf::Color::Black, option[i], DURATION_OPTION, watch, true);
        }
        for (int i = 0; i < 5; i++)
        {
            writeMovement(SIZE_OPTION, POS_X_OPTION_NUMBER_INIT + i*SPACE_OPTION_NUMBER, POS_X_OPTION_NUMBER + i*SPACE_OPTION_NUMBER, POS_Y_OPTION_UP, POS_Y_OPTION_UP,
                          THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color::White, sf::Color::Black, toString(i + 1, 0), DURATION_OPTION, watch, true);
        }
        for (int i = 0; i < 4; i++)
        {
            writeMovement(SIZE_OPTION, POS_X_OPTION_NUMBER_INIT + i*SPACE_OPTION_NUMBER, POS_X_OPTION_NUMBER + i*SPACE_OPTION_NUMBER, POS_Y_OPTION_UP + SPACE_OPTION, POS_Y_OPTION_UP + SPACE_OPTION,
                          THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color::White, sf::Color::Black, toString(i, 0), DURATION_OPTION, watch, true);
        }
        
        writeMovement(SIZE_OPTION, POS_X_OPTION_NUMBER_INIT, POS_X_OPTION_NUMBER, POS_Y_OPTION_UP + 2*SPACE_OPTION, POS_Y_OPTION_UP + 2*SPACE_OPTION,
                      THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color::White, sf::Color::Black, toString(Setting::BGMvolume*10, 0) + "%", DURATION_OPTION, watch, true);
        writeMovement(SIZE_OPTION, POS_X_OPTION_NUMBER_INIT, POS_X_OPTION_NUMBER, POS_Y_OPTION_UP + 3*SPACE_OPTION, POS_Y_OPTION_UP + 3*SPACE_OPTION,
                      THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color::White, sf::Color::Black, toString(Setting::SEvolume*10, 0) + "%", DURATION_OPTION, watch, true);
        //std::cout << textEn.getWidth(SIZE_OPTION, "0") << std::endl;
        return 1;
    }
    //=========================================================================================================================================================================================================
    //White and colored text
    write(SIZE_TITLE, POS_X_OPTION_TITLE, POS_Y_OPTION_TITLE, THICKNESS_TITLE, NB_DRAW_TITLE, sf::Color::White, sf::Color::Black, "Option");
    for (int i = 0; i < 6; i++)
    {
        write(SIZE_OPTION, POS_X_OPTION, POS_Y_OPTION_UP + i*SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color::White, sf::Color::Black, option[i]);
    }
    for (int i = 0; i < 5; i++)
    {
        write(SIZE_OPTION, POS_X_OPTION_NUMBER + i*SPACE_OPTION_NUMBER, POS_Y_OPTION_UP, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color::White, sf::Color::Black, toString(i + 1, 0));
    }
    for (int i = 0; i < 4; i++)
    {
        write(SIZE_OPTION, POS_X_OPTION_NUMBER + i*SPACE_OPTION_NUMBER, POS_Y_OPTION_UP + SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color::White, sf::Color::Black, toString(i, 0));
    }
    for (int i = 0; i < 9; i++)
    {
        write(SIZE_OPTION, POS_X_OPTION + 960, POS_Y_OPTION_UP + i*SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color::White, sf::Color::Black, controlKey[i]);
        write(SIZE_OPTION, POS_X_OPTION_NUMBER + 960, POS_Y_OPTION_UP + i*SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color::White, sf::Color::Black, key2str(Setting::arrayKey[i]));
    }
    
    write(SIZE_OPTION, POS_X_OPTION_NUMBER, POS_Y_OPTION_UP + 2*SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color(127.0*(10.0 - Setting::BGMvolume)/10.0, 0, 255), sf::Color::Black, toString(Setting::BGMvolume*10, 0) + "%");
    write(SIZE_OPTION, POS_X_OPTION_NUMBER, POS_Y_OPTION_UP + 3*SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color(127.0*(10.0 - Setting::SEvolume)/10.0, 0, 255), sf::Color::Black, toString(Setting::SEvolume*10, 0) + "%");
    
    if (index < NB_OPTION_LEFT)
    {
        write(SIZE_OPTION, POS_X_OPTION, POS_Y_OPTION_UP + index*SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color((127.0/(NB_OPTION_LEFT - 1.0))*index, 0, 255), sf::Color::Black, option[index]);
    }
    else
    {
        write(SIZE_OPTION, POS_X_OPTION + 960, POS_Y_OPTION_UP + (index - NB_OPTION_LEFT)*SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color((127.0/(NB_OPTION_RIGHT - 1.0))*(index - NB_OPTION_LEFT), 0, 255), sf::Color::Black, controlKey[index - NB_OPTION_LEFT]);
    }
        
    write(SIZE_OPTION, POS_X_OPTION_NUMBER + (Setting::nbLifeInit - 1)*SPACE_OPTION_NUMBER, POS_Y_OPTION_UP, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color(127.0 - 31.875*(Setting::nbLifeInit - 1.0), 0, 255), sf::Color::Black, toString(Setting::nbLifeInit, 0));
    
    write(SIZE_OPTION, POS_X_OPTION_NUMBER + Setting::nbBombInit*SPACE_OPTION_NUMBER, POS_Y_OPTION_UP + SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color(127.0 - 42.5*Setting::nbBombInit, 0, 255), sf::Color::Black, toString(Setting::nbBombInit, 0));
    //=========================================================================================================================================================================================================
    //Key selection
    if (chosingKey)
    {
        double coeff1 = - 2.0*std::fabs((watch % PERIOD_KEY) - PERIOD_KEY/2)/double(PERIOD_KEY) + 1.0;
        double coeff2 = (127.0/(NB_OPTION_RIGHT - 1.0))*(index - NB_OPTION_LEFT);
        /*
        std::cout << "Coeff 1 = " << coeff1 << std::endl;
        std::cout << "Coeff 2 = " << coeff2 << std::endl;
        std::cout << "R = " << coeff2 + (255.0 - coeff2)*coeff1 << std::endl;
        std::cout << "G = " << 255.0*coeff1 << std::endl;
        std::cout << "B = " << 255 << std::endl;
        std::cout << std::endl;
        */
        write(SIZE_OPTION, POS_X_OPTION_NUMBER + 960, POS_Y_OPTION_UP + (index - NB_OPTION_LEFT)*SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color(coeff2 + (255.0 - coeff2)*coeff1, 255.0*coeff1, 255), sf::Color::Black, key2str(Setting::arrayKey[index - NB_OPTION_LEFT]));
        if (isKeyPressed() && !moveTicker)
        {
            moveTicker = 15;
            bool keyAlreadyTaken = false;
            for (int i = 0; i < 9; i++)
            {
                keyAlreadyTaken = keyAlreadyTaken || (getKeyPressed() == Setting::arrayKey[i] && i != index - NB_OPTION_LEFT);
            }
            if (keyAlreadyTaken)
            {
                Sound::addSound(Sound::bufferExit);
            }
            else
            {
                Setting::arrayKey[index - NB_OPTION_LEFT] = getKeyPressed(); Sound::addSound(Sound::bufferGo);
            }
            chosingKey = false;
        }
        return 1;
    } 
    //=========================================================================================================================================================================================================
    //Color shifting (Cursor)
    if (watchColor)
    {
        double coeff;
        coeff = (15.0 - watchColor)/15.0;
        if (indexColor1 < NB_OPTION_LEFT)
        {
            write(SIZE_OPTION, POS_X_OPTION, POS_Y_OPTION_UP + indexColor1*SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color((127.0/(NB_OPTION_LEFT - 1.0))*indexColor1 + (255.0 - (127.0/(NB_OPTION_LEFT - 1.0))*indexColor1)*coeff, 255*coeff, 255), sf::Color::Black, option[indexColor1]);
        }
        else
        {
            write(SIZE_OPTION, POS_X_OPTION + 960, POS_Y_OPTION_UP + (indexColor1 - NB_OPTION_LEFT)*SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color((127.0/(NB_OPTION_RIGHT - 1.0))*(indexColor1 - NB_OPTION_LEFT) + (255.0 - (127.0/(NB_OPTION_RIGHT - 1.0))*(indexColor1 - NB_OPTION_LEFT))*coeff, 255*coeff, 255), sf::Color::Black, controlKey[indexColor1 - NB_OPTION_LEFT]);
        }
            
        coeff = watchColor/15.0;
        if (indexColor2 < NB_OPTION_LEFT)
        {
            write(SIZE_OPTION, POS_X_OPTION, POS_Y_OPTION_UP + indexColor2*SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color((127.0/(NB_OPTION_LEFT - 1.0))*indexColor2 + (255.0 - (127.0/(NB_OPTION_LEFT - 1.0))*indexColor2)*coeff, 255*coeff, 255), sf::Color::Black, option[indexColor2]);   
        }
        else
        {
            write(SIZE_OPTION, POS_X_OPTION + 960, POS_Y_OPTION_UP + (indexColor2 - NB_OPTION_LEFT)*SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color((127.0/(NB_OPTION_RIGHT - 1.0))*(indexColor2 - NB_OPTION_LEFT) + (255.0 - (127.0/(NB_OPTION_RIGHT - 1.0))*(indexColor2 - NB_OPTION_LEFT))*coeff, 255*coeff, 255), sf::Color::Black, controlKey[indexColor2 - NB_OPTION_LEFT]);   
        }
    }
    //if (moveTicker) {moveTicker = moveTicker - 1;}
    if (watchColor) {watchColor = watchColor - 1;}
    //=========================================================================================================================================================================================================
    //Color shifting (Numbers)
    if (watchColor2)
    {
        if (index == 0)
        {
            double coeff;
            coeff = (15.0 - watchColor2)/15.0;
            write(SIZE_OPTION, POS_X_OPTION_NUMBER + indexColor1*SPACE_OPTION_NUMBER, POS_Y_OPTION_UP, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color(31.875*(4.0 - indexColor1) + (255.0 - 31.875*(4.0 - indexColor1))*coeff, 255*coeff, 255), sf::Color::Black, toString(indexColor1 + 1, 0));

            coeff = watchColor2/15.0;
            write(SIZE_OPTION, POS_X_OPTION_NUMBER + indexColor2*SPACE_OPTION_NUMBER, POS_Y_OPTION_UP, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color(31.875*(4.0 - indexColor2) + (255.0 - 31.875*(4.0 - indexColor2))*coeff, 255*coeff, 255), sf::Color::Black, toString(indexColor2 + 1, 0));
        }
        else if (index == 1)
        {
            double coeff;
            coeff = (15.0 - watchColor2)/15.0;
            write(SIZE_OPTION, POS_X_OPTION_NUMBER + indexColor1*SPACE_OPTION_NUMBER, POS_Y_OPTION_UP + SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color(42.5*(3.0 - indexColor1) + (255.0 - 42.5*(3.0 - indexColor1))*coeff, 255*coeff, 255), sf::Color::Black, toString(indexColor1, 0));

            coeff = watchColor2/15.0;
            write(SIZE_OPTION, POS_X_OPTION_NUMBER + indexColor2*SPACE_OPTION_NUMBER, POS_Y_OPTION_UP + SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color(42.5*(3.0 - indexColor2) + (255.0 - 42.5*(3.0 - indexColor2))*coeff, 255*coeff, 255), sf::Color::Black, toString(indexColor2, 0));
        }
        
    }
    if (watchColor2) {watchColor2 = watchColor2 - 1;}
    //=========================================================================================================================================================================================================
    //Color shifting (Percentages)
    if (watchColor3)
    {
        if (index == 2)
        {
            double coeff;
            coeff = watchColor3/15.0;
            write(SIZE_OPTION, POS_X_OPTION_NUMBER, POS_Y_OPTION_UP + 2*SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION,
                  sf::Color((1.0 - coeff)*127.0*(10.0 - Setting::BGMvolume)/10.0 + coeff*127.0*(10.0 - BGMvolume0)/10.0, 0, 255), sf::Color::Black, toString(Setting::BGMvolume*10, 0) + "%");
        }
        else if (index == 3)
        {
            double coeff;
            coeff = watchColor3/15.0;
            textEn.write(SIZE_OPTION, POS_X_OPTION_NUMBER, POS_Y_OPTION_UP + 3*SPACE_OPTION, THICKNESS_OPTION, NB_DRAW_OPTION,
                         sf::Color((1.0 - coeff)*127.0*(10.0 - Setting::SEvolume)/10.0 + coeff*127.0*(10.0 - SEvolume0)/10.0, 0, 255), sf::Color::Black, toString(Setting::SEvolume*10, 0) + "%");
        }
    }
    //=========================================================================================================================================================================================================
    if (watchColor3) {watchColor3 = watchColor3 - 1;}
    
    moveCursor(15);
    
    if (index == 0) {if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && !moveTicker) {indexColor1 = Setting::nbLifeInit - 1; Setting::nbLifeInit = (Setting::nbLifeInit % 5) + 1;
                                                                                            moveTicker = 15; watchColor2 = 15; indexColor2 = Setting::nbLifeInit - 1; Sound::addSound(Sound::bufferCursor);}
                else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && !moveTicker) {indexColor1 = Setting::nbLifeInit - 1; Setting::nbLifeInit = ((Setting::nbLifeInit + 3) % 5) + 1;
                                                                                            moveTicker = 15; watchColor2 = 15; indexColor2 = Setting::nbLifeInit - 1; Sound::addSound(Sound::bufferCursor);}}
                        
    if (index == 1) {if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && !moveTicker) {indexColor1 = Setting::nbBombInit; Setting::nbBombInit = (Setting::nbBombInit + 1) % 4;
                                                                                            moveTicker = 15; watchColor2 = 15; indexColor2 = Setting::nbBombInit; Sound::addSound(Sound::bufferCursor);}
                else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && !moveTicker) {indexColor1 = Setting::nbBombInit; Setting::nbBombInit = (Setting::nbBombInit + 3) % 4;
                                                                                            moveTicker = 15; watchColor2 = 15; indexColor2 = Setting::nbBombInit; Sound::addSound(Sound::bufferCursor);}}
                
    if (index == 2) {if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && !moveTicker) {BGMvolume0 = Setting::BGMvolume; Setting::BGMvolume = (Setting::BGMvolume + 1) % 11; moveTicker = 15; watchColor3 = 15; Sound::setVolumeMusic(); Sound::addSound(Sound::bufferCursor);}
                else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && !moveTicker) {BGMvolume0 = Setting::BGMvolume; Setting::BGMvolume = (Setting::BGMvolume + 10) % 11; moveTicker = 15; watchColor3 = 15; Sound::setVolumeMusic(); Sound::addSound(Sound::bufferCursor);}}
                
    if (index == 3) {if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && !moveTicker) {SEvolume0 = Setting::SEvolume; Setting::SEvolume = (Setting::SEvolume + 1) % 11; moveTicker = 15; watchColor3 = 15; Sound::setVolumeSound(); Sound::addSound(Sound::bufferCursor);}
                else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && !moveTicker) {SEvolume0 = Setting::SEvolume; Setting::SEvolume = (Setting::SEvolume + 10) % 11; moveTicker = 15; watchColor3 = 15; Sound::setVolumeSound(); Sound::addSound(Sound::bufferCursor);}}

    
    if (index == 5 && (sf::Keyboard::isKeyPressed(sf::Keyboard::W)
                    || sf::Keyboard::isKeyPressed(sf::Keyboard::X)
                    || sf::Keyboard::isKeyPressed(sf::Keyboard::Return)
                    || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) && !moveTicker)
    {
        stateID[1] = 0;
        watch = 0;
        moveTicker = 15;
        index = 0;
        indexColor1 = 0;
        indexColor2 = 0;
        watchColor = 0;
        Setting::write();
        Sound::addSound(Sound::bufferExit);
    }
    
    if (index > NB_OPTION_LEFT - 1) {if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && !moveTicker) {moveTicker = 15; chosingKey = true; Sound::addSound(Sound::bufferCursor);}}
    
    if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) || sf::Keyboard::isKeyPressed(sf::Keyboard::X)) && !moveTicker && index != 5)
    {
        indexColor1 = index;
        index = 5;
        indexColor2 = index;
        watchColor = 15;
        moveTicker = 15;
        Sound::addSound(Sound::bufferExit);
    }
    return 1;
}

int Mover::update70()
{
    window.mainWindow.draw(window.spriteMenu);
    watch = watch + 1;
    if (watch == DURATION_OPTION)
    {
        stateID[0] = stateID[1];
        watch = 0;
        return 1;
    }
    if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Return) 
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::W)
          || sf::Keyboard::isKeyPressed(sf::Keyboard::X)) && !moveTicker)
        {
            stateID[0] = stateID[1];
            watch = 0;
            moveTicker = 15;
            return 1;
        }

    writeMovement(SIZE_TITLE, POS_X_OPTION_TITLE, POS_X_OPTION_TITLE, POS_Y_OPTION_TITLE, POS_Y_OPTION_TITLE_INIT,
                  THICKNESS_TITLE, NB_DRAW_TITLE, sf::Color::White, sf::Color::Black, "Option", DURATION_OPTION, watch, false);
    for (int i = 0; i < 6; i++)
    {
        writeMovement(SIZE_OPTION, POS_X_OPTION, POS_X_OPTION_INIT, POS_Y_OPTION_UP + i*SPACE_OPTION, POS_Y_OPTION_UP + i*SPACE_OPTION,
                      THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color::White, sf::Color::Black, option[i], DURATION_OPTION, watch, false);
    }
    for (int i = 0; i < 5; i++)
    {
        writeMovement(SIZE_OPTION, POS_X_OPTION_NUMBER + i*SPACE_OPTION_NUMBER, POS_X_OPTION_NUMBER_INIT + i*SPACE_OPTION_NUMBER, POS_Y_OPTION_UP, POS_Y_OPTION_UP,
                      THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color::White, sf::Color::Black, toString(i + 1, 0), DURATION_OPTION, watch, false);
    }
    for (int i = 0; i < 4; i++)
    {
        writeMovement(SIZE_OPTION, POS_X_OPTION_NUMBER + i*SPACE_OPTION_NUMBER, POS_X_OPTION_NUMBER_INIT + i*SPACE_OPTION_NUMBER, POS_Y_OPTION_UP + SPACE_OPTION, POS_Y_OPTION_UP + SPACE_OPTION,
                      THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color::White, sf::Color::Black, toString(i, 0), DURATION_OPTION, watch, false);
    }
        
    writeMovement(SIZE_OPTION, POS_X_OPTION_NUMBER, POS_X_OPTION_NUMBER_INIT, POS_Y_OPTION_UP + 2*SPACE_OPTION, POS_Y_OPTION_UP + 2*SPACE_OPTION,
                  THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color::White, sf::Color::Black, toString(Setting::BGMvolume*10, 0) + "%", DURATION_OPTION, watch, false);
    writeMovement(SIZE_OPTION, POS_X_OPTION_NUMBER, POS_X_OPTION_NUMBER_INIT, POS_Y_OPTION_UP + 3*SPACE_OPTION, POS_Y_OPTION_UP + 3*SPACE_OPTION,
                  THICKNESS_OPTION, NB_DRAW_OPTION, sf::Color::White, sf::Color::Black, toString(Setting::SEvolume*10, 0) + "%", DURATION_OPTION, watch, false);
    
    return 1;
}
